/*global $, $, ajaxcalls_vars, document, control_vars, window, control_vars*/
///////////////////////////////////////////////////////////////////////////////////////////


// shows sliders according to field selection
// *********************************************

// function to shows sliders according to field selection

function slider_show() {

    if (($('#listing_type_hidden').val() == 'wesell' || $('#listing_type_hidden').val() == 'weauction') && ($('#commercial_type').val() !== 'vacant-land' && $('#dwelling_type').val() !== 'vacant-land')) {
        $("#land_size_div").show();
        $("#floor_space_div").show();
    }
    else if ($('#listing_type_hidden').val() == 'welet' && $('#property_type_hidden').val() == 'commercial' && $('#commercial_type').val() !== 'vacant-land' && $('#dwelling_type').val() !== 'vacant-land') {
        $("#land_size_div").hide();
        $("#floor_space_div").show();
    }
    else if (($('#property_type_hidden').val() == 'residential' && $("#dwelling_type").val() == 'vacant-land') || ($('#property_type_hidden').val() == 'commercial' && $("#commercial_type").val() == 'vacant-land')) {
        $("#land_size_div").show();
        $("#floor_space_div").hide();
    }
    else {
        $("#land_size_div").show();
        $("#floor_space_div").show();
    }

    if ($('#property_type_hidden').val() == 'residential' && $('#dwelling_type').val() == 'vacant-land') {
        $('#search_form').find('#extras').css('display', 'none');
    }
    else {
        $('#search_form').find('#extras').css('display', 'block');
    }

    if ($('#property_type_hidden').val() == 'commercial' && $('#commercial_type').val() == 'vacant-land') {
        $('#search_form').find('#com_extras').css('display', 'none');
    }
    else {
        $('#search_form').find('#com_extras').css('display', 'block');
    }
}

//shows rental slider if welet selected when page reloaded
function advancedSetup(adv_search_set) {

    if ($('#listing_type').val() === 'welet') {
        $("#price_welet").show();
        $("#price_other").hide();
    }
    else {
        $("#price_welet").hide();
        $("#price_other").show();
    }

    if ($('#property_type_hidden').val() == 'residential') {
        $("#property_type_form").html('').append(residential);
    }
    else if ($('#property_type_hidden').val() == 'commercial') {
        $("#property_type_form").html('').append(commercial);
    }
    else if ($('#property_type_hidden').val() == 'farm') {
        $("#property_type_form").html('').append(farm);
    }
    else {
        $("#property_type_form").html('');
    }

    slider_show();

    adv_search_set = JSON.parse(adv_search_set);

    if (adv_search_set != null) {

        $('#dwelling_type').val(adv_search_set.sub_type);
        if (adv_search_set.bedrooms > 5) {
            adv_search_set.bedrooms = 5
        }
        $('#bedrooms').val(adv_search_set.bedrooms);
        $('#bathrooms').val(adv_search_set.bathrooms);
        $('#parking').val(adv_search_set.parking);
        $('#commercial_type').val(adv_search_set.sub_type);
        $('#farm_type').val(adv_search_set.sub_type);

        if (adv_search_set.security == 'on') {
            $('#security').prop('checked', true);
        }

        if (adv_search_set.pets == 'on') {
            $('#pets').prop('checked', true);
        }

        if (adv_search_set.aircon == 'on') {
            $('#aircon').prop('checked', true);
        }

        if (adv_search_set.bic == 'on') {
            $('#bic').prop('checked', true);
        }

        if (adv_search_set.pool == 'on') {
            $('#pool').prop('checked', true);
        }

        if (adv_search_set.tennis_court == 'on') {
            $('#tennis_court').prop('checked', true);
        }

        var floor_space_array = adv_search_set.floor_space_range.split('-');
        var floorMin = floor_space_array[0].trim();
        var floorMax = floor_space_array[1].trim();
        floorMax = floorMax.replace('+', '');


        var land_size_array = adv_search_set.land_size_range.split('-');
        var landMin = land_size_array[0].trim();
        var landMax = land_size_array[1].trim();
        landMax = landMax.replace('+', '');

        var price_array = adv_search_set.price_range.split('-');
        var saleMin = price_array[0].trim();
        var saleMax = price_array[1].trim();
        saleMax = saleMax.replace('R', '');
        saleMax = saleMax.replace('+', '');
        saleMin = saleMin.replace('R', '');

        var rent_price_array = adv_search_set.price_range_rent.split('-');
        var rentMin = rent_price_array[0].trim();
        var rentMax = rent_price_array[1].trim();
        rentMax = rentMax.replace('R', '');
        rentMax = rentMax.replace('+', '');
        rentMin = rentMin.replace('R', '');

        sliders(rentMin, rentMax, saleMin, saleMax, landMin, landMax, floorMin, floorMax);
    } else {
        sliders(0, 30000, 0, 15000000, 0, 20000, 0, 2000);
    }
}

function currencyFormatDE(num) {
    if (num == null) {
        return num;
    }
    else {
        return parseFloat(num).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
    }
}

function replaceAt(s, n, t) {
    return s.substring(0, n) + t + s.substring(n + 1);
}


////////////////////////////////////////////////////////////////////////////////////////////
/// start filtering -jslint checked
////////////////////////////////////////////////////////////////////////////////////////////


function start_filtering(newpage, view) {

    "use strict";
    if (view == 'grid') {
        $('#grid_view').addClass('icon_selected');
        $('#list_view').removeClass('icon_selected');
    }
    else if (view == 'list') {
        $('#list_view').addClass('icon_selected');
        $('#grid_view').removeClass('icon_selected');
    }

    var action, category, city, area, order, ajaxurl, page_id, province, suburb, agent, price_range, price_range_rent,
        floor_space_range, land_size_range, bedrooms, bathrooms;
    // get action vars
    action = $('#a_filter_action').attr('data-value');

    if ($(window).width() <= 990) {
        action = $('#a_filter_action_mobile').attr('data-value');
    }
    // get category

    category = $('#a_filter_categ').attr('data-value');

    if ($(window).width() <= 990) {
        category = $('#a_filter_categ_mobile').attr('data-value');
    }

    // get province
    province = $('#a_filter_provinces').val();
    // get city
    suburb = $('#a_filter_suburbs').val();
    // get city
    city = $('#a_filter_cities').val();
    // get area
    area = $('#a_filter_areas').val();
    // get order
    order = $('#a_filter_order').attr('data-value');

    if (order == 0) {
        order = $('#a_filter_order_mobile').attr('data-value');
    }

    var title_in, title_in2, title_1, title_2, title_3, category_2;

    if (category == 'property') {
        title_1 = 'Property ';
    } else {
        category_2 = category.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        title_1 = category_2 + ' property ';
    }

    switch (action) {
        case 'wesell':
            title_2 = 'for sale ';
            break;
        case 'welet':
            if (category == 'residential') {
                title_2 = 'to rent ';
            } else {
                title_2 = 'to let ';
            }
            break;
        case 'weauction':
            title_2 = 'for sale by auction ';
            break;
        default:
            title_2 = '';
            break;
    }

    if (suburb == '') {
        if (city == '') {
            if (province == '') {
                title_3 = 'in South Africa';
            } else {
                title_3 = 'in ' + province;
            }
        } else {
            title_3 = 'in ' + city;
        }
    } else {
        if (suburb.charAt(0) == 0) {
            title_3 = 'in Multiple suburbs';
        } else {
            title_3 = 'in ' + suburb;
        }

    }

    title_in = title_1 + title_2 + title_3;
    title_in2 = title_1 + title_3;

    $(".bread-container").html('').append('<h1>' + title_in + '</h1>');
    document.title = title_in2 + ' | In2Assets';

    agent = $('#agent').val();

    ajaxurl = '/property/ajax?page=' + newpage;
    page_id = 1;

    $('#listing_ajax_container').empty();
    $('#listing_loader').show();

    if ($('#floor_space').val()) {
        floor_space_range = $('#floor_space').val().split('-');
        var floorMin = floor_space_range[0].trim();
        var floorMax = floor_space_range[1].trim();
        floorMax = floorMax.replace('+', '');
    }

    if ($('#land_size').val()) {
        land_size_range = $('#land_size').val().split('-');
        var landMin = land_size_range[0].trim();
        var landMax = land_size_range[1].trim();
        landMax = landMax.replace('+', '');
    }

    if ($('#price').val()) {
        price_range = $('#price').val().split('-');
        var saleMin = price_range[0].trim();
        var saleMax = price_range[1].trim();
        saleMax = saleMax.replace('R', '');
        saleMax = saleMax.replace('+', '');
        saleMin = saleMin.replace('R', '');
    }

    if ($('#price_rent').val()) {
        price_range_rent = $('#price_rent').val().split('-');
        var saleMin_rent = price_range_rent[0].trim();
        var saleMax_rent = price_range_rent[1].trim();
        saleMax_rent = saleMax_rent.replace('R', '');
        saleMax_rent = saleMax_rent.replace('+', '');
        saleMin_rent = saleMin_rent.replace('R', '');
    }

    var dwelling_type = $("#dwelling_type").val();
    var commercial_type = $("#commercial_type").val();
    var farm_type = $("#farm_type").val();
    bedrooms = $("#bedrooms").val();
    bathrooms = $("#bathrooms").val();

    $.ajax({
        type: 'GET',
        url: ajaxurl,
        data: {
            'action': 'ajax_filter_listings',
            'action_values': action,
            'category_values': category,
            'city': city,
            'area': area,
            'province': province,
            'suburb': suburb,
            'agent': agent,
            'order': order,
            'newpage': newpage,
            'page_id': page_id,
            'floorMin': floorMin,
            'floorMax': floorMax,
            'landMin': landMin,
            'landMax': landMax,
            'saleMin': saleMin,
            'saleMax': saleMax,
            'saleMin_rent': saleMin_rent,
            'saleMax_rent': saleMax_rent,
            'dwelling_type': dwelling_type,
            'commercial_type': commercial_type,
            'farm_type': farm_type,
            'bedrooms': bedrooms,
            'bathrooms': bathrooms
        },
        success: function (data) {

            switch (order) {
                case '0':
                    $('#order_hidden').val('default');
                    break;
                case '1':
                    $('#order_hidden').val('price_descending');
                    break;
                case '2':
                    $('#order_hidden').val('price_ascending');
                    break;
                case '3':
                    $('#order_hidden').val('most_recent');
                    break;
                default:
                    break;
            }

            $('#listing_loader').hide();
            $('#listing_ajax_container').empty().append(data);
            $('.pagination_nojax').hide();
            $('#page_idx').val(data.last_page);

            $('#pageDetails').html('Page ' + data.current_page + ' of ' + data.last_page);
            $('#propertyCount').html(data.total + ' Properties Found');
            var x, last, first;

            first = data.last_page - 9;

            if (data.current_page - 5 > 0) {
                x = data.current_page - 4;
                if (first < 0) {
                    first = 1;
                }
                if (x > first) {
                    x = first;
                } else {
                    first = data.current_page - 4;
                }
            } else {
                x = 1;
                first = 1;
            }

            if (x + 9 < data.last_page) {
                last = x + 9;
            } else {
                last = data.last_page;
            }

            while (x <= last) {

                if (x == first) {
                    $('#pageCount').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="gotoPage(' + x + ')" class="pageNumber' + x + '"  ><span id="pagenumber' + x + '" ><strong>' + x + '</strong></span></a>&nbsp;');
                }
                else if (x < last) {
                    $('#pageCount').append('<a href="#" onclick="gotoPage(' + x + ')" class="pageNumber' + x + '" ><span id="pagenumber' + x + '" ><strong>' + x + '</strong></span></a>&nbsp;');
                }
                else {
                    $('#pageCount').append('<a href="#" onclick="gotoPage(' + x + ')" class="pageNumber' + x + '" ><span id="pagenumber' + x + '" ><strong>' + x + '</strong></span></a>&nbsp;');
                }
                if (x == data.current_page) {
                    $('#pagenumber' + x).addClass('active');
                    $('.pageNumber' + x).addClass('active');
                }
                x++;
            }

            if (data.last_page == 0) {
                $('#paginateNewer').hide();
                $('#paginateOlder').hide();
                $('#pageDetails').hide();
                $('#listing_ajax_container').append('<div class="heading-block title-center nobottomborder">'
                    + '<br/>'
                    + '<h1>No Properties Found</h1>'
                    + '<span>Please widen your filter.</span>'
                    + '</div>')
            } else {
                if (data.current_page !== data.last_page) {
                    $('#paginateNewer').show();
                }
                $('#pageDetails').show();

                var property_type = '';
                var property_details = '';
                var pageCount = data.last_page;

                $.each(data.data, function (i, item) {

                    if (item.listing_title == '') {
                        if (item.farm_type != null) {
                            property_type = item.farm_type;
                        }
                        if (item.commercial_type != null) {
                            property_type = item.commercial_type;
                        }
                        if (item.dwelling_type != null) {
                            property_type = item.dwelling_type;
                        }
                        var listing_title = property_type + ' in ' + item.suburb_town_city;
                    }
                    else {
                        listing_title = item.listing_title;
                    }
                    if (listing_title.length >= 23 && view !== 'list') {
                        var index = listing_title.substr(0, 23).lastIndexOf(' ');
                        listing_title = replaceAt(listing_title, index, '<br>');
                    }
                    if (item.farm_type != null) {
                        item.id = item.actual_id;

                        property_details = '<span class="infohouse" data-original-title="Farm Type">' +
                            item.farm_type + '</span>';
                    }
                    if (item.commercial_type != null) {
                        item.id = item.actual_id;

                        if (item.floor_space > 0) {
                            property_details = '<span class="infosize" data-original-title="Floorspace">' +
                                item.floor_space + ' m&#178;</span><span class="infohouse" data-original-title="Commercial Type">' +
                                item.commercial_type + '</span>';
                        } else {
                            property_details = '<span class="infohouse" data-original-title="Commercial Type">' +
                                item.commercial_type + '</span>';
                        }
                    }

                    if (item.dwelling_type != null) {
                        item.id = item.actual_id;

                        if (property_type != 'Vacant Land') {
                            property_details = '<span class="inforoom" data-original-title="Bedrooms">' +
                                item.bedrooms + '</span><span class="infobath" data-original-title="Bathrooms">' +
                                item.bathrooms + '</span><span class="infohouse" data-original-title="Dwelling Type">' +
                                item.dwelling_type + '</span>';
                        } else {
                            property_details = '<span class="infohouse" data-original-title="Dwelling Type">' +
                                item.dwelling_type + '</span>'
                        }
                    }

                    if (item.auction && item.auction.status == 'pending' && item.final_price <= 0) {
                        property_details = '<span class="infodate" data-original-title="Start Date">Auction Date: ' +
                            item.auction.start_date + '</span>';
                    }

                    var property_label = '';
                    switch (item.listing_type) {
                        case 'wesell':
                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-wesell">'
                                + '<div class="ribbon-inside wesell"><img class="property_tag" src="/images/for-sale-tag.png" alt="WeSell" style="border: 0px;"></div>'
                                + '</div>';
                            break;

                        case 'welet':
                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-welet">'
                                + '<div class="ribbon-inside welet"><img class="property_tag" src="/images/to-let-tag.png" alt="To Rent" style="border: 0px;"></div>'
                                + '</div>';
                            break;

                        case 'in2rental':
                            if (item.sale_type == 'Private') {
                                property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-in2rental">'
                                    + '<div class="ribbon-inside in2rental"><img class="property_tag" src="/images/to-let-tag.png" alt="To Let" style="border: 0px;"></div>'
                                    + '</div>';
                            }
                            else {
                                property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-wesell">'
                                    + '<div class="ribbon-inside wesell"><img class="property_tag" src="/images/to-let-tag.png" alt="WeSell" style="border: 0px;"></div>'
                                    + '</div>';
                            }
                            break;

                        case 'in2assets':
                            if (item.sale_type == 'Private') {
                                property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-in2assets">'
                                    + '<div class="ribbon-inside in2assets"><img class="property_tag" src="/images/for-sale-tag.png" alt="For Sale" style="border: 0px;"></div>'
                                    + '</div>';
                            }
                            else {
                                property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-in2assets">'
                                    + '<div class="ribbon-inside in2assets"><img class="property_tag" src="/images/for-sale-tag.png" alt="For Sale" style="border: 0px;"></div>'
                                    + '</div>';
                            }
                            break;


                        case 'weauction':

                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-in2assets">'
                                + '<div class="ribbon-inside in2assets"><img class="property_tag" src="/images/on-auction-tag.png" alt="For Sale " style="border: 0px;"></div>'
                                + '</div>';
                            break;
                    }

                    switch (item.vetting_status) {
                        case 4:
                        case 10:
                        case 12:
                        case 13:
                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-sold">'
                                + '<div class="ribbon-inside sold"><img class="property_tag" src="/images/in2assets-sold-tag.png" ' +
                                'alt="Sold" style="border: 0px;"></div>'
                                + '</div>';
                            break;
                        case 5:
                            if (item.listing_type == 'welet') {
                                property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-sold">'
                                    + '<div class="ribbon-inside sold"><img class="property_tag" src="/images/letted-tag.png" ' +
                                    'alt="Letted" style="border: 0px;"></div>'
                                    + '</div>';
                            }
                            else {
                                property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-sold">'
                                    + '<div class="ribbon-inside sold"><img class="property_tag" src="/images/letted-tag.png" ' +
                                    'alt="Letted" style="border: 0px;"></div>'
                                    + '</div>';
                            }
                            break;

                        case 14:
                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-open-house">'
                                + '<div class="ribbon-inside"><img class="property_tag" src="/images/withdrawn-tag.png" ' +
                                'alt="Withdrawn" style="border: 0px;"></div>'
                                + '</div>';
                            break;

                        case 15:
                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-open-house">'
                                + '<div class="ribbon-inside"><img class="property_tag" src="/images/postponed-tag.png" alt="Postponed" style="border: 0px;"></div>'
                                + '</div>';
                            break;

                        case 16:
                            property_label = '<div class="ribbon-wrapper-default ribbon-wrapper-open-house">'
                                + '<div class="ribbon-inside"><img class="property_tag" src="/images/stc-tag.png" alt="S.T.C." style="border: 0px;"></div>'
                                + '</div>';
                            break;
                    }
                    if (item.actual_id == '1582') {
                        property_label = item.actual_id + '<div class="ribbon-wrapper-default ribbon-wrapper-in2assets">'
                            + '<div class="ribbon-inside in2assets"><img class="property_tag" src="/images/liquidation-sale.png" alt="For Sale" style="border: 0px;"></div>'
                            + '</div>';
                    }

                    var price;
                    var smallImage = '';

                    if (item.lead_image != null) {
                        smallImage = item.lead_image.replace(/\..+$/, '');
                        smallImage = smallImage + '_265_163';
                    }

                    var re = /(?:\.([^.]+))?$/;
                    var ext = re.exec(item.lead_image)[1];

                    var formattedPrice2 = currencyFormatDE(item.final_price);
                    var formattedPrice3 = currencyFormatDE(item.price);

                    if (item.listing_type == 'welet' || item.listing_type == 'in2rental') {
                        if(item.price <= 0){
                            price = 'POA';
                        }else{
                            if (item.rental_terms == 1) {
                                price = 'Rental R' + formattedPrice3 + 'pm';
                            }
                            else if (item.rental_terms == 2) {
                                price = 'Rental R' + formattedPrice3 + 'm&sup2;';
                            }
                            else {
                                price = 'Rental R' + formattedPrice3 + 'pm';
                            }
                        }
                    }
                    else if (item.auction && item.final_price <= 0 && item.listing_type != 'in2assets') {
                        var formattedPrice1 = currencyFormatDE(item.auction.guideline);

                        if (item.auction.guideline > 1) {
                            price = 'Guideline R' + formattedPrice1;
                        }
                        else {
                            price = 'POA';
                        }
                    }
                    else if (item.final_price > 1) {
                        price = 'Final Price R' + formattedPrice2;
                    }
                    else {
                        if (item.price > 1) {
                            price = 'Guideline R' + formattedPrice3;
                        }
                        else {
                            price = 'POA';
                        }
                    }

                    $('#listing_ajax_container').append('<div class="col-md-3 col-sm-6 listing_wrapper" data-org="4">'
                        + '<div class="property_listing" data-link="/property/' + item.id + '/' + item.slug + '" >'
                        + '<div class="listing-unit-img-wrapper">'
                        + '<a href="/property/' + item.id + '/' + item.slug + '">'
                        + '<img width="265" height="163" src="/images/listing_image_placeholder.png" ' +
                        'class="img-responsive wp-post-image" data-src="' + location.protocol + "//" + location.host + '/' + smallImage + '.' + ext + '" ' +
                        'alt="' + item.listing_title + ' Property Lead Image" data-src-retina="' + location.protocol + "//" + location.host + '/' + smallImage + '.' + ext + '" ' +
                        'onerror="this.onerror=null;this.src=\'/images/listing_image_placeholder.png\';"/>'
                        + '</a>'
                        + '<div class="listing-cover"></div>'
                        + '<a href="/property/' + item.id + '/' + item.slug + '">'
                        + '<span class="listing-cover"><i class="fa fa-search property-icon"></i></span>'
                        + '</a>'
                        + '</div>'
                        + '<h4 class="listTitle"><a href="/property/' + item.id + '/' + item.slug + '">'
                        + listing_title
                        + property_label
                        + '</a></h4>'
                        + '<div class="property_location">'
                        + property_details
                        + '</div>'
                        + '<div class="listing_details the_grid_view">'
                        + item.short_descrip
                        + '</div>'
                        + '<div class="listing_details the_list_view">'
                        + truncate(item.short_descrip)
                        + '</div>'
                        + '<div class="listing_prop_details"></div>'
                        + '<div class="listing_unit_price_wrapper"> ' + price
                        + '<div class="listing_actions">'
                        + '<div class="share_unit">'
                        + '<a href="http://www.facebook.com/sharer.php?u=http://www.in2assets.co.za/property/' + item.id + '/' + item.slug + '" target="_blank" class="social_facebook"></a>'
                        + '<a href="http://twitter.com/home?status=Property+on+In2Assets+http://www.in2assets.co.za/property/' + item.id + '/' + item.slug + '" target="_blank" class="social_tweet" ></a>'
                        + '<a href="https://plus.google.com/share?url=http://www.in2assets.co.za/property/' + item.id + '/' + item.slug + '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" target="_blank" class="social_google"></a>'
                        + '</div>'
                        + '<span class="share_list"  data-original-title="share" ></span>'
                        + '<span class="icon-fav icon-fav-off" data-original-title="add to favorites" data-propertyid="' + item.id + '"></span>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>');
                });

                if (view == 'list') {
                    $('#listing_ajax_container').addClass('ajax12');
                    $('.listing_wrapper').hide().removeClass('col-md-3').addClass('col-md-12').fadeIn(400);
                    $('.the_grid_view').fadeOut(10, function () {
                        $('.the_list_view').fadeIn(300);
                    });
                }
            }

            setFavourites();
            $("img").unveil();

        },
        error: function (errorThrown) {

        }
    });//end ajax
}


////////////////////////////////////////////////////////////////////////////////////////////
///city-area-selection
///////////////////////////////////////////////////////////////////////////////////////////
$('#filter_city li').click(function (event) {
    $('#a_filter_areas').attr('data-value', 'all');
    $('#a_filter_areas').html('All Areas <span class="caret caret_filter"></span>');
    event.preventDefault();
    var value_city, is_city, area_value;
    value_city = String($(this).attr('data-value2'));

    if (value_city === 'all') {
        $('#filter_area li').each(function () {
            $(this).show();
        });
    } else {
        $('#filter_area li').each(function () {
            is_city = String($(this).attr('data-parentcity'));
            is_city = is_city.replace(" ", "-");
            area_value = String($(this).attr('data-value'));

            if (is_city === value_city) {
                $(this).show();
            } else if (is_city === 'all') {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    }
    $('#filter_area li[data-value="please-select-area"]').hide();
    $('#filter_area li[data-value="select-suburb-town"]').hide();
});

///////////////////////////////////////////////////////////////////////////////////////////
////////  property listing listing
////////////////////////////////////////////////////////////////////////////////////////////

$('.listing_filters_head li').click(function () {
    var pick, value, parent, classVal;
    pick = $(this).text();
    value = $(this).attr('data-value');
    classVal = $(this).attr('class');
    parent = $(this).parent().parent();
    parent.find('.filter_menu_trigger').text(pick).append('<span class="caret caret_filter"></span>').attr('data-value', value);
    var view;
    if ($('#grid_view').attr('class') == 'icon_selected') {
        view = 'grid'
    }
    else {
        view = 'list'
    }
    start_filtering(1, view);
    $.get("/setproperty/" + set_pagination)
        .done(function () {
            $('#setpage').html('(' + set_pagination + ')<span class="caret caret_filter"></span>')
        });
    switch (classVal) {
        case 'listing_type':
            switch (value) {
                case 'welet':
                    $("#listing_type_hidden").val('welet');
                    $("#price_welet").show();
                    $("#price_other").hide();
                    break;
                default:
                    $("#listing_type_hidden").val(value);
                    $("#price_welet").hide();
                    $("#price_other").show();
                    break;
            }
            break;
        case 'property_type':
            if (value == 'residential') {
                $("#property_type_form").html('').append(residential);
                $('#categ').text(pick).append('<span class="caret caret_filter"></span>').attr('data-value', value);
                $("#property_type_hidden").val('residential');
            }
            else if (value == 'commercial') {
                $("#property_type_form").html('').append(commercial);
                $('#categ').text(pick).append('<span class="caret caret_filter"></span>').attr('data-value', value);
                $("#property_type_hidden").val('commercial');
            }
            else if (value == 'farm') {
                $("#property_type_form").html('').append(farm);
                $('#categ').text(pick).append('<span class="caret caret_filter"></span>').attr('data-value', value);
                $("#property_type_hidden").val('farm');
            }
            else {
                $("#property_type_form").html('');
                $('#categ').text('All Property Types').append('<span class="caret caret_filter"></span>').attr('data-value', value);
                $("#property_type_hidden").val('property');
            }
            break;
    }

});

$('.menu_f .listing_filter_mobile li').click(function () {

    var pick, value, parent;
    pick = $(this).text();
    value = $(this).attr('data-value');
    parent = $(this).parent().parent();
    parent.find('.filter_menu_trigger').text(pick).append('<span class="caret caret_filter"></span>').attr('data-value', value);
    start_filtering(1, 'grid');
    $.get("/setproperty/" + set_pagination)
        .done(function () {
            $('#setpage').html('(' + set_pagination + ')<span class="caret caret_filter"></span>')
        });

});


$('#sidebar_filter_city li').click(function (event) {
    event.preventDefault();
    var value_city, is_city, area_value;
    value_city = String($(this).attr('data-value2')).toLowerCase();

    $('#sidebar_filter_area li').each(function () {
        is_city = String($(this).attr('data-parentcity')).toLowerCase();
        is_city = is_city.replace(" ", "-");

        area_value = String($(this).attr('data-value')).toLowerCase();
        if (is_city === value_city || value_city === 'all') {
            $(this).show();
        } else {
            $(this).hide();
        }

    });

});


$('#adv-search-city li').click(function (event) {
    event.preventDefault();
    var value_city, is_city, area_value;
    value_city = String($(this).attr('data-value2')).toLowerCase();

    $('#adv-search-area li').each(function () {
        is_city = String($(this).attr('data-parentcity')).toLowerCase();
        is_city = is_city.replace(" ", "-");
        area_value = String($(this).attr('data-value')).toLowerCase();

        if (is_city === value_city || value_city === 'all') {
            $(this).show();
        } else {
            $(this).hide();
        }

    });

});

var all_browsers_stuff;

$('#property_city_submit').change(function () {
    var city_value, area_value;
    city_value = $(this).val();

    all_browsers_stuff = $('#property_area_submit_hidden').html();
    $('#property_area_submit').empty().append(all_browsers_stuff);
    $('#property_area_submit option').each(function () {
        area_value = $(this).attr('data-parentcity');

        if (city_value === area_value || area_value === 'all') {
            //  $(this).show();
        } else {
            //$(this).hide();
            $(this).remove();
        }
    });
});


$('#adv_short_select_city li').click(function (event) {
    event.preventDefault();
    var value_city, is_city, area_value;
    value_city = String($(this).attr('data-value2')).toLowerCase();
    $('#adv_short_select_area li').each(function () {
        is_city = String($(this).attr('data-parentcity')).toLowerCase();
        is_city = is_city.replace(" ", "-");
        area_value = String($(this).attr('data-value')).toLowerCase();
        if (is_city === value_city || value_city === 'all') {
            $(this).show();
        } else {
            $(this).hide();
        }

    });

});

$('#mobile-adv-city li').click(function (event) {
    event.preventDefault();
    var value_city, is_city, area_value;
    value_city = String($(this).attr('data-value2')).toLowerCase();

    $('#mobile-adv-area li').each(function () {
        is_city = String($(this).attr('data-parentcity')).toLowerCase();
        is_city = is_city.replace(" ", "-");
        area_value = String($(this).attr('data-value')).toLowerCase();
        if (is_city === value_city || value_city === 'all') {
            $(this).show();
        } else {
            $(this).hide();
        }

    });

});

//ADD A FAVORITE
$('#listing_ajax_container').on('click', '.icon-fav-off', function (e) {
    e.preventDefault();
    var propertyID = $(this).attr('data-propertyid');

    $.ajax({
        url: "/favourites",
        method: "post",
        data: {property: propertyID, add: 'add'}
    }).done(function (data) {
        if (data.update === 'added') {
            $('.icon-fav[data-propertyid="' + propertyID + '"]').removeClass('icon-fav-off').addClass('icon-fav-on');
        }
        bootbox.dialog({
            title: data.title,
            message: data.body
        });
    });
});

//REMOVE A FAVORITE
$('#listing_ajax_container').on('click', '.icon-fav-on', function (e) {
    e.preventDefault();
    var propertyID = $(this).attr('data-propertyid');

    $.ajax({
        url: "/favourites",
        method: "post",
        data: {property: propertyID, remove: 'remove'}
    }).done(function (data) {
        if (data.update === 'removed') {
            $('.icon-fav[data-propertyid="' + propertyID + '"]').removeClass('icon-fav-on').addClass('icon-fav-off');
        }
        bootbox.dialog({
            title: data.title,
            message: data.body
        });
    });
});

$('#listing_ajax_container').on('click', '.share_list', function (e) {
    e.stopPropagation();
    var sharediv = $(this).parent().find('.share_unit');
    sharediv.toggle();
    $(this).toggleClass('share_on');
});

function sliders(rentMin, rentMax, saleMin, saleMax, landMin, landMax, floorMin, floorMax) {

// sliderJS for rental price
// *************************

    $(function () {
        $(".slider_price_widget_rent").slider({
            range: true,
            min: 0,
            max: 30000,
            step: 1000,
            values: [rentMin, rentMax],
            slide: function (event, ui1) {
                if (ui1.values[1] === 30000) {
                    $("#price_rent").val("R" + ui1.values[0] + " - R" + ui1.values[1] + '+');
                    $("#price_rent_pref").val("R" + ui1.values[0] + " - R" + ui1.values[1] + '+');
                }
                else {
                    $("#price_rent").val("R" + ui1.values[0] + " - R" + ui1.values[1]);
                    $("#price_rent_pref").val("R" + ui1.values[0] + " - R" + ui1.values[1]);
                }
            }
        });
        //sets original price values
        if ($(".slider_price_widget_rent").slider("values", 1) === 30000) {
            $("#price_rent").val("R" + $(".slider_price_widget_rent").slider("values", 0) +
                " - R" + $(".slider_price_widget_rent").slider("values", 1) + '+');
            $("#price_rent_pref").val("R" + $(".slider_price_widget_rent").slider("values", 0) +
                " - R" + $(".slider_price_widget_rent").slider("values", 1) + '+');
        }else{
            $("#price_rent").val("R" + $(".slider_price_widget_rent").slider("values", 0) +
                " - R" + $(".slider_price_widget_rent").slider("values", 1));
            $("#price_rent_pref").val("R" + $(".slider_price_widget_rent").slider("values", 0) +
                " - R" + $(".slider_price_widget_rent").slider("values", 1));
        }

    });

// *************************


// sliderJS for sales price
// *************************


    $(function () {
        $(".slider_price_widget").slider({
            range: true,
            min: 0,
            max: 15000000,
            step: 50000,
            values: [saleMin, saleMax],
            slide: function (event, ui) {
                if (ui.values[1] === 15000000) {
                    $("#price").val("R" + ui.values[0] + " - R" + ui.values[1] + '+');
                    $("#price_pref").val("R" + ui.values[0] + " - R" + ui.values[1] + '+');
                }
                else {
                    $("#price").val("R" + ui.values[0] + " - R" + ui.values[1]);
                    $("#price_pref").val("R" + ui.values[0] + " - R" + ui.values[1]);
                }
            }
        });
        //sets original price values

        if ($(".slider_price_widget").slider("values", 1) === 15000000) {
            $("#price").val("R" + $(".slider_price_widget").slider("values", 0) +
                " - R" + $(".slider_price_widget").slider("values", 1) + '+');
            $("#price_pref").val("R" + $(".slider_price_widget").slider("values", 0) +
                " - R" + $(".slider_price_widget").slider("values", 1) + '+');
        } else {
            $("#price").val("R" + $(".slider_price_widget").slider("values", 0) +
                " - R" + $(".slider_price_widget").slider("values", 1));
            $("#price_pref").val("R" + $(".slider_price_widget").slider("values", 0) +
                " - R" + $(".slider_price_widget").slider("values", 1));
        }


    });

// sliderJS for land size
// *************************

    $(function () {
        $(".slider_land_widget").slider({
            range: true,
            min: 0,
            max: 20000,
            step: 100,
            values: [landMin, landMax],
            slide: function (event, uils) {
                if (uils.values[1] === 20000) {
                    $("#land_size").val(uils.values[0] + " - " + uils.values[1] + '+');
                    $("#land_size_pref").val(uils.values[0] + " - " + uils.values[1] + '+');
                }
                else {
                    $("#land_size").val(uils.values[0] + " - " + uils.values[1]);
                    $("#land_size_pref").val(uils.values[0] + " - " + uils.values[1]);
                }
            }
        });

        //sets original land_size values
        if ($(".slider_land_widget").slider("values", 1) === 20000) {
            $("#land_size").val($(".slider_land_widget").slider("values", 0) +
                " - " + $(".slider_land_widget").slider("values", 1) + '+');
            $("#land_size_pref").val($(".slider_land_widget").slider("values", 0) +
                " - " + $(".slider_land_widget").slider("values", 1) + '+');
        }else{
            $("#land_size").val($(".slider_land_widget").slider("values", 0) +
                " - " + $(".slider_land_widget").slider("values", 1));
            $("#land_size_pref").val($(".slider_land_widget").slider("values", 0) +
                " - " + $(".slider_land_widget").slider("values", 1));
        }

    });

    // *************************

// sliderJS for floor space
// *************************

    $(function () {
        $(".slider_floor_space_widget").slider({
            range: true,
            min: 0,
            max: 2000,
            step: 10,
            values: [floorMin, floorMax],
            slide: function (event, uifs) {
                if (uifs.values[1] === 2000) {
                    $("#floor_space").val(uifs.values[0] + " - " + uifs.values[1] + '+');
                    $("#floor_space_pref").val(uifs.values[0] + " - " + uifs.values[1] + '+');
                }
                else {
                    $("#floor_space").val(uifs.values[0] + " - " + uifs.values[1]);
                    $("#floor_space_pref").val(uifs.values[0] + " - " + uifs.values[1]);
                }
            }
        });

        //sets original floor_space values
        if($(".slider_floor_space_widget").slider("values", 1) === 2000){
            $("#floor_space").val($(".slider_floor_space_widget").slider("values", 0) +
                " - " + $(".slider_floor_space_widget").slider("values", 1) + '+');
            $("#floor_space_pref").val($(".slider_floor_space_widget").slider("values", 0) +
                " - " + $(".slider_floor_space_widget").slider("values", 1) + '+');
        }else{
            $("#floor_space").val($(".slider_floor_space_widget").slider("values", 0) +
                " - " + $(".slider_floor_space_widget").slider("values", 1));
            $("#floor_space_pref").val($(".slider_floor_space_widget").slider("values", 0) +
                " - " + $(".slider_floor_space_widget").slider("values", 1));
        }


    });


//shows rental slider when welet selected
    $('#listing_type').change(function () {
        if ($(this).val() === 'welet') {
            $("#price_welet").show();
            $("#price_other").hide();
            $("#price").val("R0 - R15000000+");
            $("#price_pref").val("R0 - R15000000+");
        }
        else {
            $("#price_welet").hide();
            $("#price_other").show();
            $("#price_rent").val("R0 - R30000");
            $("#price_rent_pref").val("R0 - R30000");
        }
    });

// inserts dependent on property type selection


    $('#search_form').on('change', '#property_type', function () {

        property_type_extras();

    });


    $('#search_form').on('change', '.floor_land', function () {

        slider_show();

    });


    ///////////////////////////////////////////////////////////////////////////////////////////
    ////////  property prefrences
    ////////////////////////////////////////////////////////////////////////////////////////////

    function getPreferences() {

        $.ajax({
            url: "/api/getPreferences",
            method: "get",
        }).done(function (preferences) {
            if (preferences === '') {
                sliders(0, 30000, 0, 15000000, 0, 20000, 0, 2000);
            }
            else {
                switch (preferences['listing_type']) {
                    case 'wesell':
                        var list_type = 'For Sale<span class="caret caret_filter"></span>';
                        $("#listing_type").val(preferences['listing_type']);
                        $('#a_action').val(preferences['listing_type']);
                        $('#a_action').html(list_type);
                        $('#listing_type_hidden').val(preferences['listing_type']);
                        break;
                    case 'welet':
                        var list_type = 'To Rent<span class="caret caret_filter"></span>';
                        $("#listing_type").val(preferences['listing_type']);
                        $('#a_action').val(preferences['listing_type']);
                        $('#a_action').html(list_type);
                        $('#listing_type_hidden').val(preferences['listing_type']);
                        break;
                    case 'weauction':
                        var list_type = 'Auction<span class="caret caret_filter"></span>';
                        $("#listing_type").val(preferences['listing_type']);
                        $('#a_action').val(preferences['listing_type']);
                        $('#a_action').html(list_type);
                        $('#listing_type_hidden').val(preferences['listing_type']);
                        break;
                    default:
                        break;
                }

                switch (preferences['sale_type']) {

                    case '':
                        break;
                    default:
                        $("#sale_type").val(preferences['sale_type']);
                        $('#a_sale_types').val(preferences['sale_type']);
                        $('#a_sale_types').html(preferences['sale_type'] + '<span class="caret caret_filter"></span>');
                        $('#sale_type_hidden').val(preferences['sale_type']);
                        break;
                }

                switch (preferences['property_type']) {
                    case '':
                        break;
                    default:
                        $("#property_type").val(preferences['property_type']);
                        $('#categ').val(preferences['property_type']);
                        var UCProperty_type = preferences['property_type'].toLowerCase().replace(/\b[a-z]/g, function (letter) {
                            return letter.toUpperCase();
                        });
                        $('#categ').html(UCProperty_type + '<span class="caret caret_filter"></span>');
                        $('#property_type_hidden').val(preferences['property_type']);

                        break;
                }

                property_type_extras();

                sliders(preferences['min_price'], preferences['max_price'], preferences['min_price'], preferences['max_price'], preferences['land_min'], preferences['land_max'], preferences['floor_min'], preferences['floor_max']);

                slider_show();

                $("#dwelling_type").val(preferences['dwelling_type']);
                $("#commercial_type").val(preferences['commercial_type']);
                $("#farm_type").val(preferences['farm_type']);

                $("#bedrooms").val(preferences['bedrooms']);
                $("#bathrooms").val(preferences['bathrooms']);
                $("#parking").val(preferences['parking']);

                $("#country").val(preferences['country']);
                $("#province").val(preferences['province']);
                $("#area").val(preferences['area']);
                $("#suburb_town_city").val(preferences['suburb_town_city']);

                if (preferences['security'] === 1) {
                    $("#security").prop("checked", true);
                }
                if (preferences['pets'] === 1) {
                    $("#pets").prop("checked", true);
                }
                if (preferences['aircon'] === 1) {
                    $("#aircon").prop("checked", true);
                }
                if (preferences['bic'] === 1) {
                    $("#bic").prop("checked", true);
                }
                if (preferences['pool'] === 1) {
                    $("#pool").prop("checked", true);
                }
                if (preferences['tennis_court'] === 1) {
                    $("#tennis_court").prop("checked", true);
                }
            }
        });
    }

    $("#preferences").click(function (e) {
        e.preventDefault();
        getPreferences();
    });

    // *************************

}

///////////////////////////////////////////////////////////////////////////////////////////
///////     You Tube Embed
///////////////////////////////////////////////////////////////////////////////////////////

var videoEmbed = {
    invoke: function () {

        $('#wesell_youtube').html(function (i, html) {
            return videoEmbed.convertMedia(html, 'wesell');
        });

    },
    convertMedia: function (html, size) {
        var pattern = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;

        if (size == 'wesell') {
            if (pattern.test(html)) {
                var replacement = '<iframe width="450" height="300" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';
                var html = html.replace(pattern, replacement);
            }
        }
        return html;
    }
};


setTimeout(function () {
    videoEmbed.invoke();
}, 3000);


//function setPropertyType(property_type) {
//    var firstChar = property_type.substring(0, 1);
//    firstChar = firstChar.toUpperCase();
//    var tail = property_type.substring(1);
//    var UCProperty_type = firstChar + tail;
//    $('.property_type_link').html(UCProperty_type + '&nbsp;<span class="caret"></span></a>');
//    $('#property_type').val(property_type);
//}

function setPropertyType(property_type) {

    switch (property_type) {
        case 'Commercial':
            property_type = 'Commercial';
            $('.property_type_link').removeClass('btn-primary').addClass('btn-info');
            $('#li-commercial').addClass('li-selected');
            $('#li-residential').removeClass('li-selected');
            $('#li-farm').removeClass('li-selected');
            break;
        case 'Residential':
            property_type = 'Residential';
            $('.property_type_link').removeClass('btn-info').addClass('btn-primary');
            $('#li-commercial').removeClass('li-selected');
            $('#li-residential').addClass('li-selected');
            $('#li-farm').removeClass('li-selected');
            break;
        case 'Farm':
            property_type = 'Farm';
            $('.property_type_link').removeClass('btn-info').addClass('btn-primary');
            $('#li-commercial').removeClass('li-selected');
            $('#li-residential').removeClass('li-selected');
            $('#li-farm').addClass('li-selected');
            break;
    }
    var firstChar = property_type.substring(0, 1);
    firstChar = firstChar.toLowerCase();
    var tail = property_type.substring(1);
    var LCProperty_type = firstChar + tail;
    $('.property_type_link').html(property_type + '&nbsp;&nbsp;<span class="caret-right"></span>');
    $('#property_type').val(LCProperty_type);
}

function setListingType(listing_type) {
    //var firstChar = listing_type.substring(0, 1);
    //firstChar = firstChar.toUpperCase();
    //var tail = listing_type.substring(1);
    //var UCListing_type = firstChar + tail;
    //UCListing_type = UCListing_type.replace('-', ' ');
    //$('.listing_type_link').html(UCListing_type + '&nbsp;<span class="caret"></span></a>');
    //switch (listing_type) {
    //    case 'for-sale':
    //        $('#listing_type').val('in2assets');
    //        break;
    //    case 'to-let':
    //        $('#listing_type').val('in2rental');
    //        break;
    //    default:
    //        $('#listing_type').val('in2assets');
    //        break;
    //}
    //$('.listing_type_link').html(UCListing_type + '&nbsp;<span class="caret"></span></a>');

    switch (listing_type) {
        case 'for-sale':
            $('#listing_type').val('in2assets');
            $('.for-sale-caret').css('display', 'block');
            $('.to-let-caret').css('display', 'none');
            break;
        case 'to-let':
            $('#listing_type').val('in2rental');
            $('.for-sale-caret').css('display', 'none');
            $('.to-let-caret').css('display', 'block');
            break;
        default:
            $('#listing_type').val('in2assets');
            $('.for-sale-caret').css('display', 'block');
            $('.to-let-caret').css('display', 'none');
            break;
    }


}
