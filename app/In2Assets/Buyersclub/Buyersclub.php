<?php

namespace In2Assets\Buyersclub;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use In2Assets\Buyersclub\Events\BuyersclubWasPublished;
use Laracasts\Commander\Events\EventGenerator;

class Buyersclub extends Model
{
    use EventGenerator;

    protected $fillable = ['gallery_id', 'title', 'body', 'short_body', 'lead_image', 'category', 'status', 'file', 'slug'];

    //Notes belong to a user
    public function user()
    {
        return $this->belongsTo('In2Assets\Users\User');
    }

    public static function publish($gallery_id, $title, $body, $short_body, $lead_image, $category, $status, $file, $slug)
    {
        $buyersclub = new static(compact('gallery_id', 'title', 'body', 'short_body', 'lead_image', 'category', 'status', 'file', 'slug'));
        $buyersclub->raise(new BuyersclubWasPublished($body));

        return $buyersclub;
    }
}
