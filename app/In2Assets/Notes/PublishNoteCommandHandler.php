<?php

namespace In2Assets\Notes;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

class PublishNoteCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    protected $noteRepository;

    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $note = Note::publish($command->title, $command->body);

        $note = $this->noteRepository->save($note, $command->userId);

        $this->dispatchEventsFor($note);

        return $note;
    }
}
