<?php

namespace In2Assets\Notes;

use In2Assets\Users\User;

class NoteRepository
{
    public function getAllForUser(User $user)
    {
        return $user->notes()->with('user')->latest()->get();
    }

    public function save(Note $note, $userId)
    {
        return User::findOrFail($userId)
            ->notes()
            ->save($note);
    }

    public function findById($id)
    {
        return Note::whereId($id)->first();
    }
}
