<?php

namespace In2Assets\Academys;

use In2Assets\Users\User;

class AcademyRepository
{
    public function getAllForUser(User $user)
    {
        return $user->academys()->with('user')->latest()->get();
    }

    public function save(Academy $academy)
    {
        return $academy->save();
    }

    public function findBySlug($slug)
    {
        return Academy::whereSlug($slug)->first();
    }

    public function findAll()
    {
        return Academy::orderBy('created_at', 'desc');
    }

    //paginated list of all users;
    public function getPaginated($howMany = 10)
    {
        return Academy::whereStatus('published')->orderBy('created_at', 'desc')->simplePaginate($howMany);
    }

    public function getHome()
    {
        return Academy::take(4)->orderBy('created_at', 'desc')->get();
    }

    //paginated list of all users;
    public function getCategoryPaginated($category)
    {
        return Academy::whereCategory($category)->whereStatus('published')->orderBy('created_at', 'desc')->simplePaginate(10);
    }

    public function getSearch($q, $category)
    {
        return Academy::whereRaw("MATCH(title,body) AGAINST(? IN BOOLEAN MODE) AND (category = '$category') AND (status = 'published')", [$q]);
    }
}
