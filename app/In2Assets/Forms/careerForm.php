<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class careerForm extends FormValidator
{
    //Validation rules for registration form
    protected $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required',
        'contact' => 'required',
        'file' => 'required',
    ];
}
