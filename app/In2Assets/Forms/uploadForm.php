<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class uploadForm extends FormValidator
{
    //Validation rules for registration form
    protected $rules = [
        'title' => 'required',
        'file' => 'required',
        'type' => 'required',
    ];
}
