<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class RegistrationForm extends FormValidator
{
    //Validation rules for registration form
    protected $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required',

    ];
}
