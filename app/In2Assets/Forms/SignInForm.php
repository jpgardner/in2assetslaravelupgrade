<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class SignInForm extends FormValidator
{
    //Validation rules for registration form
    protected $rules = [
        'email' => 'required',
        'password' => 'required',
    ];
}
