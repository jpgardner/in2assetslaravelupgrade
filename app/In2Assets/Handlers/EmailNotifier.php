<?php

namespace In2Assets\Handlers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use In2Assets\Mailers\UserMailer;
use In2Assets\Registration\Events\UserHasRegistered;
use Laracasts\Commander\Events\EventListener;

class EmailNotifier extends EventListener
{
    /**
     * @var UserMailer
     */
    private $mailer;

    public function __construct(UserMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMailers($event, $type)
    {
        //Send Registration Notification to User
        if ($type == 'reg_user') {
            $this->mailer->sendWelcomeMessageTo($event->user);
        } elseif ($type == 'auction_bidder' || $type == 'added_user') {
        }
        //Send Registration Notification to Admin
        $this->mailer->sendAdminNewUser($event->user);
        //Store In DB delayed notification for User.
        $senddate = Carbon::now()->addDays(2);
        \DB::table('email_notifications')->insert(
            ['userid' => $event->user->id, 'notification' => 'registration', 'send_date' => $senddate]
        );
    }

    public function whenUserHasRegistered(UserHasRegistered $event)
    {
        $url = route('users.add');
        $previous_url = URL::previous();

        //check if admin is creating user
        if ($previous_url == $url) {
            //send welcome email if send email is checked
            if (Request::get('send_email')) {
                if (Request::get('auction_bidder')) {
                    $this->sendMailers($event, 'auction_bidder');
                } else {
                    $this->sendMailers($event, 'added_user');
                }
            }
        } else {
            $this->sendMailers($event, 'reg_user');
        }
    }
}
