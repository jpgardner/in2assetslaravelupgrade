<?php

namespace App\Models;

class Contact extends Model
{
    protected $table = 'lead_contacts';

    protected $guarded = ['id'];
}
