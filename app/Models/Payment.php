<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment_sessions';

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne(\App\Models\User::class);
    }
}
