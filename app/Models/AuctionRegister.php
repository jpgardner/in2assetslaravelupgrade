<?php

namespace App\Models;

//status key
// 1.pending
// 2.approved
// 3.rejected

class AuctionRegister extends Model
{
    protected $table = 'auction_register';

    protected $fillable = [];

    public function auction()
    {
        return $this->belongsTo(\App\Models\Auction::class);
    }
}
