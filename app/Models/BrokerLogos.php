<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class BrokerLogos extends Model
{
    protected $table = 'broker_logos';

    protected $fillable = ['broker_name', 'logo_url'];

    use SoftDeletes;
}
