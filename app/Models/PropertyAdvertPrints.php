<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyAdvertPrints extends Model
{
    protected $table = 'advertising_prints';

    protected $guarded = ['id'];

    use SoftDeletes;
}
