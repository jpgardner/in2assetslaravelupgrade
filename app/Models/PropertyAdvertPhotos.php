<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyAdvertPhotos extends Model
{
    protected $table = 'advertising_photography';

    protected $guarded = ['id'];

    use SoftDeletes;
}
