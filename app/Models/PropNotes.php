<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropNotes extends Model
{
    protected $table = 'prop_notes';

    protected $guarded = ['id', 'user_id', 'property_id', 'lead_id'];

    use SoftDeletes;

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class, 'property_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function lead()
    {
        return $this->belongsTo(\App\Models\Lead::class, 'lead_id');
    }
}
