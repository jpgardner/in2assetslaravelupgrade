<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * Created by PhpStorm.
 * User: Erich
 * Date: 2016/02/10
 * Time: 9:41 AM.
 */

/**
 * Types
 * 1. Boolean
 * 2. Numeric
 * 3. Text.
 */
class Attributes extends Model
{
    protected $table = 'attributes';

    protected $guarded = ['id'];

    public static $rules =
        [
            'name' => 'required',
        ];

    public function property()
    {
        $this->belongsToMany(\App\Models\Property::class)->withPivot('value');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
