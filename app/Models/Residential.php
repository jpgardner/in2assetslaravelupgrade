<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Residential extends Model
{
    protected $table = 'residential';

    protected $fillable = ['property_id', 'dwelling_type'];

    use SoftDeletes;

    public static $rules =
        [
            'dwelling_type' => 'required',
        ];

    public static $rules2 =
        [
            'dwelling_type' => 'required',
        ];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class, 'property_id');
    }

    public function featured()
    {
        return $this->belongsTo('App\Models\featured', 'property_id');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }

    public function isValid2()
    {
        $validation = Validator::make($this->attributes, static::$rules2);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
