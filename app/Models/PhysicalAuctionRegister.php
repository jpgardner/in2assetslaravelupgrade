<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//status key
// 1.pending
// 2.approved
// 3.rejected

class PhysicalAuctionRegister extends Model
{
    protected $table = 'physical_auction_register';

    protected $fillable = [];

    public function physical_auction()
    {
        return $this->belongsTo(\App\Models\PhysicalAuction::class);
    }
}
