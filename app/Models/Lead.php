<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    protected $table = 'leads';

    protected $guarded = ['id'];

    use SoftDeletes;
}
