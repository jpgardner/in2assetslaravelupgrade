<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Competition extends Model
{
    protected $table = 'competition';

    protected $guarded = ['id'];

    public static $rules =
        [
            'firstname' => 'required',
            'lastname' => 'required',
            'cell' => 'required',
            'email' => 'required',
        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
