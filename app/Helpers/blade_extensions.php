<?php
/**
 * Created by PhpStorm.
 * User: Erich
 * Date: 2016/11/22
 * Time: 10:20 AM.
 */
Blade::extend(function ($value) {
    return preg_replace('/(\s*)@(break|continue)(\s*)/', '$1<?php $2; ?>$3', $value);
});
