<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class BuyersController extends Controller
{
    protected $buyers;

    public function __construct(Buyers $buyers)
    {
        $this->buyers = $buyers;
    }

    /**
     * Display a listing of the resource.
     * GET /buyer.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /buyer/create.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('buyers.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /buyer.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Request::all();
        $input['user_id'] = Auth::user()->id;

        if (! $this->buyers->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->buyers->errors);
        }

        $this->buyers->save();

        if (Auth::user()->hasRole('BuyerPending')) {
        } else {
            Auth::user()->assignRole(9);
        }

        return Redirect::route('portalBuyers_path');
    }

    /**
     * Display the specified resource.
     * GET /buyer/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /buyer/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //

        $buyer = $this->buyers->whereUser_id($id)->first();

        return view('buyers.edit', ['buyer' => $buyer]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /buyer/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $buyer = $this->buyers->whereId($id)->first();

        $input = Request::all();
        $input['user_id'] = Auth::user()->id;

        if (! $buyer->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->buyers->errors);
        }

        $buyer->update();

        if (Auth::user()->hasRole('BuyerPending')) {
        } else {
            Auth::user()->assignRole(10);
        }

        return Redirect::route('portalBuyers_path');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /buyer/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function adminBuyers($role)
    {
        return view('buyers.admin.buyers', ['role' => $role]);
    }

    public function getAdminBuyersTable($role)
    {
        if ($role == 'all') {
            $users = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 5)->orWhere('role_user.role_id', '=', 9)->get();
        // $queries = DB::getQueryLog();
            // $query = end($queries);
            // dd($query);
        } elseif ($role == 'buyer') {
            $users = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 5)->get();
        } elseif ($role == 'buyerpending') {
            $users = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 9)->get();
        }

        return Datatable::collection(new Collection($users))
            ->showColumns('firstname', 'lastname', 'suburb_town_city')
            ->addColumn('role', function ($User) {
                if ($User->role_id == 5) {
                    return 'Buyer';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('action', function ($User) {
                if ($User->role_id == 9) {
                    return '<a href="/admin/users/edit/'.$User->user_id.'/" title="View User" ><i class="i-circled i-light i-alt i-small icon-user4"></i></a>
				<a href="/admin/buyers/show/'.$User->user_id.'" title="Preview Buyer" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
				<a href="/admin/messages/'.$User->user_id.'/buyers" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
                } else {
                    return '<a href="/admin/users/edit/'.$User->user_id.'/" title="View User" ><i class="i-circled i-light i-alt i-small icon-user4"></i></a>
				<a href="/admin/messages/'.$User->user_id.'/buyers" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
                }
            })
            ->searchColumns('firstname', 'lastname', 'suburb_town_city', 'role')
            ->orderColumns('firstname', 'lastname', 'suburb_town_city', 'role')
            ->make();
    }

    public function adminBuyersShow($id)
    {
        $buyer = $this->buyers->whereUser_id($id)->join('users', 'users.id', '=', 'buyers.user_id')->first();

        return view('buyers.admin.show', ['buyer' => $buyer]);
    }

    public function adminBuyersStatus($status, $id)
    {
        if ($status == 'approve') {
            DB::table('role_user')->whereUser_id($id)->whereRole_id(9)->update(['role_id' => 5]);
        } elseif ($status == 'reject') {
            DB::table('role_user')->whereUser_id($id)->whereRole_id(9)->delete();
        }

        return Redirect::route('admin.buyers', ['role' => 'all']);
    }
}
