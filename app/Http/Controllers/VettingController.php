<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use In2Assets\Core\CommandBus;
use In2Assets\Forms\uploadForm;
use In2Assets\Mailers\UserMailer;
use In2Assets\Uploads\FileRepository;
use In2Assets\Uploads\Upload;
use In2Assets\Uploads\UploadFileCommand;
use In2Assets\Users\User;
use In2Assets\Users\UserRepository;

require_once __DIR__.'/tivvit/TivvitAutoload.php';

class VettingController extends Controller
{
    public function __construct(Vetting $vetting, FileRepository $fileRepository, UserRepository $userRepository,
                                Property $property, Auction $auction, ServiceProvider $service_provider, PropNotes $prop_notes,
                                User $user, PhysicalAuction $physical_auction)
    {
        $this->vetting = $vetting;
        $this->FileRepository = $fileRepository;
        $this->UserRepository = $userRepository;
        $this->property = $property;
        $this->auction = $auction;
        $this->physical_auction = $physical_auction;
        $this->service_provider = $service_provider;
        $this->prop_notes = $prop_notes;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     * GET /vetting.
     *
     * @return Response
     */
    public function index()
    {
        //
        $stillToVet = $this->vetting->join('property', 'vetting.property_id', '=', 'property.id')->
        where('vetting.vetting_status', 3)->get();

        return view('vetting.index', ['properties' => $stillToVet, 'title' => 'to be vetted']);
    }

    /**
     * Show the form for creating a new resource.
     * GET /vetting/create.
     *
     * @return Response
     */
    public function show($id)
    {
        //
        $images = $this->property->
        join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')->where('property.id', $id)->get();

        switch ($images[0]['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();

        break;
        endswitch;

        $property['property_id'] = $id;

        $user = $this->UserRepository->findbyId($property['user_id']);

        $images = DB::table('uploads')->select()->whereClass('property')->whereType('image')->whereLink($property['property_id'])->get();

        if ($property['listing_type'] == 'welet' || $property['listing_type'] == 'wesell') {
            if ($property['attourney'] == 0) {
                $attorney = DB::table('own_attourney')->whereProperty_id($id)->first();
            } else {
                $attorney = $this->service_provider->whereId($property['attourney'])->first();
            }
        } else {
            $attorney = null;
        }
        $features = App::make('AttributesController')->getAttributesByType(1);
        $numeric_features = App::make('AttributesController')->getAttributesByType(2);
        $text_features = App::make('AttributesController')->getAttributesByType(3);

        $hasFeatures = App::make('PropertyController')->hasBooleanAttributes($property->property_id);

        $agent = $this->user->whereId($property->rep_id_number)->first();

        if ($agent == null) {
            $agent = $this->user->whereId(1)->first();
            $property->rep_firstname = 'In2assets';
            $property->rep_surname = '';
            $property->rep_cell_num = '0861 444 769';
            $property->rep_email = 'marketing@in2assets.com';
            $property->rep_id_number = '1';
        }

        $broker = App::make('PropertyController')->getBroker($property->property_id);

        if ($property->listing_type == 'weauction') {
            $auctionDets = $this->auction->whereProperty_id($id)->first();
            if ($auctionDets['auction_type'] == 2 || $auctionDets['auction_type'] == 3) {
                $physicalAuction = null;
            } elseif ($auctionDets['auction_type'] == 1) {
                $physicalAuction = $this->physical_auction->whereId($auctionDets['physical_auction'])->first();
            } elseif ($auctionDets['auction_type'] == 5) {
                $physicalAuction = null;
            } else {
                $physicalAuction = null;
            }

            return view('vetting.show', ['property' => $property, 'images' => $images, 'attorney' => $attorney,
                'features' => $features, 'numeric_features' => $numeric_features, 'text_features' => $text_features,
                'showTitle' => '', 'hasFeatures' => $hasFeatures, 'agent' => $agent, 'broker' => $broker,
                'auctionDets' => $auctionDets, 'physicalAuction' => $physicalAuction, ]);
        } else {
            return view('vetting.show', ['property' => $property, 'images' => $images, 'attorney' => $attorney,
                'features' => $features, 'numeric_features' => $numeric_features, 'text_features' => $text_features,
                'showTitle' => '', 'hasFeatures' => $hasFeatures, 'agent' => $agent, 'broker' => $broker, ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * GET /vetting/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $property = $this->vetting->join('property', 'vetting.property_id', '=', 'property.id')->
        leftJoin('residential', 'vetting.property_id', '=', 'residential.property_id')->
        leftJoin('commercial', 'vetting.property_id', '=', 'commercial.property_id')->
        leftJoin('farm', 'vetting.property_id', '=', 'farm.property_id')->
        where('vetting.property_id', $id)->first();

        $property['property_id'] = $id;
        $user = Auth::user();
        $files = $user->files()->where('link', '=', $property['id'])->where('type', '=', 'image')->with('user')->get();

        $provinces = DB::table('areas')->groupBy('province')->get();
        $areas = DB::table('areas')->groupBy('area')->get();
        $cities = DB::table('areas')->groupBy('suburb_town_city')->get();

        $images = DB::table('uploads')->select()->whereClass('property')->whereType('image')->whereLink($property['property_id'])->get();

        return view('vetting.edit', ['property' => $property, 'files' => $files, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'images' => $images]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /vetting/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $propertyInput = Request::only(['street_address', 'area', 'suburb_town_city', 'province', 'country',
            'short_descrip', 'long_descrip', ]);

        $property = new Property();

        $property = $property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.id', $id)->first();

        $property->fill($propertyInput)->update();

        $vettingInput = Request::only(['owner_title', 'owner_firstname', 'owner_surname', 'ownership_type',
            'rep_title', 'rep_firstname', 'rep_surname', 'rep_id_number', 'rep_cell_num', 'rep_email', 'extra_owner_title1',
            'extra_owner_firstname1', 'extra_owner_surname1', 'extra_owner_title2', 'extra_owner_firstname2',
            'extra_owner_surname2', 'extra_owner_title3', 'extra_owner_firstname3', 'extra_owner_surname3', ]);

        // validate and save to vetting table if not valid return and delete $this->property
        // dd($vettingInput);

        $this->vetting = $this->vetting->whereId($id)->first();

        $this->vetting->fill($vettingInput);

        $this->vetting->update();

        $user = $this->UserRepository->findbyId($property['user_id']);
        $images = $this->FileRepository->getImageGallery($user, $id);

        return view('vetting.show', ['property' => $property, 'images' => $images]);
    }

    public function mark_as()
    {
        $id = Request::get('id');
        $mark = Request::get('mark');

        $listing_type = $this->property->select('listing_type')->whereId($id)->first();

        if ($mark == 'sold') {
            $this->vetting = $this->vetting->whereProperty_id($id)->first();
            if ($listing_type['listing_type'] == 'weauction') {
                $this->auction = $this->auction->whereProperty_id($id)->first();

                $this->auction->status = 'sold';
                $this->auction->update();

                $this->vetting->vetting_status = 10;
            } else {
                $this->vetting->vetting_status = 4;
            }
            $this->vetting->update();

            return 'done';
        } elseif ($mark == 'rented') {
            $this->vetting = $this->vetting->whereProperty_id($id)->first();
            $this->vetting->vetting_status = 5;
            $this->vetting->update();

            return 'done';
        } elseif ($mark == 'not_sold') {
            $this->auction = $this->auction->whereProperty_id($id)->first();

            $this->auction->status = 'created';
            $this->auction->physical_auction = null;
            $this->auction->update();

            $this->vetting->vetting_status = 9;

            return 'done';
        } elseif ($mark == 'featured') {
            $this->property = $this->property->whereId($id)->first();
            $this->property->featured = 1;
            $this->property->save();

            return 'done';
        } elseif ($mark == 'not_featured') {
            $this->property = $this->property->whereId($id)->first();
            $this->property->featured = 0;
            $this->property->save();

            return 'done';
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /vetting/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function statuschange($id)
    {
        //
        if (Request::get('message') != '') {
            $this->prop_notes->note = Request::get('message');
            $this->prop_notes->property_id = $id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();
        } else {
            if (Request::get('error')) {
                $note = 'Property vetted but error was found in data';
            } elseif (Request::get('approved')) {
                $note = 'Property vetted and approved';
            } elseif (Request::get('rejected')) {
                $note = 'Property has been rejected';
            } else {
                $note = 'vetting note';
            }
            $this->prop_notes->note = $note;
            $this->prop_notes->property_id = $id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->created_at = date('Y-m-d H:i:s');
            $this->prop_notes->save();
        }

        $this->vetting = $this->vetting->where('property_id', '=', $id)->first();
        $this->vetting['vetted_by'] = Auth::user()->id;
        $datetime = date('Y-m-d H:i:s');

        if (Request::get('in2assets_only')) {
            $this->vetting['in2assets_only'] = 1;
        } else {
            $this->vetting['in2assets_only'] = 0;
        }

        $this->vetting['mandate'] = Request::get('mandate');

        $property = $this->vetting->join('property', 'vetting.property_id', '=', 'property.id')->
        where('vetting.property_id', $id)->first();

        $user = User::whereId($property->user_id)->first();

        //$this->auction = $this->auction->whereProperty_id($id)->first();

        //dd($this->auction);
        $userName = 'Dear '.$user->title.'. '.$user->lastname;

        if (Request::get('error')) {
            $this->vetting['vetting_status'] = 1;
            //Notify User that Property Approved.
            $subject = 'In2Assets - Property Listing Update';
            $adminMail = $user->email;
        // Notify::sendMessage($adminMail, '2', 'property', $property->id, $subject, $message, 'vettingcorrection');
        } elseif (Request::get('approved')) {
            $property = $this->property->whereId($id)->with('residential')->with('commercial')->with('farm')->with('vetting')->first();
            //dd($property);
            $landlordId = $property->user_id;
            $rental = $property->price;

            $propUser = User::whereId($landlordId)->first();

            if (! $propUser->hasTag('SELLER')) {
                $tag = DB::table('tags')->where('name', '=', 'SELLER')->first();
                $propUser->tags()->attach($tag->id);
            }

            if ($property->listing_type == 'weauction' || $property->listing_type == 'in2assets') {
                $socialAds = DB::select('SELECT * FROM social_advertising WHERE property_id = '.$id);
                $date_info = '';
                if ($socialAds[0]->start_date != null) {
                    $date_info = '<p>Please begin this marketing campaign on '.date('d F Y', strtotime($socialAds[0]->start_date)).'</p>';
                }
                if (! empty($socialAds)) {
                    //Notify concerned parties.
                    $addressLine = 'Hi Kerrilee';
                    $subject = 'Digital marketing property ref '.$property->reference;
                    $adminMail = 'kerrilees@brabys.co.za';
                    $emailData = "<p>Property ref number: $property->reference is now live</p>
                              <p><a href='https://www.in2assets.co.za/property/$id/$property->slug'>https://www.in2assets.co.za/property/$id/$property->slug</a></p>
						<ul>
						<li>Facebook amount: R".$socialAds[0]->facebook.'</li>
						<li>Google display amount: R'.$socialAds[0]->display.'</li>
						<li>Google search amount: R'.$socialAds[0]->search."</li>
						</ul>
						$date_info
						<br>
						<p>Kind regards</p>
						<p>The In2assets team</p>";

                    $data = ['name' => $addressLine, 'subject' => $subject, 'messages' => $emailData];
                    // Queue email
                    Mail::queue('emails.registration.confirm', $data, function ($message) use ($adminMail, $addressLine, $subject) {
                        $message->to($adminMail, $addressLine)
                            ->cc('neeraj@in2assets.com', 'Neeraj Ramautar')
                            ->cc('rstenzhorn@in2assets.com', 'Rainer Stenzhorn')
                            ->cc('revarshang@brabysmedia.com', 'Revarshan Govender')
                            ->subject($subject);
                    });
                }
            }
            if ($property->listing_type == 'welet' || $property->listing_type == 'in2rental') {
                $this->vetting['vetting_status'] = 2;
                $leaseId = DB::table('lease_agreements')->insertGetId(['landlord_id' => $landlordId, 'tenant_id' => 0, 'property_id' => $id, 'lease_status' => 'new', 'created_at' => $datetime, 'updated_at' => $datetime]);
                DB::table('lease_amendments')->insert(['lease_agreement_id' => $leaseId, 'current_rental' => $rental, 'created_at' => $datetime, 'updated_at' => $datetime]);
                //Notify User that Property Approved.
                $subject = 'In2Assets - Property Approved';
                $adminMail = $user->email;
            // Notify::sendMessage($adminMail, '2', 'property', $property->id, $subject, $message, 'vettingapproved');
            } elseif ($property->listing_type == 'wesell' || $property->listing_type == 'in2assets') {
                $this->vetting['vetting_status'] = 2;
//                $deedId = DB::table('deed_of_sale')->insertGetId(['owner_id' => $landlordId, 'buyer_id' => 0, 'property_id' => $id, 'deed_status' => 'new', 'created_at' => $datetime, 'updated_at' => $datetime]);
//                DB::table('deed_amendment')->insert(['deed_of_sale_id' => $deedId, 'created_at' => $datetime, 'updated_at' => $datetime]);
//                //Notify User that Property Approved.
//                $subjectSocial = 'Property ';
//                $nameSocial = 'Dear ' . $propUser->firstname . ',<br>';
//                $userMailSocial = $property->owner_email;
//                $emailDataSocial = '';
//                $dataSocial = ['name' => $nameSocial, 'subject' => $subjectSocial, 'messages' => $emailDataSocial];
//                // Queue email
//                Mail::queue('emails.wesell.property_live', $dataSocial, function ($message) use ($userMailSocial, $nameSocial, $subjectSocial) {
//                    $message->to($userMailSocial, $nameSocial)
//                        ->subject($subjectSocial);
//                });

                if ($property->listing_type == 'wesell') {
                    $agent_name = $property->vetting->rep_firstname.' '.$property->vetting->rep_surname;
                    $agent_number = $property->vetting->rep_cell_num;
                    if ($property->owner_email == '') {
                        $userMail = $propUser->email;
                    } else {
                        $userMail = $property->owner_email;
                    }
                    $subject = 'Your Property is Live';
                    $name = 'Dear '.$propUser->firstname.',<br>';
                    $emailData = '<p>Wonderful news! We’ve pushed the “Publish” button on your listing and it has been published to all the major property
                    portals in South Africa. It will take a little while for your listing to display on </p>
                    <br>
                    <img src="https://www.in2assets.co.za/public/images/wesell/logos001.jpg" />
                    <img src="https://www.in2assets.co.za/public/images/wesell/logos002.jpg" />
                    <br>
                    <p>and has been published instantly as <a href="www.wesell.co.za/property/'.$property->id.'" target="_blank">WeSell.co.za</a> and
                    <a href="www.in2assets.co.za/property/'.$property->id.'" target="_blank">In2assets.com</a> (click on the links to view your listing online).</p>
                    <img src="https://www.in2assets.co.za/public/images/wesell/next-steps.jpg" />
                    <h4>Preparing for viewings</h4>
                    <p>It’s time to get your ducks in a row and prepare yourself for buyers visiting your home.
                    Your online agent will be fielding calls from prospective buyers, vetting them and then sending them to you to
                    view your property. Read some tips on <a href="http://www.wesell.co.za/wesell-how-to-guide/viewing" target="_blank">how to be prepared for buyer visits</a>.</p>
                    <h4>Viewing calendar</h4>
                    <p>If you haven’t done so already, tell us when you are available to view by filling in your viewing calendar for
                    the week. This allows us to book viewings for you without too much to-and-fro between us, you and prospective buyers.
                    We will simply check your schedule and make a tentative booking on your behalf. We know that life can be hectic and
                    ever-changing, so if your best-laid plans change and you need to adjust your viewing schedule, please do so! </p>
                    <h4>Keeping your home in tip-top shape</h4>
                    <p>It can be quite a challenge keeping your home in showroom condition while life goes on inside it! Keep reminding
                    yourself that the effort will be worth it as it could mean a quicker sale at higher selling price and more money in
                    your bank. <a href="http://www.wesell.co.za/wesell-how-to-guide/home_styling" target="_blank">See some top home styling tips here.</a></p>
                    <h4>Negotiations</h4>
                    <p>Remember, your WeSell online agent will be guiding you through the whole process and will be actively assisting
                    you in turning buyer interest into good offers. A good offer is not just about price, in fact there are a few options.</p>
                    <p>Read more about the negotiation process <a href="http://www.wesell.co.za/wesell-how-to-guide/negotiating" target="_blank">here</a>.</p>
                    <p>That’s all for now, but I’ll touch base with you regularly to keep you
                    updated. Remember I’m just a phone call away if you have any queries or need help with anything.</p>
                    <p>Sincerely</p>
                    <p><strong>'.$agent_name.'</strong></p>
                    <p><strong>'.$agent_number.'</strong></p>';
                    $data = ['name' => $name, 'subject' => $subject, 'messages' => $emailData];
                    // Queue email
                    Mail::queue('emails.wesell.property_live', $data, function ($message) use ($userMail, $name, $subject) {
                        $message->to($userMail, $name)
                            ->subject($subject);
                    });
                }
            } elseif ($property->listing_type == 'weauction') {
                //no message
                $this->vetting['vetting_status'] = 9;
                $this->vetting->update();

                return Redirect::route('admin.auctions.create', ['property_id' => $id]);
            }

            //			https://www.in2assets.co.za/uploads/users/82/124/image_JIvRKxCGTVMs.jpg
            if ($this->vetting['vetting_status'] == 2 && $this->vetting['in2assets_only'] == 0) {
                $this->createTivvitObjects($property, $this->vetting);
            }
        } elseif (Request::get('rejected')) {
            $this->vetting['vetting_status'] = 8;
            //Notify User that Property Rejected.
            $subject = 'In2Assets - Property Rejected';
            $adminMail = $user->email;
            // Notify::sendMessage($adminMail, '2', 'property', $property->id, $subject, $message, 'vettingrejected');
        }

        if ($this->vetting['vetting_status'] == 2) {
            App::make('LeadsController')->addNewActual($this->vetting['rep_id_number'], 'Mandate', 1);
        }

        $this->vetting->update();

        return Redirect::route('property.admin', ['status' => 'all']);
    }

    public function updatePropertiesDaily()
    {
        try {
            $properties = DB::select('select  p.id as propertyid, p.*,c.*,f.*,v.*,r.*, a.auction_type '.
                'from property p '.
                'left join residential r on p.id = r.property_id '.
                'left join commercial c on p.id = c.property_id '.
                'left join farm f on p.id = f.property_id '.
                'left join vetting v on v.property_id = p.id '.
                'left join auction a on a.property_id = p.id '.
                "where v.vetting_status in ('2','9','16','4','6','10','12','13') ".
                'and p.tivvit_listing_num is not null '.
                'and (p.updated_at  >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)'.
                'or r.updated_at  >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)'.
                'or c.updated_at  >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)'.
                'or f.updated_at  >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)'.
                'or p.deleted_at  >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)'.
                'or v.updated_at  >= DATE_ADD(CURDATE(), INTERVAL -1 DAY))'
            );
            foreach ($properties as $property) {
                //				dd($property);
                if (is_null($property->deleted_at)) {
                    $vetting = $this->vetting->where('property_id', '=', $property->property_id)->first();
                    if (! is_null($vetting)) {
                        $this->createTivvitObjects($property, $vetting);
                    }
                    //				dd('completed listing the first property.');
                } else {
                    removePropertyTivvit($property->property_id);
                }
            }
            echo 'Complete';
        } catch (Exception $e) {
            echo "Error Details: $e";
            //			de('Caught exception: '.  $e);
        }
    }

    public function updateProperty()
    {
        try {
            $propertyid = Request::get('id');

            $properties = DB::select('select  p.id as propertyid, p.*,c.*,f.*,v.*,r.*, a.auction_type '.
                'from property p '.
                'left join residential r on p.id = r.property_id '.
                'left join commercial c on p.id = c.property_id '.
                'left join farm f on p.id = f.property_id '.
                'left join vetting v on v.property_id = p.id '.
                'left join auction a on a.property_id = p.id '.
                'where p.id = '.$propertyid
            );
//            de('here');
            foreach ($properties as $property) {
                $vetting = $this->vetting->where('property_id', '=', $propertyid)->first();

//                dd($vetting);
                if (! is_null($vetting)) {
                    $this->createTivvitObjects($property, $vetting);
                }
                //				dd('completed listing the first property.');
            }

            return 1;
        } catch (Exception $e) {
            de('Caught exception: '.$e);

            return $e;
            //			de('Caught exception: '.  $e);
        }

        //		de('completed listing properties');
    }

    public function removePropertyTivvit($propertyid)
    {
        $properties = DB::select('select  p.id as propertyid, p.*,c.*,f.*,v.*,r.* '.
            'from property p '.
            'left join residential r on p.id = r.property_id '.
            'left join commercial c on p.id = c.property_id '.
            'left join farm f on p.id = f.property_id '.
            'left join vetting v on v.property_id = p.id '.
            'where p.id = '.$propertyid
        );

        foreach ($properties as $property) {
            $pnum = $property->tivvit_listing_num;
            $extref = $property->reference;
            $webref = $property->tivvit_web_ref;
            $psite = $property->listing_type;
        }

        if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
            //DEV
            //dd('Dev');
            $bid = 'SD037';
            $password = 'dem6';
            $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
            $PublicKey = 'ryojvlzmdalyglrj';

            $serviceurl = 'http://sandbox.tivvit.com/TivvitServices.asmx?WSDL';

            TivvitWsdlClass::$WSDL_URL = $serviceurl;
            TivvitWsdlClass::$WSDL_URL_DOMAIN = 'http://sandbox.tivvit.com/';
            TivvitWsdlClass::$VALUE_WSDL_URL = $serviceurl;
        } else {
            //LIVE
            //dd('Live');
            if ($psite == 'wesell') {
                $bid = 'SW105';
                $password = 'dem6';
                $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                $PublicKey = 'ryojvlzmdalyglrj';
            } else {
                $bid = 'SS257';
                $password = 'dem6';
                $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                $PublicKey = 'ryojvlzmdalyglrj';
            }

            $serviceurl = 'http://services.tivvit.com/TivvitServices.asmx?WSDL';
            TivvitWsdlClass::$WSDL_URL = $serviceurl;
            TivvitWsdlClass::$WSDL_URL_DOMAIN = 'http://services.tivvit.com/';
            TivvitWsdlClass::$VALUE_WSDL_URL = $serviceurl;
        }

        $len = strlen($password);
        $pad = 16 - ($len % 16);
        $password .= str_repeat(chr($pad), $pad);
        $encryptkey = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $SecretKey, $password, MCRYPT_MODE_CBC, $PublicKey));

        $tivvitServiceRemove = new TivvitServiceRemove();
        // sample call for TivvitServiceRemove::setSoapHeaderUserInfo() in order to initialize required SoapHeader
        $tivvitServiceRemove->setSoapHeaderUserInfo(new TivvitStructUserInfo($bid, $encryptkey));

        $searchtype = 'PropertyNum';

        // sample call for TivvitServiceRemove::RemoveProperty()
        $removePropertyParameters = new TivvitStructPropertyRem($searchtype, $bid, $pnum, $webref, $extref);

        if ($tivvitServiceRemove->RemoveProperty(new TivvitStructRemoveProperty($removePropertyParameters))) {
            $this->property = $this->property->whereId($propertyid)->first();
            $this->property->tivvit_listing_num = null;
            $this->property->tivvit_web_ref = null;
            $this->property->update();
            print_r($tivvitServiceRemove->getResult());
        } else {
            print_r($tivvitServiceRemove->getLastError());
        }

        return 1;
    }

    public function removeProperty()
    {
        $propertyid = Request::get('id');

        $this->removePropertyTivvit($propertyid);

        return 1;
    }

    public function getFeatureValue($name, $property_id)
    {
        $query = 'SELECT value FROM attributes
                    INNER JOIN attributes_property
                    ON attributes.id = attributes_id
                    WHERE name = "'.$name.'"
                    AND property_id = "'.$property_id.'"';

        return DB::select($query);
    }

    public function createTivvitObjects($property, $vetting)
    {
        try {
            if (! is_null($property->property_id)) {
                $propertyID = $property->property_id;
            } elseif (! is_null($property->id)) {
                $propertyID = $property->id;
            } else {
                $propertyID = $property->propertyid;
            }

            if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
                //DEV
//            de('DEV');
                $bid = 'SD037';
                $password = 'dem6';
                $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                $PublicKey = 'ryojvlzmdalyglrj';

                //			$bid = "SS257";
                //			$password = "dem6";
                //			$SecretKey = "hcxilkqbbhczfeultgbskdmaunivmfuo";
                //			$PublicKey = "ryojvlzmdalyglrj";
                $serviceurl = 'http://sandbox.tivvit.com/TivvitServices.asmx?WSDL';

                TivvitWsdlClass::$WSDL_URL = $serviceurl;
                TivvitWsdlClass::$WSDL_URL_DOMAIN = 'http://sandbox.tivvit.com/';
                TivvitWsdlClass::$VALUE_WSDL_URL = $serviceurl;
            } else {
                //LIVE
//                dd('LIVE');
                if ($property->listing_type == 'wesell') {
                    $bid = 'SW105';
                    $password = 'dem6';
                    $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                    $PublicKey = 'ryojvlzmdalyglrj';
                } else {
                    $bid = 'SS257';
                    $password = 'dem6';
                    $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                    $PublicKey = 'ryojvlzmdalyglrj';
                }
                $serviceurl = 'http://services.tivvit.com/TivvitServices.asmx?WSDL';
                TivvitWsdlClass::$WSDL_URL = $serviceurl;
                TivvitWsdlClass::$WSDL_URL_DOMAIN = 'http://services.tivvit.com/';
                TivvitWsdlClass::$VALUE_WSDL_URL = $serviceurl;
            }

            $len = strlen($password);
            $pad = 16 - ($len % 16);
            $password .= str_repeat(chr($pad), $pad);
            $encryptkey = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $SecretKey, $password, MCRYPT_MODE_CBC, $PublicKey));

            $user = User::whereId($vetting->rep_id_number)->first();

            $agentdata = ['bid' => $bid,
                'agent' => $vetting->rep_id_number, //todo add agent code
                'extref' => $vetting->rep_id_number,
                'name' => $vetting->rep_firstname,
                'surname' => $vetting->rep_surname,
                'cell' => $user->cellnumber,
                'phone' => $user->phonenumber,
                'email' => $vetting->rep_email,
                'photo' => 'https://www.in2assets.co.za/'.$user->agent_photo,
            ];

//        dd($agentdata['photo']);
            $dtToday = date('Y-m-d');

            $tivvitServiceUpdate = new TivvitServiceUpdate();
            //		// sample call for TivvitServiceUpdate::setSoapHeaderUserInfo() in order to initialize required SoapHeader
            $tivvitServiceUpdate->setSoapHeaderUserInfo(new TivvitStructUserInfo($bid, $encryptkey));

            //$tivvit_property = new TivvitStructProperty($property);

            // sample call for TivvitServiceUpdate::UpdateProperty()
            //$updateprop = new TivvitStructUpdateProperty($property);
            $dtToday = date('Y-m-d');
            //		$tivvitAgent = new TivvitStructAgent(0,8,8);

            $tivvitAgent = new TivvitStructAgent(0, 0, 0, 0, $dtToday, $bid, $agentdata['agent'], $agentdata['extref'], ' ', $agentdata['name'],
                $agentdata['surname'], $agentdata['phone'], $agentdata['cell'], null, null, $agentdata['email'], null, $agentdata['photo'], null, null, null, 'Agent',
                null, null, null);
            $res = $tivvitServiceUpdate->UpdateAgent(new TivvitStructUpdateAgent($tivvitAgent));
            $agentid = 0;
            if ($res) {
                $tivvitResult = $tivvitServiceUpdate->getResult();
                $response = '<xml>';
                $response .= $tivvitResult->UpdateAgentResult->response;
                $response .= '</xml>';
                //			dd($response);
                $xmlResponse = simplexml_load_string($response);
                $agentid = $xmlResponse->agent;
//            de($xmlResponse->agent);
//            de($tivvitResult);
                //dd('Completed no errors agent updated');
            } else {

                //dd('Completed with errors');
            }

            $tivvitStructAgentInd = new TivvitStructAgentInd($agentdata['name'], $agentdata['surname'], $agentdata['phone'],
                $agentdata['email'], $agentdata['cell'], $agentid, $agentdata['photo']);

            $tivvitStructArrayOfAgentInd = new TivvitStructArrayOfAgentInd($tivvitStructAgentInd);
            switch ($property->rental_terms) {
                case '0':
                    $tivvit_rentper = TivvitEnumRentper::VALUE_NA;

                    break;
                case '1':
                    $tivvit_rentper = TivvitEnumRentper::VALUE_PM;

                    break;
                case '2':
                    $tivvit_rentper = TivvitEnumRentper::VALUE_SQ;

                    break;
                default:
                    $tivvit_rentper = TivvitEnumRentper::VALUE_NA;

                    break;
            }
            $levy = 0;
            $price = $property->price;
            if ($property->auction_type == 2 || $property->auction_type == 3) {
                $price = '9999999999';
            } elseif ($price == '0') {
                $price = '9999999999';
            } elseif ($price == '1') {
                $price = '9999999999';
            }

            $listingp = ' ';
            $soldp = ' ';
            $size = ' ';

            $bedrooms = 0;
            $bathrooms = 0;
            $garages = 0;

            if ($property->property_type == 'Residential') {
                if ($bedroomsDets = $this->getFeatureValue('bedrooms', $propertyID)) {
                    $bedrooms = $bedroomsDets[0]->value;
                    $bedroomsDets[0]->value = '';
                }
                if ($bathroomsDets = $this->getFeatureValue('bathrooms', $propertyID)) {
                    $bathrooms = $bathroomsDets[0]->value;
                    $bathroomsDets[0]->value = '';
                }
                if ($garagesDets = $this->getFeatureValue('garages', $propertyID)) {
                    $garages = $garagesDets[0]->value;
                    $garagesDets[0]->value = '';
                }
            }
            //de($bedrooms);

            if (($bedrooms == '') || ($bedrooms == 'S') || ($bedrooms == ' ')) {
                $bedrooms = 0;
            }

            if (($bathrooms == '') || ($bathrooms == ' ')) {
                if (($bathrooms == '') || ($bathrooms == ' ')) {
                    $bathrooms = 0;
                }
            }

            if (($garages == '') || ($garages == ' ')) {
                $garages = 0;
            }

            $features = App::make('AttributesController')->getAttributesByType(1);

            foreach ($features as $feature) {
                if ($featureDets = $this->getFeatureValue($feature->name, $propertyID)) {
                    $property->{$feature->name} = $featureDets[0]->value;

                    $featureDets[0]->value = '';
                }
            }

            $age = '0';
            if ($property->pool == 1) {
                $pool = true;
            } else {
                $pool = false;
            }
            if ($property->view == 1) {
                $view = true;
            } else {
                $view = false;
            }
            $study = $property->study;
            $carport = false;
            if ($property->staff_quarters == 1) {
                $servant = true;
            } else {
                $servant = false;
            }
            $laundry = $property->laundry;
            if ($property->security_system == 1) {
                $security = true;
            } else {
                $security = false;
            }
            if ($property->parking == 1) {
                $parking = true;
            } else {
                $parking = false;
            }
            $reception = false;
            if ($property->pets == 1) {
                $pets = true;
            } else {
                $pets = false;
            }
            $paving = false;
            $living = $property->lounge;
            $eatarea = $property->dining_room;
            $statusd = null;
            $listingd = $dtToday; //$dtToday;
            $expiringd = null;
            $priced = null;

            //open = 1
            //shared = 2
            //sole = 3

            switch ($vetting['in2assets_only'] == 1) {
                case 1:
                    $ltype = TivvitEnumLtype::VALUE_OPENMANDATE;

                    break;
                case 2:
                    $ltype = TivvitEnumLtype::VALUE_SHAREDMANDATE;

                    break;
                case 3:
                    $ltype = TivvitEnumLtype::VALUE_SOLEMANDATE;

                    break;
                default:
                    $ltype = TivvitEnumLtype::VALUE_SOLEMANDATE;

                    break;
            }
            if (strpos($property->reference, 'SALE') !== false) {
                $ptype = TivvitEnumPtype::VALUE_FORSALE;
            } elseif (strpos($property->reference, 'RENT') !== false) {
                $ptype = TivvitEnumPtype::VALUE_TORENT;
            } else {
                $ptype = TivvitEnumPtype::VALUE_NOTLISTED;
            }
            //		$ptype =  TivvitEnumPtype::VALUE_FORSALE;
            switch ($property->listing_type) {
                case 'wesell':
                case 'in2assets':
                case 'weauction':
                    $ptype = TivvitEnumPtype::VALUE_FORSALE;

                    break;
                case 'welet':
                case 'in2rentals':
                    $ptype = TivvitEnumPtype::VALUE_TORENT;

                    break;
            }
            $rental = 0;

            if ($property->listing_type == 'in2rental') {
                $rental = $property->price;
            }
            $soldd = null;
            $special = ' ';
            $pantry = $property->pantry;
            if ($property->entrance_hall == 1) {
                $hall = true;
            } else {
                $hall = false;
            }
            $sprinkler = $property->sprinklers;
            if ($property->tv_room == 1) {
                $tvroom = true;
            } else {
                $tvroom = false;
            }
            if ($property->guest_toilet == 1) {
                $guestt = true;
            } else {
                $guestt = false;
            }
            $pgarden = false;
            $patio = $property->patio;
            if ($property->alarm == 1) {
                $alarm = true;
            } else {
                $alarm = false;
            }
            $heating = false;

            $tennis = false;

            $incwater = false;
            $gservice = false;
            $pstaff = false;
            $cservice = false;

            $type = $property->property_type;

            if ($property->property_type == 'Farm') {
                $type = 'Farms';
            }

            if ($property->dwelling_type == 'Flat') {
                $type = 'Apartment';
            }

            if ($property->dwelling_type == 'House') {
                $type = 'House';
            }

            switch ($property->property_type) {
                case 'Residential':
                    $htm = TivvitEnumHtm::VALUE_RESIDENTIAL;
                    if ($property->dwelling_type == 'Vacant Land') {
                        $type = 'VACANT LAND';
                    }

                    break;
                case 'Commercial':
                    $htm = TivvitEnumHtm::VALUE_COMMERCIAL;
                    if ($property->commercial_type == 'Vacant Land') {
                        $type = 'VACANT LAND';
                    } elseif (($property->commercial_type == 'Industrial Property')) {
                        $type = 'COMMERCIAL';
                        $htm = TivvitEnumHtm::VALUE_INDUSTRIAL;
                    }

                    break;
                case 'Farm':
                    $htm = TivvitEnumHtm::VALUE_FARM;
                    if ($property->farm_type == 'Vacant Land') {
                        $type = 'VACANT LAND';
                    }

                    break;
            }

            //dd($type);

            $intercom = $property->intercom;
            $gate = $property->elec_gate;
            $slights = $property->sec_lights;
            $sfence = $property->sec_fence;
            $shared = ' ';
            $recroom = $property->recreation_room;
            $sole = true;
            $mandstart = null;
            $mandend = null;
            $brooms = 0;
            $bphase = false;
            $bsign = ' ';
            $bperiod = ' ';
            $bfloor = ' ';
            $bgrental = 0;
            $bnrental = 0;
            $bopcost = 0;
            $brentesc = 0;
            $bopcostesc = 0;
            $boccdate = null;
            $bfitallow = 0;
            $byard = 0;
            $bupper = 0;
            $munval = 0;
            $bondbal = 0;
            $ratestax = 0;
            if ($property->granny_flat == 1) {
                $flat = true;
            } else {
                $flat = false;
            }
            $flatkitc = false;
            $fbedroom = 0;
            $fbathroom = 0;
            $flatloun = false;
            $yield = 0;
            $vatvend = ' ';
            $standsize = ' ';
            $mainsize = ' ';
            $outbsize = ' ';
            $altersize = ' ';
            $garagesize = ' ';
            $grossinc = ' ';
            $netinc = ' ';
            $mlsrecd = null;
            $shareprop = ' ';
            $virtualtour = ' ';
            $cmlsstart = null;
            $cmlsend = null;
            $onint = 0;
            $lastmod = $dtToday;
            $saleinprog = ' ';
            $mlsopenhr = null;
            $grosscom = 0;
            $mlssplits = ' ';
            $mlssplitl = ' ';
            $mes = ' ';
            $kitchen = $property->kitchen;
            $aircon = $property->aircon;
            $marketval = 0;
            $negotiable = false;
            $minprice = 0;
            $sqprice = 0;
            $ispic = true;
            $webupdate = null;
            $pq = 0;
            $shareblock = ' ';
            $sbnoofshar = 0;
            if ($property->show_street_address == 1) {
                $xval = 1;
            } else {
                $xval = 0;
            }
            $yval = $property->x_coord;
            $xval2 = $property->y_coord;
            $yval2 = $property->x_coord;

            //dd($property->propertyid);
            $availfrom = null;

            if (is_null($property->tivvit_listing_num) || $property->tivvit_listing_num == 'error') {
                $pnum = ' ';
            } else {
                $pnum = $property->tivvit_listing_num;
            }
            //$pnum= 'IM5191';//todo http://demo6.uspdesigns.co.za/viewproperty13274251.cp
            $lotnum = ' ';
            $iduni = ' ';
            $cnum = ' ';

            $province = trim($property->province);
            $city = trim($property->area);
            $suburb = $property->suburb_town_city;
            $streetno = $property->street_number;
            $street = $property->street_address;
            $complex = $property->complex_name;
            $notes = $property->long_descrip;
            $pnote = ' ';
            $schools = ' ';
            $vicinity = ' ';
            $rooftype = ' ';
            $garden = ' ';
            $windowtype = ' ';
            $stories = ' ';
            $status = ' ';
            $odomain = ' ';
            if (strpos($property->reference, 'AUCT') !== false) {
                $odomain = 'Auction';
                $htm = TivvitEnumHtm::VALUE_AUCTIONS;
            }
            if ($property->vetting_status == 2) {
                $status = 'New';
            } elseif ($property->vetting_status == 4 || $property->vetting_status == 6 || $property->vetting_status == 10 ||
                $property->vetting_status == 12 || $property->vetting_status == 13 || $property->vetting_status == 14
            ) {
                $status = 'Sold';
                $odomain = 'Sold';
            }

            $section = ' ';
            $stand = ' ';
            if (is_numeric($property->complex_number)) {
                $unit = $property->complex_number;
            } elseif (strlen($property->complex_number) > 0) {
                $unit = 0;
            } else {
                $unit = ' ';
            }
            //de($unit);
            $garageno = ' ';
            $staffno = ' ';
            $ceiltype = ' ';
            $walltype = ' ';
            $floortype = ' ';
            $ccond = ' ';
            $ucond = ' ';
            $details = ' ';
            $features = null;
            $exterior = ' ';
            $ocompany = ' ';
            $ocontact = ' ';
            $otel = ' ';
            $ofax = ' ';
            $ocell = ' ';
            $oemail = ' ';

            $mlsno = ' ';
            $vdate = ' ';
            $bcont1 = ' ';
            $bcon1tel = ' ';
            $bcont2 = ' ';
            $bcon2tel = ' ';
            $bagree = ' ';
            $bnotes = ' ';
            $bcomp = ' ';
            $bmunicipal = ' ';
            $bbuilding = ' ';
            $bfnumber = ' ';
            $bparking = ' ';
            $bondhold = ' ';
            $tenant = ' ';
            $tentel1 = ' ';
            $tentel2 = ' ';
            $tentel3 = ' ';
            $texp = ' ';
            $bustype = ' ';
            if ($property->ownership_type == 'Company (Pty) Limited') {
                $ownership = '(Pty) Ltd';
            } else {
                $ownership = ' ';
            }

            $zoning = ' ';
            $mlsagent = ' ';
            $mlsarea = ' ';
            $mlsobid = ' ';
            $mlsopnum = ' ';
            $mlsocnum = ' ';
            $mlsbid = ' ';
            $mlsmyprop = 'My Property';
            $devname = ' ';
            $manag = ' ';
            $managcon = ' ';
            $managtel = ' ';
            $bodycorp = ' ';
            $petsallowed = ' ';
            $secfreeh = ' ';
            $grade = ' ';
            $mlsagentcel = ' ';
            $mlsviewins = ' ';
            $mlsattendee = ' ';
            $other = ' ';
            $other2 = ' ';
            $reasonsel = ' ';
            $tvaerial = $property->aerial;
            $mnetaerial = $property->dstv;
            $source = 'South Africa';
            $salestatus = ' ';
            $mlsnote = ' ';
            $cid = ' ';
            $bic = $property->bic;
            $showers = $property->shower;
            $stove = ' ';
            $servtoil = ' ';
            $lapa = $property->lapa_braai;
            $remgar = ' ';
            $floorplnpc = ' ';
            $intpnum = ' ';
            $region = ' ';
            $agownerid = ' ';

            $lotsize = 0;
            if ($lotsizeDets = $this->getFeatureValue('land_size', $propertyID)) {
                $lotsize = $lotsizeDets[0]->value;
                $lotsizeDets[0]->value = ' ';
            }

            $psizetype = 'm2';
            if ($psizeDets = $this->getFeatureValue('land_measurement', $propertyID)) {
                $psizetype = $psizeDets[0]->value;
                $psizeDets[0]->value = ' ';
            }

            if ($psizetype == 'M2') {
                $psizetype = 'm2';
            } elseif ($psizetype == 'HA') {
                $psizetype = 'ha';
                $lotsize /= 10000;
            }

            $secpno = ' ';
            $sbno = ' ';
            $sbcert = ' ';
            $farmtype = $property->farm_type;
            $livestock = ' ';
            $wildlife = ' ';
            $produce = ' ';
            $outbuild = ' ';
            $waterholes = ' ';
            $lodges = ' ';
            $farmimp = ' ';
            $compounds = ' ';
            $oneline = ' ';
            $bidgroup = ' ';
            $sellerdata = null; //new TivvitStructArrayOfSeller(new TivvitStructSeller(null,null,null,null,null,null,null,null,null));

            $picdataarray = [];

            $images = $this->property->
            join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')->where('property.id', $propertyID)->get();

            $leadimage = 'https://www.in2assets.co.za/'.$property->lead_image;
            $leadimage = str_replace('.jpg', '_835_467.jpg', $leadimage);

            array_push($picdataarray, new TivvitStructPicture(1, $leadimage));

            $count = 1;
            foreach ($images as $image) {
                $url = 'https://www.in2assets.co.za/';
                //$url .= $image->file;
                $url .= str_replace('.jpg', '_835_467.jpg', $image->file);
                $count = $count + 1;
                array_push($picdataarray, new TivvitStructPicture($count, $url, 'image'));
            }
            $picdata = new TivvitStructArrayOfPicture($picdataarray);

            $xnum = $property->reference;
            $cUR = ' ';
            $uniqueID = $property->reference;
            $rooms = ' ';
            $onshow = null; //new TivvitStructArrayOfOnshow(new TivvitStructOnshow(strtotime('22-01-2016 13:00'),strtotime('23-01-2016 14:00'),' ', '13:00','s14:00'));
            $auction = null;
            if ($property->listing_type == 'weauction') {
                $this->auction = $this->auction->whereProperty_id($propertyID)->first();
                if (! is_null($this->auction)) {
                    $description = null;

                    if ($this->auction->auction_type == 1) {
                        if (! is_null($this->auction->physical_auction)) {
                            $physical_auction = $this->physical_auction->whereId($this->auction->physical_auction)->first();
                            $description = $physical_auction->auction_name;
                        }
                        $venue = $physical_auction->street_address;
                    } elseif ($this->auction->auction_type == 5) {
                        $venue = 'Call agent for directions';
                        $description = 'On site';
                    } else {
                        $venue = 'online';
                        $description = 'online';
                    }
                    if ($this->auction->auction_type == 2) {
                        $auction = new TivvitStructAuction($venue, $description, '1', date('Ymd', strtotime($this->auction->end_date)), date('g:i a', strtotime($this->auction->end_date)));
                    } else {
                        $auction = new TivvitStructAuction($venue, $description, '1', date('Ymd', strtotime($this->auction->start_date)), date('g:i a', strtotime($this->auction->start_date)));
                    }
                }
            }

            //dd($auction);
            $tivvit_property = new TivvitStructProperty(
                $rental, $tivvit_rentper, $levy, $price, $listingp, $soldp, $size, $lotsize,
                $bedrooms, $bathrooms, $garages, $age, $pool, $view, $study, $carport,
                $servant, $laundry, $security, $parking, $reception, $pets, $paving, $living,
                $eatarea, $statusd, $listingd, $expiringd, $priced, $ltype, $ptype, $soldd,
                $special, $pantry, $hall, $sprinkler, $tvroom, $guestt, $pgarden, $patio, $alarm,
                $heating, $tennis, $incwater, $gservice, $pstaff, $cservice, $htm, $intercom,
                $gate, $slights, $sfence, $recroom, $shared, $sole, $mandstart, $mandend, $brooms,
                $bphase, $bsign, $bperiod, $bfloor, $bgrental, $bnrental, $bopcost, $brentesc,
                $bopcostesc, $boccdate, $bfitallow, $byard, $bupper, $munval, $bondbal, $ratestax,
                $flat, $flatkitc, $fbedroom, $fbathroom, $flatloun, $yield, $vatvend, $standsize,
                $mainsize, $outbsize, $altersize, $garagesize, $grossinc, $netinc, $mlsrecd, $shareprop,
                $virtualtour, $cmlsstart, $cmlsend, $onint, $lastmod, $saleinprog, $mlsopenhr, $grosscom,
                $mlssplits, $mlssplitl, $mes, $kitchen, $aircon, $marketval, $negotiable, $minprice,
                $sqprice, $ispic, $webupdate, $pq, $shareblock, $sbnoofshar, $xval, $yval, $xval2,
                $yval2, $propertyID, $availfrom, $bid, $pnum, $lotnum, $iduni,
                $cnum, $type, $province, $city, $suburb,
                $streetno, $street, $complex, $notes, $pnote,
                $schools, $vicinity, $rooftype, $garden, $windowtype,
                $stories, $status, $section, $stand, $unit,
                $garageno, $staffno, $ceiltype, $walltype, $floortype,
                $ccond, $ucond, $details, $features, $exterior,
                $ocompany, $ocontact, $otel, $ofax, $ocell,
                $oemail, $odomain, $mlsno, $vdate, $bcont1,
                $bcon1tel, $bcont2, $bcon2tel, $bagree, $bnotes,
                $bcomp, $bmunicipal, $bbuilding, $bfnumber, $bparking,
                $bondhold, $tenant, $tentel1, $tentel2, $tentel3,
                $texp, $bustype, $ownership, $zoning, $mlsagent,
                $mlsarea, $mlsobid, $mlsopnum, $mlsocnum, $mlsbid,
                $mlsmyprop, $devname, $manag, $managcon, $managtel,
                $bodycorp, $petsallowed, $secfreeh, $grade,
                $mlsagentcel, $mlsviewins, $mlsattendee, $other,
                $other2, $reasonsel, $tvaerial, $mnetaerial, $source,
                $salestatus, $mlsnote, $cid, $bic, $showers,
                $stove, $servtoil, $lapa, $remgar, $floorplnpc,
                $intpnum, $region, $agownerid, $psizetype, $secpno,
                $sbno, $sbcert, $farmtype, $livestock, $wildlife,
                $produce, $outbuild, $waterholes, $lodges, $farmimp,
                $compounds, $oneline, $bidgroup, $tivvitStructArrayOfAgentInd, $sellerdata, $picdata, $xnum, $cUR, $uniqueID, $rooms,
                $onshow, $auction
            );

            $this->tivvitPush($tivvit_property, $property, $propertyID);
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function tivvitPush($property, $dbprop, $propertyID)
    {
        if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
            //DEV
//            de('DEV');
            $bid = 'SD037';
            $password = 'dem6';
            $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
            $PublicKey = 'ryojvlzmdalyglrj';

            //			$bid = "SS257";
            //			$password = "dem6";
            //			$SecretKey = "hcxilkqbbhczfeultgbskdmaunivmfuo";
            //			$PublicKey = "ryojvlzmdalyglrj";
            $serviceurl = 'http://sandbox.tivvit.com/TivvitServices.asmx?WSDL';

            TivvitWsdlClass::$WSDL_URL = $serviceurl;
            TivvitWsdlClass::$WSDL_URL_DOMAIN = 'http://sandbox.tivvit.com/';
            TivvitWsdlClass::$VALUE_WSDL_URL = $serviceurl;
        } else {
            //LIVE
            //dd('LIVE');
            if ($dbprop->listing_type == 'wesell') {
                $bid = 'SW105';
                $password = 'dem6';
                $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                $PublicKey = 'ryojvlzmdalyglrj';
            } else {
                $bid = 'SS257';
                $password = 'dem6';
                $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
                $PublicKey = 'ryojvlzmdalyglrj';
            }
            $serviceurl = 'http://services.tivvit.com/TivvitServices.asmx?WSDL';
            TivvitWsdlClass::$WSDL_URL = $serviceurl;
            TivvitWsdlClass::$WSDL_URL_DOMAIN = 'http://services.tivvit.com/';
            TivvitWsdlClass::$VALUE_WSDL_URL = $serviceurl;
        }

        $len = strlen($password);
        $pad = 16 - ($len % 16);
        $password .= str_repeat(chr($pad), $pad);
        $encryptkey = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $SecretKey, $password, MCRYPT_MODE_CBC, $PublicKey));

        $wsdl = [];
        // we use the WSDL latest version
        //		$wsdl[tivvitWsdlClass::WSDL_URL] = $serviceurl;
        $wsdl[tivvitWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
        $wsdl[tivvitWsdlClass::WSDL_TRACE] = true;

        // create PHP stream context
        $options = [];
        $options['http'] = [];
        $options['http']['header'] = [];
        $tivvitHeaders = [];
        // You must copy-paste your AppID here
        $tivvitHeaders['userName'] = $bid;

        $tivvitHeaders['password'] = $encryptkey;

        foreach ($tivvitHeaders as $tivvitHeaderName => $tivvitHeaderValue) {
            array_push($options['http']['header'], "$tivvitHeaderName: $tivvitHeaderValue");
        }
        $options['http']['header'] = implode("\r\n", $options['http']['header']);
        $context = stream_context_create($options);
        // Add the PHP stream context to the WSDL options
        $wsdl[tivvitWsdlClass::WSDL_STREAM_CONTEXT] = $context;

        $wsdlObject = new TivvitWsdlClass($wsdl);

        $tivvitServiceAuthenticate = new TivvitServiceAuthenticate();

        $tivvitServiceAuthenticate->setSoapHeaderUserInfo(new TivvitStructUserInfo($bid, $encryptkey));
        $tivvitServiceAuthenticate->AuthenticateMe(new TivvitStructAuthenticateMe($bid, $encryptkey));
        //		de($bid);
        //		if($tivvitServiceAuthenticate->AuthenticateMe(new TivvitStructAuthenticateMe($bid,$encryptkey)))
        //			de($tivvitServiceAuthenticate->getResult());
        //		else
        //			de($tivvitServiceAuthenticate->getLastError());
        //		dd('Complete about to send to tivvit');
        //		/******************************
        //		 * Example for TivvitServiceGet
        //		 */
        //		$tivvitServiceGet = new TivvitServiceGet();
        //		// sample call for TivvitServiceGet::setSoapHeaderUserInfo() in order to initialize required SoapHeader
        //		$tivvitServiceGet->setSoapHeaderUserInfo(new TivvitStructUserInfo($bid,$encryptkey));
//
        //		if($tivvitServiceGet->GetSuburbs())
        //			print_r($tivvitServiceGet->getResult());
        //		else
        //			print_r($tivvitServiceGet->getLastError());

        //		$tivvitServiceUpdate = new TivvitServiceUpdate();
        // sample call for TivvitServiceUpdate::setSoapHeaderUserInfo() in order to initialize required SoapHeader
        //		$tivvitServiceUpdate->setSoapHeaderUserInfo(new TivvitStructUserInfo($bid,$encryptkey));
        // sample call for TivvitServiceUpdate::UpdateAgent()

        //		$dtToday = date('Y-m-d');
        //		if($tivvitServiceUpdate->UpdateAgent(new TivvitStructUpdateAgent(new TivvitStructAgent(25,0,0,0,$dtToday,$bid,'007','007',null,'james','bond','0312445887','0310070077',null,null,'jamesbond@hotmai.com',null,'picture',null,null,'M','Agent',null,null,null))))
        //			print_r($tivvitServiceUpdate->getResult());
        //		else
        //			print_r($tivvitServiceUpdate->getLastError());
//
//
        //Success: UpdatedIM519113274251

        $tivvitServiceUpdate = new TivvitServiceUpdate();
        //		// sample call for TivvitServiceUpdate::setSoapHeaderUserInfo() in order to initialize required SoapHeader
        $tivvitServiceUpdate->setSoapHeaderUserInfo(new TivvitStructUserInfo($bid, $encryptkey));

        //$tivvit_property = new TivvitStructProperty($property);

        // sample call for TivvitServiceUpdate::UpdateProperty()
        //$updateprop = new TivvitStructUpdateProperty($property);
        //		$dtToday = date('Y-m-d');
        ////		$tivvitAgent = new TivvitStructAgent(0,8,8);
        //		$tivvitAgent = new TivvitStructAgent(0,0,0,0,$dtToday,$bid,$agentdata['agent'],$agentdata['extref'],' ',$agentdata['name'],
        //			$agentdata['surname'],$agentdata['cell'],$agentdata['cell'],null,null,$agentdata['email'],null,' ',null,null,null,'Agent',
        //			null,null,null);
        //		$res = $tivvitServiceUpdate->UpdateAgent(new TivvitStructUpdateAgent($tivvitAgent));
        //		if($res){
        //			$tivvitResult = $tivvitServiceUpdate->getResult();
        //			$response = '<xml>';
        //			$response .= $tivvitResult->UpdateAgentResult->response;
        //			$response .= '</xml>';
        ////			dd($response);
        //			$xmlResponse = simplexml_load_string($response);
        //			de($xmlResponse->agent);
        //			de($tivvitResult);
        //			dd('Completed no errors agent updated');
        //		}else{
//
        //			dd('Completed with errors');
        //		}
        //de('vetting controller:');
        //dd($tivvitServiceUpdate->UpdateProperty(new TivvitStructUpdateProperty($property)));

        if ($tivvitServiceUpdate->UpdateProperty(new TivvitStructUpdateProperty($property))) {
            // dd('here');
            $tivvitResult = $tivvitServiceUpdate->getResult();
            $response = '<xml>';
            $response .= $tivvitResult->UpdatePropertyResult->response;
            $response .= '</xml>';
            de($response);
            $xmlResponse = simplexml_load_string($response);
//            de($xmlResponse->pnum);

            $prop_to_update = $this->property->whereId($propertyID)->first();

            //			de($dbprop->id);
            //			de($this->property);
            $prop_to_update->tivvit_listing_num = $xmlResponse->pnum;
            $prop_to_update->tivvit_web_ref = $xmlResponse->intpnum;
            $prop_to_update->update();

//            de('Completed property push no errors');
        } else {
            //dd('error here');
            //$tivvitResult = $tivvitServiceUpdate->getLastError();
            //de($tivvitResult);
            //			dd($tivvitServiceUpdate->getLastError());
            //			dd($dbprop->id);

            $prop_to_update = $this->property->whereId($propertyID)->first();

            $prop_to_update->tivvit_listing_num = 'error';
            $prop_to_update->tivvit_web_ref = 'error';
            $prop_to_update->update();
            //dd($tivvitResult);
//			de($tivvitServiceUpdate->getLastError());
//            de('Completed property push with errors');
        }

        //		dd('Complete');
    }

    public function tivvitIntergrate($id_uni, $property_type, $province, $city, $suburb, $Streetno, $street, $complex, $Notes, $rental, $Rentper,
                                     $price, $lotsize, $bedrooms, $bathrooms, $garages, $pool, $view, $study, $carport, $laundry, $security, $parking, $pets, $eatarea, $dtMandate,
                                     $status, $hall, $tvroom, $guesttoilet, $alarm, $tennis, $intercom, $slights, $sfence, $sole, $ownership, $onint, $lastmod, $aircon,
                                     $tvaerial, $mnetaerial, $bic, $showers, $lapa, $ispic, $intpnum, $farmtype, $xnum, $xval, $xval2, $yval2, $uniqueid, $Propertyid, $ltype, $ptype,
                                     $htm, $odomain, $agentdata, $picdata)
    {
        $bid = 'SD037';

        $password = 'dem6';
        $SecretKey = 'hcxilkqbbhczfeultgbskdmaunivmfuo';
        $PublicKey = 'ryojvlzmdalyglrj';
        $mcrypt_cipher = MCRYPT_RIJNDAEL_256;
        $mcrypt_mode = MCRYPT_MODE_CBC;

        $len = strlen($password);
        $pad = 16 - ($len % 16);
        $password .= str_repeat(chr($pad), $pad);
        $encryptkey = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $SecretKey, $password, MCRYPT_MODE_CBC, $PublicKey));

        $url = 'http://sandbox.tivvit.com/TivvitServices.asmx?wsdl';
        $ns = 'http://sandbox.tivvit.com/';
        $soapClient = new SoapClient($url);

        //Body of the Soap Header.
        $headerbody = ['userName' => $bid,
            'password' => $encryptkey, ];
        //Create Soap Header.
        $header = new SOAPHeader($ns, 'UserInfo', $headerbody);
        //set the Headers of Soap Client.
        $soapClient->__setSoapHeaders($header);
        $soapFunction = 'AuthenticateMe';
        $soapFunctionParameters = ['userName' => $bid, 'password' => $encryptkey];
        $soapResult = $soapClient->__soapCall($soapFunction, $soapFunctionParameters);

        //dd($soapResult);

        $propertyArray = [
            'bid' => $bid, //char(5) 	Tivvit Branch ID (Mandatory)
            'id_uni' => $id_uni, //char(10) 	Unique Id of listing - for Internal use only
            'Type' => $property_type, //char(30) 	Type of Property eg Townhouse or Apartment  or House  (Mandatory)
            'province' => $province, //char(25)	Province- mandatory
            'city' => $city, //char(25)	City- mandatory
            'suburb' => $suburb,  //char(40)	Suburb- mandatory
            'Streetno' => $Streetno,  //char(10)	Street Number- mandatory
            'street' => $street,  //char(40)	Streeet Name- mandatory
            'complex' => $complex,  //char(25)	Complex Name
            'Notes' => $Notes,  //Description of Listing - suggest 140 words (Mandatory)
            'rental' => $rental,  //numeric(14, 2)	Rental Price
            'Rentper' => $Rentper, //Rentper	If rental then mandatory  <rentper>NA or PM or PA or PW or PD or SQ</rentper>
            'Price' => $price,  //numeric(14, 2)	Price if listing is for sale - mandatory
            'lotsize' => $lotsize,  //numeric(12, 2)	Erf Size
            'bedrooms' => $bedrooms,  //numeric(3, 1)	Number of Bedrooms - mandatory
            'bathrooms' => $bathrooms,  //numeric(3, 1)	Number of bathrooms - mandatory
            'garages' => $garages,  //numeric(3, 1)	Number of garages- mandatory
            'pool' => $pool,  //Boolean	Pool
            'view_' => $view,  //Boolean	View
            'study' => $study,  //Boolean	Study
            'carport' => $carport,  //Boolean	Carport
            'laundry' => $laundry,  //Boolean	Laundry
            'security' => $security,  //Boolean	Security
            'parking' => $parking,  //Boolean	Parking
            'pets' => $pets,  //Boolean	Pets Allowed
            'eatarea' => $eatarea,  //Boolean	Dining Area
            'listingd' => $dtMandate,  //DateTime	Mandate start Date (Mandatory)
            'status' => $status,  //char(20)	Status of listing -eg Sold, New
            'ltype' => $ltype,    //ltype	Ltype	See Ltype Object (Mandatory) <ltype>OpenMandate or MLSMandate or SharedMandate or SoleMandate or OfficeListing or GroupListing or OppositionMandate or CountryWide</ltype>
            'ptype' => $ptype,    //ptype	Ptype	See Ptype object(Mandatory) <ptype>ForSale or ToRent or Both or NotListed</ptype>
            'hall' => $hall,  //Boolean	Entrance Hall
            'tvroom' => $tvroom,        //Boolean
            'guestt' => $guesttoilet,  //Boolean
            'alarm' => $alarm,  //Boolean
            'tennis' => $tennis,  //Boolean
            'intercom' => $intercom,  //Boolean
            'slights' => $slights,  //Boolean
            'sfence' => $sfence,  //Boolean
            'sole' => $sole,  //Boolean	Sole Mandate
            'ownership' => $ownership,  //char(10)	Ownership Type - one of Trust,(Pty) Ltd,Private,CC or blank
            'onint' => $onint,  //Boolean	On internet - False = Yes, True = No
            'lastmod' => $lastmod,  //DateTime	Last Modified Date Time
            'aircon' => $aircon,  //Boolean	Has Air conditioning
            'tvaerial' => $tvaerial,  //Boolean
            'mnetaerial' => $mnetaerial, //Boolean
            'bic' => $bic,  //char(10)	Built in Cupboard
            'showers' => $showers,  //char(5)	Shower Description
            'lapa' => $lapa,  //char(10)	Lapa
            'ispic' => $ispic,  //Boolean	Has Pictures
            'intpnum' => $intpnum,  //char(10)	Web Reference Number (Tivvit Assigned)
            'farmtype' => $farmtype,  //char(20)	Farm Type if Farm
            'xnum' => $xnum,  //char(20)	Your Ref Number(Mandatory)
            'xval' => $xval,  //numeric(18, 2)	Used to Show Address 0=No,1=Yes
            'xval2' => $xval2,  //numeric(18, 2)	Latitude
            'yval2' => $yval2,  //numeric(18, 2)	Longitude
            'uniqueid' => $uniqueid,  //varchar(50)	Unique Ref Number (Internal)
            'Propertyid' => $Propertyid,  //integer	Inter Number of Listing (Internal)
            'htm' => $htm,  //Htm 	Web Grouping (see Htm Object) (Mandatory)
            'odamain' => $odomain,  //char(50)	Used for Banners - Sold Banner,Onshow,Reserved,Reduced, Repossessed,Auction,Rented,Underoffer
            'agentdata' => $agentdata,  //Array of AgentInd	Agent involved - Array of Agents (Mandatory)
            'picdata' => $picdata,  //Array of Picture	Images - Array of Picture (Mandatory)
        ];

        //print_r($soapClient->__getFunctions());
        //print_r($soapClient->__getTypes());

//		$soapClient2 = new SoapClient($url);
//		$soapClient2->__setSoapHeaders($header);
//
//		//$soapResult1 = $soapClient2->__soapCall($soapFunction, $soapFunctionParameters) ;
//		//$propertyObject = $this->array_to_objecttree($propertyArray);
//		//dd($propertyObject);
//		$soapResult1 = $soapClient2->__soapCall('UpdateProperty', $propertyArray);
//
//		dd($soapResult1);
    }
}
