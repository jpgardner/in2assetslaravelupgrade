<?php

namespace App\Http\Controllers;

use App\Helpers\AppHelper;
use App\Models\Lead;
use App\Models\Targets;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use In2Assets\Users\User;
use Laracasts\Flash\Flash;

class LeadsController_LOCAL_16712 extends Controller
{
    protected $lead;

    public function __construct(User $user, PropNotes $prop_notes, Lead $lead, Contact $contact, Commission $commission,
                                Targets $targets, Property $property, Residential $residential, Commercial $commercial,
                                Farm $farm, Vetting $vetting, Actual $actual)
    {
        $this->user = $user;
        $this->prop_notes = $prop_notes;
        $this->lead = $lead;
        $this->contact = $contact;
        $this->commission = $commission;
        $this->targets = $targets;
        $this->property = $property;
        $this->residential = $residential;
        $this->commercial = $commercial;
        $this->farm = $farm;
        $this->vetting = $vetting;
        $this->actual = $actual;
    }

    /**
     * Display a listing of the resource.
     * GET /leads.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /leads/create.
     *
     * @return Response
     */
    public function create()
    {
        $leads = DB::select('SELECT l.id FROM leads l WHERE l.created_at = l.updated_at AND l.bdm_allocated = '.Auth::user()->id);
        foreach ($leads as $lead) {
            $query = "SELECT p.note FROM prop_notes p
                  INNER JOIN note_types n ON n.id = p.note_type
                  WHERE p.other_id = $lead->id
                  AND n.type = 'Lead'";
            $previous_notes = DB::select($query);
            if (empty($previous_notes)) {
                $contacts = $this->contact
                    ->select('lead_contacts.id')
                    ->whereLead_id($lead->id)->get();
                if ($contacts->count() == '0') {
                    $allCommissions = $this->commission
                        ->select('commissions.id')
                        ->whereLead_id($lead->id)->get();
                    if ($allCommissions->count() == '0') {
                        $uploadQuery = "SELECT * FROM uploads u WHERE u.type = 'lead' AND u.link =".$lead->id;
                        $images = DB::select($uploadQuery);
                        if (empty($images)) {
                            return Redirect::route('leads.edit', ['id' => $lead->id]);
                        }
                    }
                }
            }
        }

        $this->lead = new Lead();
        $this->lead->bdm_allocated = Auth::user()->id;
        $this->lead->save();
        DB::table('lead_deed_details')->insert(['lead_id' => $this->lead->id]);

        return Redirect::route('leads.edit', ['id' => $this->lead->id]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /leads.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Request::only(['lead_type', 'lead_ref_type', 'lead_ref', 'lead_prop_trans_type', 'bdm_allocated']);

        $this->lead->fill($input)->save();

        $this->prop_notes->note = Request::get('notes');
        $this->prop_notes->other_id = $this->lead->id;
        $this->prop_notes->users_id = Auth::user()->id;
        $this->prop_notes->save();
    }

    /**
     * Display the specified resource.
     * GET /leads/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /leads/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $lead_types = DB::table('lead_types')->where('active', '1')->pluck('type', 'id');
        $lead_ref_types = DB::table('lead_ref_types')->where('active', '1')->pluck('type', 'id');
        $lead_prop_trans_types = DB::table('lead_prop_trans_types')->where('active', '1')->pluck('type', 'id');
        $company_types = DB::table('company_types')->where('active', '1')->pluck('type', 'id');

        if (Auth::user()->hasRole('LeadReviewer')) {
            $agents = $this->user
                ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
                ->whereHas('roles', function ($q) {
                    $q->where('name', 'Agent');
                })->pluck('fullname', 'id');

            $agents = [Auth::user()->id => Auth::user()->firstname.' '.Auth::user()->lastname] + $agents;
        } else {
            $agents = [Auth::user()->id => Auth::user()->firstname.' '.Auth::user()->lastname];
        }

        $allAgents = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->pluck('fullname', 'id');

        $lead = $this->lead->whereId($id)->first();

        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  INNER JOIN note_types n ON n.id = p.note_type
                  WHERE p.other_id = $id
                  AND n.type = 'Lead'
                  ORDER BY p.created_at DESC";

        $previous_notes = DB::select($query);

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $contact_types = DB::table('roles')->whereContact_type('1')->pluck('description', 'id');

        $contacts = $this->contact
            ->select('lead_contacts.*', 'roles.description', 'company_types.type')
            ->leftJoin('roles', 'lead_contacts.role_id', '=', 'roles.id')
            ->leftJoin('company_types', 'lead_contacts.company_type', '=', 'company_types.id')
            ->whereLead_id($id)->get();

        $user = $this->user->whereId(Auth::user()->id)->first();
        $files = $user->files()->where('link', '=', $id)
            ->where('type', '=', 'image')->where('class', '=', 'lead')
            ->with('user')->orderBy('uploads.position')
            ->get();

        $query = "SELECT SUM(c.commission) AS total
                    FROM commissions c
                    INNER JOIN commission_types ct
                    ON c.type = ct.id
                    WHERE (ct.type = 'BDM' OR ct.type = 'SLA') AND c.lead_id = $id
                    AND c.deleted_at IS NULL";
        $commission = DB::select($query);

        $query2 = "SELECT SUM(c.commission) AS total
                    FROM commissions c
                    INNER JOIN commission_types ct
                    ON c.type = ct.id
                    WHERE ct.type = 'Seller' AND c.lead_id = $id
                    AND c.deleted_at IS NULL";
        $rebate = DB::select($query2);

        $allCommissions = $this->commission
            ->select('commissions.id', 'commissions.name', 'commissions.commission', 'commissions.created_at',
                'commissions.approved_date', 'users.firstname', 'commission_types.type')
            ->join('commission_types', 'commissions.type', '=', 'commission_types.id')
            ->leftJoin('users', 'commissions.approved_by', '=', 'users.id')
            ->whereLead_id($id)->get();

        if (! $deed_details = DB::table('lead_deed_details')->whereLead_id($id)->first()) {
            DB::table('lead_deed_details')->insert(['lead_id' => $id]);
            $deed_details = DB::table('lead_deed_details')->whereLead_id($id)->first();
        }

        return view::make('leads.edit', ['lead_types' => $lead_types, 'lead_ref_types' => $lead_ref_types,
            'lead_prop_trans_types' => $lead_prop_trans_types, 'company_types' => $company_types, 'agents' => $agents,
            'lead' => $lead, 'previous_notes' => $previous_notes, 'cities' => $cities, 'contact_types' => $contact_types,
            'contacts' => $contacts, 'files' => $files, 'commission' => $commission, 'rebate' => $rebate,
            'allCommissions' => $allCommissions, 'allAgents' => $allAgents, 'deed_details' => $deed_details, ]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /leads/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $this->lead = $this->lead->whereId($id)->first();

        $ref_type = DB::table('lead_ref_types')->where('type', '=', 'Refer to BDM')->first();

        $note_type = DB::table('note_types')->where('type', '=', 'Lead')->first();

        $bdm = $this->user->whereId(Request::get('bdm_referred'))->first();

        if (Request::get('lead_ref_type') == $ref_type->id) {
            if (Request::get('bdm_referred') != $this->lead['bdm_allocated']) {
                $this->prop_notes->note = 'Lead Referred to '.$bdm->firstname.' '.$bdm->lastname;
                $this->prop_notes->other_id = $this->lead->id;
                $this->prop_notes->users_id = Auth::user()->id;
                $this->prop_notes->note_type = $note_type->id;
                $this->prop_notes->save();

                $url = 'https://www.in2assets.co.za/leads/'.$id.'/edit';
                $bdm_name = $bdm->firstname.' '.$bdm->lastnamep;
                $bdm_email = $bdm->email;

                $subject = 'You have received a new lead';
                $name = 'Dear '.$bdm_name.',<br>';

                $emailData = "<h4>You have received a new lead <a href='".$url."'>Click here</a>.</h4><p>";
                $data = ['name' => $name, 'subject' => $subject, 'messages' => $emailData];
                // Queue email
                Mail::queue('emails.registration.adminnotice', $data, function ($message) use ($bdm_name, $bdm_email, $subject) {
                    $message->to($bdm_email, $bdm_name)
                        ->cc('neeraj@in2assets.com', 'Neeraj Ramautar')
                        ->subject($subject);
                });
                Flash::message('An email has been sent to '.$bdm_email);
            }
        }

        $input = Request::except(['notes', 'Suburb', 'contact_type', 'contact_firstname', 'contact_number', 'contact_email',
            'contact_lastname', 'leadDatatable_length', 'company_type', 'company_name', 'company_email', 'company_number',
            'contact_other', 'company_other', 'bdm_comm', 'bdm_percent', 'seller_rebate', 'seller_percent', 'sla_comm',
            'sla_percent', 'commDatatable_length', 'bdm_referred', 'erf_num', 'erf_type', 'erf_description', 'erf_size',
            'last_sold_price', 'title_deed_num', 'portion_num', 'company_reg_num',
        ]);

        $this->lead->fill($input)->update();

        if (Request::get('notes') != '') {
            $this->prop_notes->note = Request::get('notes');
            $this->prop_notes->other_id = $this->lead->id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->note_type = $note_type->id;
            $this->prop_notes->save();
        }

        DB::table('lead_deed_details')->whereLead_id($id)->update(['erf_num' => Request::get('erf_num'), 'erf_type'  => Request::get('erf_type'),
            'erf_description' => Request::get('erf_description'), 'erf_size' => Request::get('erf_size'), 'last_sold_price' => Request::get('last_sold_price'),
            'title_deed_num' => Request::get('title_deed_num'), 'portion_num' => Request::get('portion_num'), ]);

        return Redirect::route('leads.edit', ['id' => $this->lead->id]);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /leads/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $lead = $this->lead->whereId($id)->first();

        $lead->delete();

        Flash::message('Lead has been deleted!');

        return Redirect::back();
        //
    }

    public function saveNote()
    {
        $note_type = DB::table('note_types')->where('type', '=', 'Lead')->first();
        if (Request::get('notes') != '') {
            $this->prop_notes->note = Request::get('notes');
            $this->prop_notes->other_id = Request::get('lead_id');
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->note_type = $note_type->id;
            $this->prop_notes->save();
        }

        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  INNER JOIN note_types n ON n.id = p.note_type
                  WHERE p.other_id = ".Request::get('lead_id')."
                  AND n.type = 'Lead'
                  ORDER BY p.created_at DESC";

        $previous_notes = DB::select($query);

        return $previous_notes;
    }

    public function saveContact()
    {
        $input = Request::all();

        $this->contact->fill($input)->save();

        $this->addNewActual(Auth::user()->id, 'DB_Growth', 1);

        $contacts = $this->contact
            ->select('lead_contacts.*', 'roles.description', 'company_types.type')
            ->leftJoin('roles', 'lead_contacts.role_id', '=', 'roles.id')
            ->leftJoin('company_types', 'lead_contacts.company_type', '=', 'company_types.id')
            ->whereLead_id($input['lead_id'])->get();

        return $contacts;
    }

    public function updateContact($id)
    {
        $input = Request::all();

        $this->contact = $this->contact->whereId($id)->first();
        $this->contact->fill($input)->update();

        return 'Done';
    }

    public function getLeadContacts($lead_id)
    {
        $contacts = $this->contact->whereLead_id($lead_id)->get();
        $contacts = $contacts->toArray();

        return Datatable::collection(new Collection($contacts))
            ->showColumns('firstname', 'lastname', 'email', 'tel')
            ->addColumn('type', function ($Contact) {
                if ($Contact['role_id'] >= '0') {
                    switch ($Contact['role_id']) {
                        case '4':
                            return 'Owner';

                            break;
                        case '5':
                            return 'Buyer';

                            break;
                        case '13':
                            return 'Managing Agent';

                            break;
                        case '14':
                            return 'Key Holder';

                            break;
                        case '15':
                            return 'Letting Agent';

                            break;
                        case '17':
                            return 'Building Manager';

                            break;
                        case '18':
                            return 'Director';

                            break;
                        case '19':
                            return 'Trustee';

                            break;
                        case '20':
                            return 'Member';

                            break;
                        case '21':
                            return $Contact['contact_other'];

                            break;
                        default:
                            return 'None';

                            break;
                    }
                } elseif ($Contact['company_type'] >= '0') {
                    switch ($Contact['company_type']) {
                        case '1':
                            return 'CC';

                            break;
                        case '2':
                            return 'PTY';

                            break;
                        case '3':
                            return 'Trust';

                            break;
                        case '7':
                            return $Contact['company_other'];

                            break;
                        default:
                            return 'None';

                            break;
                    }
                }
            })
            ->addColumn('updated_at', function ($Contact) {
                return date('Y-m-d', strtotime($Contact['updated_at']));
            })
            ->addColumn('action', function ($Contact) {
                return '<a href="/contact/'.$Contact['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                        <a href="/contact/'.$Contact['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
            })
            ->searchColumns('firstname', 'lastname', 'email', 'tel', 'type', 'updated_at')
            ->orderColumns('firstname', 'lastname', 'email', 'tel', 'type', 'updated_at')
            ->make();
    }

    public function getLeadCommissions($lead_id)
    {
        $commissions = $this->commission
            ->select('commissions.id', 'commissions.name', 'commissions.commission', 'commissions.created_at',
                'commissions.approved_date', 'users.firstname', 'commission_types.type')
            ->join('commission_types', 'commissions.type', '=', 'commission_types.id')
            ->leftJoin('users', 'commissions.approved_by', '=', 'users.id')
            ->whereLead_id($lead_id)->get();

        $commissions = $commissions->toArray();

        return Datatable::collection(new Collection($commissions))
            ->showColumns('name', 'type')
            ->addColumn('commission', function ($Commission) {
                if ($Commission['type'] == 'Seller') {
                    return '-'.$Commission['commission'].'%';
                } else {
                    return $Commission['commission'].'%';
                }
            })
            ->showColumns('created_at', 'firstname', 'approved_date')
            ->addColumn('action', function ($Commission) {
                if (Auth::user()->hasRole('LeadReviewer')) {
                    return '<a href="/commission/'.$Commission['id'].'/approve" title="Approve" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
                            <a href="/commission/'.$Commission['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                } else {
                    return 'No Actions';
                }
            })
            ->searchColumns('name', 'type', 'commission', 'created_at', 'firstname', 'approved_date')
            ->orderColumns('name', 'type', 'commission', 'created_at', 'firstname', 'approved_date')
            ->make();
    }

    public function editContact($id)
    {
        $contact = $this->contact->whereId($id)->first();
        if ($contact->role_id != null) {
            $contact_types = DB::table('roles')->whereContact_type('1')->pluck('description', 'id');

            return view::make('leads.contacts.edit', ['contact' => $contact, 'contact_types' => $contact_types]);
        } else {
            $company_types = DB::table('company_types')->whereActive('1')->pluck('type', 'id');

            return view::make('leads.contacts.company_edit', ['contact' => $contact, 'company_types' => $company_types]);
        }
    }

    public function deleteContact($id)
    {
        $contact = $this->contact->whereId($id)->first();
        $lead_id = $contact->lead_id;
        $contact->delete();

        $lead = $this->lead->whereId($lead_id)->first();

        $this->addNewActual($lead->bdm_allocated, 'DB_Growth', -1);

        return Redirect::route('leads.edit', ['id' => $lead_id]);
    }

    public function apiDeleteContact()
    {
        $contact = $this->contact->whereId(Request::get('contact_id'))->first();
        $contact->delete();

        return 'Done';
    }

    public function addCommission()
    {
        $input = Request::all();

        $typeInput = $input['type'];

        $typeDets = DB::table('commission_types')->whereType($typeInput)->first();

        $input['type'] = $typeDets->id;

        $this->commission->fill($input)->save();

        return 'Done';
    }

    public function approveCommissions($id)
    {
        $commission = $this->commission->whereId($id)->first();
        $commission->approved_date = date('Y-m-d H:i:s');
        $commission->approved_by = Auth::user()->id;
        $lead_id = $commission->lead_id;
        $commission->update();

        return Redirect::route('leads.edit', ['id' => $lead_id]);
    }

    public function deleteCommissions($id)
    {
        $commission = $this->commission->whereId($id)->first();
        $lead_id = $commission->lead_id;
        $commission->delete();

        return Redirect::route('leads.edit', ['id' => $lead_id]);
    }

    //follow up
    public function getUserLeads($user_id)
    {
        $user = User::find($user_id);
        if ($user->hasRole('LeadReviewer')) {
            $leads = $this->lead
                ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
                ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                ->orderBy('created_at')->get();
        } else {
            $leads = $this->lead->whereBdm_allocated($user_id)
                ->orWhere(function ($q) {
                    $q->where('lead_ref_types.type', '=', 'Refer to BDM')
                        ->where('leads.lead_ref', '=', Auth::user()->firstname.' '.Auth::user()->lastname)
                        ->where('leads.deleted_at', '=', null);
                })
                ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
                ->leftJoin('lead_ref_types', 'lead_ref_type', '=', 'lead_ref_types.id')
                ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                ->orderBy('created_at')->get();
        }

        foreach ($leads as $lead) {
            switch ($lead->lead_type_description) {
                case 'Property':
                    $lead->content = $lead->street_number.' '.$lead->street_name;
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }

                    break;
                case 'Person':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('role_id', '<>', '', 'and')
                        ->join('roles', 'role_id', '=', 'roles.id')
                        ->first();
                    if ($contact != null) {
                        $lead->content = $contact->firstname.' '.$contact->lastname;
                    } else {
                        $lead->content = ' ';
                    }
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->name;

                    break;
                case 'Company/Trust':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('company_type', '<>', '', 'and')
                        ->join('company_types', 'company_type', '=', 'company_types.id')
                        ->first();
                    $lead->content = $contact->firstname;
                    if ($lead->content == '') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->type;

                    break;
                default:
                    defaultLabel:
                    $note = $this->prop_notes->join('note_types', 'note_type', '=', 'note_types.id')
                        ->whereOther_id($lead->id)
                        ->where('note_types.type', '=', 'Lead')
                        ->orderBy('prop_notes.id', 'Desc')
                        ->first();
                    $lead->content = $note['note'];
                    if ($lead->content == null) {
                        $lead->content = 'Empty';
                    }

                    break;
            }
        }

        return view('leads.index', ['user' => $user, 'leads' => $leads, 'archived' => '']);
    }

    public function getLeadFollowUp($user_id)
    {
        if ($user_id == 'Archived') {
            $leads = $this->lead
                ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
                ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                ->onlyTrashed()
                ->orderBy('created_at')->get();
        } else {
            $user = User::find($user_id);
            if ($user->hasRole('LeadReviewer')) {
                $leads = $this->lead->orderBy('created_at')->get();
            } else {
                $leads = $this->lead->whereBdm_allocated($user_id)
                    ->orWhere(function ($q) {
                        $q->where('lead_ref_types.type', '=', 'Refer to BDM')
                            ->where('leads.lead_ref', '=', Auth::user()->firstname.' '.Auth::user()->lastname)
                            ->where('leads.deleted_at', '=', null);
                    })
                    ->select('leads.*', 'lead_ref_types.id AS lead_ref_types_id')
                    ->leftJoin('lead_ref_types', 'leads.lead_ref_type', '=', 'lead_ref_types.id')
                    ->orderBy('created_at')->get();
            }
        }
        $leads = $leads->toArray();

        return Datatable::collection(new Collection($leads))
            ->addColumn('bdm', function ($Lead) {
                if ($Lead['bdm_allocated'] == 0) {
                    $firstname = 'None allocated';
                } else {
                    $bdm = User::find($Lead['bdm_allocated']);
                    $firstname = $bdm['firstname'];
                }

                return '<a href="/leads/'.$Lead['bdm_allocated'].'/user" >'.$firstname.'</a>';
            })
            ->addColumn('street_number', function ($Lead) {
                if ($Lead['street_number'] != null) {
                    return $Lead['street_number'];
                } else {
                    return 'NA';
                }
            })
            ->addColumn('street_name', function ($Lead) {
                if ($Lead['street_name'] != null) {
                    return $Lead['street_name'];
                } else {
                    return 'NA';
                }
            })
            ->addColumn('suburb_town_city', function ($Lead) {
                if ($Lead['suburb_town_city'] != null) {
                    return $Lead['suburb_town_city'];
                } else {
                    return 'NA';
                }
            })
            ->addColumn('contact_firstname', function ($Lead) {
                $contact = $this->contact->whereLead_id($Lead['id'])->first();
                if ($contact == null) {
                    return 'No Contacts';
                }

                return $contact->firstname;
            })
            ->addColumn('contact_email', function ($Lead) {
                $contact = $this->contact->whereLead_id($Lead['id'])->first();
                if ($contact == null) {
                    return 'No Contacts';
                }

                return $contact->email;
            })
            ->showColumns('created_at')
            ->addColumn('note', function ($Lead) {
                $query = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  INNER JOIN note_types n ON n.id = p.note_type
                  WHERE p.other_id = '.$Lead['id']."
                  AND n.type = 'Lead'
                  ORDER BY p.created_at DESC
                  LIMIT 1";
                $last_note = DB::select($query);
                if (empty($last_note)) {
                    return 'No Notes';
                }

                return $last_note[0]->note.' <i>...'.$last_note[0]->date.'</i>';
            })
            ->addColumn('actions', function ($Lead) use ($user_id) {
                if ($user_id == 'Archived') {
                    return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/restore" title="Restore" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
                } else {
                    return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                }
            })
            ->searchColumns('bdm', 'street_number', 'street_name', 'suburb_town_city', 'contact_firstname', 'contact_email', 'created_at', 'note')
            ->orderColumns('bdm', 'street_number', 'street_name', 'suburb_town_city', 'contact_firstname', 'contact_email', 'created_at', 'note')
            ->make();
    }

    public function deleteLead($lead_id)
    {
        $lead = $this->lead->whereId($lead_id)->first();

        $lead->delete();

        Flash::message('Lead has been deleted');

        return Redirect::route('user.leads', Auth::user()->id);
    }

    public function getArchivedLeads()
    {
        $user = Auth::user();
        $leads = $this->lead
            ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
            ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
            ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
            ->onlyTrashed()
            ->orderBy('created_at')->get();

        foreach ($leads as $lead) {
            switch ($lead->lead_type_description) {
                case 'Property':
                    $lead->content = $lead->street_number.' '.$lead->street_name;
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }

                    break;
                case 'Person':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('role_id', '<>', '', 'and')
                        ->join('roles', 'role_id', '=', 'roles.id')
                        ->first();
                    if ($contact != null) {
                        $lead->content = $contact->firstname.' '.$contact->lastname;
                    } else {
                        $lead->content = ' ';
                    }
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->name;

                    break;
                case 'Company/Trust':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('company_type', '<>', '', 'and')
                        ->join('company_types', 'company_type', '=', 'company_types.id')
                        ->first();
                    $lead->content = $contact->firstname;
                    if ($lead->content == '') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->type;

                    break;
                default:
                    defaultLabel:
                    $note = $this->prop_notes->join('note_types', 'note_type', '=', 'note_types.id')
                        ->whereOther_id($lead->id)
                        ->where('note_types.type', '=', 'Lead')
                        ->orderBy('prop_notes.id', 'Desc')
                        ->first();
                    $lead->content = $note['note'];
                    if ($lead->content == null) {
                        $lead->content = 'Empty';
                    }

                    break;
            }
        }

        return view('leads.index', ['user' => $user, 'leads' => $leads, 'archived' => 'Archived']);
    }

    public function restoreArchivedLead($lead_id)
    {
        $lead = $this->lead->whereId($lead_id)->withTrashed()->first();
        $lead->deleted_at = null;
        $lead->save();

        return Redirect::route('user.leads', Auth::user()->id);
    }

    public function getAgentReport()
    {
        if (Request::get('start_date')) {
            $date = Request::get('start_date');
        } else {
            $date = date('Y-m-01');
        }

        return view('leads.agents_report', ['date' => $date]);
    }

    public function setToLastTarget($user_id)
    {
        $target = $this->targets->where('user_id', '=', $user_id)
            ->orderBy('id', 'desc')->first();

        if ($target != null) {
            $input['mandate_target'] = $target->mandate_target;
            $input['sales_target'] = $target->sales_target;
            $input['db_target'] = $target->db_target;
            $input['sla_target'] = $target->letting_target;
            $input['letting_target'] = $target->letting_target;
            $input['user_id'] = $user_id;
            $this->targets->fill($input)->save();
        }

        return true;
    }

    public function addNewActual($bdm_id, $type, $value)
    {
        $type_data = DB::table('agent_actuals_types')->whereType($type)->first();

        $input['agent_id'] = $bdm_id;
        $input['value'] = $value;
        $input['type'] = $type_data->id;

        $this->actual->fill($input)->save();

        return true;
    }

    public function getAgentTable($date)
    {
        $users = User::select('users.id', 'firstname', 'lastname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('roles.name', '=', 'Agent')
            ->get();

        $end_date = date('Y-m-d', strtotime('+1 month', strtotime($date)));

        foreach ($users as $user) {
            $targets = $this->targets->where('user_id', '=', $user->id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $date)
                ->orderBy('id', 'desc')->first();
            if ($targets == null) {
                $this->setToLastTarget($user->id);
                $targets = $this->targets->where('user_id', '=', $user->id)
                    ->where('created_at', '<=', $end_date)
                    ->where('created_at', '>=', $date)
                    ->orderBy('id', 'desc')->first();
            }
            if ($targets == null) {
                $targets = new Targets();
            }

            $user->mandates = $targets['mandate_target'];
            $user->sales = $targets['sales_target'];
            $user->db = $targets['db_target'];
            $user->sla = $targets['sla_target'];
            $user->letting = $targets['letting_target'];

            if ($user->mandates == null) {
                $user->mandates = 0;
            }
            if ($user->sales == null) {
                $user->sales = 0;
            }
            if ($user->db == null) {
                $user->db = 0;
            }
            if ($user->sla == null) {
                $user->sla = 0;
            }
            if ($user->letting == null) {
                $user->letting = 0;
            }

            $actual_data = $this->actual
                ->select(DB::raw('count(\'agent_actuals.*\') AS actCount'), 'agent_actuals_types.type')
                ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
                ->whereAgent_id($user->id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $date)
                ->groupBy('agent_actuals.type')
                ->get();

            $user->mandates_act = 0;
            $user->sales_act = 0;
            $user->sla_act = 0;
            $user->db_act = 0;
            $user->letting_act = 0;

            foreach ($actual_data as $actual) {
                switch ($actual->type) {
                    case 'Mandate':
                        $user->mandates_act = $actual->actCount;

                        break;
                    case 'Sales':
                        $user->sales_act = $actual->actCount;

                        break;
                    case 'SLA':
                        $user->sla_act = $actual->actCount;

                        break;
                    case 'DB_Growth':
                        $user->db_act = $actual->actCount;

                        break;
                    case 'Letting':
                        $user->letting_act = $actual->actCount;

                        break;
                }
            }
        }

        $users = $users->toArray();

        return Datatable::collection(new Collection($users))
            ->addColumn('bdm', function ($User) use ($date) {
                return '<a href="/leads/'.$date.'/agent/'.$User['id'].'" >'.$User['firstname'].'</a>';
            })
            ->showcolumns('lastname', 'mandates', 'mandates_act', 'sales', 'sales_act', 'db', 'db_act', 'sla', 'sla_act', 'letting', 'letting_act')
            ->addColumn('action', function ($User) {
                return '<a href="/leads/agent/'.$User['id'].'/cumulative" title="Cumulative report" ><i class="i-circled i-light i-alt i-small icon-files"></i></a>';
            })
            ->searchColumns('bdm', 'lastname', 'mandates', 'mandates_act', 'sales', 'sales_act', 'db', 'db_act', 'sla', 'sla_act', 'letting', 'letting_act')
            ->orderColumns('bdm', 'lastname', 'mandates', 'mandates_act', 'sales', 'sales_act', 'db', 'db_act', 'sla', 'sla_act', 'letting', 'letting_act')
            ->make();
    }

    public function getAgentCharts($date, $user_id)
    {
        $end_date = date('Y-m-d', strtotime('+1 month', strtotime($date)));

        $targets = $this->targets->where('user_id', '=', $user_id)
            ->where('created_at', '<=', $end_date)
            ->where('created_at', '>=', $date)
            ->orderBy('id', 'desc')->first();

        if ($targets == null) {
            $targets = new Targets();
        }

        $actual_data = $this->actual
            ->select(DB::raw('sum(value) AS actCount'), 'agent_actuals_types.type')
            ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
            ->whereAgent_id($user_id)
            ->where('created_at', '<=', $end_date)
            ->where('created_at', '>=', $date)
            ->groupBy('agent_actuals.type')
            ->get();

        $targets->mandate_actual = 0;
        $targets->sales_actual = 0;
        $targets->sla_actual = 0;
        $targets->db_growth_actual = 0;
        $targets->letting_actual = 0;

        foreach ($actual_data as $actual) {
            switch ($actual->type) {
                case 'Mandate':
                    $targets->mandate_actual = $actual->actCount;

                    break;
                case 'Sales':
                    $targets->sales_actual = $actual->actCount;

                    break;
                case 'SLA':
                    $targets->sla_actual = $actual->actCount;

                    break;
                case 'DB_Growth':
                    $targets->db_growth_actual = $actual->actCount;

                    break;
                case 'Letting':
                    $targets->letting_actual = $actual->actCount;

                    break;
            }
        }

        $user = User::find($user_id);

        $weeklyMandate = $this->getWeeklyTotals($user_id, $date, 'Mandate');

        return view('leads.agent_charts', ['date' => $date, 'user' => $user, 'targets' => $targets,
            'weeklyMandate' => $weeklyMandate, ]);
    }

    public function getWeeklyTotals($user_id, $date, $type)
    {
        $originalDate = $date;

        $count = 1;
        while ($count <= 4) {
            if ($count == 4) {
                $end_date = date('Y-m-d', strtotime('+1 month', strtotime($originalDate)));
            } else {
                $end_date = date('Y-m-d', strtotime('+1 week', strtotime($date)));
            }
            $actual_data = $this->actual
                ->select(DB::raw('sum(value) AS actCount'))
                ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
                ->whereAgent_id($user_id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $date)
                ->where('agent_actuals_types.type', '=', $type)
                ->get();

            $date = $end_date;
            if ($actual_data[0]->actCount == null) {
                $data[$count] = 0;
            } else {
                $data[$count] = $actual_data[0]->actCount;
            }

            $count++;
        }

        return $data;
    }

    public function saveAgentTargets($user_id)
    {
        $input = Request::except('mandate_actual', 'sale_actual', 'db_actual', 'sla_actual', 'letting_actual');
        $input['user_id'] = $user_id;
        $this->targets->fill($input)->save();

        $date = date('Y-m-01');

        return Redirect::route('api.agent.charts', ['date' => $date, 'user_id' => $user_id]);
    }

    public function getTargetsOverPeriod($start_date, $end_date, $agent_id)
    {
        $start = (new DateTime($start_date))->modify('first day of this month');
        $end = (new DateTime($end_date))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $targets = new Collection();

        $count = 0;
        foreach ($period as $dt) {
            $end_date = date('Y-m-d', strtotime('+1 month', strtotime($dt->format('Y-m'))));
            $thisTarget = $this->targets->where('user_id', '=', $agent_id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $dt->format('Y-m'))
                ->orderBy('id', 'desc')->first();
            if ($thisTarget == null) {
            } else {
                $actual_data = $this->actual
                    ->select(DB::raw('sum(value) AS actCount'), 'agent_actuals_types.type')
                    ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
                    ->whereAgent_id($agent_id)
                    ->where('created_at', '<=', $end_date)
                    ->where('created_at', '>=', $dt->format('Y-m'))
                    ->groupBy('agent_actuals.type')
                    ->get();

                $thisTarget->mandate_actual = 0;
                $thisTarget->sales_actual = 0;
                $thisTarget->sla_actual = 0;
                $thisTarget->db_growth_actual = 0;
                $thisTarget->letting_actual = 0;

                foreach ($actual_data as $actual) {
                    switch ($actual->type) {
                        case 'Mandate':
                            $thisTarget->mandate_actual = $actual->actCount;

                            break;
                        case 'Sales':
                            $thisTarget->sales_actual = $actual->actCount;

                            break;
                        case 'SLA':
                            $thisTarget->sla_actual = $actual->actCount;

                            break;
                        case 'DB_Growth':
                            $thisTarget->db_growth_actual = $actual->actCount;

                            break;
                        case 'Letting':
                            $thisTarget->letting_actual = $actual->actCount;

                            break;
                    }
                }

                $thisTarget = $thisTarget->toArray();
                $targets[$count] = $thisTarget;
            }
            $count++;
        }

        return $targets;
    }

    public function getCumulativeReport($user_id)
    {
        if (Request::get('start_date')) {
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
            $user_id = Request::get('agents');
        } else {
            $target = $this->targets->whereUser_id($user_id)->orderBy('id', 'asc')->first();
            $start_date = date('Y-m-d', strtotime($target['created_at']));

            if ($start_date == '1970-01-01') {
                $start_date = date('Y-m-01');
            }
            $end_date = date('Y-m-d');
        }

        $checkTargets = $this->getTargetsOverPeriod($start_date, $end_date, $user_id);

        $total['Mandates_target'] = 0;
        $total['Sales_target'] = 0;
        $total['SLA_target'] = 0;
        $total['DB_target'] = 0;
        $total['Letting_target'] = 0;
        $total['Mandates_actual'] = 0;
        $total['Sales_actual'] = 0;
        $total['SLA_actual'] = 0;
        $total['DB_actual'] = 0;
        $total['Letting_actual'] = 0;

        foreach ($checkTargets as $dbData) {
            $total['Mandates_target'] += $dbData['mandate_target'];
            $total['Sales_target'] += $dbData['sales_target'];
            $total['SLA_target'] += $dbData['sla_target'];
            $total['DB_target'] += $dbData['db_target'];
            $total['Letting_target'] += $dbData['letting_target'];
            $total['Mandates_actual'] += $dbData['mandate_actual'];
            $total['Sales_actual'] += $dbData['sales_actual'];
            $total['SLA_actual'] += $dbData['sla_actual'];
            $total['DB_actual'] += $dbData['db_growth_actual'];
            $total['Letting_actual'] += $dbData['letting_actual'];
        }

        $this_month = date('Y-m-1');
        $end_month = date('Y-m-d', strtotime('+1 month', strtotime($this_month)));
        $targets = $this->targets->where('user_id', '=', $user_id)
            ->where('created_at', '<=', $end_month)
            ->where('created_at', '>=', $this_month)
            ->orderBy('id', 'desc')->first();

        if ($targets == null) {
            $targets = new Targets();
        }

        $actual_data = $this->actual
            ->select(DB::raw('sum(value) AS actCount'), 'agent_actuals_types.type')
            ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
            ->whereAgent_id($user_id)
            ->where('created_at', '<=', $end_month)
            ->where('created_at', '>=', $this_month)
            ->groupBy('agent_actuals.type')
            ->get();

        $targets->mandate_actual = 0;
        $targets->sales_actual = 0;
        $targets->sla_actual = 0;
        $targets->db_growth_actual = 0;
        $targets->letting_actual = 0;

        foreach ($actual_data as $actual) {
            switch ($actual->type) {
                case 'Mandate':
                    $targets->mandate_actual = $actual->actCount;

                    break;
                case 'Sales':
                    $targets->sales_actual = $actual->actCount;

                    break;
                case 'SLA':
                    $targets->sla_actual = $actual->actCount;

                    break;
                case 'DB_Growth':
                    $targets->db_growth_actual = $actual->actCount;

                    break;
                case 'Letting':
                    $targets->letting_actual = $actual->actCount;

                    break;
            }
        }

        $user = User::find($user_id);

        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();

        return view('leads.cumulative_report', ['start_date' => $start_date, 'end_date' => $end_date, 'user' => $user,
            'targets' => $targets, 'agents' => $agents, 'total' => $total, ]);
    }

    public function getCumulativeTable($agent_id, $start_date, $end_date)
    {
        $targets = $this->getTargetsOverPeriod($start_date, $end_date, $agent_id);

        $targets->toArray();

        return Datatable::collection($targets)
            ->addColumn('year_month', function ($Target) {
                return date('Y / m', strtotime($Target['created_at']));
            })
            ->showColumns('mandate_target', 'mandate_actual', 'sales_target', 'sales_actual', 'db_target', 'db_growth_actual', 'sla_target',
                'sla_actual', 'letting_target', 'letting_actual')
            ->searchColumns('year_month', 'mandate_target', 'mandate_actual', 'sales_target', 'sales_actual', 'db_target', 'db_growth_actual', 'sla_target',
                'sla_actual', 'letting_target', 'letting_actual')
            ->orderColumns('year_month', 'mandate_target', 'mandate_actual', 'sales_target', 'sales_actual', 'db_target', 'db_growth_actual', 'sla_target',
                'sla_actual', 'letting_target', 'letting_actual')
            ->make();
    }

    public function convertLead()
    {
        $lead_id = Request::get('lead_id');
        $lead = $this->lead->whereId($lead_id)->first();

        $returnArray = AppHelper::checkMultipleWhichEmpty($lead->lead_prop_trans_type, $lead->street_number, $lead->street_name,
            $lead->province, $lead->area, $lead->suburb_town_city, $lead->property_type, $lead->x_coord);

        if (empty($returnArray)) {
            if ($lead->lead_prop_trans_type == 4 || $lead->lead_prop_trans_type == 5) {
                return 'Incorrect Listing Type';
            }
            //create property return property id
            $lead_prop_trans_type = DB::table('lead_prop_trans_types')->whereId($lead->lead_prop_trans_type)->first();
            $propertyInput['listing_type'] = $lead_prop_trans_type->listing_type;
            $propertyInput['street_number'] = $lead->street_number;
            $propertyInput['street_address'] = $lead->street_name;
            $propertyInput['complex_number'] = $lead->complex_number;
            $propertyInput['complex_name'] = $lead->complex_name;
            $propertyInput['country'] = $lead->country;
            $propertyInput['province'] = $lead->province;
            $propertyInput['area'] = $lead->area;
            $propertyInput['suburb_town_city'] = $lead->suburb_town_city;
            $propertyInput['property_type'] = $lead->property_type;
            $propertyInput['x_coord'] = $lead->x_coord;
            $propertyInput['y_coord'] = $lead->y_coord;
            $propertyInput['property_type'] = ucfirst($lead->property_type);
            $propertyInput['user_id'] = $lead->bdm_allocated;
            //check owner

            $owner = DB::table('lead_contacts')
                ->leftJoin('roles', 'role_id', '=', 'roles.id')
                ->where('lead_id', '=', $lead_id)
                ->where('roles.name', '=', 'Owner')
                ->where('email', '<>', '')
                ->first();

            if (! empty($owner->email)) {
                if (! ($user = $this->user->whereEmail($owner->email)->first())) {
                    $this->user->firstname = $owner->firstname;
                    $this->user->lastname = $owner->lastname;
                    $this->user->email = $owner->email;
                    $this->user->phonenumber = $owner->tel;
                    $this->user->save();
                    User::find($this->user->id)->assignRole(4);
                    $propertyInput['owner_id'] = $this->user->id;
                } else {
                    User::find($user->id)->assignRole(4);
                    $propertyInput['owner_id'] = $user->id;
                }
            }

            $reference = App::make('PropertyController')->newReference($propertyInput['listing_type']);
            $propertyInput['reference'] = $reference;
            $this->property->fill($propertyInput)->save();

            //create property_type join

            switch ($propertyInput['property_type']) {
                case 'Residential':
                    $this->residential->property_id = $this->property->id;
                    $this->residential->save();

                    break;
                case 'Commercial':
                    $this->commercial->property_id = $this->property->id;
                    $this->commercial->save();

                    break;
                case 'Farm':
                    $this->farm->property_id = $this->property->id;
                    $this->farm->save();

                    break;
            }

            //create vetting join
            $this->vetting->property_id = $this->property->id;

            //check company
            $company = DB::table('lead_contacts')->join('company_types', 'company_type', '=', 'company_types.id')
                ->where('lead_id', '=', $lead_id)->where('company_type', '<>', '')->first();

            if (! empty($company->email)) {
                if (! ($user = $this->user->whereEmail($company->email)->first())) {
                    $this->user->firstname = $company->firstname;
                    $this->user->email = $company->email;
                    $this->user->phonenumber = $company->tel;
                    $this->user->save();
                } else {
                    User::find($user->id)->assignRole(4);
                    $propertyInput['owner_id'] = $user->id;
                }
            }

            $this->vetting->rep_id_number = $lead->bdm_allocated;
            $this->vetting->vetting_status = 1;
            $this->vetting->save();
            //update contact
            DB::table('lead_contacts')
                ->where('lead_id', '=', $lead_id)
                ->update(['property_id' => $this->property->id]);

            //check for property notes and change
            $note_type = DB::table('note_types')->whereType('Property')->first();
            DB::table('prop_notes')
                ->join('note_types', 'note_type', '=', 'note_types.id')
                ->where('other_id', '=', $lead_id)
                ->where('type', '=', 'Lead')
                ->update(['note_type' => $note_type->id, 'other_id' => $this->property->id]);

            $this->prop_notes->note = 'Lead Converted to Property';
            $this->prop_notes->other_id = $this->property->id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->note_type = $note_type->id;
            $this->prop_notes->save();

            //check and change images
            DB::table('uploads')
                ->where('class', '=', 'Lead')
                ->where('link', '=', $lead_id)
                ->update(['class' => 'Property', 'link' => $this->property->id]);

            if (! empty(DB::table('uploads')
                ->where('class', '=', 'Property')
                ->where('link', '=', $this->property->id)->first())
            ) {
                //change image folder name
                rename('../public/uploads/users/'.$lead->bdm_allocated.'/lead_'.$lead_id,
                    '../public/uploads/users/'.$lead->bdm_allocated.'/'.$this->property->id);
            }

            $lead->delete();

            $this->addNewActual($lead->bdm_allocated, 'DB_Growth', 1);
            $ref_type = DB::table('lead_ref_types')->where('type', '=', 'SLA')->first();
            if ($lead->lead_ref_type == $ref_type->id) {
                $this->addNewActual($lead->bdm_allocated, 'SLA', 1);
            }
            if ($lead_prop_trans_type->listing_type == 'in2rental') {
                $this->addNewActual($lead->bdm_allocated, 'Letting', 1);
            }

            return 'Done'.$this->property->id;
        } else {
            return $returnArray;
        }
    }

    public function propertySubmitPackage()
    {
        $input['lead_type'] = 5;
        $input['lead_ref_type'] = 5;
        $input['lead_ref'] = Request::get('lead_ref');
        $input['lead_prop_trans_type'] = 5;
        $input['bdm_allocated'] = 0;
        $input['property_type'] = Request::get('property_type');
        $input['title_desc'] = Request::get('title_desc');
        $input['title_type'] = Request::get('title_type');
        $input['street_number'] = Request::get('street_number');
        $input['street_name'] = Request::get('street_name');

        $prov_area_sub_array = explode(',', Request::get('suburb'));

        $input['country'] = 'South Africa';
        if (trim($prov_area_sub_array[0]) == '') {
            $input['province'] = '';
            $input['area'] = '';
            $input['suburb_town_city'] = '';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (trim($prov_area_sub_array[1]) == '') {
                $input['area'] = '';
                $input['suburb_town_city'] = '';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (trim($prov_area_sub_array[2]) == '') {
                    $input['suburb_town_city'] = '';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        $this->lead->fill($input)->save();

        $note_type = DB::table('note_types')->whereType('Lead')->first();
        $this->prop_notes->note = Request::get('property_description');
        $this->prop_notes->other_id = $this->lead->id;
        if (Auth::check()) {
            $this->prop_notes->users_id = Auth::user()->id;
        } else {
            $this->prop_notes->users_id = 1;
        }
        $this->prop_notes->note_type = $note_type->id;
        $this->prop_notes->save();

        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = 4;
        $inputContact['firstname'] = Request::get('owner_firstname');
        $inputContact['lastname'] = Request::get('owner_surname');
        $inputContact['email'] = Request::get('owner_email');
        $inputContact['tel'] = Request::get('owner_cell');

        $this->contact->fill($inputContact)->save();

        $subject = 'User has submitted a Property';
        $name = 'Dear Administrator,<br>';
        $emailData = '<h4>Someone is interested in a package deal.</h4><p>'.$inputContact['firstname'].' '.$inputContact['lastname'].'
            has submitted details of their property for our opinion. Please check the lead table for details.</p>';
        $data = ['name' => $name, 'subject' => $subject, 'messages' => $emailData];
        // Queue email
        Mail::queue('emails.wesell.property_live', $data, function ($message) use ($userMail, $name, $subject) {
            $message->to('info@in2assets.com', $name)
                ->cc('rmoodley@in2assets.com', 'Rowena Moodley')
                ->cc('neeraj@in2assets.com', 'Neeraj Ramautar')
                ->subject($subject);
        });

        Flash::message('Your details have been saved and we will contact you shortly');

        return Redirect::back();
    }

    public function propertySubmit()
    {
        $input['lead_type'] = 4;
        $input['lead_ref_type'] = 5;
        $input['lead_ref'] = Request::get('lead_ref');
        $input['lead_prop_trans_type'] = 5;
        $input['bdm_allocated'] = 0;
        $input['property_type'] = Request::get('property_type');
        $input['title_desc'] = Request::get('title_desc');
        $input['title_type'] = Request::get('title_type');
        $input['street_number'] = Request::get('street_number');
        $input['street_name'] = Request::get('street_name');

        $prov_area_sub_array = explode(',', Request::get('suburb'));

        $input['country'] = 'South Africa';
        if (trim($prov_area_sub_array[0]) == '') {
            $input['province'] = '';
            $input['area'] = '';
            $input['suburb_town_city'] = '';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (trim($prov_area_sub_array[1]) == '') {
                $input['area'] = '';
                $input['suburb_town_city'] = '';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (trim($prov_area_sub_array[2]) == '') {
                    $input['suburb_town_city'] = '';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        $this->lead->fill($input)->save();

        $note_type = DB::table('note_types')->whereType('Lead')->first();
        $this->prop_notes->note = Request::get('property_description');
        $this->prop_notes->other_id = $this->lead->id;
        if (Auth::check()) {
            $this->prop_notes->users_id = Auth::user()->id;
        } else {
            $this->prop_notes->users_id = 1;
        }
        $this->prop_notes->note_type = $note_type->id;
        $this->prop_notes->save();

        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = 4;
        $inputContact['firstname'] = Request::get('owner_firstname');
        $inputContact['lastname'] = Request::get('owner_surname');
        $inputContact['email'] = Request::get('owner_email');
        $inputContact['tel'] = Request::get('owner_cell');
        $userMail = Request::get('owner_email');
        $this->contact->fill($inputContact)->save();

        $subject = 'User has submitted a Property';
        $name = 'Dear Administrator,<br>';
        $emailData = '<p>'.$inputContact['firstname'].' '.$inputContact['lastname'].'
            has submitted details of their property for our opinion. Please check the lead table for details.</p>';
        $data = ['name' => $name, 'subject' => $subject, 'messages' => $emailData];
        // Queue email
        Mail::queue('emails.registration.adminnotice', $data, function ($message) use ($userMail, $name, $subject) {
            $message->to('info@in2assets.com', $name)
                    ->cc('rmoodley@in2assets.com', 'Rowena Moodley')
                    ->cc('neeraj@in2assets.com', 'Neeraj Ramautar')
                    ->subject($subject);
        });

        Flash::message('Your details have been saved and we will contact you shortly');

        return Redirect::back();
    }

    public function networkSubmit()
    {
        $input['lead_ref'] = Request::get('lead_ref');
        $input['property_description'] = Request::get('property_description');

        $this->lead->fill($input)->save();

        $note_type = DB::table('note_types')->whereType('Lead')->first();
        $this->prop_notes->note = Request::get('property_description');
        $this->prop_notes->other_id = $this->lead->id;
        if (Auth::check()) {
            $this->prop_notes->users_id = Auth::user()->id;
        } else {
            $this->prop_notes->users_id = 1;
        }
        $this->prop_notes->note_type = $note_type->id;
        $this->prop_notes->save();

        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = 1;
        $inputContact['firstname'] = Request::get('owner_firstname');
        $inputContact['lastname'] = Request::get('owner_surname');
        $inputContact['email'] = Request::get('owner_email');
        $inputContact['tel'] = Request::get('owner_cell');

        $this->contact->fill($inputContact)->save();

        $userMails['email'] = 'rstenzhorn@in2assets.com';
        $userMails['email'] = 'tmunsamy@in2assets.com';

        foreach ($userMails as $userMail) {
            $subject = 'User has submitted a Property';
            $name = 'Dear Administrator,<br>';
            $emailData = '<p>'.$inputContact['firstname'].' '.$inputContact['lastname'].'
            has submitted details of their company and are interested in networking with us.</p>';
            $data = ['name' => $name, 'subject' => $subject, 'messages' => $emailData];
            // Queue email
            Mail::queue('emails.wesell.property_live', $data, function ($message) use ($userMail, $name, $subject) {
                $message->to($userMail, $name)
                    ->subject($subject);
            });
        }

        Flash::message('Your details have been saved and we will contact you shortly');

        return Redirect::back();
    }
}
