<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class DeedOfSaleController extends Controller
{
    protected $deed_of_sale;

    public function __construct(DeedOfSale $deed_of_sale, Property $property, DeedAmendment $deed_amendment)
    {
        $this->deed_of_sale = $deed_of_sale;
        $this->deed_amendment = $deed_amendment;
        $this->property = $property;
    }

    //show deed of sale original copy
    public function show($id, $buyer_id)
    {
        $buyer = DB::table('users')->whereId($buyer_id)->first();
        $property = $this->property->whereId($id)->with('commercial')->with('residential')->with('farm')->with('vetting')->first();
        $owner = DB::table('users')->whereId($property->user_id)->first();
        $deed = $this->deed_of_sale->whereProperty_id($id)->whereOwner_id($property->user_id)->whereBuyer_id($buyer_id)->with('amendments')->first();
        $occupational_interest_in_words = App::make('LeaseAgreementsController')->convert_number_to_words($deed->amendments->occupational_interest);
        $subject_to_bond_in_words = App::make('LeaseAgreementsController')->convert_number_to_words($deed->amendments->subject_to_bond);
        $payment_period_in_words = App::make('LeaseAgreementsController')->convert_number_to_words($deed->amendments->payment_period);
        $interest_in_words = App::make('LeaseAgreementsController')->convert_number_to_words($deed->amendments->interest);
        $commission_in_words = App::make('LeaseAgreementsController')->convert_number_to_words($deed->amendments->commission);

        return view::make('deed_of_sale.show', ['id' => $id, 'buyer_id' => $buyer_id, 'property' => $property, 'buyer' => $buyer, 'owner' => $owner, 'deed' => $deed,
            'occupational_interest_in_words' => $occupational_interest_in_words, 'subject_to_bond_in_words' => $subject_to_bond_in_words, 'payment_period_in_words' => $payment_period_in_words, 'interest_in_words' => $interest_in_words,
            'commission_in_words' => $commission_in_words, ]);
    }

    //user edit for deed of sale
    public function edit($id, $buyer_id)
    {
        $property = $this->property->whereId($id)->with('commercial')->with('residential')->with('farm')->with('vetting')->first();

        $deed = $this->deed_of_sale->whereProperty_id($id)->whereOwner_id($property->user_id)->whereBuyer_id($buyer_id)->with('amendments')->first();

        return view::make('deed_of_sale.edit', ['deed' => $deed, 'property' => $property]);
    }

    //user update for deed of sale
    public function update($id, $buyer_id)
    {
        $input = Request::all();

        if ($input['deposit_payment_date'] != '') {
            $deposit_payment_date = DateTime::createFromFormat('d/m/Y', $input['deposit_payment_date']);
            $newDeposit_payment_date = $deposit_payment_date->format('Y-m-d');
            $input['deposit_payment_date'] = $newDeposit_payment_date;
        }

        if ($input['guarantee_period_date'] != '') {
            $guarantee_period_date = DateTime::createFromFormat('d/m/Y', $input['guarantee_period_date']);
            $newGuarantee_period_date = $guarantee_period_date->format('Y-m-d');
            $input['guarantee_period_date'] = $newGuarantee_period_date;
        }

        if ($input['confirmation_date'] != '') {
            $confirmation_date = DateTime::createFromFormat('d/m/Y', $input['confirmation_date']);
            $newConfirmation_date = $confirmation_date->format('Y-m-d');
            $input['confirmation_date'] = $newConfirmation_date;
        }

        $property = $this->property->whereId($id)->with('commercial')->with('residential')->with('farm')->with('vetting')->first();
        $deed = $this->deed_of_sale->whereProperty_id($id)->whereOwner_id($property->user_id)->whereBuyer_id($buyer_id)->first();
        $deedId = $deed->id;
        $this->deed_amendment = $this->deed_amendment->whereDeed_of_sale_id($deedId)->first();
        $this->deed_amendment->fill($input)->save();

        return Redirect::route('deed_of_sale.show', ['id' => $id, 'buyer_id' => $buyer_id]);
    }

    //creating a new deed of sale for user
    public function newDeed($id, $buyer_id)
    {
        $property = $this->property->whereId($id)->with('commercial')->with('residential')->with('farm')->with('vetting')->first();
        $result = $this->deed_of_sale->whereProperty_id($id)->whereOwner_id($property->user_id)->whereBuyer_id($buyer_id)->with('amendments')->first();

        if (! $result) {
            $datetime = date('Y-m-d H:i:s');
            $deedId = DB::table('deed_of_sale')->insertGetId(['owner_id' => $property->user_id, 'buyer_id' => $buyer_id, 'property_id' => $id, 'deed_status' => 'buyer', 'created_at' => $datetime, 'updated_at' => $datetime]);

            $deed = $this->deed_of_sale->whereProperty_id($id)->whereOwner_id($property->user_id)->whereBuyer_id('0')->with('amendments')->first();

            unset($deed->amendments['id']);
            unset($deed->amendments['deed_of_sale_id']);
            unset($deed->amendments['deleted_at']);
            unset($deed->amendments['created_at']);
            unset($deed->amendments['updated_at']);

            $this->deed_amendment->fill($deed->amendments->toArray());
            $this->deed_amendment->deed_of_sale_id = $deedId;
            $this->deed_amendment->save();

            return view::make('makecontact.show', ['id' => $id]);
        } else {
            return view::make('makecontact.show', ['id' => $id]);
        }
    }

    //approve deed of sale *Admin*
    public function approve($id, $buyer_id, $status)
    {
        $property = $this->property->whereId($id)->with('commercial')->with('residential')->with('farm')->with('vetting')->first();

        $deed = $this->deed_of_sale->whereProperty_id($id)->whereOwner_id($property->user_id)->whereBuyer_id($buyer_id)->first();
        $deed->deed_status = $status;
        $deed->save();
        if (Auth::user()->hasRole('Admin')) {
            return view::make('deed_of_sale.admin.index');
        }

        return view::make('makecontact.show', ['id' => $id]);
    }

    //admin view of list of deed of sale
    public function adminDeed()
    {
        return view::make('deed_of_sale.admin.index');
    }

    // builds admin lease agreement table

    public function getAdminDeed()
    {
        $deed_of_sale = $this->deed_of_sale
            ->join('users as owner', 'owner.id', '=', 'deed_of_sale.owner_id')
            ->join('users as buyer', 'buyer.id', '=', 'deed_of_sale.buyer_id')
            ->join('property', 'property.id', '=', 'deed_of_sale.property_id')
            ->select('deed_of_sale.updated_at', 'owner.id as oId', 'owner.title as oTitle', 'owner.firstname as oFirst', 'owner.lastname as Last', 'buyer.id as bId',
                'buyer.title as bTitle', 'buyer.firstname as bFirst', 'buyer.lastname as bLast', 'property.street_address', 'property.id as property_id', 'deed_of_sale.deed_status')
            ->where('deed_of_sale.deed_status', '=', 'in2assets')
            ->orWhere('deed_of_sale.deed_status', '=', 'approved')
            ->get();

        return Datatable::collection($deed_of_sale)
            ->showColumns('updated_at', 'street_address')
            ->addColumn('owner', function ($Lease) {
                return $Lease->oTitle.' '.$Lease->oFirst.' '.$Lease->Last;
            })
            ->addColumn('buyer', function ($Lease) {
                return $Lease->bTitle.' '.$Lease->bFirst.' '.$Lease->bLast;
            })
            ->showColumns('deed_status')
            ->addColumn('action', function ($Lease) {
                if ($Lease->deed_status === 'in2assets') {
                    return '<a href="/deed_of_sale/'.$Lease->property_id.'/'.$Lease->bId.'" title="Edit Lease" ><i class="i-circled i-light i-small icon-edit"></i></a>&nbsp
            				<a href="/property/'.$Lease->oId.'/message" title="Message Owner" ><i class="i-circled i-light i-small icon-comments"></i></a>';
                }
                if ($Lease->deed_status === 'approved') {
                    return '<a href="/property/'.$Lease->oId.'/message" title="Message Owner" ><i class="i-circled i-light i-small icon-comments"></i></a>';
                }
            })
            ->searchColumns('street_address', 'owner', 'buyer')
            ->orderColumns('street_address', 'owner', 'buyer')
            ->make();
    }
}
