<?php

namespace App\Http\Controllers\Tivvit\Tivvit\Response;

/**
 * File for class TivvitStructTivvitResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructTivvitResponse originally named TivvitResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructTivvitResponse extends TivvitWsdlClass
{
    /**
     * The method
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $method;

    /**
     * The response
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $response;

    /**
     * Constructor method for TivvitResponse.
     * @see parent::__construct()
     * @param string $_method
     * @param string $_response
     * @return TivvitStructTivvitResponse
     */
    public function __construct($_method = null, $_response = null)
    {
        parent::__construct(['method' => $_method, 'response' => $_response], false);
    }

    /**
     * Get method value.
     * @return string|null
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set method value.
     * @param string $_method the method
     * @return string
     */
    public function setMethod($_method)
    {
        return $this->method = $_method;
    }

    /**
     * Get response value.
     * @return string|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set response value.
     * @param string $_response the response
     * @return string
     */
    public function setResponse($_response)
    {
        return $this->response = $_response;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructTivvitResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
