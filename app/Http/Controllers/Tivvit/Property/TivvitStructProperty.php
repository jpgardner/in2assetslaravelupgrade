<?php

namespace App\Http\Controllers\Tivvit\Property;

use App\Http\Controllers\Tivvit\Htm\TivvitEnumHtm;
use App\Http\Controllers\Tivvit\Ltype\TivvitEnumLtype;
use App\Http\Controllers\Tivvit\Ptype\TivvitEnumPtype;
use App\Http\Controllers\Tivvit\Rentper\TivvitEnumRentper;

/**
 * File for class TivvitStructProperty.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructProperty originally named Property
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructProperty extends TivvitWsdlClass
{
    /**
     * The rental
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $rental;

    /**
     * The rentper
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumRentper
     */
    public $rentper;

    /**
     * The levy
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $levy;

    /**
     * The price
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $price;

    /**
     * The listingp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $listingp;

    /**
     * The soldp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $soldp;

    /**
     * The size
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $size;

    /**
     * The lotsize
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $lotsize;

    /**
     * The bedrooms
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bedrooms;

    /**
     * The bathrooms
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bathrooms;

    /**
     * The garages
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $garages;

    /**
     * The age
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $age;

    /**
     * The pool
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $pool;

    /**
     * The view_
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $view_;

    /**
     * The study
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $study;

    /**
     * The carport
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $carport;

    /**
     * The servant
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $servant;

    /**
     * The laundry
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $laundry;

    /**
     * The security
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $security;

    /**
     * The parking
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $parking;

    /**
     * The reception
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $reception;

    /**
     * The pets
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $pets;

    /**
     * The paving
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $paving;

    /**
     * The living
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $living;

    /**
     * The eatarea
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $eatarea;

    /**
     * The statusd
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $statusd;

    /**
     * The listingd
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $listingd;

    /**
     * The expiringd
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $expiringd;

    /**
     * The priced
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $priced;

    /**
     * The ltype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumLtype
     */
    public $ltype;

    /**
     * The ptype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumPtype
     */
    public $ptype;

    /**
     * The soldd
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $soldd;

    /**
     * The special
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $special;

    /**
     * The pantry
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $pantry;

    /**
     * The hall
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $hall;

    /**
     * The sprinkler
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $sprinkler;

    /**
     * The tvroom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $tvroom;

    /**
     * The guestt
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $guestt;

    /**
     * The pgarden
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $pgarden;

    /**
     * The patio
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $patio;

    /**
     * The alarm
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $alarm;

    /**
     * The heating
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $heating;

    /**
     * The tennis
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $tennis;

    /**
     * The incwater
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $incwater;

    /**
     * The gservice
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $gservice;

    /**
     * The pstaff
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $pstaff;

    /**
     * The cservice
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $cservice;

    /**
     * The htm
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumHtm
     */
    public $htm;

    /**
     * The intercom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $intercom;

    /**
     * The gate
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $gate;

    /**
     * The slights
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $slights;

    /**
     * The sfence
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $sfence;

    /**
     * The recroom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $recroom;

    /**
     * The shared
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $shared;

    /**
     * The sole
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $sole;

    /**
     * The mandstart
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $mandstart;

    /**
     * The mandend
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $mandend;

    /**
     * The brooms
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $brooms;

    /**
     * The bphase
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $bphase;

    /**
     * The bsign
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $bsign;

    /**
     * The bperiod
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $bperiod;

    /**
     * The bfloor
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $bfloor;

    /**
     * The bgrental
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bgrental;

    /**
     * The bnrental
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bnrental;

    /**
     * The bopcost
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bopcost;

    /**
     * The brentesc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $brentesc;

    /**
     * The bopcostesc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bopcostesc;

    /**
     * The boccdate
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $boccdate;

    /**
     * The bfitallow
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bfitallow;

    /**
     * The byard
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $byard;

    /**
     * The bupper
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $bupper;

    /**
     * The munval
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $munval;

    /**
     * The bondbal
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $bondbal;

    /**
     * The ratestax
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $ratestax;

    /**
     * The flat
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $flat;

    /**
     * The flatkitc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $flatkitc;

    /**
     * The fbedroom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $fbedroom;

    /**
     * The fbathroom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $fbathroom;

    /**
     * The flatloun
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $flatloun;

    /**
     * The yield
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $yield;

    /**
     * The vatvend
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $vatvend;

    /**
     * The standsize
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $standsize;

    /**
     * The mainsize
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $mainsize;

    /**
     * The outbsize
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $outbsize;

    /**
     * The altersize
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $altersize;

    /**
     * The garagesize
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $garagesize;

    /**
     * The grossinc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $grossinc;

    /**
     * The netinc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $netinc;

    /**
     * The mlsrecd
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $mlsrecd;

    /**
     * The shareprop
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $shareprop;

    /**
     * The virtualtour
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $virtualtour;

    /**
     * The cmlsstart
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $cmlsstart;

    /**
     * The cmlsend
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $cmlsend;

    /**
     * The onint
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $onint;

    /**
     * The lastmod
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $lastmod;

    /**
     * The saleinprog
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $saleinprog;

    /**
     * The mlsopenhr
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $mlsopenhr;

    /**
     * The grosscom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $grosscom;

    /**
     * The mlssplits
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $mlssplits;

    /**
     * The mlssplitl
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $mlssplitl;

    /**
     * The mes
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $mes;

    /**
     * The kitchen
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $kitchen;

    /**
     * The aircon
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $aircon;

    /**
     * The marketval
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $marketval;

    /**
     * The negotiable
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $negotiable;

    /**
     * The minprice
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $minprice;

    /**
     * The sqprice
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $sqprice;

    /**
     * The ispic
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $ispic;

    /**
     * The webupdate
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $webupdate;

    /**
     * The pq
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $pq;

    /**
     * The shareblock
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var bool
     */
    public $shareblock;

    /**
     * The sbnoofshar
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $sbnoofshar;

    /**
     * The xval
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $xval;

    /**
     * The yval
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $yval;

    /**
     * The xval2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $xval2;

    /**
     * The yval2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $yval2;

    /**
     * The PropertyID
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $PropertyID;

    /**
     * The availfrom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $availfrom;

    /**
     * The bid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bid;

    /**
     * The pnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pnum;

    /**
     * The lotnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $lotnum;

    /**
     * The id_uni
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $id_uni;

    /**
     * The cnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cnum;

    /**
     * The type
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $type;

    /**
     * The province
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $province;

    /**
     * The city
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $city;

    /**
     * The suburb
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $suburb;

    /**
     * The streetno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $streetno;

    /**
     * The street
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $street;

    /**
     * The complex
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $complex;

    /**
     * The notes
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $notes;

    /**
     * The pnote
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pnote;

    /**
     * The schools
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $schools;

    /**
     * The vicinity
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $vicinity;

    /**
     * The rooftype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $rooftype;

    /**
     * The garden
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $garden;

    /**
     * The windowtype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $windowtype;

    /**
     * The stories
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $stories;

    /**
     * The status
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $status;

    /**
     * The section
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $section;

    /**
     * The stand
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $stand;

    /**
     * The unit
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $unit;

    /**
     * The garageno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $garageno;

    /**
     * The staffno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $staffno;

    /**
     * The ceiltype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ceiltype;

    /**
     * The walltype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $walltype;

    /**
     * The floortype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $floortype;

    /**
     * The ccond
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ccond;

    /**
     * The ucond
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ucond;

    /**
     * The details
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $details;

    /**
     * The features
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfFeatures
     */
    public $features;

    /**
     * The exterior
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $exterior;

    /**
     * The ocompany
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ocompany;

    /**
     * The ocontact
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ocontact;

    /**
     * The otel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $otel;

    /**
     * The ofax
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ofax;

    /**
     * The ocell
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ocell;

    /**
     * The oemail
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $oemail;

    /**
     * The odomain
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $odomain;

    /**
     * The mlsno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsno;

    /**
     * The vdate
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $vdate;

    /**
     * The bcont1
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bcont1;

    /**
     * The bcon1tel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bcon1tel;

    /**
     * The bcont2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bcont2;

    /**
     * The bcon2tel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bcon2tel;

    /**
     * The bagree
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bagree;

    /**
     * The bnotes
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bnotes;

    /**
     * The bcomp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bcomp;

    /**
     * The bmunicipal
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bmunicipal;

    /**
     * The bbuilding
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bbuilding;

    /**
     * The bfnumber
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bfnumber;

    /**
     * The bparking
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bparking;

    /**
     * The bondhold
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bondhold;

    /**
     * The tenant
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $tenant;

    /**
     * The tentel1
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $tentel1;

    /**
     * The tentel2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $tentel2;

    /**
     * The tentel3
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $tentel3;

    /**
     * The texp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $texp;

    /**
     * The bustype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bustype;

    /**
     * The ownership
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ownership;

    /**
     * The zoning
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $zoning;

    /**
     * The mlsagent
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsagent;

    /**
     * The mlsarea
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsarea;

    /**
     * The mlsobid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsobid;

    /**
     * The mlsopnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsopnum;

    /**
     * The mlsocnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsocnum;

    /**
     * The mlsbid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsbid;

    /**
     * The mlsmyprop
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsmyprop;

    /**
     * The devname
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $devname;

    /**
     * The man_ag
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $man_ag;

    /**
     * The man_ag_con
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $man_ag_con;

    /**
     * The man_ag_tel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $man_ag_tel;

    /**
     * The bodycorp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bodycorp;

    /**
     * The petsallowed
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $petsallowed;

    /**
     * The sec_freeh
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $sec_freeh;

    /**
     * The grade
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $grade;

    /**
     * The mlsagentcel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsagentcel;

    /**
     * The mlsviewins
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsviewins;

    /**
     * The mlsattendee
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsattendee;

    /**
     * The other
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $other;

    /**
     * The other2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $other2;

    /**
     * The reasonsel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $reasonsel;

    /**
     * The tvaerial
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $tvaerial;

    /**
     * The mnetaerial
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mnetaerial;

    /**
     * The source
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $source;

    /**
     * The salestatus
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $salestatus;

    /**
     * The mlsnote
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $mlsnote;

    /**
     * The cid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cid;

    /**
     * The bic
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bic;

    /**
     * The showers
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $showers;

    /**
     * The stove
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $stove;

    /**
     * The servtoil
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $servtoil;

    /**
     * The lapa
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $lapa;

    /**
     * The remgar
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $remgar;

    /**
     * The floorplnpc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $floorplnpc;

    /**
     * The intpnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $intpnum;

    /**
     * The region
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $region;

    /**
     * The agownerid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $agownerid;

    /**
     * The psizetype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $psizetype;

    /**
     * The secpno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $secpno;

    /**
     * The sbno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $sbno;

    /**
     * The sbcert
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $sbcert;

    /**
     * The farmtype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $farmtype;

    /**
     * The livestock
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $livestock;

    /**
     * The wild_life
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $wild_life;

    /**
     * The produce
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $produce;

    /**
     * The outbuild
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $outbuild;

    /**
     * The water_holes
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $water_holes;

    /**
     * The lodges
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $lodges;

    /**
     * The farm_imp
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $farm_imp;

    /**
     * The compounds
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $compounds;

    /**
     * The oneline
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $oneline;

    /**
     * The bidgroup
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bidgroup;

    /**
     * The AgentData
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfAgentInd
     */
    public $AgentData;

    /**
     * The sellerdata
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfSeller
     */
    public $sellerdata;

    /**
     * The picdata
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfPicture
     */
    public $picdata;

    /**
     * The xnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $xnum;

    /**
     * The CUR
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $CUR;

    /**
     * The UniqueID
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $UniqueID;

    /**
     * The Rooms
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfRooms
     */
    public $Rooms;

    /**
     * The onshow
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfOnshow
     */
    public $onshow;

    /**
     * The auction
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructAuction
     */
    public $auction;

    /**
     * Constructor method for Property.
     * @see parent::__construct()
     * @param decimal $_rental
     * @param TivvitEnumRentper $_rentper
     * @param int $_levy
     * @param decimal $_price
     * @param int $_listingp
     * @param int $_soldp
     * @param int $_size
     * @param decimal $_lotsize
     * @param decimal $_bedrooms
     * @param decimal $_bathrooms
     * @param decimal $_garages
     * @param decimal $_age
     * @param bool $_pool
     * @param bool $_view_
     * @param bool $_study
     * @param bool $_carport
     * @param bool $_servant
     * @param bool $_laundry
     * @param bool $_security
     * @param bool $_parking
     * @param bool $_reception
     * @param bool $_pets
     * @param bool $_paving
     * @param bool $_living
     * @param bool $_eatarea
     * @param dateTime $_statusd
     * @param dateTime $_listingd
     * @param dateTime $_expiringd
     * @param dateTime $_priced
     * @param TivvitEnumLtype $_ltype
     * @param TivvitEnumPtype $_ptype
     * @param dateTime $_soldd
     * @param bool $_special
     * @param bool $_pantry
     * @param bool $_hall
     * @param bool $_sprinkler
     * @param bool $_tvroom
     * @param bool $_guestt
     * @param bool $_pgarden
     * @param bool $_patio
     * @param bool $_alarm
     * @param bool $_heating
     * @param bool $_tennis
     * @param bool $_incwater
     * @param bool $_gservice
     * @param bool $_pstaff
     * @param bool $_cservice
     * @param TivvitEnumHtm $_htm
     * @param bool $_intercom
     * @param bool $_gate
     * @param bool $_slights
     * @param bool $_sfence
     * @param bool $_recroom
     * @param bool $_shared
     * @param bool $_sole
     * @param dateTime $_mandstart
     * @param dateTime $_mandend
     * @param decimal $_brooms
     * @param bool $_bphase
     * @param bool $_bsign
     * @param int $_bperiod
     * @param int $_bfloor
     * @param decimal $_bgrental
     * @param decimal $_bnrental
     * @param decimal $_bopcost
     * @param decimal $_brentesc
     * @param decimal $_bopcostesc
     * @param dateTime $_boccdate
     * @param decimal $_bfitallow
     * @param int $_byard
     * @param int $_bupper
     * @param decimal $_munval
     * @param decimal $_bondbal
     * @param decimal $_ratestax
     * @param bool $_flat
     * @param bool $_flatkitc
     * @param decimal $_fbedroom
     * @param decimal $_fbathroom
     * @param bool $_flatloun
     * @param decimal $_yield
     * @param bool $_vatvend
     * @param int $_standsize
     * @param int $_mainsize
     * @param int $_outbsize
     * @param int $_altersize
     * @param int $_garagesize
     * @param int $_grossinc
     * @param int $_netinc
     * @param dateTime $_mlsrecd
     * @param bool $_shareprop
     * @param bool $_virtualtour
     * @param dateTime $_cmlsstart
     * @param dateTime $_cmlsend
     * @param bool $_onint
     * @param dateTime $_lastmod
     * @param bool $_saleinprog
     * @param dateTime $_mlsopenhr
     * @param decimal $_grosscom
     * @param int $_mlssplits
     * @param int $_mlssplitl
     * @param int $_mes
     * @param bool $_kitchen
     * @param bool $_aircon
     * @param decimal $_marketval
     * @param bool $_negotiable
     * @param decimal $_minprice
     * @param decimal $_sqprice
     * @param bool $_ispic
     * @param dateTime $_webupdate
     * @param decimal $_pq
     * @param bool $_shareblock
     * @param int $_sbnoofshar
     * @param decimal $_xval
     * @param decimal $_yval
     * @param decimal $_xval2
     * @param decimal $_yval2
     * @param int $_propertyID
     * @param dateTime $_availfrom
     * @param string $_bid
     * @param string $_pnum
     * @param string $_lotnum
     * @param string $_id_uni
     * @param string $_cnum
     * @param string $_type
     * @param string $_province
     * @param string $_city
     * @param string $_suburb
     * @param string $_streetno
     * @param string $_street
     * @param string $_complex
     * @param string $_notes
     * @param string $_pnote
     * @param string $_schools
     * @param string $_vicinity
     * @param string $_rooftype
     * @param string $_garden
     * @param string $_windowtype
     * @param string $_stories
     * @param string $_status
     * @param string $_section
     * @param string $_stand
     * @param string $_unit
     * @param string $_garageno
     * @param string $_staffno
     * @param string $_ceiltype
     * @param string $_walltype
     * @param string $_floortype
     * @param string $_ccond
     * @param string $_ucond
     * @param string $_details
     * @param TivvitStructArrayOfFeatures $_features
     * @param string $_exterior
     * @param string $_ocompany
     * @param string $_ocontact
     * @param string $_otel
     * @param string $_ofax
     * @param string $_ocell
     * @param string $_oemail
     * @param string $_odomain
     * @param string $_mlsno
     * @param string $_vdate
     * @param string $_bcont1
     * @param string $_bcon1tel
     * @param string $_bcont2
     * @param string $_bcon2tel
     * @param string $_bagree
     * @param string $_bnotes
     * @param string $_bcomp
     * @param string $_bmunicipal
     * @param string $_bbuilding
     * @param string $_bfnumber
     * @param string $_bparking
     * @param string $_bondhold
     * @param string $_tenant
     * @param string $_tentel1
     * @param string $_tentel2
     * @param string $_tentel3
     * @param string $_texp
     * @param string $_bustype
     * @param string $_ownership
     * @param string $_zoning
     * @param string $_mlsagent
     * @param string $_mlsarea
     * @param string $_mlsobid
     * @param string $_mlsopnum
     * @param string $_mlsocnum
     * @param string $_mlsbid
     * @param string $_mlsmyprop
     * @param string $_devname
     * @param string $_man_ag
     * @param string $_man_ag_con
     * @param string $_man_ag_tel
     * @param string $_bodycorp
     * @param string $_petsallowed
     * @param string $_sec_freeh
     * @param string $_grade
     * @param string $_mlsagentcel
     * @param string $_mlsviewins
     * @param string $_mlsattendee
     * @param string $_other
     * @param string $_other2
     * @param string $_reasonsel
     * @param string $_tvaerial
     * @param string $_mnetaerial
     * @param string $_source
     * @param string $_salestatus
     * @param string $_mlsnote
     * @param string $_cid
     * @param string $_bic
     * @param string $_showers
     * @param string $_stove
     * @param string $_servtoil
     * @param string $_lapa
     * @param string $_remgar
     * @param string $_floorplnpc
     * @param string $_intpnum
     * @param string $_region
     * @param string $_agownerid
     * @param string $_psizetype
     * @param string $_secpno
     * @param string $_sbno
     * @param string $_sbcert
     * @param string $_farmtype
     * @param string $_livestock
     * @param string $_wild_life
     * @param string $_produce
     * @param string $_outbuild
     * @param string $_water_holes
     * @param string $_lodges
     * @param string $_farm_imp
     * @param string $_compounds
     * @param string $_oneline
     * @param string $_bidgroup
     * @param TivvitStructArrayOfAgentInd $_agentData
     * @param TivvitStructArrayOfSeller $_sellerdata
     * @param TivvitStructArrayOfPicture $_picdata
     * @param string $_xnum
     * @param string $_cUR
     * @param string $_uniqueID
     * @param TivvitStructArrayOfRooms $_rooms
     * @param TivvitStructArrayOfOnshow $_onshow
     * @param TivvitStructAuction $_auction
     * @return TivvitStructProperty
     */
    public function __construct($_rental, $_rentper, $_levy, $_price, $_listingp, $_soldp, $_size, $_lotsize, $_bedrooms, $_bathrooms, $_garages, $_age, $_pool, $_view_, $_study, $_carport, $_servant, $_laundry, $_security, $_parking, $_reception, $_pets, $_paving, $_living, $_eatarea, $_statusd, $_listingd, $_expiringd, $_priced, $_ltype, $_ptype, $_soldd, $_special, $_pantry, $_hall, $_sprinkler, $_tvroom, $_guestt, $_pgarden, $_patio, $_alarm, $_heating, $_tennis, $_incwater, $_gservice, $_pstaff, $_cservice, $_htm, $_intercom, $_gate, $_slights, $_sfence, $_recroom, $_shared, $_sole, $_mandstart, $_mandend, $_brooms, $_bphase, $_bsign, $_bperiod, $_bfloor, $_bgrental, $_bnrental, $_bopcost, $_brentesc, $_bopcostesc, $_boccdate, $_bfitallow, $_byard, $_bupper, $_munval, $_bondbal, $_ratestax, $_flat, $_flatkitc, $_fbedroom, $_fbathroom, $_flatloun, $_yield, $_vatvend, $_standsize, $_mainsize, $_outbsize, $_altersize, $_garagesize, $_grossinc, $_netinc, $_mlsrecd, $_shareprop, $_virtualtour, $_cmlsstart, $_cmlsend, $_onint, $_lastmod, $_saleinprog, $_mlsopenhr, $_grosscom, $_mlssplits, $_mlssplitl, $_mes, $_kitchen, $_aircon, $_marketval, $_negotiable, $_minprice, $_sqprice, $_ispic, $_webupdate, $_pq, $_shareblock, $_sbnoofshar, $_xval, $_yval, $_xval2, $_yval2, $_propertyID, $_availfrom, $_bid = null, $_pnum = null, $_lotnum = null, $_id_uni = null, $_cnum = null, $_type = null, $_province = null, $_city = null, $_suburb = null, $_streetno = null, $_street = null, $_complex = null, $_notes = null, $_pnote = null, $_schools = null, $_vicinity = null, $_rooftype = null, $_garden = null, $_windowtype = null, $_stories = null, $_status = null, $_section = null, $_stand = null, $_unit = null, $_garageno = null, $_staffno = null, $_ceiltype = null, $_walltype = null, $_floortype = null, $_ccond = null, $_ucond = null, $_details = null, $_features = null, $_exterior = null, $_ocompany = null, $_ocontact = null, $_otel = null, $_ofax = null, $_ocell = null, $_oemail = null, $_odomain = null, $_mlsno = null, $_vdate = null, $_bcont1 = null, $_bcon1tel = null, $_bcont2 = null, $_bcon2tel = null, $_bagree = null, $_bnotes = null, $_bcomp = null, $_bmunicipal = null, $_bbuilding = null, $_bfnumber = null, $_bparking = null, $_bondhold = null, $_tenant = null, $_tentel1 = null, $_tentel2 = null, $_tentel3 = null, $_texp = null, $_bustype = null, $_ownership = null, $_zoning = null, $_mlsagent = null, $_mlsarea = null, $_mlsobid = null, $_mlsopnum = null, $_mlsocnum = null, $_mlsbid = null, $_mlsmyprop = null, $_devname = null, $_man_ag = null, $_man_ag_con = null, $_man_ag_tel = null, $_bodycorp = null, $_petsallowed = null, $_sec_freeh = null, $_grade = null, $_mlsagentcel = null, $_mlsviewins = null, $_mlsattendee = null, $_other = null, $_other2 = null, $_reasonsel = null, $_tvaerial = null, $_mnetaerial = null, $_source = null, $_salestatus = null, $_mlsnote = null, $_cid = null, $_bic = null, $_showers = null, $_stove = null, $_servtoil = null, $_lapa = null, $_remgar = null, $_floorplnpc = null, $_intpnum = null, $_region = null, $_agownerid = null, $_psizetype = null, $_secpno = null, $_sbno = null, $_sbcert = null, $_farmtype = null, $_livestock = null, $_wild_life = null, $_produce = null, $_outbuild = null, $_water_holes = null, $_lodges = null, $_farm_imp = null, $_compounds = null, $_oneline = null, $_bidgroup = null, $_agentData = null, $_sellerdata = null, $_picdata = null, $_xnum = null, $_cUR = null, $_uniqueID = null, $_rooms = null, $_onshow = null, $_auction = null)
    {
        parent::__construct(['rental' => $_rental, 'rentper' => $_rentper, 'levy' => $_levy, 'price' => $_price, 'listingp' => $_listingp, 'soldp' => $_soldp, 'size' => $_size, 'lotsize' => $_lotsize, 'bedrooms' => $_bedrooms, 'bathrooms' => $_bathrooms, 'garages' => $_garages, 'age' => $_age, 'pool' => $_pool, 'view_' => $_view_, 'study' => $_study, 'carport' => $_carport, 'servant' => $_servant, 'laundry' => $_laundry, 'security' => $_security, 'parking' => $_parking, 'reception' => $_reception, 'pets' => $_pets, 'paving' => $_paving, 'living' => $_living, 'eatarea' => $_eatarea, 'statusd' => $_statusd, 'listingd' => $_listingd, 'expiringd' => $_expiringd, 'priced' => $_priced, 'ltype' => $_ltype, 'ptype' => $_ptype, 'soldd' => $_soldd, 'special' => $_special, 'pantry' => $_pantry, 'hall' => $_hall, 'sprinkler' => $_sprinkler, 'tvroom' => $_tvroom, 'guestt' => $_guestt, 'pgarden' => $_pgarden, 'patio' => $_patio, 'alarm' => $_alarm, 'heating' => $_heating, 'tennis' => $_tennis, 'incwater' => $_incwater, 'gservice' => $_gservice, 'pstaff' => $_pstaff, 'cservice' => $_cservice, 'htm' => $_htm, 'intercom' => $_intercom, 'gate' => $_gate, 'slights' => $_slights, 'sfence' => $_sfence, 'recroom' => $_recroom, 'shared' => $_shared, 'sole' => $_sole, 'mandstart' => $_mandstart, 'mandend' => $_mandend, 'brooms' => $_brooms, 'bphase' => $_bphase, 'bsign' => $_bsign, 'bperiod' => $_bperiod, 'bfloor' => $_bfloor, 'bgrental' => $_bgrental, 'bnrental' => $_bnrental, 'bopcost' => $_bopcost, 'brentesc' => $_brentesc, 'bopcostesc' => $_bopcostesc, 'boccdate' => $_boccdate, 'bfitallow' => $_bfitallow, 'byard' => $_byard, 'bupper' => $_bupper, 'munval' => $_munval, 'bondbal' => $_bondbal, 'ratestax' => $_ratestax, 'flat' => $_flat, 'flatkitc' => $_flatkitc, 'fbedroom' => $_fbedroom, 'fbathroom' => $_fbathroom, 'flatloun' => $_flatloun, 'yield' => $_yield, 'vatvend' => $_vatvend, 'standsize' => $_standsize, 'mainsize' => $_mainsize, 'outbsize' => $_outbsize, 'altersize' => $_altersize, 'garagesize' => $_garagesize, 'grossinc' => $_grossinc, 'netinc' => $_netinc, 'mlsrecd' => $_mlsrecd, 'shareprop' => $_shareprop, 'virtualtour' => $_virtualtour, 'cmlsstart' => $_cmlsstart, 'cmlsend' => $_cmlsend, 'onint' => $_onint, 'lastmod' => $_lastmod, 'saleinprog' => $_saleinprog, 'mlsopenhr' => $_mlsopenhr, 'grosscom' => $_grosscom, 'mlssplits' => $_mlssplits, 'mlssplitl' => $_mlssplitl, 'mes' => $_mes, 'kitchen' => $_kitchen, 'aircon' => $_aircon, 'marketval' => $_marketval, 'negotiable' => $_negotiable, 'minprice' => $_minprice, 'sqprice' => $_sqprice, 'ispic' => $_ispic, 'webupdate' => $_webupdate, 'pq' => $_pq, 'shareblock' => $_shareblock, 'sbnoofshar' => $_sbnoofshar, 'xval' => $_xval, 'yval' => $_yval, 'xval2' => $_xval2, 'yval2' => $_yval2, 'PropertyID' => $_propertyID, 'availfrom' => $_availfrom, 'bid' => $_bid, 'pnum' => $_pnum, 'lotnum' => $_lotnum, 'id_uni' => $_id_uni, 'cnum' => $_cnum, 'type' => $_type, 'province' => $_province, 'city' => $_city, 'suburb' => $_suburb, 'streetno' => $_streetno, 'street' => $_street, 'complex' => $_complex, 'notes' => $_notes, 'pnote' => $_pnote, 'schools' => $_schools, 'vicinity' => $_vicinity, 'rooftype' => $_rooftype, 'garden' => $_garden, 'windowtype' => $_windowtype, 'stories' => $_stories, 'status' => $_status, 'section' => $_section, 'stand' => $_stand, 'unit' => $_unit, 'garageno' => $_garageno, 'staffno' => $_staffno, 'ceiltype' => $_ceiltype, 'walltype' => $_walltype, 'floortype' => $_floortype, 'ccond' => $_ccond, 'ucond' => $_ucond, 'details' => $_details, 'features' => ($_features instanceof TivvitStructArrayOfFeatures) ? $_features : null, 'exterior' => $_exterior, 'ocompany' => $_ocompany, 'ocontact' => $_ocontact, 'otel' => $_otel, 'ofax' => $_ofax, 'ocell' => $_ocell, 'oemail' => $_oemail, 'odomain' => $_odomain, 'mlsno' => $_mlsno, 'vdate' => $_vdate, 'bcont1' => $_bcont1, 'bcon1tel' => $_bcon1tel, 'bcont2' => $_bcont2, 'bcon2tel' => $_bcon2tel, 'bagree' => $_bagree, 'bnotes' => $_bnotes, 'bcomp' => $_bcomp, 'bmunicipal' => $_bmunicipal, 'bbuilding' => $_bbuilding, 'bfnumber' => $_bfnumber, 'bparking' => $_bparking, 'bondhold' => $_bondhold, 'tenant' => $_tenant, 'tentel1' => $_tentel1, 'tentel2' => $_tentel2, 'tentel3' => $_tentel3, 'texp' => $_texp, 'bustype' => $_bustype, 'ownership' => $_ownership, 'zoning' => $_zoning, 'mlsagent' => $_mlsagent, 'mlsarea' => $_mlsarea, 'mlsobid' => $_mlsobid, 'mlsopnum' => $_mlsopnum, 'mlsocnum' => $_mlsocnum, 'mlsbid' => $_mlsbid, 'mlsmyprop' => $_mlsmyprop, 'devname' => $_devname, 'man_ag' => $_man_ag, 'man_ag_con' => $_man_ag_con, 'man_ag_tel' => $_man_ag_tel, 'bodycorp' => $_bodycorp, 'petsallowed' => $_petsallowed, 'sec_freeh' => $_sec_freeh, 'grade' => $_grade, 'mlsagentcel' => $_mlsagentcel, 'mlsviewins' => $_mlsviewins, 'mlsattendee' => $_mlsattendee, 'other' => $_other, 'other2' => $_other2, 'reasonsel' => $_reasonsel, 'tvaerial' => $_tvaerial, 'mnetaerial' => $_mnetaerial, 'source' => $_source, 'salestatus' => $_salestatus, 'mlsnote' => $_mlsnote, 'cid' => $_cid, 'bic' => $_bic, 'showers' => $_showers, 'stove' => $_stove, 'servtoil' => $_servtoil, 'lapa' => $_lapa, 'remgar' => $_remgar, 'floorplnpc' => $_floorplnpc, 'intpnum' => $_intpnum, 'region' => $_region, 'agownerid' => $_agownerid, 'psizetype' => $_psizetype, 'secpno' => $_secpno, 'sbno' => $_sbno, 'sbcert' => $_sbcert, 'farmtype' => $_farmtype, 'livestock' => $_livestock, 'wild_life' => $_wild_life, 'produce' => $_produce, 'outbuild' => $_outbuild, 'water_holes' => $_water_holes, 'lodges' => $_lodges, 'farm_imp' => $_farm_imp, 'compounds' => $_compounds, 'oneline' => $_oneline, 'bidgroup' => $_bidgroup, 'AgentData' => ($_agentData instanceof TivvitStructArrayOfAgentInd) ? $_agentData : null, 'sellerdata' => ($_sellerdata instanceof TivvitStructArrayOfSeller) ? $_sellerdata : null, 'picdata' => ($_picdata instanceof TivvitStructArrayOfPicture) ? $_picdata : null, 'xnum' => $_xnum, 'CUR' => $_cUR, 'UniqueID' => $_uniqueID, 'Rooms' => ($_rooms instanceof TivvitStructArrayOfRooms) ? $_rooms : null, 'onshow' => ($_onshow instanceof TivvitStructArrayOfOnshow) ? $_onshow : null, 'auction' => $_auction], false);
    }

    /**
     * Get rental value.
     * @return decimal
     */
    public function getRental()
    {
        return $this->rental;
    }

    /**
     * Set rental value.
     * @param decimal $_rental the rental
     * @return decimal
     */
    public function setRental($_rental)
    {
        return $this->rental = $_rental;
    }

    /**
     * Get rentper value.
     * @return TivvitEnumRentper
     */
    public function getRentper()
    {
        return $this->rentper;
    }

    /**
     * Set rentper value.
     * @uses TivvitEnumRentper::valueIsValid()
     * @param TivvitEnumRentper $_rentper the rentper
     * @return TivvitEnumRentper
     */
    public function setRentper($_rentper)
    {
        if (! TivvitEnumRentper::valueIsValid($_rentper)) {
            return false;
        }

        return $this->rentper = $_rentper;
    }

    /**
     * Get levy value.
     * @return int
     */
    public function getLevy()
    {
        return $this->levy;
    }

    /**
     * Set levy value.
     * @param int $_levy the levy
     * @return int
     */
    public function setLevy($_levy)
    {
        return $this->levy = $_levy;
    }

    /**
     * Get price value.
     * @return decimal
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price value.
     * @param decimal $_price the price
     * @return decimal
     */
    public function setPrice($_price)
    {
        return $this->price = $_price;
    }

    /**
     * Get listingp value.
     * @return int
     */
    public function getListingp()
    {
        return $this->listingp;
    }

    /**
     * Set listingp value.
     * @param int $_listingp the listingp
     * @return int
     */
    public function setListingp($_listingp)
    {
        return $this->listingp = $_listingp;
    }

    /**
     * Get soldp value.
     * @return int
     */
    public function getSoldp()
    {
        return $this->soldp;
    }

    /**
     * Set soldp value.
     * @param int $_soldp the soldp
     * @return int
     */
    public function setSoldp($_soldp)
    {
        return $this->soldp = $_soldp;
    }

    /**
     * Get size value.
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set size value.
     * @param int $_size the size
     * @return int
     */
    public function setSize($_size)
    {
        return $this->size = $_size;
    }

    /**
     * Get lotsize value.
     * @return decimal
     */
    public function getLotsize()
    {
        return $this->lotsize;
    }

    /**
     * Set lotsize value.
     * @param decimal $_lotsize the lotsize
     * @return decimal
     */
    public function setLotsize($_lotsize)
    {
        return $this->lotsize = $_lotsize;
    }

    /**
     * Get bedrooms value.
     * @return decimal
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * Set bedrooms value.
     * @param decimal $_bedrooms the bedrooms
     * @return decimal
     */
    public function setBedrooms($_bedrooms)
    {
        return $this->bedrooms = $_bedrooms;
    }

    /**
     * Get bathrooms value.
     * @return decimal
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * Set bathrooms value.
     * @param decimal $_bathrooms the bathrooms
     * @return decimal
     */
    public function setBathrooms($_bathrooms)
    {
        return $this->bathrooms = $_bathrooms;
    }

    /**
     * Get garages value.
     * @return decimal
     */
    public function getGarages()
    {
        return $this->garages;
    }

    /**
     * Set garages value.
     * @param decimal $_garages the garages
     * @return decimal
     */
    public function setGarages($_garages)
    {
        return $this->garages = $_garages;
    }

    /**
     * Get age value.
     * @return decimal
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set age value.
     * @param decimal $_age the age
     * @return decimal
     */
    public function setAge($_age)
    {
        return $this->age = $_age;
    }

    /**
     * Get pool value.
     * @return bool
     */
    public function getPool()
    {
        return $this->pool;
    }

    /**
     * Set pool value.
     * @param bool $_pool the pool
     * @return bool
     */
    public function setPool($_pool)
    {
        return $this->pool = $_pool;
    }

    /**
     * Get view_ value.
     * @return bool
     */
    public function getView_()
    {
        return $this->view_;
    }

    /**
     * Set view_ value.
     * @param bool $_view_ the view_
     * @return bool
     */
    public function setView_($_view_)
    {
        return $this->view_ = $_view_;
    }

    /**
     * Get study value.
     * @return bool
     */
    public function getStudy()
    {
        return $this->study;
    }

    /**
     * Set study value.
     * @param bool $_study the study
     * @return bool
     */
    public function setStudy($_study)
    {
        return $this->study = $_study;
    }

    /**
     * Get carport value.
     * @return bool
     */
    public function getCarport()
    {
        return $this->carport;
    }

    /**
     * Set carport value.
     * @param bool $_carport the carport
     * @return bool
     */
    public function setCarport($_carport)
    {
        return $this->carport = $_carport;
    }

    /**
     * Get servant value.
     * @return bool
     */
    public function getServant()
    {
        return $this->servant;
    }

    /**
     * Set servant value.
     * @param bool $_servant the servant
     * @return bool
     */
    public function setServant($_servant)
    {
        return $this->servant = $_servant;
    }

    /**
     * Get laundry value.
     * @return bool
     */
    public function getLaundry()
    {
        return $this->laundry;
    }

    /**
     * Set laundry value.
     * @param bool $_laundry the laundry
     * @return bool
     */
    public function setLaundry($_laundry)
    {
        return $this->laundry = $_laundry;
    }

    /**
     * Get security value.
     * @return bool
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * Set security value.
     * @param bool $_security the security
     * @return bool
     */
    public function setSecurity($_security)
    {
        return $this->security = $_security;
    }

    /**
     * Get parking value.
     * @return bool
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set parking value.
     * @param bool $_parking the parking
     * @return bool
     */
    public function setParking($_parking)
    {
        return $this->parking = $_parking;
    }

    /**
     * Get reception value.
     * @return bool
     */
    public function getReception()
    {
        return $this->reception;
    }

    /**
     * Set reception value.
     * @param bool $_reception the reception
     * @return bool
     */
    public function setReception($_reception)
    {
        return $this->reception = $_reception;
    }

    /**
     * Get pets value.
     * @return bool
     */
    public function getPets()
    {
        return $this->pets;
    }

    /**
     * Set pets value.
     * @param bool $_pets the pets
     * @return bool
     */
    public function setPets($_pets)
    {
        return $this->pets = $_pets;
    }

    /**
     * Get paving value.
     * @return bool
     */
    public function getPaving()
    {
        return $this->paving;
    }

    /**
     * Set paving value.
     * @param bool $_paving the paving
     * @return bool
     */
    public function setPaving($_paving)
    {
        return $this->paving = $_paving;
    }

    /**
     * Get living value.
     * @return bool
     */
    public function getLiving()
    {
        return $this->living;
    }

    /**
     * Set living value.
     * @param bool $_living the living
     * @return bool
     */
    public function setLiving($_living)
    {
        return $this->living = $_living;
    }

    /**
     * Get eatarea value.
     * @return bool
     */
    public function getEatarea()
    {
        return $this->eatarea;
    }

    /**
     * Set eatarea value.
     * @param bool $_eatarea the eatarea
     * @return bool
     */
    public function setEatarea($_eatarea)
    {
        return $this->eatarea = $_eatarea;
    }

    /**
     * Get statusd value.
     * @return dateTime
     */
    public function getStatusd()
    {
        return $this->statusd;
    }

    /**
     * Set statusd value.
     * @param dateTime $_statusd the statusd
     * @return dateTime
     */
    public function setStatusd($_statusd)
    {
        return $this->statusd = $_statusd;
    }

    /**
     * Get listingd value.
     * @return dateTime
     */
    public function getListingd()
    {
        return $this->listingd;
    }

    /**
     * Set listingd value.
     * @param dateTime $_listingd the listingd
     * @return dateTime
     */
    public function setListingd($_listingd)
    {
        return $this->listingd = $_listingd;
    }

    /**
     * Get expiringd value.
     * @return dateTime
     */
    public function getExpiringd()
    {
        return $this->expiringd;
    }

    /**
     * Set expiringd value.
     * @param dateTime $_expiringd the expiringd
     * @return dateTime
     */
    public function setExpiringd($_expiringd)
    {
        return $this->expiringd = $_expiringd;
    }

    /**
     * Get priced value.
     * @return dateTime
     */
    public function getPriced()
    {
        return $this->priced;
    }

    /**
     * Set priced value.
     * @param dateTime $_priced the priced
     * @return dateTime
     */
    public function setPriced($_priced)
    {
        return $this->priced = $_priced;
    }

    /**
     * Get ltype value.
     * @return TivvitEnumLtype
     */
    public function getLtype()
    {
        return $this->ltype;
    }

    /**
     * Set ltype value.
     * @uses TivvitEnumLtype::valueIsValid()
     * @param TivvitEnumLtype $_ltype the ltype
     * @return TivvitEnumLtype
     */
    public function setLtype($_ltype)
    {
        if (! TivvitEnumLtype::valueIsValid($_ltype)) {
            return false;
        }

        return $this->ltype = $_ltype;
    }

    /**
     * Get ptype value.
     * @return TivvitEnumPtype
     */
    public function getPtype()
    {
        return $this->ptype;
    }

    /**
     * Set ptype value.
     * @uses TivvitEnumPtype::valueIsValid()
     * @param TivvitEnumPtype $_ptype the ptype
     * @return TivvitEnumPtype
     */
    public function setPtype($_ptype)
    {
        if (! TivvitEnumPtype::valueIsValid($_ptype)) {
            return false;
        }

        return $this->ptype = $_ptype;
    }

    /**
     * Get soldd value.
     * @return dateTime
     */
    public function getSoldd()
    {
        return $this->soldd;
    }

    /**
     * Set soldd value.
     * @param dateTime $_soldd the soldd
     * @return dateTime
     */
    public function setSoldd($_soldd)
    {
        return $this->soldd = $_soldd;
    }

    /**
     * Get special value.
     * @return bool
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * Set special value.
     * @param bool $_special the special
     * @return bool
     */
    public function setSpecial($_special)
    {
        return $this->special = $_special;
    }

    /**
     * Get pantry value.
     * @return bool
     */
    public function getPantry()
    {
        return $this->pantry;
    }

    /**
     * Set pantry value.
     * @param bool $_pantry the pantry
     * @return bool
     */
    public function setPantry($_pantry)
    {
        return $this->pantry = $_pantry;
    }

    /**
     * Get hall value.
     * @return bool
     */
    public function getHall()
    {
        return $this->hall;
    }

    /**
     * Set hall value.
     * @param bool $_hall the hall
     * @return bool
     */
    public function setHall($_hall)
    {
        return $this->hall = $_hall;
    }

    /**
     * Get sprinkler value.
     * @return bool
     */
    public function getSprinkler()
    {
        return $this->sprinkler;
    }

    /**
     * Set sprinkler value.
     * @param bool $_sprinkler the sprinkler
     * @return bool
     */
    public function setSprinkler($_sprinkler)
    {
        return $this->sprinkler = $_sprinkler;
    }

    /**
     * Get tvroom value.
     * @return bool
     */
    public function getTvroom()
    {
        return $this->tvroom;
    }

    /**
     * Set tvroom value.
     * @param bool $_tvroom the tvroom
     * @return bool
     */
    public function setTvroom($_tvroom)
    {
        return $this->tvroom = $_tvroom;
    }

    /**
     * Get guestt value.
     * @return bool
     */
    public function getGuestt()
    {
        return $this->guestt;
    }

    /**
     * Set guestt value.
     * @param bool $_guestt the guestt
     * @return bool
     */
    public function setGuestt($_guestt)
    {
        return $this->guestt = $_guestt;
    }

    /**
     * Get pgarden value.
     * @return bool
     */
    public function getPgarden()
    {
        return $this->pgarden;
    }

    /**
     * Set pgarden value.
     * @param bool $_pgarden the pgarden
     * @return bool
     */
    public function setPgarden($_pgarden)
    {
        return $this->pgarden = $_pgarden;
    }

    /**
     * Get patio value.
     * @return bool
     */
    public function getPatio()
    {
        return $this->patio;
    }

    /**
     * Set patio value.
     * @param bool $_patio the patio
     * @return bool
     */
    public function setPatio($_patio)
    {
        return $this->patio = $_patio;
    }

    /**
     * Get alarm value.
     * @return bool
     */
    public function getAlarm()
    {
        return $this->alarm;
    }

    /**
     * Set alarm value.
     * @param bool $_alarm the alarm
     * @return bool
     */
    public function setAlarm($_alarm)
    {
        return $this->alarm = $_alarm;
    }

    /**
     * Get heating value.
     * @return bool
     */
    public function getHeating()
    {
        return $this->heating;
    }

    /**
     * Set heating value.
     * @param bool $_heating the heating
     * @return bool
     */
    public function setHeating($_heating)
    {
        return $this->heating = $_heating;
    }

    /**
     * Get tennis value.
     * @return bool
     */
    public function getTennis()
    {
        return $this->tennis;
    }

    /**
     * Set tennis value.
     * @param bool $_tennis the tennis
     * @return bool
     */
    public function setTennis($_tennis)
    {
        return $this->tennis = $_tennis;
    }

    /**
     * Get incwater value.
     * @return bool
     */
    public function getIncwater()
    {
        return $this->incwater;
    }

    /**
     * Set incwater value.
     * @param bool $_incwater the incwater
     * @return bool
     */
    public function setIncwater($_incwater)
    {
        return $this->incwater = $_incwater;
    }

    /**
     * Get gservice value.
     * @return bool
     */
    public function getGservice()
    {
        return $this->gservice;
    }

    /**
     * Set gservice value.
     * @param bool $_gservice the gservice
     * @return bool
     */
    public function setGservice($_gservice)
    {
        return $this->gservice = $_gservice;
    }

    /**
     * Get pstaff value.
     * @return bool
     */
    public function getPstaff()
    {
        return $this->pstaff;
    }

    /**
     * Set pstaff value.
     * @param bool $_pstaff the pstaff
     * @return bool
     */
    public function setPstaff($_pstaff)
    {
        return $this->pstaff = $_pstaff;
    }

    /**
     * Get cservice value.
     * @return bool
     */
    public function getCservice()
    {
        return $this->cservice;
    }

    /**
     * Set cservice value.
     * @param bool $_cservice the cservice
     * @return bool
     */
    public function setCservice($_cservice)
    {
        return $this->cservice = $_cservice;
    }

    /**
     * Get htm value.
     * @return TivvitEnumHtm
     */
    public function getHtm()
    {
        return $this->htm;
    }

    /**
     * Set htm value.
     * @uses TivvitEnumHtm::valueIsValid()
     * @param TivvitEnumHtm $_htm the htm
     * @return TivvitEnumHtm
     */
    public function setHtm($_htm)
    {
        if (! TivvitEnumHtm::valueIsValid($_htm)) {
            return false;
        }

        return $this->htm = $_htm;
    }

    /**
     * Get intercom value.
     * @return bool
     */
    public function getIntercom()
    {
        return $this->intercom;
    }

    /**
     * Set intercom value.
     * @param bool $_intercom the intercom
     * @return bool
     */
    public function setIntercom($_intercom)
    {
        return $this->intercom = $_intercom;
    }

    /**
     * Get gate value.
     * @return bool
     */
    public function getGate()
    {
        return $this->gate;
    }

    /**
     * Set gate value.
     * @param bool $_gate the gate
     * @return bool
     */
    public function setGate($_gate)
    {
        return $this->gate = $_gate;
    }

    /**
     * Get slights value.
     * @return bool
     */
    public function getSlights()
    {
        return $this->slights;
    }

    /**
     * Set slights value.
     * @param bool $_slights the slights
     * @return bool
     */
    public function setSlights($_slights)
    {
        return $this->slights = $_slights;
    }

    /**
     * Get sfence value.
     * @return bool
     */
    public function getSfence()
    {
        return $this->sfence;
    }

    /**
     * Set sfence value.
     * @param bool $_sfence the sfence
     * @return bool
     */
    public function setSfence($_sfence)
    {
        return $this->sfence = $_sfence;
    }

    /**
     * Get recroom value.
     * @return bool
     */
    public function getRecroom()
    {
        return $this->recroom;
    }

    /**
     * Set recroom value.
     * @param bool $_recroom the recroom
     * @return bool
     */
    public function setRecroom($_recroom)
    {
        return $this->recroom = $_recroom;
    }

    /**
     * Get shared value.
     * @return bool
     */
    public function getShared()
    {
        return $this->shared;
    }

    /**
     * Set shared value.
     * @param bool $_shared the shared
     * @return bool
     */
    public function setShared($_shared)
    {
        return $this->shared = $_shared;
    }

    /**
     * Get sole value.
     * @return bool
     */
    public function getSole()
    {
        return $this->sole;
    }

    /**
     * Set sole value.
     * @param bool $_sole the sole
     * @return bool
     */
    public function setSole($_sole)
    {
        return $this->sole = $_sole;
    }

    /**
     * Get mandstart value.
     * @return dateTime
     */
    public function getMandstart()
    {
        return $this->mandstart;
    }

    /**
     * Set mandstart value.
     * @param dateTime $_mandstart the mandstart
     * @return dateTime
     */
    public function setMandstart($_mandstart)
    {
        return $this->mandstart = $_mandstart;
    }

    /**
     * Get mandend value.
     * @return dateTime
     */
    public function getMandend()
    {
        return $this->mandend;
    }

    /**
     * Set mandend value.
     * @param dateTime $_mandend the mandend
     * @return dateTime
     */
    public function setMandend($_mandend)
    {
        return $this->mandend = $_mandend;
    }

    /**
     * Get brooms value.
     * @return decimal
     */
    public function getBrooms()
    {
        return $this->brooms;
    }

    /**
     * Set brooms value.
     * @param decimal $_brooms the brooms
     * @return decimal
     */
    public function setBrooms($_brooms)
    {
        return $this->brooms = $_brooms;
    }

    /**
     * Get bphase value.
     * @return bool
     */
    public function getBphase()
    {
        return $this->bphase;
    }

    /**
     * Set bphase value.
     * @param bool $_bphase the bphase
     * @return bool
     */
    public function setBphase($_bphase)
    {
        return $this->bphase = $_bphase;
    }

    /**
     * Get bsign value.
     * @return bool
     */
    public function getBsign()
    {
        return $this->bsign;
    }

    /**
     * Set bsign value.
     * @param bool $_bsign the bsign
     * @return bool
     */
    public function setBsign($_bsign)
    {
        return $this->bsign = $_bsign;
    }

    /**
     * Get bperiod value.
     * @return int
     */
    public function getBperiod()
    {
        return $this->bperiod;
    }

    /**
     * Set bperiod value.
     * @param int $_bperiod the bperiod
     * @return int
     */
    public function setBperiod($_bperiod)
    {
        return $this->bperiod = $_bperiod;
    }

    /**
     * Get bfloor value.
     * @return int
     */
    public function getBfloor()
    {
        return $this->bfloor;
    }

    /**
     * Set bfloor value.
     * @param int $_bfloor the bfloor
     * @return int
     */
    public function setBfloor($_bfloor)
    {
        return $this->bfloor = $_bfloor;
    }

    /**
     * Get bgrental value.
     * @return decimal
     */
    public function getBgrental()
    {
        return $this->bgrental;
    }

    /**
     * Set bgrental value.
     * @param decimal $_bgrental the bgrental
     * @return decimal
     */
    public function setBgrental($_bgrental)
    {
        return $this->bgrental = $_bgrental;
    }

    /**
     * Get bnrental value.
     * @return decimal
     */
    public function getBnrental()
    {
        return $this->bnrental;
    }

    /**
     * Set bnrental value.
     * @param decimal $_bnrental the bnrental
     * @return decimal
     */
    public function setBnrental($_bnrental)
    {
        return $this->bnrental = $_bnrental;
    }

    /**
     * Get bopcost value.
     * @return decimal
     */
    public function getBopcost()
    {
        return $this->bopcost;
    }

    /**
     * Set bopcost value.
     * @param decimal $_bopcost the bopcost
     * @return decimal
     */
    public function setBopcost($_bopcost)
    {
        return $this->bopcost = $_bopcost;
    }

    /**
     * Get brentesc value.
     * @return decimal
     */
    public function getBrentesc()
    {
        return $this->brentesc;
    }

    /**
     * Set brentesc value.
     * @param decimal $_brentesc the brentesc
     * @return decimal
     */
    public function setBrentesc($_brentesc)
    {
        return $this->brentesc = $_brentesc;
    }

    /**
     * Get bopcostesc value.
     * @return decimal
     */
    public function getBopcostesc()
    {
        return $this->bopcostesc;
    }

    /**
     * Set bopcostesc value.
     * @param decimal $_bopcostesc the bopcostesc
     * @return decimal
     */
    public function setBopcostesc($_bopcostesc)
    {
        return $this->bopcostesc = $_bopcostesc;
    }

    /**
     * Get boccdate value.
     * @return dateTime
     */
    public function getBoccdate()
    {
        return $this->boccdate;
    }

    /**
     * Set boccdate value.
     * @param dateTime $_boccdate the boccdate
     * @return dateTime
     */
    public function setBoccdate($_boccdate)
    {
        return $this->boccdate = $_boccdate;
    }

    /**
     * Get bfitallow value.
     * @return decimal
     */
    public function getBfitallow()
    {
        return $this->bfitallow;
    }

    /**
     * Set bfitallow value.
     * @param decimal $_bfitallow the bfitallow
     * @return decimal
     */
    public function setBfitallow($_bfitallow)
    {
        return $this->bfitallow = $_bfitallow;
    }

    /**
     * Get byard value.
     * @return int
     */
    public function getByard()
    {
        return $this->byard;
    }

    /**
     * Set byard value.
     * @param int $_byard the byard
     * @return int
     */
    public function setByard($_byard)
    {
        return $this->byard = $_byard;
    }

    /**
     * Get bupper value.
     * @return int
     */
    public function getBupper()
    {
        return $this->bupper;
    }

    /**
     * Set bupper value.
     * @param int $_bupper the bupper
     * @return int
     */
    public function setBupper($_bupper)
    {
        return $this->bupper = $_bupper;
    }

    /**
     * Get munval value.
     * @return decimal
     */
    public function getMunval()
    {
        return $this->munval;
    }

    /**
     * Set munval value.
     * @param decimal $_munval the munval
     * @return decimal
     */
    public function setMunval($_munval)
    {
        return $this->munval = $_munval;
    }

    /**
     * Get bondbal value.
     * @return decimal
     */
    public function getBondbal()
    {
        return $this->bondbal;
    }

    /**
     * Set bondbal value.
     * @param decimal $_bondbal the bondbal
     * @return decimal
     */
    public function setBondbal($_bondbal)
    {
        return $this->bondbal = $_bondbal;
    }

    /**
     * Get ratestax value.
     * @return decimal
     */
    public function getRatestax()
    {
        return $this->ratestax;
    }

    /**
     * Set ratestax value.
     * @param decimal $_ratestax the ratestax
     * @return decimal
     */
    public function setRatestax($_ratestax)
    {
        return $this->ratestax = $_ratestax;
    }

    /**
     * Get flat value.
     * @return bool
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * Set flat value.
     * @param bool $_flat the flat
     * @return bool
     */
    public function setFlat($_flat)
    {
        return $this->flat = $_flat;
    }

    /**
     * Get flatkitc value.
     * @return bool
     */
    public function getFlatkitc()
    {
        return $this->flatkitc;
    }

    /**
     * Set flatkitc value.
     * @param bool $_flatkitc the flatkitc
     * @return bool
     */
    public function setFlatkitc($_flatkitc)
    {
        return $this->flatkitc = $_flatkitc;
    }

    /**
     * Get fbedroom value.
     * @return decimal
     */
    public function getFbedroom()
    {
        return $this->fbedroom;
    }

    /**
     * Set fbedroom value.
     * @param decimal $_fbedroom the fbedroom
     * @return decimal
     */
    public function setFbedroom($_fbedroom)
    {
        return $this->fbedroom = $_fbedroom;
    }

    /**
     * Get fbathroom value.
     * @return decimal
     */
    public function getFbathroom()
    {
        return $this->fbathroom;
    }

    /**
     * Set fbathroom value.
     * @param decimal $_fbathroom the fbathroom
     * @return decimal
     */
    public function setFbathroom($_fbathroom)
    {
        return $this->fbathroom = $_fbathroom;
    }

    /**
     * Get flatloun value.
     * @return bool
     */
    public function getFlatloun()
    {
        return $this->flatloun;
    }

    /**
     * Set flatloun value.
     * @param bool $_flatloun the flatloun
     * @return bool
     */
    public function setFlatloun($_flatloun)
    {
        return $this->flatloun = $_flatloun;
    }

    /**
     * Get yield value.
     * @return decimal
     */
    public function getYield()
    {
        return $this->yield;
    }

    /**
     * Set yield value.
     * @param decimal $_yield the yield
     * @return decimal
     */
    public function setYield($_yield)
    {
        return $this->yield = $_yield;
    }

    /**
     * Get vatvend value.
     * @return bool
     */
    public function getVatvend()
    {
        return $this->vatvend;
    }

    /**
     * Set vatvend value.
     * @param bool $_vatvend the vatvend
     * @return bool
     */
    public function setVatvend($_vatvend)
    {
        return $this->vatvend = $_vatvend;
    }

    /**
     * Get standsize value.
     * @return int
     */
    public function getStandsize()
    {
        return $this->standsize;
    }

    /**
     * Set standsize value.
     * @param int $_standsize the standsize
     * @return int
     */
    public function setStandsize($_standsize)
    {
        return $this->standsize = $_standsize;
    }

    /**
     * Get mainsize value.
     * @return int
     */
    public function getMainsize()
    {
        return $this->mainsize;
    }

    /**
     * Set mainsize value.
     * @param int $_mainsize the mainsize
     * @return int
     */
    public function setMainsize($_mainsize)
    {
        return $this->mainsize = $_mainsize;
    }

    /**
     * Get outbsize value.
     * @return int
     */
    public function getOutbsize()
    {
        return $this->outbsize;
    }

    /**
     * Set outbsize value.
     * @param int $_outbsize the outbsize
     * @return int
     */
    public function setOutbsize($_outbsize)
    {
        return $this->outbsize = $_outbsize;
    }

    /**
     * Get altersize value.
     * @return int
     */
    public function getAltersize()
    {
        return $this->altersize;
    }

    /**
     * Set altersize value.
     * @param int $_altersize the altersize
     * @return int
     */
    public function setAltersize($_altersize)
    {
        return $this->altersize = $_altersize;
    }

    /**
     * Get garagesize value.
     * @return int
     */
    public function getGaragesize()
    {
        return $this->garagesize;
    }

    /**
     * Set garagesize value.
     * @param int $_garagesize the garagesize
     * @return int
     */
    public function setGaragesize($_garagesize)
    {
        return $this->garagesize = $_garagesize;
    }

    /**
     * Get grossinc value.
     * @return int
     */
    public function getGrossinc()
    {
        return $this->grossinc;
    }

    /**
     * Set grossinc value.
     * @param int $_grossinc the grossinc
     * @return int
     */
    public function setGrossinc($_grossinc)
    {
        return $this->grossinc = $_grossinc;
    }

    /**
     * Get netinc value.
     * @return int
     */
    public function getNetinc()
    {
        return $this->netinc;
    }

    /**
     * Set netinc value.
     * @param int $_netinc the netinc
     * @return int
     */
    public function setNetinc($_netinc)
    {
        return $this->netinc = $_netinc;
    }

    /**
     * Get mlsrecd value.
     * @return dateTime
     */
    public function getMlsrecd()
    {
        return $this->mlsrecd;
    }

    /**
     * Set mlsrecd value.
     * @param dateTime $_mlsrecd the mlsrecd
     * @return dateTime
     */
    public function setMlsrecd($_mlsrecd)
    {
        return $this->mlsrecd = $_mlsrecd;
    }

    /**
     * Get shareprop value.
     * @return bool
     */
    public function getShareprop()
    {
        return $this->shareprop;
    }

    /**
     * Set shareprop value.
     * @param bool $_shareprop the shareprop
     * @return bool
     */
    public function setShareprop($_shareprop)
    {
        return $this->shareprop = $_shareprop;
    }

    /**
     * Get virtualtour value.
     * @return bool
     */
    public function getVirtualtour()
    {
        return $this->virtualtour;
    }

    /**
     * Set virtualtour value.
     * @param bool $_virtualtour the virtualtour
     * @return bool
     */
    public function setVirtualtour($_virtualtour)
    {
        return $this->virtualtour = $_virtualtour;
    }

    /**
     * Get cmlsstart value.
     * @return dateTime
     */
    public function getCmlsstart()
    {
        return $this->cmlsstart;
    }

    /**
     * Set cmlsstart value.
     * @param dateTime $_cmlsstart the cmlsstart
     * @return dateTime
     */
    public function setCmlsstart($_cmlsstart)
    {
        return $this->cmlsstart = $_cmlsstart;
    }

    /**
     * Get cmlsend value.
     * @return dateTime
     */
    public function getCmlsend()
    {
        return $this->cmlsend;
    }

    /**
     * Set cmlsend value.
     * @param dateTime $_cmlsend the cmlsend
     * @return dateTime
     */
    public function setCmlsend($_cmlsend)
    {
        return $this->cmlsend = $_cmlsend;
    }

    /**
     * Get onint value.
     * @return bool
     */
    public function getOnint()
    {
        return $this->onint;
    }

    /**
     * Set onint value.
     * @param bool $_onint the onint
     * @return bool
     */
    public function setOnint($_onint)
    {
        return $this->onint = $_onint;
    }

    /**
     * Get lastmod value.
     * @return dateTime
     */
    public function getLastmod()
    {
        return $this->lastmod;
    }

    /**
     * Set lastmod value.
     * @param dateTime $_lastmod the lastmod
     * @return dateTime
     */
    public function setLastmod($_lastmod)
    {
        return $this->lastmod = $_lastmod;
    }

    /**
     * Get saleinprog value.
     * @return bool
     */
    public function getSaleinprog()
    {
        return $this->saleinprog;
    }

    /**
     * Set saleinprog value.
     * @param bool $_saleinprog the saleinprog
     * @return bool
     */
    public function setSaleinprog($_saleinprog)
    {
        return $this->saleinprog = $_saleinprog;
    }

    /**
     * Get mlsopenhr value.
     * @return dateTime
     */
    public function getMlsopenhr()
    {
        return $this->mlsopenhr;
    }

    /**
     * Set mlsopenhr value.
     * @param dateTime $_mlsopenhr the mlsopenhr
     * @return dateTime
     */
    public function setMlsopenhr($_mlsopenhr)
    {
        return $this->mlsopenhr = $_mlsopenhr;
    }

    /**
     * Get grosscom value.
     * @return decimal
     */
    public function getGrosscom()
    {
        return $this->grosscom;
    }

    /**
     * Set grosscom value.
     * @param decimal $_grosscom the grosscom
     * @return decimal
     */
    public function setGrosscom($_grosscom)
    {
        return $this->grosscom = $_grosscom;
    }

    /**
     * Get mlssplits value.
     * @return int
     */
    public function getMlssplits()
    {
        return $this->mlssplits;
    }

    /**
     * Set mlssplits value.
     * @param int $_mlssplits the mlssplits
     * @return int
     */
    public function setMlssplits($_mlssplits)
    {
        return $this->mlssplits = $_mlssplits;
    }

    /**
     * Get mlssplitl value.
     * @return int
     */
    public function getMlssplitl()
    {
        return $this->mlssplitl;
    }

    /**
     * Set mlssplitl value.
     * @param int $_mlssplitl the mlssplitl
     * @return int
     */
    public function setMlssplitl($_mlssplitl)
    {
        return $this->mlssplitl = $_mlssplitl;
    }

    /**
     * Get mes value.
     * @return int
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set mes value.
     * @param int $_mes the mes
     * @return int
     */
    public function setMes($_mes)
    {
        return $this->mes = $_mes;
    }

    /**
     * Get kitchen value.
     * @return bool
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * Set kitchen value.
     * @param bool $_kitchen the kitchen
     * @return bool
     */
    public function setKitchen($_kitchen)
    {
        return $this->kitchen = $_kitchen;
    }

    /**
     * Get aircon value.
     * @return bool
     */
    public function getAircon()
    {
        return $this->aircon;
    }

    /**
     * Set aircon value.
     * @param bool $_aircon the aircon
     * @return bool
     */
    public function setAircon($_aircon)
    {
        return $this->aircon = $_aircon;
    }

    /**
     * Get marketval value.
     * @return decimal
     */
    public function getMarketval()
    {
        return $this->marketval;
    }

    /**
     * Set marketval value.
     * @param decimal $_marketval the marketval
     * @return decimal
     */
    public function setMarketval($_marketval)
    {
        return $this->marketval = $_marketval;
    }

    /**
     * Get negotiable value.
     * @return bool
     */
    public function getNegotiable()
    {
        return $this->negotiable;
    }

    /**
     * Set negotiable value.
     * @param bool $_negotiable the negotiable
     * @return bool
     */
    public function setNegotiable($_negotiable)
    {
        return $this->negotiable = $_negotiable;
    }

    /**
     * Get minprice value.
     * @return decimal
     */
    public function getMinprice()
    {
        return $this->minprice;
    }

    /**
     * Set minprice value.
     * @param decimal $_minprice the minprice
     * @return decimal
     */
    public function setMinprice($_minprice)
    {
        return $this->minprice = $_minprice;
    }

    /**
     * Get sqprice value.
     * @return decimal
     */
    public function getSqprice()
    {
        return $this->sqprice;
    }

    /**
     * Set sqprice value.
     * @param decimal $_sqprice the sqprice
     * @return decimal
     */
    public function setSqprice($_sqprice)
    {
        return $this->sqprice = $_sqprice;
    }

    /**
     * Get ispic value.
     * @return bool
     */
    public function getIspic()
    {
        return $this->ispic;
    }

    /**
     * Set ispic value.
     * @param bool $_ispic the ispic
     * @return bool
     */
    public function setIspic($_ispic)
    {
        return $this->ispic = $_ispic;
    }

    /**
     * Get webupdate value.
     * @return dateTime
     */
    public function getWebupdate()
    {
        return $this->webupdate;
    }

    /**
     * Set webupdate value.
     * @param dateTime $_webupdate the webupdate
     * @return dateTime
     */
    public function setWebupdate($_webupdate)
    {
        return $this->webupdate = $_webupdate;
    }

    /**
     * Get pq value.
     * @return decimal
     */
    public function getPq()
    {
        return $this->pq;
    }

    /**
     * Set pq value.
     * @param decimal $_pq the pq
     * @return decimal
     */
    public function setPq($_pq)
    {
        return $this->pq = $_pq;
    }

    /**
     * Get shareblock value.
     * @return bool
     */
    public function getShareblock()
    {
        return $this->shareblock;
    }

    /**
     * Set shareblock value.
     * @param bool $_shareblock the shareblock
     * @return bool
     */
    public function setShareblock($_shareblock)
    {
        return $this->shareblock = $_shareblock;
    }

    /**
     * Get sbnoofshar value.
     * @return int
     */
    public function getSbnoofshar()
    {
        return $this->sbnoofshar;
    }

    /**
     * Set sbnoofshar value.
     * @param int $_sbnoofshar the sbnoofshar
     * @return int
     */
    public function setSbnoofshar($_sbnoofshar)
    {
        return $this->sbnoofshar = $_sbnoofshar;
    }

    /**
     * Get xval value.
     * @return decimal
     */
    public function getXval()
    {
        return $this->xval;
    }

    /**
     * Set xval value.
     * @param decimal $_xval the xval
     * @return decimal
     */
    public function setXval($_xval)
    {
        return $this->xval = $_xval;
    }

    /**
     * Get yval value.
     * @return decimal
     */
    public function getYval()
    {
        return $this->yval;
    }

    /**
     * Set yval value.
     * @param decimal $_yval the yval
     * @return decimal
     */
    public function setYval($_yval)
    {
        return $this->yval = $_yval;
    }

    /**
     * Get xval2 value.
     * @return decimal
     */
    public function getXval2()
    {
        return $this->xval2;
    }

    /**
     * Set xval2 value.
     * @param decimal $_xval2 the xval2
     * @return decimal
     */
    public function setXval2($_xval2)
    {
        return $this->xval2 = $_xval2;
    }

    /**
     * Get yval2 value.
     * @return decimal
     */
    public function getYval2()
    {
        return $this->yval2;
    }

    /**
     * Set yval2 value.
     * @param decimal $_yval2 the yval2
     * @return decimal
     */
    public function setYval2($_yval2)
    {
        return $this->yval2 = $_yval2;
    }

    /**
     * Get PropertyID value.
     * @return int
     */
    public function getPropertyID()
    {
        return $this->PropertyID;
    }

    /**
     * Set PropertyID value.
     * @param int $_propertyID the PropertyID
     * @return int
     */
    public function setPropertyID($_propertyID)
    {
        return $this->PropertyID = $_propertyID;
    }

    /**
     * Get availfrom value.
     * @return dateTime
     */
    public function getAvailfrom()
    {
        return $this->availfrom;
    }

    /**
     * Set availfrom value.
     * @param dateTime $_availfrom the availfrom
     * @return dateTime
     */
    public function setAvailfrom($_availfrom)
    {
        return $this->availfrom = $_availfrom;
    }

    /**
     * Get bid value.
     * @return string|null
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set bid value.
     * @param string $_bid the bid
     * @return string
     */
    public function setBid($_bid)
    {
        return $this->bid = $_bid;
    }

    /**
     * Get pnum value.
     * @return string|null
     */
    public function getPnum()
    {
        return $this->pnum;
    }

    /**
     * Set pnum value.
     * @param string $_pnum the pnum
     * @return string
     */
    public function setPnum($_pnum)
    {
        return $this->pnum = $_pnum;
    }

    /**
     * Get lotnum value.
     * @return string|null
     */
    public function getLotnum()
    {
        return $this->lotnum;
    }

    /**
     * Set lotnum value.
     * @param string $_lotnum the lotnum
     * @return string
     */
    public function setLotnum($_lotnum)
    {
        return $this->lotnum = $_lotnum;
    }

    /**
     * Get id_uni value.
     * @return string|null
     */
    public function getId_uni()
    {
        return $this->id_uni;
    }

    /**
     * Set id_uni value.
     * @param string $_id_uni the id_uni
     * @return string
     */
    public function setId_uni($_id_uni)
    {
        return $this->id_uni = $_id_uni;
    }

    /**
     * Get cnum value.
     * @return string|null
     */
    public function getCnum()
    {
        return $this->cnum;
    }

    /**
     * Set cnum value.
     * @param string $_cnum the cnum
     * @return string
     */
    public function setCnum($_cnum)
    {
        return $this->cnum = $_cnum;
    }

    /**
     * Get type value.
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type value.
     * @param string $_type the type
     * @return string
     */
    public function setType($_type)
    {
        return $this->type = $_type;
    }

    /**
     * Get province value.
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set province value.
     * @param string $_province the province
     * @return string
     */
    public function setProvince($_province)
    {
        return $this->province = $_province;
    }

    /**
     * Get city value.
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city value.
     * @param string $_city the city
     * @return string
     */
    public function setCity($_city)
    {
        return $this->city = $_city;
    }

    /**
     * Get suburb value.
     * @return string|null
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set suburb value.
     * @param string $_suburb the suburb
     * @return string
     */
    public function setSuburb($_suburb)
    {
        return $this->suburb = $_suburb;
    }

    /**
     * Get streetno value.
     * @return string|null
     */
    public function getStreetno()
    {
        return $this->streetno;
    }

    /**
     * Set streetno value.
     * @param string $_streetno the streetno
     * @return string
     */
    public function setStreetno($_streetno)
    {
        return $this->streetno = $_streetno;
    }

    /**
     * Get street value.
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street value.
     * @param string $_street the street
     * @return string
     */
    public function setStreet($_street)
    {
        return $this->street = $_street;
    }

    /**
     * Get complex value.
     * @return string|null
     */
    public function getComplex()
    {
        return $this->complex;
    }

    /**
     * Set complex value.
     * @param string $_complex the complex
     * @return string
     */
    public function setComplex($_complex)
    {
        return $this->complex = $_complex;
    }

    /**
     * Get notes value.
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set notes value.
     * @param string $_notes the notes
     * @return string
     */
    public function setNotes($_notes)
    {
        return $this->notes = $_notes;
    }

    /**
     * Get pnote value.
     * @return string|null
     */
    public function getPnote()
    {
        return $this->pnote;
    }

    /**
     * Set pnote value.
     * @param string $_pnote the pnote
     * @return string
     */
    public function setPnote($_pnote)
    {
        return $this->pnote = $_pnote;
    }

    /**
     * Get schools value.
     * @return string|null
     */
    public function getSchools()
    {
        return $this->schools;
    }

    /**
     * Set schools value.
     * @param string $_schools the schools
     * @return string
     */
    public function setSchools($_schools)
    {
        return $this->schools = $_schools;
    }

    /**
     * Get vicinity value.
     * @return string|null
     */
    public function getVicinity()
    {
        return $this->vicinity;
    }

    /**
     * Set vicinity value.
     * @param string $_vicinity the vicinity
     * @return string
     */
    public function setVicinity($_vicinity)
    {
        return $this->vicinity = $_vicinity;
    }

    /**
     * Get rooftype value.
     * @return string|null
     */
    public function getRooftype()
    {
        return $this->rooftype;
    }

    /**
     * Set rooftype value.
     * @param string $_rooftype the rooftype
     * @return string
     */
    public function setRooftype($_rooftype)
    {
        return $this->rooftype = $_rooftype;
    }

    /**
     * Get garden value.
     * @return string|null
     */
    public function getGarden()
    {
        return $this->garden;
    }

    /**
     * Set garden value.
     * @param string $_garden the garden
     * @return string
     */
    public function setGarden($_garden)
    {
        return $this->garden = $_garden;
    }

    /**
     * Get windowtype value.
     * @return string|null
     */
    public function getWindowtype()
    {
        return $this->windowtype;
    }

    /**
     * Set windowtype value.
     * @param string $_windowtype the windowtype
     * @return string
     */
    public function setWindowtype($_windowtype)
    {
        return $this->windowtype = $_windowtype;
    }

    /**
     * Get stories value.
     * @return string|null
     */
    public function getStories()
    {
        return $this->stories;
    }

    /**
     * Set stories value.
     * @param string $_stories the stories
     * @return string
     */
    public function setStories($_stories)
    {
        return $this->stories = $_stories;
    }

    /**
     * Get status value.
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status value.
     * @param string $_status the status
     * @return string
     */
    public function setStatus($_status)
    {
        return $this->status = $_status;
    }

    /**
     * Get section value.
     * @return string|null
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set section value.
     * @param string $_section the section
     * @return string
     */
    public function setSection($_section)
    {
        return $this->section = $_section;
    }

    /**
     * Get stand value.
     * @return string|null
     */
    public function getStand()
    {
        return $this->stand;
    }

    /**
     * Set stand value.
     * @param string $_stand the stand
     * @return string
     */
    public function setStand($_stand)
    {
        return $this->stand = $_stand;
    }

    /**
     * Get unit value.
     * @return string|null
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set unit value.
     * @param string $_unit the unit
     * @return string
     */
    public function setUnit($_unit)
    {
        return $this->unit = $_unit;
    }

    /**
     * Get garageno value.
     * @return string|null
     */
    public function getGarageno()
    {
        return $this->garageno;
    }

    /**
     * Set garageno value.
     * @param string $_garageno the garageno
     * @return string
     */
    public function setGarageno($_garageno)
    {
        return $this->garageno = $_garageno;
    }

    /**
     * Get staffno value.
     * @return string|null
     */
    public function getStaffno()
    {
        return $this->staffno;
    }

    /**
     * Set staffno value.
     * @param string $_staffno the staffno
     * @return string
     */
    public function setStaffno($_staffno)
    {
        return $this->staffno = $_staffno;
    }

    /**
     * Get ceiltype value.
     * @return string|null
     */
    public function getCeiltype()
    {
        return $this->ceiltype;
    }

    /**
     * Set ceiltype value.
     * @param string $_ceiltype the ceiltype
     * @return string
     */
    public function setCeiltype($_ceiltype)
    {
        return $this->ceiltype = $_ceiltype;
    }

    /**
     * Get walltype value.
     * @return string|null
     */
    public function getWalltype()
    {
        return $this->walltype;
    }

    /**
     * Set walltype value.
     * @param string $_walltype the walltype
     * @return string
     */
    public function setWalltype($_walltype)
    {
        return $this->walltype = $_walltype;
    }

    /**
     * Get floortype value.
     * @return string|null
     */
    public function getFloortype()
    {
        return $this->floortype;
    }

    /**
     * Set floortype value.
     * @param string $_floortype the floortype
     * @return string
     */
    public function setFloortype($_floortype)
    {
        return $this->floortype = $_floortype;
    }

    /**
     * Get ccond value.
     * @return string|null
     */
    public function getCcond()
    {
        return $this->ccond;
    }

    /**
     * Set ccond value.
     * @param string $_ccond the ccond
     * @return string
     */
    public function setCcond($_ccond)
    {
        return $this->ccond = $_ccond;
    }

    /**
     * Get ucond value.
     * @return string|null
     */
    public function getUcond()
    {
        return $this->ucond;
    }

    /**
     * Set ucond value.
     * @param string $_ucond the ucond
     * @return string
     */
    public function setUcond($_ucond)
    {
        return $this->ucond = $_ucond;
    }

    /**
     * Get details value.
     * @return string|null
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set details value.
     * @param string $_details the details
     * @return string
     */
    public function setDetails($_details)
    {
        return $this->details = $_details;
    }

    /**
     * Get features value.
     * @return TivvitStructArrayOfFeatures|null
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Set features value.
     * @param TivvitStructArrayOfFeatures $_features the features
     * @return TivvitStructArrayOfFeatures
     */
    public function setFeatures($_features)
    {
        return $this->features = $_features;
    }

    /**
     * Get exterior value.
     * @return string|null
     */
    public function getExterior()
    {
        return $this->exterior;
    }

    /**
     * Set exterior value.
     * @param string $_exterior the exterior
     * @return string
     */
    public function setExterior($_exterior)
    {
        return $this->exterior = $_exterior;
    }

    /**
     * Get ocompany value.
     * @return string|null
     */
    public function getOcompany()
    {
        return $this->ocompany;
    }

    /**
     * Set ocompany value.
     * @param string $_ocompany the ocompany
     * @return string
     */
    public function setOcompany($_ocompany)
    {
        return $this->ocompany = $_ocompany;
    }

    /**
     * Get ocontact value.
     * @return string|null
     */
    public function getOcontact()
    {
        return $this->ocontact;
    }

    /**
     * Set ocontact value.
     * @param string $_ocontact the ocontact
     * @return string
     */
    public function setOcontact($_ocontact)
    {
        return $this->ocontact = $_ocontact;
    }

    /**
     * Get otel value.
     * @return string|null
     */
    public function getOtel()
    {
        return $this->otel;
    }

    /**
     * Set otel value.
     * @param string $_otel the otel
     * @return string
     */
    public function setOtel($_otel)
    {
        return $this->otel = $_otel;
    }

    /**
     * Get ofax value.
     * @return string|null
     */
    public function getOfax()
    {
        return $this->ofax;
    }

    /**
     * Set ofax value.
     * @param string $_ofax the ofax
     * @return string
     */
    public function setOfax($_ofax)
    {
        return $this->ofax = $_ofax;
    }

    /**
     * Get ocell value.
     * @return string|null
     */
    public function getOcell()
    {
        return $this->ocell;
    }

    /**
     * Set ocell value.
     * @param string $_ocell the ocell
     * @return string
     */
    public function setOcell($_ocell)
    {
        return $this->ocell = $_ocell;
    }

    /**
     * Get oemail value.
     * @return string|null
     */
    public function getOemail()
    {
        return $this->oemail;
    }

    /**
     * Set oemail value.
     * @param string $_oemail the oemail
     * @return string
     */
    public function setOemail($_oemail)
    {
        return $this->oemail = $_oemail;
    }

    /**
     * Get odomain value.
     * @return string|null
     */
    public function getOdomain()
    {
        return $this->odomain;
    }

    /**
     * Set odomain value.
     * @param string $_odomain the odomain
     * @return string
     */
    public function setOdomain($_odomain)
    {
        return $this->odomain = $_odomain;
    }

    /**
     * Get mlsno value.
     * @return string|null
     */
    public function getMlsno()
    {
        return $this->mlsno;
    }

    /**
     * Set mlsno value.
     * @param string $_mlsno the mlsno
     * @return string
     */
    public function setMlsno($_mlsno)
    {
        return $this->mlsno = $_mlsno;
    }

    /**
     * Get vdate value.
     * @return string|null
     */
    public function getVdate()
    {
        return $this->vdate;
    }

    /**
     * Set vdate value.
     * @param string $_vdate the vdate
     * @return string
     */
    public function setVdate($_vdate)
    {
        return $this->vdate = $_vdate;
    }

    /**
     * Get bcont1 value.
     * @return string|null
     */
    public function getBcont1()
    {
        return $this->bcont1;
    }

    /**
     * Set bcont1 value.
     * @param string $_bcont1 the bcont1
     * @return string
     */
    public function setBcont1($_bcont1)
    {
        return $this->bcont1 = $_bcont1;
    }

    /**
     * Get bcon1tel value.
     * @return string|null
     */
    public function getBcon1tel()
    {
        return $this->bcon1tel;
    }

    /**
     * Set bcon1tel value.
     * @param string $_bcon1tel the bcon1tel
     * @return string
     */
    public function setBcon1tel($_bcon1tel)
    {
        return $this->bcon1tel = $_bcon1tel;
    }

    /**
     * Get bcont2 value.
     * @return string|null
     */
    public function getBcont2()
    {
        return $this->bcont2;
    }

    /**
     * Set bcont2 value.
     * @param string $_bcont2 the bcont2
     * @return string
     */
    public function setBcont2($_bcont2)
    {
        return $this->bcont2 = $_bcont2;
    }

    /**
     * Get bcon2tel value.
     * @return string|null
     */
    public function getBcon2tel()
    {
        return $this->bcon2tel;
    }

    /**
     * Set bcon2tel value.
     * @param string $_bcon2tel the bcon2tel
     * @return string
     */
    public function setBcon2tel($_bcon2tel)
    {
        return $this->bcon2tel = $_bcon2tel;
    }

    /**
     * Get bagree value.
     * @return string|null
     */
    public function getBagree()
    {
        return $this->bagree;
    }

    /**
     * Set bagree value.
     * @param string $_bagree the bagree
     * @return string
     */
    public function setBagree($_bagree)
    {
        return $this->bagree = $_bagree;
    }

    /**
     * Get bnotes value.
     * @return string|null
     */
    public function getBnotes()
    {
        return $this->bnotes;
    }

    /**
     * Set bnotes value.
     * @param string $_bnotes the bnotes
     * @return string
     */
    public function setBnotes($_bnotes)
    {
        return $this->bnotes = $_bnotes;
    }

    /**
     * Get bcomp value.
     * @return string|null
     */
    public function getBcomp()
    {
        return $this->bcomp;
    }

    /**
     * Set bcomp value.
     * @param string $_bcomp the bcomp
     * @return string
     */
    public function setBcomp($_bcomp)
    {
        return $this->bcomp = $_bcomp;
    }

    /**
     * Get bmunicipal value.
     * @return string|null
     */
    public function getBmunicipal()
    {
        return $this->bmunicipal;
    }

    /**
     * Set bmunicipal value.
     * @param string $_bmunicipal the bmunicipal
     * @return string
     */
    public function setBmunicipal($_bmunicipal)
    {
        return $this->bmunicipal = $_bmunicipal;
    }

    /**
     * Get bbuilding value.
     * @return string|null
     */
    public function getBbuilding()
    {
        return $this->bbuilding;
    }

    /**
     * Set bbuilding value.
     * @param string $_bbuilding the bbuilding
     * @return string
     */
    public function setBbuilding($_bbuilding)
    {
        return $this->bbuilding = $_bbuilding;
    }

    /**
     * Get bfnumber value.
     * @return string|null
     */
    public function getBfnumber()
    {
        return $this->bfnumber;
    }

    /**
     * Set bfnumber value.
     * @param string $_bfnumber the bfnumber
     * @return string
     */
    public function setBfnumber($_bfnumber)
    {
        return $this->bfnumber = $_bfnumber;
    }

    /**
     * Get bparking value.
     * @return string|null
     */
    public function getBparking()
    {
        return $this->bparking;
    }

    /**
     * Set bparking value.
     * @param string $_bparking the bparking
     * @return string
     */
    public function setBparking($_bparking)
    {
        return $this->bparking = $_bparking;
    }

    /**
     * Get bondhold value.
     * @return string|null
     */
    public function getBondhold()
    {
        return $this->bondhold;
    }

    /**
     * Set bondhold value.
     * @param string $_bondhold the bondhold
     * @return string
     */
    public function setBondhold($_bondhold)
    {
        return $this->bondhold = $_bondhold;
    }

    /**
     * Get tenant value.
     * @return string|null
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * Set tenant value.
     * @param string $_tenant the tenant
     * @return string
     */
    public function setTenant($_tenant)
    {
        return $this->tenant = $_tenant;
    }

    /**
     * Get tentel1 value.
     * @return string|null
     */
    public function getTentel1()
    {
        return $this->tentel1;
    }

    /**
     * Set tentel1 value.
     * @param string $_tentel1 the tentel1
     * @return string
     */
    public function setTentel1($_tentel1)
    {
        return $this->tentel1 = $_tentel1;
    }

    /**
     * Get tentel2 value.
     * @return string|null
     */
    public function getTentel2()
    {
        return $this->tentel2;
    }

    /**
     * Set tentel2 value.
     * @param string $_tentel2 the tentel2
     * @return string
     */
    public function setTentel2($_tentel2)
    {
        return $this->tentel2 = $_tentel2;
    }

    /**
     * Get tentel3 value.
     * @return string|null
     */
    public function getTentel3()
    {
        return $this->tentel3;
    }

    /**
     * Set tentel3 value.
     * @param string $_tentel3 the tentel3
     * @return string
     */
    public function setTentel3($_tentel3)
    {
        return $this->tentel3 = $_tentel3;
    }

    /**
     * Get texp value.
     * @return string|null
     */
    public function getTexp()
    {
        return $this->texp;
    }

    /**
     * Set texp value.
     * @param string $_texp the texp
     * @return string
     */
    public function setTexp($_texp)
    {
        return $this->texp = $_texp;
    }

    /**
     * Get bustype value.
     * @return string|null
     */
    public function getBustype()
    {
        return $this->bustype;
    }

    /**
     * Set bustype value.
     * @param string $_bustype the bustype
     * @return string
     */
    public function setBustype($_bustype)
    {
        return $this->bustype = $_bustype;
    }

    /**
     * Get ownership value.
     * @return string|null
     */
    public function getOwnership()
    {
        return $this->ownership;
    }

    /**
     * Set ownership value.
     * @param string $_ownership the ownership
     * @return string
     */
    public function setOwnership($_ownership)
    {
        return $this->ownership = $_ownership;
    }

    /**
     * Get zoning value.
     * @return string|null
     */
    public function getZoning()
    {
        return $this->zoning;
    }

    /**
     * Set zoning value.
     * @param string $_zoning the zoning
     * @return string
     */
    public function setZoning($_zoning)
    {
        return $this->zoning = $_zoning;
    }

    /**
     * Get mlsagent value.
     * @return string|null
     */
    public function getMlsagent()
    {
        return $this->mlsagent;
    }

    /**
     * Set mlsagent value.
     * @param string $_mlsagent the mlsagent
     * @return string
     */
    public function setMlsagent($_mlsagent)
    {
        return $this->mlsagent = $_mlsagent;
    }

    /**
     * Get mlsarea value.
     * @return string|null
     */
    public function getMlsarea()
    {
        return $this->mlsarea;
    }

    /**
     * Set mlsarea value.
     * @param string $_mlsarea the mlsarea
     * @return string
     */
    public function setMlsarea($_mlsarea)
    {
        return $this->mlsarea = $_mlsarea;
    }

    /**
     * Get mlsobid value.
     * @return string|null
     */
    public function getMlsobid()
    {
        return $this->mlsobid;
    }

    /**
     * Set mlsobid value.
     * @param string $_mlsobid the mlsobid
     * @return string
     */
    public function setMlsobid($_mlsobid)
    {
        return $this->mlsobid = $_mlsobid;
    }

    /**
     * Get mlsopnum value.
     * @return string|null
     */
    public function getMlsopnum()
    {
        return $this->mlsopnum;
    }

    /**
     * Set mlsopnum value.
     * @param string $_mlsopnum the mlsopnum
     * @return string
     */
    public function setMlsopnum($_mlsopnum)
    {
        return $this->mlsopnum = $_mlsopnum;
    }

    /**
     * Get mlsocnum value.
     * @return string|null
     */
    public function getMlsocnum()
    {
        return $this->mlsocnum;
    }

    /**
     * Set mlsocnum value.
     * @param string $_mlsocnum the mlsocnum
     * @return string
     */
    public function setMlsocnum($_mlsocnum)
    {
        return $this->mlsocnum = $_mlsocnum;
    }

    /**
     * Get mlsbid value.
     * @return string|null
     */
    public function getMlsbid()
    {
        return $this->mlsbid;
    }

    /**
     * Set mlsbid value.
     * @param string $_mlsbid the mlsbid
     * @return string
     */
    public function setMlsbid($_mlsbid)
    {
        return $this->mlsbid = $_mlsbid;
    }

    /**
     * Get mlsmyprop value.
     * @return string|null
     */
    public function getMlsmyprop()
    {
        return $this->mlsmyprop;
    }

    /**
     * Set mlsmyprop value.
     * @param string $_mlsmyprop the mlsmyprop
     * @return string
     */
    public function setMlsmyprop($_mlsmyprop)
    {
        return $this->mlsmyprop = $_mlsmyprop;
    }

    /**
     * Get devname value.
     * @return string|null
     */
    public function getDevname()
    {
        return $this->devname;
    }

    /**
     * Set devname value.
     * @param string $_devname the devname
     * @return string
     */
    public function setDevname($_devname)
    {
        return $this->devname = $_devname;
    }

    /**
     * Get man_ag value.
     * @return string|null
     */
    public function getMan_ag()
    {
        return $this->man_ag;
    }

    /**
     * Set man_ag value.
     * @param string $_man_ag the man_ag
     * @return string
     */
    public function setMan_ag($_man_ag)
    {
        return $this->man_ag = $_man_ag;
    }

    /**
     * Get man_ag_con value.
     * @return string|null
     */
    public function getMan_ag_con()
    {
        return $this->man_ag_con;
    }

    /**
     * Set man_ag_con value.
     * @param string $_man_ag_con the man_ag_con
     * @return string
     */
    public function setMan_ag_con($_man_ag_con)
    {
        return $this->man_ag_con = $_man_ag_con;
    }

    /**
     * Get man_ag_tel value.
     * @return string|null
     */
    public function getMan_ag_tel()
    {
        return $this->man_ag_tel;
    }

    /**
     * Set man_ag_tel value.
     * @param string $_man_ag_tel the man_ag_tel
     * @return string
     */
    public function setMan_ag_tel($_man_ag_tel)
    {
        return $this->man_ag_tel = $_man_ag_tel;
    }

    /**
     * Get bodycorp value.
     * @return string|null
     */
    public function getBodycorp()
    {
        return $this->bodycorp;
    }

    /**
     * Set bodycorp value.
     * @param string $_bodycorp the bodycorp
     * @return string
     */
    public function setBodycorp($_bodycorp)
    {
        return $this->bodycorp = $_bodycorp;
    }

    /**
     * Get petsallowed value.
     * @return string|null
     */
    public function getPetsallowed()
    {
        return $this->petsallowed;
    }

    /**
     * Set petsallowed value.
     * @param string $_petsallowed the petsallowed
     * @return string
     */
    public function setPetsallowed($_petsallowed)
    {
        return $this->petsallowed = $_petsallowed;
    }

    /**
     * Get sec_freeh value.
     * @return string|null
     */
    public function getSec_freeh()
    {
        return $this->sec_freeh;
    }

    /**
     * Set sec_freeh value.
     * @param string $_sec_freeh the sec_freeh
     * @return string
     */
    public function setSec_freeh($_sec_freeh)
    {
        return $this->sec_freeh = $_sec_freeh;
    }

    /**
     * Get grade value.
     * @return string|null
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set grade value.
     * @param string $_grade the grade
     * @return string
     */
    public function setGrade($_grade)
    {
        return $this->grade = $_grade;
    }

    /**
     * Get mlsagentcel value.
     * @return string|null
     */
    public function getMlsagentcel()
    {
        return $this->mlsagentcel;
    }

    /**
     * Set mlsagentcel value.
     * @param string $_mlsagentcel the mlsagentcel
     * @return string
     */
    public function setMlsagentcel($_mlsagentcel)
    {
        return $this->mlsagentcel = $_mlsagentcel;
    }

    /**
     * Get mlsviewins value.
     * @return string|null
     */
    public function getMlsviewins()
    {
        return $this->mlsviewins;
    }

    /**
     * Set mlsviewins value.
     * @param string $_mlsviewins the mlsviewins
     * @return string
     */
    public function setMlsviewins($_mlsviewins)
    {
        return $this->mlsviewins = $_mlsviewins;
    }

    /**
     * Get mlsattendee value.
     * @return string|null
     */
    public function getMlsattendee()
    {
        return $this->mlsattendee;
    }

    /**
     * Set mlsattendee value.
     * @param string $_mlsattendee the mlsattendee
     * @return string
     */
    public function setMlsattendee($_mlsattendee)
    {
        return $this->mlsattendee = $_mlsattendee;
    }

    /**
     * Get other value.
     * @return string|null
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set other value.
     * @param string $_other the other
     * @return string
     */
    public function setOther($_other)
    {
        return $this->other = $_other;
    }

    /**
     * Get other2 value.
     * @return string|null
     */
    public function getOther2()
    {
        return $this->other2;
    }

    /**
     * Set other2 value.
     * @param string $_other2 the other2
     * @return string
     */
    public function setOther2($_other2)
    {
        return $this->other2 = $_other2;
    }

    /**
     * Get reasonsel value.
     * @return string|null
     */
    public function getReasonsel()
    {
        return $this->reasonsel;
    }

    /**
     * Set reasonsel value.
     * @param string $_reasonsel the reasonsel
     * @return string
     */
    public function setReasonsel($_reasonsel)
    {
        return $this->reasonsel = $_reasonsel;
    }

    /**
     * Get tvaerial value.
     * @return string|null
     */
    public function getTvaerial()
    {
        return $this->tvaerial;
    }

    /**
     * Set tvaerial value.
     * @param string $_tvaerial the tvaerial
     * @return string
     */
    public function setTvaerial($_tvaerial)
    {
        return $this->tvaerial = $_tvaerial;
    }

    /**
     * Get mnetaerial value.
     * @return string|null
     */
    public function getMnetaerial()
    {
        return $this->mnetaerial;
    }

    /**
     * Set mnetaerial value.
     * @param string $_mnetaerial the mnetaerial
     * @return string
     */
    public function setMnetaerial($_mnetaerial)
    {
        return $this->mnetaerial = $_mnetaerial;
    }

    /**
     * Get source value.
     * @return string|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set source value.
     * @param string $_source the source
     * @return string
     */
    public function setSource($_source)
    {
        return $this->source = $_source;
    }

    /**
     * Get salestatus value.
     * @return string|null
     */
    public function getSalestatus()
    {
        return $this->salestatus;
    }

    /**
     * Set salestatus value.
     * @param string $_salestatus the salestatus
     * @return string
     */
    public function setSalestatus($_salestatus)
    {
        return $this->salestatus = $_salestatus;
    }

    /**
     * Get mlsnote value.
     * @return string|null
     */
    public function getMlsnote()
    {
        return $this->mlsnote;
    }

    /**
     * Set mlsnote value.
     * @param string $_mlsnote the mlsnote
     * @return string
     */
    public function setMlsnote($_mlsnote)
    {
        return $this->mlsnote = $_mlsnote;
    }

    /**
     * Get cid value.
     * @return string|null
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set cid value.
     * @param string $_cid the cid
     * @return string
     */
    public function setCid($_cid)
    {
        return $this->cid = $_cid;
    }

    /**
     * Get bic value.
     * @return string|null
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set bic value.
     * @param string $_bic the bic
     * @return string
     */
    public function setBic($_bic)
    {
        return $this->bic = $_bic;
    }

    /**
     * Get showers value.
     * @return string|null
     */
    public function getShowers()
    {
        return $this->showers;
    }

    /**
     * Set showers value.
     * @param string $_showers the showers
     * @return string
     */
    public function setShowers($_showers)
    {
        return $this->showers = $_showers;
    }

    /**
     * Get stove value.
     * @return string|null
     */
    public function getStove()
    {
        return $this->stove;
    }

    /**
     * Set stove value.
     * @param string $_stove the stove
     * @return string
     */
    public function setStove($_stove)
    {
        return $this->stove = $_stove;
    }

    /**
     * Get servtoil value.
     * @return string|null
     */
    public function getServtoil()
    {
        return $this->servtoil;
    }

    /**
     * Set servtoil value.
     * @param string $_servtoil the servtoil
     * @return string
     */
    public function setServtoil($_servtoil)
    {
        return $this->servtoil = $_servtoil;
    }

    /**
     * Get lapa value.
     * @return string|null
     */
    public function getLapa()
    {
        return $this->lapa;
    }

    /**
     * Set lapa value.
     * @param string $_lapa the lapa
     * @return string
     */
    public function setLapa($_lapa)
    {
        return $this->lapa = $_lapa;
    }

    /**
     * Get remgar value.
     * @return string|null
     */
    public function getRemgar()
    {
        return $this->remgar;
    }

    /**
     * Set remgar value.
     * @param string $_remgar the remgar
     * @return string
     */
    public function setRemgar($_remgar)
    {
        return $this->remgar = $_remgar;
    }

    /**
     * Get floorplnpc value.
     * @return string|null
     */
    public function getFloorplnpc()
    {
        return $this->floorplnpc;
    }

    /**
     * Set floorplnpc value.
     * @param string $_floorplnpc the floorplnpc
     * @return string
     */
    public function setFloorplnpc($_floorplnpc)
    {
        return $this->floorplnpc = $_floorplnpc;
    }

    /**
     * Get intpnum value.
     * @return string|null
     */
    public function getIntpnum()
    {
        return $this->intpnum;
    }

    /**
     * Set intpnum value.
     * @param string $_intpnum the intpnum
     * @return string
     */
    public function setIntpnum($_intpnum)
    {
        return $this->intpnum = $_intpnum;
    }

    /**
     * Get region value.
     * @return string|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set region value.
     * @param string $_region the region
     * @return string
     */
    public function setRegion($_region)
    {
        return $this->region = $_region;
    }

    /**
     * Get agownerid value.
     * @return string|null
     */
    public function getAgownerid()
    {
        return $this->agownerid;
    }

    /**
     * Set agownerid value.
     * @param string $_agownerid the agownerid
     * @return string
     */
    public function setAgownerid($_agownerid)
    {
        return $this->agownerid = $_agownerid;
    }

    /**
     * Get psizetype value.
     * @return string|null
     */
    public function getPsizetype()
    {
        return $this->psizetype;
    }

    /**
     * Set psizetype value.
     * @param string $_psizetype the psizetype
     * @return string
     */
    public function setPsizetype($_psizetype)
    {
        return $this->psizetype = $_psizetype;
    }

    /**
     * Get secpno value.
     * @return string|null
     */
    public function getSecpno()
    {
        return $this->secpno;
    }

    /**
     * Set secpno value.
     * @param string $_secpno the secpno
     * @return string
     */
    public function setSecpno($_secpno)
    {
        return $this->secpno = $_secpno;
    }

    /**
     * Get sbno value.
     * @return string|null
     */
    public function getSbno()
    {
        return $this->sbno;
    }

    /**
     * Set sbno value.
     * @param string $_sbno the sbno
     * @return string
     */
    public function setSbno($_sbno)
    {
        return $this->sbno = $_sbno;
    }

    /**
     * Get sbcert value.
     * @return string|null
     */
    public function getSbcert()
    {
        return $this->sbcert;
    }

    /**
     * Set sbcert value.
     * @param string $_sbcert the sbcert
     * @return string
     */
    public function setSbcert($_sbcert)
    {
        return $this->sbcert = $_sbcert;
    }

    /**
     * Get farmtype value.
     * @return string|null
     */
    public function getFarmtype()
    {
        return $this->farmtype;
    }

    /**
     * Set farmtype value.
     * @param string $_farmtype the farmtype
     * @return string
     */
    public function setFarmtype($_farmtype)
    {
        return $this->farmtype = $_farmtype;
    }

    /**
     * Get livestock value.
     * @return string|null
     */
    public function getLivestock()
    {
        return $this->livestock;
    }

    /**
     * Set livestock value.
     * @param string $_livestock the livestock
     * @return string
     */
    public function setLivestock($_livestock)
    {
        return $this->livestock = $_livestock;
    }

    /**
     * Get wild_life value.
     * @return string|null
     */
    public function getWild_life()
    {
        return $this->wild_life;
    }

    /**
     * Set wild_life value.
     * @param string $_wild_life the wild_life
     * @return string
     */
    public function setWild_life($_wild_life)
    {
        return $this->wild_life = $_wild_life;
    }

    /**
     * Get produce value.
     * @return string|null
     */
    public function getProduce()
    {
        return $this->produce;
    }

    /**
     * Set produce value.
     * @param string $_produce the produce
     * @return string
     */
    public function setProduce($_produce)
    {
        return $this->produce = $_produce;
    }

    /**
     * Get outbuild value.
     * @return string|null
     */
    public function getOutbuild()
    {
        return $this->outbuild;
    }

    /**
     * Set outbuild value.
     * @param string $_outbuild the outbuild
     * @return string
     */
    public function setOutbuild($_outbuild)
    {
        return $this->outbuild = $_outbuild;
    }

    /**
     * Get water_holes value.
     * @return string|null
     */
    public function getWater_holes()
    {
        return $this->water_holes;
    }

    /**
     * Set water_holes value.
     * @param string $_water_holes the water_holes
     * @return string
     */
    public function setWater_holes($_water_holes)
    {
        return $this->water_holes = $_water_holes;
    }

    /**
     * Get lodges value.
     * @return string|null
     */
    public function getLodges()
    {
        return $this->lodges;
    }

    /**
     * Set lodges value.
     * @param string $_lodges the lodges
     * @return string
     */
    public function setLodges($_lodges)
    {
        return $this->lodges = $_lodges;
    }

    /**
     * Get farm_imp value.
     * @return string|null
     */
    public function getFarm_imp()
    {
        return $this->farm_imp;
    }

    /**
     * Set farm_imp value.
     * @param string $_farm_imp the farm_imp
     * @return string
     */
    public function setFarm_imp($_farm_imp)
    {
        return $this->farm_imp = $_farm_imp;
    }

    /**
     * Get compounds value.
     * @return string|null
     */
    public function getCompounds()
    {
        return $this->compounds;
    }

    /**
     * Set compounds value.
     * @param string $_compounds the compounds
     * @return string
     */
    public function setCompounds($_compounds)
    {
        return $this->compounds = $_compounds;
    }

    /**
     * Get oneline value.
     * @return string|null
     */
    public function getOneline()
    {
        return $this->oneline;
    }

    /**
     * Set oneline value.
     * @param string $_oneline the oneline
     * @return string
     */
    public function setOneline($_oneline)
    {
        return $this->oneline = $_oneline;
    }

    /**
     * Get bidgroup value.
     * @return string|null
     */
    public function getBidgroup()
    {
        return $this->bidgroup;
    }

    /**
     * Set bidgroup value.
     * @param string $_bidgroup the bidgroup
     * @return string
     */
    public function setBidgroup($_bidgroup)
    {
        return $this->bidgroup = $_bidgroup;
    }

    /**
     * Get AgentData value.
     * @return TivvitStructArrayOfAgentInd|null
     */
    public function getAgentData()
    {
        return $this->AgentData;
    }

    /**
     * Set AgentData value.
     * @param TivvitStructArrayOfAgentInd $_agentData the AgentData
     * @return TivvitStructArrayOfAgentInd
     */
    public function setAgentData($_agentData)
    {
        return $this->AgentData = $_agentData;
    }

    /**
     * Get sellerdata value.
     * @return TivvitStructArrayOfSeller|null
     */
    public function getSellerdata()
    {
        return $this->sellerdata;
    }

    /**
     * Set sellerdata value.
     * @param TivvitStructArrayOfSeller $_sellerdata the sellerdata
     * @return TivvitStructArrayOfSeller
     */
    public function setSellerdata($_sellerdata)
    {
        return $this->sellerdata = $_sellerdata;
    }

    /**
     * Get picdata value.
     * @return TivvitStructArrayOfPicture|null
     */
    public function getPicdata()
    {
        return $this->picdata;
    }

    /**
     * Set picdata value.
     * @param TivvitStructArrayOfPicture $_picdata the picdata
     * @return TivvitStructArrayOfPicture
     */
    public function setPicdata($_picdata)
    {
        return $this->picdata = $_picdata;
    }

    /**
     * Get xnum value.
     * @return string|null
     */
    public function getXnum()
    {
        return $this->xnum;
    }

    /**
     * Set xnum value.
     * @param string $_xnum the xnum
     * @return string
     */
    public function setXnum($_xnum)
    {
        return $this->xnum = $_xnum;
    }

    /**
     * Get CUR value.
     * @return string|null
     */
    public function getCUR()
    {
        return $this->CUR;
    }

    /**
     * Set CUR value.
     * @param string $_cUR the CUR
     * @return string
     */
    public function setCUR($_cUR)
    {
        return $this->CUR = $_cUR;
    }

    /**
     * Get UniqueID value.
     * @return string|null
     */
    public function getUniqueID()
    {
        return $this->UniqueID;
    }

    /**
     * Set UniqueID value.
     * @param string $_uniqueID the UniqueID
     * @return string
     */
    public function setUniqueID($_uniqueID)
    {
        return $this->UniqueID = $_uniqueID;
    }

    /**
     * Get Rooms value.
     * @return TivvitStructArrayOfRooms|null
     */
    public function getRooms()
    {
        return $this->Rooms;
    }

    /**
     * Set Rooms value.
     * @param TivvitStructArrayOfRooms $_rooms the Rooms
     * @return TivvitStructArrayOfRooms
     */
    public function setRooms($_rooms)
    {
        return $this->Rooms = $_rooms;
    }

    /**
     * Get onshow value.
     * @return TivvitStructArrayOfOnshow|null
     */
    public function getOnshow()
    {
        return $this->onshow;
    }

    /**
     * Set onshow value.
     * @param TivvitStructArrayOfOnshow $_onshow the onshow
     * @return TivvitStructArrayOfOnshow
     */
    public function setOnshow($_onshow)
    {
        return $this->onshow = $_onshow;
    }

    /**
     * Get auction value.
     * @return TivvitStructAuction|null
     */
    public function getAuction()
    {
        return $this->auction;
    }

    /**
     * Set auction value.
     * @param TivvitStructAuction $_auction the auction
     * @return TivvitStructAuction
     */
    public function setAuction($_auction)
    {
        return $this->auction = $_auction;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructProperty
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
