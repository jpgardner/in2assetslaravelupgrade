<?php

namespace App\Http\Controllers\Tivvit\Property\Rem;

use App\Http\Controllers\Tivvit\Search\Type\TivvitEnumSearchType;

/**
 * File for class TivvitStructPropertyRem.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructPropertyRem originally named PropertyRem
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructPropertyRem extends TivvitWsdlClass
{
    /**
     * The searchtype
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumSearchType
     */
    public $searchtype;

    /**
     * The bid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bid;

    /**
     * The pnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pnum;

    /**
     * The webref
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $webref;

    /**
     * The extref
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $extref;

    /**
     * Constructor method for PropertyRem.
     * @see parent::__construct()
     * @param TivvitEnumSearchType $_searchtype
     * @param string $_bid
     * @param string $_pnum
     * @param string $_webref
     * @param string $_extref
     * @return TivvitStructPropertyRem
     */
    public function __construct($_searchtype, $_bid = null, $_pnum = null, $_webref = null, $_extref = null)
    {
        parent::__construct(['searchtype' => $_searchtype, 'bid' => $_bid, 'pnum' => $_pnum, 'webref' => $_webref, 'extref' => $_extref], false);
    }

    /**
     * Get searchtype value.
     * @return TivvitEnumSearchType
     */
    public function getSearchtype()
    {
        return $this->searchtype;
    }

    /**
     * Set searchtype value.
     * @uses TivvitEnumSearchType::valueIsValid()
     * @param TivvitEnumSearchType $_searchtype the searchtype
     * @return TivvitEnumSearchType
     */
    public function setSearchtype($_searchtype)
    {
        if (! TivvitEnumSearchType::valueIsValid($_searchtype)) {
            return false;
        }

        return $this->searchtype = $_searchtype;
    }

    /**
     * Get bid value.
     * @return string|null
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set bid value.
     * @param string $_bid the bid
     * @return string
     */
    public function setBid($_bid)
    {
        return $this->bid = $_bid;
    }

    /**
     * Get pnum value.
     * @return string|null
     */
    public function getPnum()
    {
        return $this->pnum;
    }

    /**
     * Set pnum value.
     * @param string $_pnum the pnum
     * @return string
     */
    public function setPnum($_pnum)
    {
        return $this->pnum = $_pnum;
    }

    /**
     * Get webref value.
     * @return string|null
     */
    public function getWebref()
    {
        return $this->webref;
    }

    /**
     * Set webref value.
     * @param string $_webref the webref
     * @return string
     */
    public function setWebref($_webref)
    {
        return $this->webref = $_webref;
    }

    /**
     * Get extref value.
     * @return string|null
     */
    public function getExtref()
    {
        return $this->extref;
    }

    /**
     * Set extref value.
     * @param string $_extref the extref
     * @return string
     */
    public function setExtref($_extref)
    {
        return $this->extref = $_extref;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructPropertyRem
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
