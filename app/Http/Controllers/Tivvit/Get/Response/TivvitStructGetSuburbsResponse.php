<?php

namespace App\Http\Controllers\Tivvit\Get\Response;

/**
 * File for class TivvitStructGetSuburbsResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbsResponse originally named GetSuburbsResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbsResponse extends TivvitWsdlClass
{
    /**
     * The GetSuburbsResult.
     * @var TivvitStructGetSuburbsResult
     */
    public $GetSuburbsResult;

    /**
     * Constructor method for GetSuburbsResponse.
     * @see parent::__construct()
     * @param TivvitStructGetSuburbsResult $_getSuburbsResult
     * @return TivvitStructGetSuburbsResponse
     */
    public function __construct($_getSuburbsResult = null)
    {
        parent::__construct(['GetSuburbsResult' => $_getSuburbsResult], false);
    }

    /**
     * Get GetSuburbsResult value.
     * @return TivvitStructGetSuburbsResult|null
     */
    public function getGetSuburbsResult()
    {
        return $this->GetSuburbsResult;
    }

    /**
     * Set GetSuburbsResult value.
     * @param TivvitStructGetSuburbsResult $_getSuburbsResult the GetSuburbsResult
     * @return TivvitStructGetSuburbsResult
     */
    public function setGetSuburbsResult($_getSuburbsResult)
    {
        return $this->GetSuburbsResult = $_getSuburbsResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbsResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
