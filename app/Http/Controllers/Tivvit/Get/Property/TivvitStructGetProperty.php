<?php

namespace App\Http\Controllers\Tivvit\Get\Property;

use App\Http\Controllers\Tivvit\Search\Type\TivvitEnumSearchType;

/**
 * File for class TivvitStructGetProperty.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetProperty originally named GetProperty
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetProperty extends TivvitWsdlClass
{
    /**
     * The ReftoUse
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumSearchType
     */
    public $ReftoUse;

    /**
     * The refno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $refno;

    /**
     * Constructor method for GetProperty.
     * @see parent::__construct()
     * @param TivvitEnumSearchType $_reftoUse
     * @param string $_refno
     * @return TivvitStructGetProperty
     */
    public function __construct($_reftoUse, $_refno = null)
    {
        parent::__construct(['ReftoUse' => $_reftoUse, 'refno' => $_refno], false);
    }

    /**
     * Get ReftoUse value.
     * @return TivvitEnumSearchType
     */
    public function getReftoUse()
    {
        return $this->ReftoUse;
    }

    /**
     * Set ReftoUse value.
     * @uses TivvitEnumSearchType::valueIsValid()
     * @param TivvitEnumSearchType $_reftoUse the ReftoUse
     * @return TivvitEnumSearchType
     */
    public function setReftoUse($_reftoUse)
    {
        if (! TivvitEnumSearchType::valueIsValid($_reftoUse)) {
            return false;
        }

        return $this->ReftoUse = $_reftoUse;
    }

    /**
     * Get refno value.
     * @return string|null
     */
    public function getRefno()
    {
        return $this->refno;
    }

    /**
     * Set refno value.
     * @param string $_refno the refno
     * @return string
     */
    public function setRefno($_refno)
    {
        return $this->refno = $_refno;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetProperty
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
