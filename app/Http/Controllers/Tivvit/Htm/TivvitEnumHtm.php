<?php

namespace App\Http\Controllers\Tivvit\Htm;

/**
 * File for class TivvitEnumHtm.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitEnumHtm originally named htm
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitEnumHtm extends TivvitWsdlClass
{
    /**
     * Constant for value 'None'.
     * @return string 'None'
     */
    const VALUE_NONE = 'None';

    /**
     * Constant for value 'Auctions'.
     * @return string 'Auctions'
     */
    const VALUE_AUCTIONS = 'Auctions';

    /**
     * Constant for value 'Business'.
     * @return string 'Business'
     */
    const VALUE_BUSINESS = 'Business';

    /**
     * Constant for value 'Commercial'.
     * @return string 'Commercial'
     */
    const VALUE_COMMERCIAL = 'Commercial';

    /**
     * Constant for value 'Farm'.
     * @return string 'Farm'
     */
    const VALUE_FARM = 'Farm';

    /**
     * Constant for value 'SmallHoldings'.
     * @return string 'SmallHoldings'
     */
    const VALUE_SMALLHOLDINGS = 'SmallHoldings';

    /**
     * Constant for value 'HolidayHomes'.
     * @return string 'HolidayHomes'
     */
    const VALUE_HOLIDAYHOMES = 'HolidayHomes';

    /**
     * Constant for value 'HomeOffice'.
     * @return string 'HomeOffice'
     */
    const VALUE_HOMEOFFICE = 'HomeOffice';

    /**
     * Constant for value 'Industrial'.
     * @return string 'Industrial'
     */
    const VALUE_INDUSTRIAL = 'Industrial';

    /**
     * Constant for value 'Residential'.
     * @return string 'Residential'
     */
    const VALUE_RESIDENTIAL = 'Residential';

    /**
     * Constant for value 'RetirementVillages'.
     * @return string 'RetirementVillages'
     */
    const VALUE_RETIREMENTVILLAGES = 'RetirementVillages';

    /**
     * Constant for value 'VacantLand'.
     * @return string 'VacantLand'
     */
    const VALUE_VACANTLAND = 'VacantLand';

    /**
     * Constant for value 'FractionalProperty'.
     * @return string 'FractionalProperty'
     */
    const VALUE_FRACTIONALPROPERTY = 'FractionalProperty';

    /**
     * Return true if value is allowed.
     * @uses TivvitEnumHtm::VALUE_NONE
     * @uses TivvitEnumHtm::VALUE_AUCTIONS
     * @uses TivvitEnumHtm::VALUE_BUSINESS
     * @uses TivvitEnumHtm::VALUE_COMMERCIAL
     * @uses TivvitEnumHtm::VALUE_FARM
     * @uses TivvitEnumHtm::VALUE_SMALLHOLDINGS
     * @uses TivvitEnumHtm::VALUE_HOLIDAYHOMES
     * @uses TivvitEnumHtm::VALUE_HOMEOFFICE
     * @uses TivvitEnumHtm::VALUE_INDUSTRIAL
     * @uses TivvitEnumHtm::VALUE_RESIDENTIAL
     * @uses TivvitEnumHtm::VALUE_RETIREMENTVILLAGES
     * @uses TivvitEnumHtm::VALUE_VACANTLAND
     * @uses TivvitEnumHtm::VALUE_FRACTIONALPROPERTY
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value, [self::VALUE_NONE, self::VALUE_AUCTIONS, self::VALUE_BUSINESS, self::VALUE_COMMERCIAL, self::VALUE_FARM, self::VALUE_SMALLHOLDINGS, self::VALUE_HOLIDAYHOMES, self::VALUE_HOMEOFFICE, self::VALUE_INDUSTRIAL, self::VALUE_RESIDENTIAL, self::VALUE_RETIREMENTVILLAGES, self::VALUE_VACANTLAND, self::VALUE_FRACTIONALPROPERTY]);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
