<?php

namespace App\Http\Controllers\Tivvit\Features;

/**
 * File for class TivvitStructFeatures.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructFeatures originally named Features
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructFeatures extends TivvitWsdlClass
{
    /**
     * The heading
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $heading;

    /**
     * The feature
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $feature;

    /**
     * Constructor method for Features.
     * @see parent::__construct()
     * @param string $_heading
     * @param string $_feature
     * @return TivvitStructFeatures
     */
    public function __construct($_heading = null, $_feature = null)
    {
        parent::__construct(['heading' => $_heading, 'feature' => $_feature], false);
    }

    /**
     * Get heading value.
     * @return string|null
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * Set heading value.
     * @param string $_heading the heading
     * @return string
     */
    public function setHeading($_heading)
    {
        return $this->heading = $_heading;
    }

    /**
     * Get feature value.
     * @return string|null
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * Set feature value.
     * @param string $_feature the feature
     * @return string
     */
    public function setFeature($_feature)
    {
        return $this->feature = $_feature;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructFeatures
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
