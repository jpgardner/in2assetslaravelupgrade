<?php

namespace App\Http\Controllers\Tivvit\Room\Type;

/**
 * File for class TivvitEnumRoomType.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitEnumRoomType originally named RoomType
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitEnumRoomType extends TivvitWsdlClass
{
    /**
     * Constant for value 'BEDROOM'.
     * @return string 'BEDROOM'
     */
    const VALUE_BEDROOM = 'BEDROOM';

    /**
     * Constant for value 'BATHROOM'.
     * @return string 'BATHROOM'
     */
    const VALUE_BATHROOM = 'BATHROOM';

    /**
     * Constant for value 'DOMISTIC'.
     * @return string 'DOMISTIC'
     */
    const VALUE_DOMISTIC = 'DOMISTIC';

    /**
     * Constant for value 'FLAT'.
     * @return string 'FLAT'
     */
    const VALUE_FLAT = 'FLAT';

    /**
     * Constant for value 'GARAGE'.
     * @return string 'GARAGE'
     */
    const VALUE_GARAGE = 'GARAGE';

    /**
     * Constant for value 'KITCHEN'.
     * @return string 'KITCHEN'
     */
    const VALUE_KITCHEN = 'KITCHEN';

    /**
     * Constant for value 'PARKING'.
     * @return string 'PARKING'
     */
    const VALUE_PARKING = 'PARKING';

    /**
     * Constant for value 'POOL'.
     * @return string 'POOL'
     */
    const VALUE_POOL = 'POOL';

    /**
     * Return true if value is allowed.
     * @uses TivvitEnumRoomType::VALUE_BEDROOM
     * @uses TivvitEnumRoomType::VALUE_BATHROOM
     * @uses TivvitEnumRoomType::VALUE_DOMISTIC
     * @uses TivvitEnumRoomType::VALUE_FLAT
     * @uses TivvitEnumRoomType::VALUE_GARAGE
     * @uses TivvitEnumRoomType::VALUE_KITCHEN
     * @uses TivvitEnumRoomType::VALUE_PARKING
     * @uses TivvitEnumRoomType::VALUE_POOL
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value, [self::VALUE_BEDROOM, self::VALUE_BATHROOM, self::VALUE_DOMISTIC, self::VALUE_FLAT, self::VALUE_GARAGE, self::VALUE_KITCHEN, self::VALUE_PARKING, self::VALUE_POOL]);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
