<?php

namespace App\Http\Controllers\Tivvit\Auction;

/**
 * File for class TivvitStructAuction.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructAuction originally named Auction
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructAuction extends TivvitWsdlClass
{
    /**
     * The venue
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $venue;

    /**
     * The description
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $description;

    /**
     * The lots
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $lots;

    /**
     * The date
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $date;

    /**
     * The time
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $time;

    /**
     * Constructor method for Auction.
     * @see parent::__construct()
     * @param string $_venue
     * @param string $_description
     * @param string $_lots
     * @param string $_date
     * @param string $_time
     * @return TivvitStructAuction
     */
    public function __construct($_venue = null, $_description = null, $_lots = null, $_date = null, $_time = null)
    {
        parent::__construct(['venue' => $_venue, 'description' => $_description, 'lots' => $_lots, 'date' => $_date, 'time' => $_time], false);
    }

    /**
     * Get venue value.
     * @return string|null
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * Set venue value.
     * @param string $_venue the venue
     * @return string
     */
    public function setVenue($_venue)
    {
        return $this->venue = $_venue;
    }

    /**
     * Get description value.
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description value.
     * @param string $_description the description
     * @return string
     */
    public function setDescription($_description)
    {
        return $this->description = $_description;
    }

    /**
     * Get lots value.
     * @return string|null
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * Set lots value.
     * @param string $_lots the lots
     * @return string
     */
    public function setLots($_lots)
    {
        return $this->lots = $_lots;
    }

    /**
     * Get date value.
     * @return string|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date value.
     * @param string $_date the date
     * @return string
     */
    public function setDate($_date)
    {
        return $this->date = $_date;
    }

    /**
     * Get time value.
     * @return string|null
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set time value.
     * @param string $_time the time
     * @return string
     */
    public function setTime($_time)
    {
        return $this->time = $_time;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructAuction
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
