<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use In2Assets\Core\CommandBus;
use In2Assets\Forms\RegistrationForm;
use In2Assets\Forms\SignInForm;
use In2Assets\Registration\RegisterUserCommand;

class APIController extends Controller
{
    /**
     * @var SignInForm
     */
    private $signInForm;

    use CommandBus;

    private $registrationForm;

    public function __construct(SignInForm $signInForm, registrationForm $registrationForm)
    {
        $this->signInForm = $signInForm;
        $this->registrationForm = $registrationForm;
    }

    public function api($type, $id = 0)
    {
        switch ($type) {
            case 'auction-list':
                $query = 'select p.reference,p.lead_image,p.long_descrip,p.short_descrip, p.complex_number,
											  p.complex_name, p.street_number,p.street_address,p.postcode,
											  p.suburb_town_city, p.area, p.province, p.country, p.x_coord, p.y_coord,
											  u.firstname, u.lastname, u.cellnumber, pa.start_date as auction_start_date,
											  pa.auction_name,pa.province as auction_province,  pa.area as auction_area,
											  pa.suburb_town_city as auction_suburb, pa.street_address as auction_street_address,
											  pa.start_date as auction_start_date, pa.id as auction_id
											  from auction a
											  inner join physical_auctions pa on a.physical_auction = pa.id
											  inner join property p on p.id = a.property_id
											  inner join vetting v on v.property_id = p.id
											  inner join users u on u.id = v.rep_id_number
											  left join commercial c on c.property_id = p.id
											  left join residential r on r.property_id = p.id
											  left join farm f on f.property_id = p.id
											where a.start_date > CURDATE()
											order by a.start_date';
                $auction_list = DB::select($query);

                return json_encode($auction_list);

                break;

            case 'images':
                $images = DB::select("select u.title, u.file, u.type, u.extension
								  from property p
								  left join uploads u on u.link = p.id and
								  u.type = 'image' or
								  u.link = p.id and
								  u.type = 'lead_image'
								  where p.reference = ?", [$id]);

                return json_encode($images);

                break;

            case 'property-details':
                $property_details = DB::select('select p.reference,p.lead_image,p.long_descrip,p.short_descrip, p.complex_number, p.complex_name,
  												p.street_number,p.street_address,p.postcode, p.suburb_town_city, p.area,
  												p.province, p.country,p.x_coord, p.y_coord, u.firstname, u.lastname,
  												u.cellnumber
											from  property p
											inner join vetting v on v.property_id = p.id
											inner join users u on u.id = v.rep_id_number
											left join commercial c on c.property_id = p.id
											left join residential r on r.property_id = p.id
											left join farm f on f.property_id = p.id
											where p.reference = ?', [$id]);

                return json_encode($property_details);

                break;
        }
    }

    public function authenticate($email, $password)
    {
        //Validate
        $formData = ['email' => $email, 'password' => $password];
        $this->signInForm->validate($formData);
        if (Auth::attempt($formData)) {
            $user_details = DB::select('select u.title, u.firstname, u.lastname, u.email from users u
  										where u.email= ? ', [$email]);

            return json_encode($user_details);
        } else {
            return 'Invalid User Authentication';
        }
    }

    public function register($email, $password, $firstname, $lastname)
    {
        $allow_contact = 1;

        try {
            $user = $this->execute(
                new RegisterUserCommand('', $firstname, $lastname, '', '', '', '', '', '', '', $email, $password,
                    '0', '', '', '', '', $allow_contact)
            );
            //Authenticate User
            Auth::login($user);

            Auth::user()->roles()->attach(11);

            Auth::logout();

            return 'User Successfully registered';
        } catch (\Illuminate\Database\QueryException $e) {
            //			App::make('ErrorsController')->log_error($e);
            $array = explode(' ', $e->errorInfo[2]);
            if ($array[0] == 'Duplicate') {
                return 'Email already exists';
            } else {
                return $e->errorInfo[2];
            }
        }
    }

    public function changePass($email, $oldPassword, $newPassword, $newPasswordConfirm)
    {
        $authenticate = $this->authenticate($email, $oldPassword);

        if ($authenticate == 'Invalid User Authentication') {
            return 'Old Password is incorrect';
        } elseif ($newPassword !== $newPasswordConfirm) {
            return 'New passwords do not match';
        } else {
            $hashPass = Hash::make($newPassword);
            DB::update("UPDATE `users` SET `password` = '$hashPass'
  										where email= ? ", [$email]);

            return 'Success';
        }
    }

    public function forgotPass($email)
    {
        $emailArray = ['email' => $email];
        switch ($response = Password::remind($emailArray, function ($message) {
            $message->subject('Password Reminder');
        })) {
            case Password::INVALID_USER:

                return 'Invalid email';

            case Password::REMINDER_SENT:

                return 'Password reset sent';

        }
    }
}
