<?php

namespace App\Http\Controllers;

use App\In2Assets\Mailers\UserMailer;
use App\In2Assets\Users\User;
use App\Mail\newLead;
use App\Models\Actual;
use App\Models\Commercial;
use App\Models\Commission;
use App\Models\Contact;
use App\Models\Farm;
use App\Models\Lead;
use App\Models\Property;
use App\Models\PropNotes;
use App\Models\Residential;
use App\Models\Targets;
use App\Models\Vetting;
use DateInterval;
use DatePeriod;
use DateTime;
use Dompdf\Dompdf;
use Excel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;

class LeadsController extends Controller
{
    protected $lead;

    public function __construct(User $user, PropNotes $prop_notes, Lead $lead, Contact $contact, Commission $commission,
                                Targets $targets, Property $property, Residential $residential, Commercial $commercial,
                                Farm $farm, Vetting $vetting, Actual $actual, UserMailer $mailer)
    {
        $this->mailer = $mailer;
        $this->user = $user;
        $this->prop_notes = $prop_notes;
        $this->lead = $lead;
        $this->contact = $contact;
        $this->commission = $commission;
        $this->targets = $targets;
        $this->property = $property;
        $this->residential = $residential;
        $this->commercial = $commercial;
        $this->farm = $farm;
        $this->vetting = $vetting;
        $this->actual = $actual;
    }

    /**
     * Display a listing of the resource.
     * GET /leads.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /leads/create.
     *
     * @return Response
     */
    public function create()
    {
        $leads = DB::select('SELECT l.id FROM leads l WHERE l.created_at = l.updated_at AND l.bdm_allocated = '.Auth::user()->id);
        foreach ($leads as $lead) {
            $query = "SELECT p.note FROM prop_notes p
                  WHERE p.lead_id = $lead->id";
            $previous_notes = DB::select($query);
            if (empty($previous_notes)) {
                $contacts = $this->contact
                    ->select('lead_contacts.id')
                    ->whereLead_id($lead->id)->get();
                $users = $this->user->join('lead_user', 'user_id', '=', 'lead_user.user_id')->where('users.id', '=', 'lead_user.user_id')
                    ->where('lead_user.lead_id', '=', $lead->id)->get();
                if ($contacts->count() == '0' && $users->count() == '0') {
                    $allCommissions = $this->commission
                        ->select('commissions.id')
                        ->whereLead_id($lead->id)->get();
                    if ($allCommissions->count() == '0') {
                        $uploadQuery = "SELECT * FROM uploads u WHERE u.type = 'lead' AND u.link =".$lead->id;
                        $images = DB::select($uploadQuery);
                        if (empty($images)) {
                            return redirect()->route('leads.edit', ['id' => $lead->id]);
                        }
                    }
                }
            }
        }

        $this->lead = new Lead();
        $this->lead->bdm_allocated = Auth::user()->id;

        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        $this->lead->reference = $reference;

        $this->lead->save();

        $bdm = $this->user->whereId(Request::get('bdm_referred'))->first();

        if (Request::get('refer_to_bdm')) {
            if (Request::get('bdm_referred') != $this->lead['bdm_allocated']) {
                $this->prop_notes->note = 'Lead Referred to '.$bdm->firstname.' '.$bdm->lastname;
                $this->prop_notes->lead_id = $this->lead->id;
                $this->prop_notes->users_id = Auth::user()->id;
                $this->prop_notes->save();

                $url = 'https://www.in2assets.co.za/leads/'.$this->lead->id.'/edit';
                $bdm_name = $bdm->firstname.' '.$bdm->lastnamep;
                $bdm_email = $bdm->email;

                $subject = 'You have received a new lead';
                $name = 'Dear '.$bdm_name.',<br>';

                $emailData = "<h4>You have received a new lead <a href='".$url."'>Click here</a>.</h4><p>";

                Mail::to($bdm_email)
                    ->cc('tmunsamy@in2assets.com')
                    ->send(new newLead($subject, $name, $emailData, $bdm_email));
                Flash::message('An email has been sent to '.$bdm_email);
            }
        }

        $propertiesDB = $this->property->select('reference', 'street_number', 'street_address')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
            $address[] = $reference->street_number.' - '.$reference->street_address;
        }
        $references = implode('", "', $references);
        $address = implode('", "', $address);

        DB::table('lead_deed_details')->insert(['lead_id' => $this->lead->id]);

        return redirect()->route('leads.edit', ['id' => $this->lead->id, 'address' => $address, 'references' => $references]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /leads.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Request::only(['lead_type', 'lead_ref_type', 'lead_ref', 'lead_prop_trans_type', 'bdm_allocated']);

        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        $input['reference'] = $reference;

        $this->lead->fill($input)->save();

        $this->prop_notes->note = Request::get('notes');
        $this->prop_notes->lead_id = $this->lead->id;
        $this->prop_notes->users_id = Auth::user()->id;
        $this->prop_notes->save();
    }

    /**
     * Display the specified resource.
     * GET /leads/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /leads/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        //---------------------Checks if someone is editing redirects back to table
        // ---------------------been editing for longer than 1 hour deletes that session and allows new user

        $lead_in_use = DB::select('SELECT * FROM lead_session WHERE lead_id = '.$id);
        if (! empty($lead_in_use)) {
            if ($lead_in_use[0]->user_id != Auth::user()->id) {
                if (strtotime($lead_in_use[0]->created_at) <= strtotime('-1 hours')) {
                    DB::delete('DELETE FROM lead_session WHERE lead_id ='.$id);
                } else {
                    return redirect()->route('user.leads', Auth::user()->id);
                }
            }
        }
        //----------SESSION for locking leads

        $lead_session = DB::select('SELECT * FROM lead_session WHERE user_id = '.Auth::user()->id);
        if (! empty($lead_session)) {
            DB::update('UPDATE lead_session SET lead_id = '.$id.' WHERE user_id = '.Auth::user()->id);
        } else {
            DB::insert('INSERT INTO lead_session (user_id, lead_id) VALUES ('.Auth::user()->id.', '.$id.')');
        }

        $lead_types = DB::table('lead_types')->where('active', '1')->pluck('type', 'id');
        $lead_types->put('', 'None');
        $lead_ref_types = DB::table('lead_ref_types')->where('active', '1')->pluck('type', 'id');
        $lead_ref_types->put('', 'None');
        $lead_prop_trans_types = DB::table('lead_prop_trans_types')->where('active', '1')->pluck('type', 'id');
        $lead_prop_trans_types->put('', 'None');
        $company_types = DB::table('company_types')->where('active', '1')->pluck('type', 'id');
        $company_types->put('', 'None');

        if (Auth::user()->hasRole('LeadReviewer')) {
            $agents = $this->user
                ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
                ->whereHas('roles', function ($q) {
                    $q->where('name', 'Agent');
                })->pluck('fullname', 'id');

            $agents->map(function ($agent) {
                $extra_id = Auth::user()->id;
                $agent["$extra_id"] = Auth::user()->firstname.' '.Auth::user()->lastname;
                $agent['0'] = 'None';

                return $agent;
            });
        } else {
            $agents = Auth::user()->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
                ->pluck('fullname', 'id');
            $agents->map(function ($agent) {
                $agent['0'] = 'None';

                return $agent;
            });
        }

        $allAgents = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->pluck('fullname', 'id');

        $lead = $this->lead->whereId($id)->withTrashed()->first();

        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name
                  FROM prop_notes p
                  LEFT JOIN users u ON u.id = p.users_id
                  WHERE p.lead_id = $id
                  ORDER BY p.created_at DESC";

        $previous_notes = DB::select($query);

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $contacts = $this->contact
            ->select('lead_contacts.*', 'tags.description', 'company_types.type')
            ->leftJoin('tags', 'lead_contacts.role_id', '=', 'tags.id')
            ->leftJoin('company_types', 'lead_contacts.company_type', '=', 'company_types.id')
            ->whereLead_id($id)->get();

        $user = $this->user->whereId(Auth::user()->id)->first();
        $files = $user->files()->where('link', '=', $id)
            ->where('type', '=', 'image')->where('class', '=', 'lead')
            ->with('user')->orderBy('uploads.position')
            ->get();

        $query = "SELECT SUM(c.commission) AS total
                    FROM commissions c
                    INNER JOIN commission_types ct
                    ON c.type = ct.id
                    WHERE (ct.type = 'BDM' OR ct.type = 'SLA') AND c.lead_id = $id
                    AND c.deleted_at IS NULL";
        $commission = DB::select($query);

        $query2 = "SELECT SUM(c.commission) AS total
                    FROM commissions c
                    INNER JOIN commission_types ct
                    ON c.type = ct.id
                    WHERE ct.type = 'Seller' AND c.lead_id = $id
                    AND c.deleted_at IS NULL";
        $rebate = DB::select($query2);

        $allCommissions = $this->commission
            ->select('commissions.id', 'commissions.name', 'commissions.commission', 'commissions.created_at',
                'commissions.approved_date', 'users.firstname', 'commission_types.type')
            ->join('commission_types', 'commissions.type', '=', 'commission_types.id')
            ->leftJoin('users', 'commissions.approved_by', '=', 'users.id')
            ->whereLead_id($id)->get();

        if (! $deed_details = DB::table('lead_deed_details')->whereLead_id($id)->first()) {
            DB::table('lead_deed_details')->insert(['lead_id' => $id]);
            $deed_details = DB::table('lead_deed_details')->whereLead_id($id)->first();
        }

        $propertiesDB = $this->property->select('reference', 'street_number', 'street_address')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
            $address[] = $reference->street_number.' - '.$reference->street_address;
        }
        $references = implode('", "', $references);
        $address = implode('", "', $address);

        if ($lead->property_id != null) {
            $property = $this->property->whereId($lead->property_id)->withTrashed()->first();
            $lead->country = 'South Africa';
            $lead->province = $property->province;
            $lead->area = $property->area;
            $lead->suburb_town_city = $property->suburb_town_city;
            $lead->street_number = $property->street_number;
            $lead->street_name = $property->street_address;
            $lead->complex_number = $property->complex_number;
            $lead->complex_name = $property->complex_name;
            $lead->property_type = strtolower($property->property_type);
            $lead->x_coord = $property->x_coord;
            $lead->y_coord = $property->y_coord;
            $lead->prop_ref = $property->reference;
        }

        $usersDB = $this->user->select('firstname', 'lastname', 'email', 'cellnumber')->get();
        foreach ($usersDB as $user) {
            $emails[] = $user->email;
            $names[] = $user->firstname.' - '.$user->lastname;
            $tel_nums[] = $user->cellnumber;
        }
        $emails = implode('", "', $emails);
        $names = implode('", "', $names);
        $tel_nums = implode('", "', $tel_nums);

        $autotags = DB::select('SELECT name FROM tags');
        $autotag_array = [];
        foreach ($autotags as $autotag) {
            $autotag_array[] = strtoupper(trim($autotag->name));
        }

        return view::make('leads.edit', ['lead_types' => $lead_types, 'lead_ref_types' => $lead_ref_types,
            'lead_prop_trans_types' => $lead_prop_trans_types, 'company_types' => $company_types, 'agents' => $agents,
            'lead' => $lead, 'previous_notes' => $previous_notes, 'cities' => $cities, 'contacts' => $contacts,
            'files' => $files, 'commission' => $commission, 'rebate' => $rebate, 'allCommissions' => $allCommissions,
            'allAgents' => $allAgents, 'deed_details' => $deed_details, 'address' => $address, 'references' => $references,
            'emails' => $emails, 'names' => $names, 'tel_nums' => $tel_nums, 'autotag' => json_encode($autotag_array), ]);
    }

    public function endLeadEdit($lead_id)
    {
        DB::delete('DELETE FROM lead_session WHERE lead_id ='.$lead_id.' AND user_id = '.Auth::user()->id);
//
        return 'Done';
    }

    //follow up
    public function getUserLeads($user_id)
    {

//        $this->contactToUsers();

        DB::delete('DELETE FROM lead_session WHERE user_id = '.Auth::user()->id);

        $user = User::find($user_id);
        if ($user->hasRole('LeadReviewer')) {
            $leads = $this->lead
                ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
                ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                ->orderBy('created_at')->get();
        } else {
            $leads = $this->lead->whereBdm_allocated($user_id)
                ->orWhere(function ($q) {
                    $q->where('leads.refer_to_bdm', '=', 1)
                        ->where('leads.lead_ref', '=', Auth::user()->firstname.' '.Auth::user()->lastname)
                        ->where('leads.deleted_at', '=', null);
                })
                ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
                ->leftJoin('lead_ref_types', 'lead_ref_type', '=', 'lead_ref_types.id')
                ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                ->orderBy('created_at')->get();
        }

        $leads = $this->getLeadsbyDescription($leads);

        $agents = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })
            ->orderBy('firstname')
            ->pluck('fullname', 'id');

        $agents->map(function ($agent) {
            $extra_id = Auth::user()->id;
            $agent["$extra_id"] = Auth::user()->firstname.' '.Auth::user()->lastname;
            $agent['0'] = 'None';

            return $agent;
        });

        $lead_ref_types = DB::table('lead_ref_types')->where('active', '1')->orderBy('type')->pluck('type', 'id');

        $lead_ref_types->map(function ($lead_ref_type) {
            $lead_ref_type['0'] = 'None';

            return $lead_ref_type;
        });
        $contacts = DB::select('SELECT CONCAT(u.firstname, " ", u.lastname) AS name
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                GROUP BY name
                                UNION ALL
                                SELECT CONCAT(lc.firstname, " ", lc.lastname) AS name
                                FROM lead_contacts lc
                                INNER JOIN leads l ON lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                GROUP BY NAME');
        $contact_array = [];
        foreach ($contacts as $contact) {
            $contact_array[] = trim($contact->name);
        }

        $tagDB = DB::table('tags')->get();
        foreach ($tagDB as $tag) {
            $tags[] = $tag->name;
        }
        $tags = implode('", "', $tags);

        return view('leads.index', ['user' => $user, 'leads' => $leads, 'archived' => '', 'agents' => $agents,
            'lead_ref_types' => $lead_ref_types, 'contacts' => json_encode($contact_array), 'tags' => $tags,
            'current_tags' => [], ]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /leads/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $this->lead = $this->lead->whereId($id)->first();

        $bdm = $this->user->whereId(Request::get('bdm_referred'))->first();

        $input = Request::except(['notes', 'Suburb', 'contact_type', 'contact_firstname', 'contact_number', 'contact_email',
            'contact_lastname', 'leadDatatable_length', 'company_type', 'company_name', 'company_email', 'company_number',
            'contact_other', 'company_other', 'bdm_comm', 'bdm_percent', 'seller_rebate', 'seller_percent', 'sla_comm',
            'sla_percent', 'commDatatable_length', 'bdm_referred', 'erf_num', 'erf_type', 'erf_description', 'erf_size',
            'last_sold_price', 'title_deed_num', 'portion_num', 'company_reg_num', 'address', 'message', 'user', 'propos_date',
        ]);

        if (Request::get('refer_to_bdm')) {
            if (Request::get('bdm_referred') != $this->lead['bdm_allocated']) {
                $this->prop_notes->note = 'Lead Referred to '.$bdm->firstname.' '.$bdm->lastname;
                $this->prop_notes->lead_id = $this->lead->id;
                $this->prop_notes->users_id = Auth::user()->id;
                $this->prop_notes->save();

                $url = 'https://www.in2assets.co.za/leads/'.$id.'/edit';
                $bdm_name = $bdm->firstname.' '.$bdm->lastnamep;
                $bdm_email = $bdm->email;

                $subject = 'You have received a new lead';
                $name = 'Dear '.$bdm_name.',<br>';

                $emailData = "<h4>You have received a new lead <a href='".$url."'>Click here</a>.</h4><p>";

                Mail::to($bdm_email)
                    ->cc('tmunsamy@in2assets.com')
                    ->send(new newLead($subject, $name, $emailData, $bdm_email));

                Flash::message('An email has been sent to '.$bdm_email);

                $input['check_lead'] = null;
            }
        }

        if (Request::get('refer_to_bdm')) {
            $input['refer_to_bdm'] = 1;
        } else {
            $input['refer_to_bdm'] = 0;
        }

        $input['proposal_date'] = date('Y-m-d H:i:s', strtotime($input['proposal_date']));

        $this->lead->fill($input)->update();

        if (Request::get('notes') != '') {
            $this->prop_notes->note = Request::get('notes');
            $this->prop_notes->lead_id = $this->lead->id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();
        }

        if ($this->lead->refer_to_bdm == 1) {
            $this->lead->update(['bdm_allocated' => Request::get('bdm_referred')]);

            return redirect()->route('user.leads', ['id' => Auth::user()->id]);
        }

        DB::table('lead_deed_details')->whereLead_id($id)->update(['erf_num' => Request::get('erf_num'), 'erf_type' => Request::get('erf_type'),
            'erf_description' => Request::get('erf_description'), 'erf_size' => Request::get('erf_size'), 'last_sold_price' => Request::get('last_sold_price'),
            'title_deed_num' => Request::get('title_deed_num'), 'portion_num' => Request::get('portion_num'), ]);
        Flash::message('Lead has been updated');

        return redirect()->route('leads.edit', ['id' => $this->lead->id]);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /leads/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $lead = $this->lead->whereId($id)->first();

        $lead->delete();

        Flash::message('Lead has been deleted!');

        return redirect()->back();
    }

    public function saveNote()
    {
        if (Request::get('notes') != '') {
            $this->prop_notes->note = Request::get('notes');
            $this->prop_notes->lead_id = Request::get('lead_id');
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();
        }

        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name
                  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.lead_id = ".Request::get('lead_id').'
                  ORDER BY p.created_at DESC';

        $previous_notes = DB::select($query);

        return $previous_notes;
    }

    public function resetActivity()
    {
        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name
                  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.lead_id = ".Request::get('lead_id').'
                  ORDER BY p.created_at DESC';

        $previous_notes = DB::select($query);

        return $previous_notes;
    }

    public function saveContact()
    {
        $input = Request::all();

        if (is_numeric(Request::get('tel'))) {
            $tel = Request::get('tel');
        } else {
            $tel = '';
        }

        $lead = $this->lead->whereId($input['lead_id'])->first();

        if (! isset($input['company_type']) || $input['company_type'] == null) {
            $value = null;
            $tag = DB::table('tags')->where('name', '=', Request::get('contact_type'))->first();
            $name = strtoupper(Request::get('contact_type'));
            if ($tag == null) {
                $value = DB::table('tags')->insertGetId(
                    ['name' => $name, 'description' => $name, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
            } else {
                $value = $tag->id;
            }
        }

        if (Request::get('email') != '' && Request::get('firstname') != '') {
            $lastname = trim(Request::get('lastname'));
            if (trim(Request::get('lastname')) == '') {
                $lastname = '.';
            }
            $user_id = App::make(\App\Http\Controllers\RegistrationController::class)->storeLeadUser(Request::get('email'), Request::get('firstname'),
                $lastname, $tel, 11);
            $user = User::find($user_id);
            $this->mailer->sendWelcomeMessageToAddedUser($user);
            if (! DB::select('SELECT * FROM lead_user WHERE user_id = '.$user_id.' AND lead_id = '.$input['lead_id'].' AND role_id
             = '.$value)
            ) {
                DB::insert('INSERT INTO lead_user SET user_id = '.$user_id.', lead_id = '.$input['lead_id'].', role_id = '.
                    $value);
            }
            if ($lead->property_id != null) {
                if (! DB::table('property_user')->whereProperty_id($lead->property_id)->whereUser_id($user_id)->first()) {
                    DB::insert('INSERT INTO property_user SET property_id = '.$lead->property_id.', user_id = '.
                        $user_id.', role_id = '.$value);
                }
            }
            App::make(\App\Http\Controllers\UsersController::class)->addTag($user_id, $value);
        } else {
            $input['role_id'] = $value;
            $this->contact->fill($input)->save();
        }

        $this->addNewActual(Auth::user()->id, 'DB_Growth', 1);

        $contacts = DB::select('SELECT u.firstname, u.lastname, u.email, u.cellnumber AS tel, u.created_at, lu.role_id
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND lu.lead_id = '.$input['lead_id'].'
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                UNION ALL
                                SELECT lc.firstname, lc.lastname, lc.email, lc.tel, lc.created_at, lc.role_id
                                FROM lead_contacts lc
                                INNER JOIN leads l ON l.id = lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                AND l.id = '.$input['lead_id']);

        return $contacts;
    }

    public function updateContact($id)
    {
        $input = Request::all();
        $this->contact = $this->contact->whereId($id)->first();
        $lead = $this->lead->whereId(Request::get('lead_id'))->first();
        $value = null;
        $tag = DB::table('tags')->where('name', '=', Request::get('contact_type'))->first();
        $name = strtoupper(Request::get('contact_type'));
        if ($tag === null) {
            $value = DB::table('tags')->insertGetId(
                ['name' => $name, 'description' => $name, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );
        } else {
            $value = $tag->id;
        }
        if (Request::get('email') != '' && Request::get('firstname') != '') {
            $lastname = trim(Request::get('lastname'));
            if (trim(Request::get('lastname')) == '') {
                $lastname = '.';
            }
            $user_id = App::make(\App\Http\Controllers\RegistrationController::class)->storeLeadUser(Request::get('email'), Request::get('firstname'),
                $lastname, Request::get('tel'), 11);
            $user = User::find($user_id);
            $this->mailer->sendWelcomeMessageToAddedUser($user);
            if (! DB::select('SELECT * FROM lead_user WHERE user_id = '.$user_id.' AND lead_id = '.$input['lead_id'].' AND role_id
             = '.$value)
            ) {
                DB::insert('INSERT INTO lead_user SET user_id = '.$user_id.', lead_id = '.$input['lead_id'].', role_id = '.
                    $value);
            }
            if ($lead->property_id != null) {
                if (! DB::table('property_user')->whereProperty_id($lead->property_id)->whereUser_id($user_id)->first()) {
                    DB::insert('INSERT INTO property_user SET property_id = '.$lead->property_id.', user_id = '.
                        $user_id.', role_id = '.$value);
                }
            }
            App::make(\App\Http\Controllers\UsersController::class)->addTag($user_id, $value);

            $this->contact->delete();
        } else {
            $this->contact->fill($input)->update();
        }

        return 'Done';
    }

    public function getLeadContacts($lead_id)
    {
        $query = 'SELECT u.id, u.id AS user_id,  u.firstname, u.lastname, u.email, u.cellnumber AS tel, u.updated_at,
                  lu.lead_id, lu.role_id, NULL AS company_type, NULL AS company_other, "Other" AS contact_other
                  FROM users u
                  INNER JOIN lead_user lu ON u.id = lu.user_id
                  WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                  AND lu.lead_id = '.$lead_id.'
                  AND CONCAT(u.firstname, " ",u.lastname) <> " "
                  UNION ALL
                  SELECT lc.id, "None" AS user_id, lc.firstname, lc.lastname, lc.email, lc.tel, lc.updated_at, lc.lead_id,
                  lc.role_id, lc.company_type, lc.company_other, lc.contact_other
                  FROM lead_contacts lc
                  INNER JOIN leads l ON l.id = lc.lead_id
                  WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                  AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                  AND l.id = '.$lead_id;

        $contacts = DB::select($query);
        foreach ($contacts as $contact) {
            if ($contact->role_id >= 0) {
                $query = 'select name from tags where id = '.$contact->role_id;

                $tag = DB::select($query);

                if (empty($tag[0]->name)) {
                    $contact->tagname = 'NA';
                } else {
                    $contact->tagname = $tag[0]->name;
                }
            }
            if ($contact->role_id >= '0') {
                $contact->type = $contact->tagname;
            } elseif ($contact->company_type >= '0') {
                switch ($contact->company_type) {
                    case '1':
                        $contact->type = 'CC';

                        break;
                    case '2':
                        $contact->type = 'PTY';

                        break;
                    case '3':
                        $contact->type = 'Trust';

                        break;
                    case '7':
                        $contact->type = $contact->company_other;

                        break;
                    default:
                        $contact->type = 'None';

                        break;
                }
            }
            if (is_numeric($contact->user_id)) {
                $contact->actions = '<a href="/admin/users/edit/'.$contact->user_id.'" title="User Edit" >
                        <i class="i-circled i-light i-alt i-small fa fa-user"></i></a>
                        <a href="/leads/user/'.$contact->lead_id.'/'.$contact->user_id.'/'.$contact->role_id.
                    '/delete" title="Remove from lead" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                        ';
            } else {
                $contact->actions = '<a href="/contact/'.$contact->id.'/edit" title="Edit" >
                    <i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                        <a href="/contact/'.$contact->id.'/delete" title="Delete" >
                        <i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
            }
        }

        return Datatables::of($contacts)
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getLeadCommissions($lead_id)
    {
        $commissions = $this->commission
            ->select('commissions.id', 'commissions.name', 'commissions.commission', 'commissions.created_at',
                'commissions.approved_date', 'users.firstname', 'commission_types.type')
            ->join('commission_types', 'commissions.type', '=', 'commission_types.id')
            ->leftJoin('users', 'commissions.approved_by', '=', 'users.id')
            ->whereLead_id($lead_id)->get();

        $property = $this->property->whereId($lead_id)->first();

        $gross_type = DB::select("SELECT id FROM commission_types WHERE type = 'Gross'");
        if (! $gross = $this->commission->whereLead_id($lead_id)->whereType($gross_type[0]->id)->first()) {
            $gross = new Commission();
        }
        $sla_type = DB::select("SELECT id FROM commission_types WHERE type = 'SLA'");
        if (! $sla = $this->commission->whereLead_id($lead_id)->whereType($sla_type[0]->id)->first()) {
            $sla = new Commission();
        }
        $seller_type = DB::select("SELECT id FROM commission_types WHERE type = 'Seller'");
        if (! $seller = $this->commission->whereLead_id($lead_id)->whereType($seller_type[0]->id)->first()) {
            $seller = new Commission();
        }
        $absorb_cost_type = DB::select("SELECT id FROM commission_types WHERE type = 'Absorbed Costs'");
        if (! $absorb_cost = $this->commission->whereLead_id($lead_id)->whereType($absorb_cost_type[0]->id)->first()) {
            $absorb_cost = new Commission();
        }

        foreach ($commissions as $commission) {
            if ($property->final_price != 0) {
                $gross_calc = $gross->commission / $property->final_price * 100;
                $gross_percent = number_format($gross_calc, 2);
                $sla_calc = $sla->commission / $property->final_price * 100;
                $sla_percent = number_format($sla_calc, 2);
                $seller_calc = $seller->commission / $property->final_price * 100;
                $seller_percent = number_format($seller_calc, 2);
                $absorb_cost_calc = $absorb_cost->commission / $property->final_price * 100;
                $absorb_cost_percent = number_format($absorb_cost_calc, 2);
            } else {
                $gross_percent = number_format(0, 2);
                $sla_percent = number_format(0, 2);
                $seller_percent = number_format(0, 2);
                $absorb_cost_percent = number_format(0, 2);
            }
            switch ($commission->type) {
                case 'BDM':
                    $percent = number_format($commission->commission / ($gross->commission - $sla->commission - $absorb_cost->commission) * 100, 2);

                    break;
                case 'Seller':
                    $percent = $seller_percent;

                    break;
                case 'SLA':
                    $percent = $sla_percent;

                    break;
                case 'Absorbed Costs':
                    $percent = $absorb_cost_percent;

                    break;
                case 'Gross':
                    $percent = $gross_percent;

                    break;
                default:
                    $percent = 0;

                    break;
            }

            $commission->commission = $percent.'%';

            switch ($commission->type) {
                case 'BDM':
                case 'Seller':
                case 'SLA':
                case 'Absorbed Costs':
                case 'Gross':
                    $amount = $commission->commission;

                    break;
                default:
                    $amount = 0;

                    break;
            }

            if ($commission->amount == null) {
                $commission->amount = 0;
            }

            $commission->amount = 'R '.number_format((float) $amount, 2, ',', ' ');

            if ($commission->type == 'BDM') {
                $commission->action = '<a href="/commission/'.$commission->id.'/delete" title="Remove from lead" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
            } else {
                $commission->action = 'No Action';
            }
        }

        return Datatables::of($commissions)
            ->rawColumns(['commission', 'amount', 'action'])
            ->make(true);
    }

    public function editContact($id)
    {
        $contact = $this->contact->whereId($id)->first();
        if ($contact->role_id != null) {
            $contact_types = DB::table('roles')->whereContact_type('1')->pluck('description', 'id');

            return view::make('leads.contacts.edit', ['contact' => $contact, 'contact_types' => $contact_types]);
        } else {
            $company_types = DB::table('company_types')->whereActive('1')->pluck('type', 'id');

            return view::make('leads.contacts.company_edit', ['contact' => $contact, 'company_types' => $company_types]);
        }
    }

    public function deleteContact($id)
    {
        $contact = $this->contact->whereId($id)->first();
        $lead_id = $contact->lead_id;
        $contact->delete();

        $lead = $this->lead->whereId($lead_id)->first();

        $this->addNewActual($lead->bdm_allocated, 'DB_Growth', -1);

        return redirect()->route('leads.edit', ['id' => $lead_id]);
    }

    public function apiDeleteContact()
    {
        $contact = $this->contact->whereId(Request::get('contact_id'))->first();
        $contact->delete();

        return 'Done';
    }

    public function addCommission()
    {
        $input = Request::all();

        $typeInput = $input['type'];

        $typeDets = DB::table('commission_types')->whereType($typeInput)->first();

        $input['type'] = $typeDets->id;

        $this->commission->fill($input)->save();

        return 'Done';
    }

    public function approveCommissions($id)
    {
        $commission = $this->commission->whereId($id)->first();
        $commission->approved_date = date('Y-m-d H:i:s');
        $commission->approved_by = Auth::user()->id;
        $lead_id = $commission->lead_id;
        $commission->update();

        return redirect()->route('leads.edit', ['id' => $lead_id]);
    }

    public function deleteCommissions($id)
    {
        $commission = $this->commission->whereId($id)->first();
        $lead_id = $commission->lead_id;
        $commission->delete();

        return redirect()->route('leads.edit', ['id' => $lead_id]);
    }

    public function getLeadFollowUp($user_id)
    {
        if ($user_id == 'Archived') {
            $leads = $this->lead
                ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
                ->leftJoin('lead_prop_trans_types', 'leads.lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                ->onlyTrashed()
                ->orderBy('created_at', 'asc')->get();
        } else {
            $user = User::find($user_id);
            if ($user->hasRole('LeadReviewer')) {
                $leads = $this->lead->select('created_at', 'reference', 'bdm_allocated', 'lead_title', 'street_number',
                    'street_name', 'suburb_town_city', 'lead_prop_trans_type', 'lead_prop_trans_type_other', 'check_lead', 'id')
                    ->orderBy('created_at')->get();
            } else {
                $leads = $this->lead->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
                    ->whereBdm_allocated($user_id)
                    ->orWhere(function ($q) {
                        $q->where('leads.refer_to_bdm', '=', 1)
                            ->where('leads.lead_ref', '=', Auth::user()->firstname.' '.Auth::user()->lastname)
                            ->where('leads.deleted_at', '=', null);
                    })
                    ->select('leads.*', 'lead_ref_types.id AS lead_ref_types_id', 'lead_prop_trans_types.type as listing_type')
                    ->leftJoin('lead_ref_types', 'leads.lead_ref_type', '=', 'lead_ref_types.id')
                    ->leftJoin('lead_prop_trans_types', 'leads.lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
                    ->orderBy('created_at', 'asc')->get();
            }
        }

        $lead_editors = User::whereHas('roles', function ($q) {
            $q->where('name', 'Admin')->orWhere('name', 'Agent')->orWhere('name', 'LeadReviewer');
        })->get();

        $lead_editor_ids = [];

        foreach ($lead_editors as $lead_editor) {
            $lead_editor_ids[] = $lead_editor->id;
        }

        $lead_id_array = [];

        foreach ($lead_editor_ids as $lead_editor_id) {
            $sessions = DB::select('SELECT * FROM lead_session WHERE user_id ='.$lead_editor_id);

            if (! empty($sessions)) {
                $lead_id_array[$lead_editor_id] = $sessions[0]->lead_id;
            }
        }

        foreach ($leads as $lead) {
//            $lead->created_at = date('Y-m-d H:i', strtotime($lead->created_at));
            if ($lead->bdm_allocated == 0) {
                $lead->bdm = 'None allocated';
            } else {
                $bdm = User::find($lead->bdm_allocated);
                $firstname = $bdm['firstname'];
                $lead->bdm = '<a href="/leads/'.$lead->bdm_allocated.'/user" >'.$firstname.'</a>';
            }
            if ($lead->street_number != null) {
                $address = $lead->street_number;
            } else {
                $address = '';
            }
            if ($lead->street_name != null) {
                $address .= ' '.$lead->street_name;
            } else {
                $address .= '';
            }
            if ($lead->suburb_town_city != null) {
                $address .= ' '.$lead->suburb_town_city;
            } else {
                $address .= '';
            }
            if ($address == '') {
                $lead->address = 'No Address';
            } else {
                $lead->address = $address;
            }
            $type = DB::table('lead_prop_trans_types')->select('type')->whereId($lead->lead_prop_trans_type)
                ->first();
            if (! empty($type)) {
                if ($type->type == 'Other') {
                    if ($lead->lead_prop_trans_type_other == '') {
                        $lead->transaction_type = 'Other';
                    }
                    $lead->transaction_type = $lead->lead_prop_trans_type_other;
                } else {
                    $lead->transaction_type = $type->type;
                }
            } else {
                $lead->transaction_type = 'Not Selected';
            }
            $query = 'SELECT u.firstname, u.lastname, u.email, u.cellnumber AS tel
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                AND lu.lead_id = '.$lead->id.'
                                UNION ALL
                                SELECT lc.firstname, lc.lastname, lc.email, lc.tel AS tel
                                FROM lead_contacts lc
                                INNER JOIN leads l ON l.id = lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                AND l.id = '.$lead->id.'
                                LIMIT 1';

            $contact = DB::select($query);

            if (empty($contact)) {
                $lead->contact_firstname = 'No Contacts';
                $lead->contact_email = 'No Contacts';
            } else {
                $lead->contact_firstname = $contact[0]->firstname.' '.$contact[0]->lastname;
                $email = '';
                if ($contact[0]->email != '') {
                    $email = '<strong>Email:</strong> '.$contact[0]->email.'<br>';
                }
                $tel = '';
                if ($contact[0]->tel != '') {
                    $tel = '<strong>Tel: </strong>'.$contact[0]->tel;
                }
                $lead->contact_email = $email.$tel;
            }

            $queryN = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.lead_id = '.$lead->id.'
                  ORDER BY p.created_at DESC
                  LIMIT 1';
            $last_note = DB::select($queryN);

            if (empty($last_note)) {
                $lead->note = 'No Notes';
            } else {
                $lead->note = $last_note[0]->note.' <i>...'.$last_note[0]->date.'</i>';
            }

            $query = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                                  INNER JOIN users u ON u.id = p.users_id
                                  WHERE p.lead_id = '.$lead->id;
            $last_note = DB::select($query);
            if (count($last_note) > 1) {
                $lead->check_lead = 1;
            }
            if ((Auth::user()->id == $lead->bdm_allocated && $lead->check_lead == null) || (Auth::user()->hasRole('Admin') && $lead->check_lead == null)) {
                $lead->actions = '<a href="/leads/'.$lead->id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$lead->id.'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                            <a href="/leads/'.$lead->id.'/check_lead" title="Mark as Viewed" ><i class="i-circled i-light i-alt i-small icon-check" style="color:#FF3100"></i></a>';
            } elseif (Auth::user()->hasRole('Admin') && $lead->check_lead == 1) {
                $lead->actions = '<a href="/leads/'.$lead->id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$lead->id.'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                            <a href="/leads/'.$lead->id.'/check_lead" title="Mark as Viewed" ><i class="i-circled i-light i-alt i-small icon-check" style="color:#00FF48"></i></a>';
            } else {
                $lead->actions = '<a href="/leads/'.$lead->id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$lead->id.'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
            }
        }

        return Datatables::of($leads)
            ->rawColumns(['bdm', 'contact_email', 'note', 'actions'])
            ->make(true);
    }

    public function deleteLead($lead_id)
    {
        $lead = $this->lead->whereId($lead_id)->first();

        $lead->delete();

        Flash::message('Lead has been deleted');

        return redirect()->route('user.leads', Auth::user()->id);
    }

    public function checkLead($lead_id)
    {
        $lead = $this->lead->whereId($lead_id)->first();

        $lead->check_lead = 1;

        $lead->save();

        $this->prop_notes->note = 'Lead actioned';
        $this->prop_notes->lead_id = $lead_id;

        $this->prop_notes->users_id = Auth::user()->id;

        $this->prop_notes->save();

        Flash::message('Lead has been marked as actioned');

        return redirect()->route('user.leads', Auth::user()->id);
    }

    public function getArchivedLeads()
    {
        $user = Auth::user();
        $leads = $this->lead
            ->select('leads.*', 'lead_types.type as lead_type_description', 'lead_prop_trans_types.type as listing_type')
            ->leftJoin('lead_types', 'lead_type', '=', 'lead_types.id')
            ->leftJoin('lead_prop_trans_types', 'lead_prop_trans_type', '=', 'lead_prop_trans_types.id')
            ->onlyTrashed()
            ->orderBy('created_at')->get();

        foreach ($leads as $lead) {
            switch ($lead->lead_type_description) {
                case 'Property':
                    $lead->content = $lead->street_number.' '.$lead->street_name;
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }

                    break;
                case 'Person':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('role_id', '<>', '', 'and')
                        ->join('tags', 'role_id', '=', 'tags.id')
                        ->first();
                    if ($contact != null) {
                        $lead->content = $contact->firstname.' '.$contact->lastname;
                    } else {
                        $lead->content = ' ';
                    }
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->name;

                    break;
                case 'Company/Trust':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('company_type', '<>', '', 'and')
                        ->join('company_types', 'company_type', '=', 'company_types.id')
                        ->first();
                    $lead->content = $contact->firstname;
                    if ($lead->content == '') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->type;

                    break;
                default:
                    defaultLabel:
                    $note = $this->prop_notes
                        ->whereLead_id($lead->id)
                        ->orderBy('prop_notes.id', 'Desc')
                        ->first();
                    $lead->content = $note['note'];
                    if ($lead->content == null) {
                        $lead->content = 'Empty';
                    }

                    break;
            }
        }

        $agents = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->pluck('fullname', 'id');

        $agents->put(Auth::user()->id, Auth::user()->firstname.' '.Auth::user()->lastname);
        $lead_ref_types = DB::table('lead_ref_types')->where('active', '1')->pluck('type', 'id');

        $contacts = DB::select('SELECT CONCAT(lc.firstname, " ",lc.lastname) AS name FROM lead_contacts lc WHERE
                                CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL AND CONCAT(lc.firstname, " ",lc.lastname)
                                <> " " GROUP BY name');

        $contact_array = [];
        foreach ($contacts as $contact) {
            $contact_array[] = trim($contact->name);
        }

        $tagDB = DB::table('tags')->get();
        foreach ($tagDB as $tag) {
            $tags[] = $tag->name;
        }
        $tags = implode('", "', $tags);

        return view('leads.index', ['user' => $user, 'leads' => $leads, 'agents' => $agents,
            'lead_ref_types' => $lead_ref_types, 'archived' => 'Archived', 'contacts' => json_encode($contact_array),
            'tags' => $tags, 'current_tags' => [], ]);
    }

    public function restoreArchivedLead($lead_id)
    {
        $lead = $this->lead->whereId($lead_id)->withTrashed()->first();
        $lead->deleted_at = null;
        $lead->save();

        return redirect()->route('user.leads', Auth::user()->id);
    }

    public function getAgentReport()
    {
        if (Request::get('start_date')) {
            $date = Request::get('start_date');
        } else {
            $date = date('Y-m-01');
        }

        return view('leads.agents_report', ['date' => $date]);
    }

    public function setToLastTarget($user_id)
    {
        $target = $this->targets->where('user_id', '=', $user_id)
            ->orderBy('id', 'desc')->first();

        if ($target != null) {
            $input['mandate_target'] = $target->mandate_target;
            $input['sales_target'] = $target->sales_target;
            $input['db_target'] = $target->db_target;
            $input['sla_target'] = $target->letting_target;
            $input['letting_target'] = $target->letting_target;
            $input['user_id'] = $user_id;
            $this->targets->fill($input)->save();
        }

        return true;
    }

    public function addNewActual($bdm_id, $type, $value)
    {
        $type_data = DB::table('agent_actuals_types')->whereType($type)->first();

        $input['agent_id'] = $bdm_id;
        $input['value'] = $value;
        $input['type'] = $type_data->id;

        $this->actual->fill($input)->save();

        return true;
    }

    public function getAgentTable($date)
    {
        $users = User::select('users.id', 'firstname', 'lastname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('roles.name', '=', 'Agent')
            ->get();

        $end_date = date('Y-m-d', strtotime('+1 month', strtotime($date)));

        foreach ($users as $user) {
            $targets = $this->targets->where('user_id', '=', $user->id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $date)
                ->orderBy('id', 'desc')->first();
            if ($targets == null) {
                $this->setToLastTarget($user->id);
                $targets = $this->targets->where('user_id', '=', $user->id)
                    ->where('created_at', '<=', $end_date)
                    ->where('created_at', '>=', $date)
                    ->orderBy('id', 'desc')->first();
            }
            if ($targets == null) {
                $targets = new Targets();
            }

            $user->mandates = $targets['mandate_target'];
            $user->sales = $targets['sales_target'];
            $user->db = $targets['db_target'];
            $user->sla = $targets['sla_target'];
            $user->letting = $targets['letting_target'];

            if ($user->mandates == null) {
                $user->mandates = 0;
            }
            if ($user->sales == null) {
                $user->sales = 0;
            }
            if ($user->db == null) {
                $user->db = 0;
            }
            if ($user->sla == null) {
                $user->sla = 0;
            }
            if ($user->letting == null) {
                $user->letting = 0;
            }

            $actual_data = $this->actual
                ->select(DB::raw('count(\'agent_actuals.*\') AS actCount'), 'agent_actuals_types.type')
                ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
                ->whereAgent_id($user->id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $date)
                ->groupBy('agent_actuals.type')
                ->get();

            $user->mandates_act = 0;
            $user->sales_act = 0;
            $user->sla_act = 0;
            $user->db_act = 0;
            $user->letting_act = 0;

            foreach ($actual_data as $actual) {
                switch ($actual->type) {
                    case 'Mandate':
                        $user->mandates_act = $actual->actCount;

                        break;
                    case 'Sales':
                        $user->sales_act = $actual->actCount;

                        break;
                    case 'SLA':
                        $user->sla_act = $actual->actCount;

                        break;
                    case 'DB_Growth':
                        $user->db_act = $actual->actCount;

                        break;
                    case 'Letting':
                        $user->letting_act = $actual->actCount;

                        break;
                }
            }

            $user->bdm = '<a href="/leads/'.$date.'/agent/'.$user->id.'" >'.$user->firstname.'</a>';
            $user->action = '<a href="/leads/agent/'.$user->id.'/cumulative" title="Cumulative report" ><i class="i-circled i-light i-alt i-small icon-files"></i></a>';
        }

        return Datatables::of($users)
            ->rawColumns(['bdm', 'action'])
            ->make(true);
    }

    public function getAgentCharts($date, $user_id)
    {
        $end_date = date('Y-m-d', strtotime('+1 month', strtotime($date)));

        $targets = $this->targets->where('user_id', '=', $user_id)
            ->where('created_at', '<=', $end_date)
            ->where('created_at', '>=', $date)
            ->orderBy('id', 'desc')->first();

        if ($targets == null) {
            $targets = new Targets();
        }

        $actual_data = $this->actual
            ->select(DB::raw('sum(value) AS actCount'), 'agent_actuals_types.type')
            ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
            ->whereAgent_id($user_id)
            ->where('created_at', '<=', $end_date)
            ->where('created_at', '>=', $date)
            ->groupBy('agent_actuals.type')
            ->get();

        $targets->mandate_actual = 0;
        $targets->sales_actual = 0;
        $targets->sla_actual = 0;
        $targets->db_growth_actual = 0;
        $targets->letting_actual = 0;

        foreach ($actual_data as $actual) {
            switch ($actual->type) {
                case 'Mandate':
                    $targets->mandate_actual = $actual->actCount;

                    break;
                case 'Sales':
                    $targets->sales_actual = $actual->actCount;

                    break;
                case 'SLA':
                    $targets->sla_actual = $actual->actCount;

                    break;
                case 'DB_Growth':
                    $targets->db_growth_actual = $actual->actCount;

                    break;
                case 'Letting':
                    $targets->letting_actual = $actual->actCount;

                    break;
            }
        }

        $user = User::find($user_id);

        $weeklyMandate = $this->getWeeklyTotals($user_id, $date, 'Mandate');

        return view('leads.agent_charts', ['date' => $date, 'user' => $user, 'targets' => $targets,
            'weeklyMandate' => $weeklyMandate, ]);
    }

    public function getWeeklyTotals($user_id, $date, $type)
    {
        $originalDate = $date;

        $count = 1;
        while ($count <= 4) {
            if ($count == 4) {
                $end_date = date('Y-m-d', strtotime('+1 month', strtotime($originalDate)));
            } else {
                $end_date = date('Y-m-d', strtotime('+1 week', strtotime($date)));
            }
            $actual_data = $this->actual
                ->select(DB::raw('sum(value) AS actCount'))
                ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
                ->whereAgent_id($user_id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $date)
                ->where('agent_actuals_types.type', '=', $type)
                ->get();

            $date = $end_date;
            if ($actual_data[0]->actCount == null) {
                $data[$count] = 0;
            } else {
                $data[$count] = $actual_data[0]->actCount;
            }

            $count++;
        }

        return $data;
    }

    public function saveAgentTargets($user_id)
    {
        $input = Request::except('mandate_actual', 'sale_actual', 'db_actual', 'sla_actual', 'letting_actual');
        $input['user_id'] = $user_id;
        $this->targets->fill($input)->save();

        $date = date('Y-m-01');

        return redirect()->route('api.agent.charts', ['date' => $date, 'user_id' => $user_id]);
    }

    public function getTargetsOverPeriod($start_date, $end_date, $agent_id)
    {
        $start = (new DateTime($start_date))->modify('first day of this month');
        $end = (new DateTime($end_date))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $targets = new Collection();

        $count = 0;
        foreach ($period as $dt) {
            $end_date = date('Y-m-d', strtotime('+1 month', strtotime($dt->format('Y-m'))));
            $thisTarget = $this->targets->where('user_id', '=', $agent_id)
                ->where('created_at', '<=', $end_date)
                ->where('created_at', '>=', $dt->format('Y-m'))
                ->orderBy('id', 'desc')->first();
            if ($thisTarget == null) {
            } else {
                $actual_data = $this->actual
                    ->select(DB::raw('sum(value) AS actCount'), 'agent_actuals_types.type')
                    ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
                    ->whereAgent_id($agent_id)
                    ->where('created_at', '<=', $end_date)
                    ->where('created_at', '>=', $dt->format('Y-m'))
                    ->groupBy('agent_actuals.type')
                    ->get();

                $thisTarget->mandate_actual = 0;
                $thisTarget->sales_actual = 0;
                $thisTarget->sla_actual = 0;
                $thisTarget->db_growth_actual = 0;
                $thisTarget->letting_actual = 0;

                foreach ($actual_data as $actual) {
                    switch ($actual->type) {
                        case 'Mandate':
                            $thisTarget->mandate_actual = $actual->actCount;

                            break;
                        case 'Sales':
                            $thisTarget->sales_actual = $actual->actCount;

                            break;
                        case 'SLA':
                            $thisTarget->sla_actual = $actual->actCount;

                            break;
                        case 'DB_Growth':
                            $thisTarget->db_growth_actual = $actual->actCount;

                            break;
                        case 'Letting':
                            $thisTarget->letting_actual = $actual->actCount;

                            break;
                    }
                }

                $thisTarget = $thisTarget->toArray();
                $targets[$count] = $thisTarget;
            }
            $count++;
        }

        return $targets;
    }

    public function getCumulativeReport($user_id)
    {
        if (Request::get('start_date')) {
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
            $user_id = Request::get('agents');
        } else {
            $target = $this->targets->whereUser_id($user_id)->orderBy('id', 'asc')->first();
            $start_date = date('Y-m-d', strtotime($target['created_at']));

            if ($start_date == '1970-01-01') {
                $start_date = date('Y-m-01');
            }
            $end_date = date('Y-m-d');
        }

        $checkTargets = $this->getTargetsOverPeriod($start_date, $end_date, $user_id);

        $total['Mandates_target'] = 0;
        $total['Sales_target'] = 0;
        $total['SLA_target'] = 0;
        $total['DB_target'] = 0;
        $total['Letting_target'] = 0;
        $total['Mandates_actual'] = 0;
        $total['Sales_actual'] = 0;
        $total['SLA_actual'] = 0;
        $total['DB_actual'] = 0;
        $total['Letting_actual'] = 0;

        foreach ($checkTargets as $dbData) {
            $total['Mandates_target'] += $dbData['mandate_target'];
            $total['Sales_target'] += $dbData['sales_target'];
            $total['SLA_target'] += $dbData['sla_target'];
            $total['DB_target'] += $dbData['db_target'];
            $total['Letting_target'] += $dbData['letting_target'];
            $total['Mandates_actual'] += $dbData['mandate_actual'];
            $total['Sales_actual'] += $dbData['sales_actual'];
            $total['SLA_actual'] += $dbData['sla_actual'];
            $total['DB_actual'] += $dbData['db_growth_actual'];
            $total['Letting_actual'] += $dbData['letting_actual'];
        }

        $this_month = date('Y-m-1');
        $end_month = date('Y-m-d', strtotime('+1 month', strtotime($this_month)));
        $targets = $this->targets->where('user_id', '=', $user_id)
            ->where('created_at', '<=', $end_month)
            ->where('created_at', '>=', $this_month)
            ->orderBy('id', 'desc')->first();

        if ($targets == null) {
            $targets = new Targets();
        }

        $actual_data = $this->actual
            ->select(DB::raw('sum(value) AS actCount'), 'agent_actuals_types.type')
            ->join('agent_actuals_types', 'agent_actuals.type', '=', 'agent_actuals_types.id')
            ->whereAgent_id($user_id)
            ->where('created_at', '<=', $end_month)
            ->where('created_at', '>=', $this_month)
            ->groupBy('agent_actuals.type')
            ->get();

        $targets->mandate_actual = 0;
        $targets->sales_actual = 0;
        $targets->sla_actual = 0;
        $targets->db_growth_actual = 0;
        $targets->letting_actual = 0;

        foreach ($actual_data as $actual) {
            switch ($actual->type) {
                case 'Mandate':
                    $targets->mandate_actual = $actual->actCount;

                    break;
                case 'Sales':
                    $targets->sales_actual = $actual->actCount;

                    break;
                case 'SLA':
                    $targets->sla_actual = $actual->actCount;

                    break;
                case 'DB_Growth':
                    $targets->db_growth_actual = $actual->actCount;

                    break;
                case 'Letting':
                    $targets->letting_actual = $actual->actCount;

                    break;
            }
        }

        $user = User::find($user_id);

        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();

        return view('leads.cumulative_report', ['start_date' => $start_date, 'end_date' => $end_date, 'user' => $user,
            'targets' => $targets, 'agents' => $agents, 'total' => $total, ]);
    }

    public function getCumulativeTable($agent_id, $start_date, $end_date)
    {
        $targets = $this->getTargetsOverPeriod($start_date, $end_date, $agent_id);

        foreach ($targets as $target) {
            $target->year_month = date('Y / m', strtotime($target->created_at));
        }

        return Datatables::of($targets)
            ->rawColumns(['year_month'])
            ->make(true);
    }

    public function convertLead()
    {
        $lead_id = Request::get('lead_id');
        $lead = $this->lead->whereId($lead_id)->first();

        $returnArray = AppHelper::checkMultipleWhichEmpty($lead->lead_prop_trans_type, $lead->street_number, $lead->street_name,
            $lead->province, $lead->area, $lead->suburb_town_city, $lead->property_type, $lead->x_coord);

        if (empty($returnArray)) {
            if ($lead->lead_prop_trans_type == 4 || $lead->lead_prop_trans_type == 5) {
                return 'Incorrect Listing Type';
            }
            //create property return property id
            $lead_prop_trans_type = DB::table('lead_prop_trans_types')->whereId($lead->lead_prop_trans_type)->first();
            $propertyInput['listing_type'] = $lead_prop_trans_type->listing_type;
            $propertyInput['street_number'] = $lead->street_number;
            $propertyInput['street_address'] = $lead->street_name;
            $propertyInput['complex_number'] = $lead->complex_number;
            $propertyInput['complex_name'] = $lead->complex_name;
            $propertyInput['country'] = $lead->country;
            $propertyInput['province'] = $lead->province;
            $propertyInput['area'] = $lead->area;
            $propertyInput['suburb_town_city'] = $lead->suburb_town_city;
            $propertyInput['property_type'] = $lead->property_type;
            $propertyInput['x_coord'] = $lead->x_coord;
            $propertyInput['y_coord'] = $lead->y_coord;
            $propertyInput['property_type'] = ucfirst($lead->property_type);
            $propertyInput['user_id'] = $lead->bdm_allocated;

            $reference = App::make(\App\Http\Controllers\PropertyController::class)->newReference($propertyInput['listing_type']);
            $propertyInput['reference'] = $reference;
            $this->property->fill($propertyInput)->save();

            //check all users connected to lead and save then as property_users
            // Can use App::make('App\Http\Controllers\PropertyController')->Prop_userAdd($pId, $uId, $rId);

            $lead_users = DB::table('lead_user')->whereLead_id($lead_id)->get();

            foreach ($lead_users as $lead_user) {
                App::make(\App\Http\Controllers\PropertyController::class)->Prop_userAdd($this->property->id, $lead_user->user_id, $lead_user->role_id);
            }

            //create property_type join

            switch ($propertyInput['property_type']) {
                case 'Residential':
                    $this->residential->property_id = $this->property->id;
                    $this->residential->save();

                    break;
                case 'Commercial':
                    $this->commercial->property_id = $this->property->id;
                    $this->commercial->save();

                    break;
                case 'Farm':
                    $this->farm->property_id = $this->property->id;
                    $this->farm->save();

                    break;
            }

            //create vetting join
            $this->vetting->property_id = $this->property->id;

            //check company
            $company = DB::table('lead_contacts')->join('company_types', 'company_type', '=', 'company_types.id')
                ->where('lead_id', '=', $lead_id)->where('company_type', '<>', '')->first();
            $tag = DB::table('tags')->where('name', '=', 'OWNER')->first();
            if (! empty($company->email)) {
                if (! ($user = $this->user->whereEmail($company->email)->first())) {
                    $this->user->firstname = $company->firstname;
                    $this->user->email = $company->email;
                    $this->user->phonenumber = $company->tel;
                    $this->user->save();
                    DB::table('property_user')->insert(['property_id' => $this->property->id, 'user_id' => $this->user->id,
                        'role_id' => $tag->id, ]);
                } else {
                    User::find($user->id)->assignTag($tag->id);
                    DB::table('property_user')->insert(['property_id' => $this->property->id, 'user_id' => $user->id,
                        'role_id' => $tag->id, ]);
                }
            }

            $this->vetting->rep_id_number = $lead->bdm_allocated;
            $this->vetting->vetting_status = 1;
            $this->vetting->save();
            //update contact
            DB::table('lead_contacts')
                ->where('lead_id', '=', $lead_id)
                ->update(['property_id' => $this->property->id]);

            //check for property notes and change

            DB::table('prop_notes')
                ->where('lead_id', '=', $lead_id)
                ->update(['property_id' => $this->property->id]);

            $this->prop_notes->note = 'Lead Converted to Property';
            $this->prop_notes->property_id = $this->property->id;
            $this->prop_notes->lead_id = $lead_id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();

            //check and change images
            DB::table('uploads')
                ->where('class', '=', 'Lead')
                ->where('link', '=', $lead_id)
                ->update(['class' => 'Property', 'link' => $this->property->id]);

            if (! empty(DB::table('uploads')
                ->where('class', '=', 'Property')
                ->where('link', '=', $this->property->id)->first())
            ) {
                //change image folder name
                rename('../public/uploads/users/'.$lead->bdm_allocated.'/lead_'.$lead_id,
                    '../public/uploads/users/'.$lead->bdm_allocated.'/'.$this->property->id);
            }

            $lead->property_id = $this->property->id;

            $lead->save();

            $lead->delete();

            $this->addNewActual($lead->bdm_allocated, 'DB_Growth', 1);
            $ref_type = DB::table('lead_ref_types')->where('type', '=', 'SLA')->first();
            if ($lead->lead_ref_type == $ref_type->id) {
                $this->addNewActual($lead->bdm_allocated, 'SLA', 1);
            }
            if ($lead_prop_trans_type->listing_type == 'in2rental') {
                $this->addNewActual($lead->bdm_allocated, 'Letting', 1);
            }

            return 'Done'.$this->property->id;
        } else {
            return $returnArray;
        }
    }

    public function contactToUsers()
    {
        $contacts = DB::select("SELECT * FROM lead_contacts WHERE email <> '' AND lastname <> '' AND company_type IS NULL");

        foreach ($contacts as $contact) {
//            try {
//                $newUserId = App::make('App\Http\Controllers\RegistrationController')->storeLeadUser($contact->email, $contact->firstname, $contact->lastname,
//                    $contact->tel, $contact->role_id);
//
//
//
//                if (!DB::select("SELECT * FROM lead_user WHERE user_id = " . $newUserId . " AND lead_id = " . $contact->lead_id)) {
//                    DB::insert("INSERT INTO lead_user SET user_id = " . $newUserId . ", lead_id = " . $contact->lead_id . ", role_id = "
//                        . $contact->role_id);
//                }
//            } catch (exception $e) {
//                de($contact->email . ' ' . $contact->role_id . ' ' . $contact->firstname . ' ' . $contact->lastname . ' ' . $contact->tel);
//                de('Message: ' .$e->getMessage());
//            }

            de($contact->lead_id.' '.$contact->email.' '.$contact->role_id.' '.$contact->firstname.' '.$contact->lastname.' '.$contact->tel);
        }

//        $notes = DB::select("SELECT pn.note FROM prop_notes pn INNER JOIN leads l ON pn.lead_id = l.id WHERE pn.note LIKE
//                            '%reference%' AND lead_ref = 'Property Enquiry' ");

//        foreach ($notes as $note) {
//            $pos = strpos($note->note, 'number: ');
//            $references[] = substr((substr($note->note, $pos + strlen('number: '))), 0, 11);
//        }
//
//        foreach ($references as $reference) {
//            $query = "UPDATE prop_notes INNER JOIN property ON reference = '" . $reference . "' INNER JOIN leads ON prop_notes.lead_id
//            = leads.id SET prop_notes.property_id = property.id, leads.lead_ref_type = 9 WHERE prop_notes.note LIKE '%" . $reference . "%'";
//            DB::update($query);
//        }

//        dd('Complete');
    }

    public function propertySubmitPackage()
    {
        $input['lead_type'] = 5;
        $input['lead_ref_type'] = 5;
        $input['lead_ref'] = Request::get('lead_ref');
        $input['lead_prop_trans_type'] = 5;
        $input['bdm_allocated'] = 0;
        $input['property_type'] = Request::get('property_type');
        $input['title_desc'] = Request::get('title_desc');
        $input['title_type'] = Request::get('title_type');
        $input['street_number'] = Request::get('street_number');
        $input['street_name'] = Request::get('street_name');

        $prov_area_sub_array = explode(',', Request::get('suburb'));

        $input['country'] = 'South Africa';
        if (trim($prov_area_sub_array[0]) == '') {
            $input['province'] = '';
            $input['area'] = '';
            $input['suburb_town_city'] = '';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (trim($prov_area_sub_array[1]) == '') {
                $input['area'] = '';
                $input['suburb_town_city'] = '';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (trim($prov_area_sub_array[2]) == '') {
                    $input['suburb_town_city'] = '';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        $input['reference'] = $reference;

        $this->lead->fill($input)->save();

        $this->prop_notes->note = Request::get('property_description');
        $this->prop_notes->lead_id = $this->lead->id;
        if (Auth::check()) {
            $this->prop_notes->users_id = Auth::user()->id;
        } else {
            $this->prop_notes->users_id = 1;
        }
        $this->prop_notes->save();
        $tag = DB::table('tags')->where('name', '=', 'OWNER')->first();
        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = $tag->id;
        $inputContact['firstname'] = Request::get('owner_firstname');
        $inputContact['lastname'] = Request::get('owner_surname');
        $inputContact['email'] = Request::get('owner_email');
        $inputContact['tel'] = Request::get('owner_cell');

        $this->contact->fill($inputContact)->save();
        $userMail = $inputContact['email'];
        $subject = 'User has submitted a Property';
        $name = 'Dear Administrator,<br>';
        $emailData = '<h4>Someone is interested in a package deal.</h4><p>'.$inputContact['firstname'].' '.
            $inputContact['lastname'].' has submitted details of their property for our opinion. Please check the
            lead table for details.</p>';

        Mail::to($userMail)
            ->send(new newLead($subject, $name, $emailData, $userMail));

        Flash::message('Your details have been saved and we will contact you shortly');

        return redirect()->back();
    }

    public function propertySubmit()
    {
        $input['lead_type'] = 4;
        $input['lead_ref_type'] = 5;
        $input['lead_ref'] = Request::get('lead_ref');
        $input['lead_prop_trans_type'] = 5;
        $input['bdm_allocated'] = 0;
        $input['property_type'] = Request::get('property_type');
        $input['title_desc'] = Request::get('title_desc');
        $input['title_type'] = Request::get('title_type');
        $input['street_number'] = Request::get('street_number');
        $input['street_name'] = Request::get('street_name');

        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            foreach ($validate->errors()->all() as $error) {
                Flash::message('Please check the "I am not a robot" field.');
            }

            return redirect()->back()->withInput($input);
        }

        $prov_area_sub_array = explode(',', Request::get('suburb'));

        $input['country'] = 'South Africa';
        if (trim($prov_area_sub_array[0]) == '') {
            $input['province'] = '';
            $input['area'] = '';
            $input['suburb_town_city'] = '';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (trim($prov_area_sub_array[1]) == '') {
                $input['area'] = '';
                $input['suburb_town_city'] = '';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (trim($prov_area_sub_array[2]) == '') {
                    $input['suburb_town_city'] = '';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        $input['reference'] = $reference;

        $this->lead->fill($input)->save();

        $this->prop_notes->note = Request::get('property_description');
        $this->prop_notes->lead_id = $this->lead->id;
        if (Auth::check()) {
            $this->prop_notes->users_id = Auth::user()->id;
        } else {
            $this->prop_notes->users_id = 1;
        }
        $this->prop_notes->save();
        $tag = DB::table('tags')->where('name', '=', 'OWNER')->first();
        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = $tag->id;
        $inputContact['firstname'] = Request::get('owner_firstname');
        $inputContact['lastname'] = Request::get('owner_surname');
        $inputContact['email'] = Request::get('owner_email');
        $inputContact['tel'] = Request::get('owner_cell');
        $userMail = Request::get('owner_email');
        $this->contact->fill($inputContact)->save();

        $url = 'https://www.in2assets.co.za/leads/'.$this->lead->id.'/edit';

        switch ($input['lead_ref']) {
            case 'online-letting':
                $regards = 'with regards to letting';
                $header = 'Letting';

                break;
            case 'online-buy':
                $regards = 'with regards to buying';
                $header = 'Buying';

                break;
            case 'online-sell':
                $regards = 'with regards to selling';
                $header = 'Sale';

                break;
            default:
                $regards = 'with regards to selling';
                $header = 'Sale';

                break;
        }

        $subject = 'User has submitted a Property | '.$header;
        $name = 'Dear Administrator,<br>';
        $emailData = '<h1>User has submitted a Property | '.$header.'</h1><p>'.$inputContact['firstname'].' '.$inputContact['lastname'].' has submitted details of their property for our opinion '.$regards.' their property.</p><p><strong>Email: </strong>'.$inputContact['email'].'</p><p><strong>Tel: </strong>'.$inputContact['tel'].'</p><p><a href="'.$url.'">Click here</a> to view the lead.</p>';

        Mail::to('auctions@in2assets.com')
            ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, $userMail));

        Flash::message('Your details have been saved and we will contact you shortly');
        $username = $inputContact['firstname'].' '.$inputContact['lastname'];

        switch ($input['lead_ref']) {
            case 'online-letting':
                return Redirect::route('thank-you-sell', ['username' => $username]);

                break;
            case 'online-buy':
                return Redirect::route('thank-you-buy', ['username' => $username]);

                break;
            case 'online-sell':
                return Redirect::route('thank-you-sell', ['username' => $username]);

                break;
            default:
                return Redirect::route('thank-you-sell', ['username' => $username]);

                break;
        }
    }

    public function thankYouSell()
    {
        $username = Request::get('username');

        return view::make('pages.thank-you-sell', ['username' => $username]);
    }

    public function thankYouBuy()
    {
        $username = Request::get('username');

        return view::make('pages.thank-you-buy', ['username' => $username]);
    }

    public function auctionMyProperty()
    {
        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            foreach ($validate->errors()->all() as $error) {
                Flash::message('Please check the "I am not a robot" field.');
            }

            return redirect()->back()->withInput(Request::all(), ['error' => true]);
        }

        $inputLead['lead_type'] = 2;
        $inputLead['lead_ref_type'] = 3;
        $inputLead['lead_prop_trans_type'] = 3;
        $inputLead['bdm_allocated'] = 0;

        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        $inputLead['reference'] = $reference;

        $this->lead->fill($inputLead)->save();

        $fullNameArray = explode(' ', Request::get('fullname'));

        $lastname = trim(end($fullNameArray));
        if (trim(end($fullNameArray)) == trim($fullNameArray[0])) {
            $lastname = '.';
        }
        $newUserId = App::make(\App\Http\Controllers\RegistrationController::class)->storeLeadUser(Request::get('email'), $fullNameArray[0], $lastname,
            Request::get('cellnumber'), 11);
        $user = User::find($newUserId);

        $this->mailer->sendWelcomeMessageToAddedUser($user);

        $tag = DB::table('tags')->where('name', '=', 'SELLER')->first();
        if (! DB::select('SELECT * FROM lead_user WHERE user_id = '.$newUserId.' AND lead_id = '.$this->lead->id)) {
            DB::insert('INSERT INTO lead_user SET user_id = '.$newUserId.', lead_id = '.$this->lead->id.', role_id = '
                .$tag->id);
        }

        $inputContact['firstname'] = $fullNameArray[0];
        $inputContact['lastname'] = '.';
        if (trim(end($fullNameArray)) != trim($fullNameArray[0])) {
            $inputContact['lastname'] = end($fullNameArray);
        }
        $inputContact['email'] = Request::get('email');
        $inputContact['tel'] = Request::get('cellnumber');
        $inputContact['lead_id'] = $this->lead->id;

        $userMail = 'tmunsamy@in2assets.com';
        $subject = 'Auction my property';
        $name = 'Dear Administrator,<br>';
        $emailData = '<p>'.$inputContact['firstname'].' '.$inputContact['lastname'].'
            has submitted details and is interested in auctioning their property</p>
            <p><strong>Email: </strong>'.$inputContact['email'].'</p>
            <p><strong>Tel: </strong>'.$inputContact['tel'].'</p>
            <a href="https://www.in2assets.co.za/leads/'.$this->lead->id.'">Go to lead</a>';

        Mail::to($userMail)
            ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, $userMail));

        Flash::message('Your details have been saved and we will contact you shortly');

        return redirect()->back();
    }

    public function networkSubmit()
    {
        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            foreach ($validate->errors()->all() as $error) {
                Flash::message('Please check the "I am not a robot" field.');
            }

            return redirect()->back()->withInput(Request::all());
        }

        $input['lead_ref'] = Request::get('lead_ref');
        $input['property_description'] = Request::get('property_description');

        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        $input['reference'] = $reference;

        $this->lead->fill($input)->save();

        $this->prop_notes->note = Request::get('property_description');
        $this->prop_notes->lead_id = $this->lead->id;
        if (Auth::check()) {
            $this->prop_notes->users_id = Auth::user()->id;
        } else {
            $this->prop_notes->users_id = 1;
        }
        $this->prop_notes->save();

        $newUserId = App::make(\App\Http\Controllers\RegistrationController::class)->storeLeadUser(Request::get('owner_email'), Request::get('owner_firstname'),
            Request::get('owner_surname'), Request::get('owner_cell'), 11);
        $user = User::find($newUserId);
        $this->mailer->sendWelcomeMessageToAddedUser($user);

        if (! DB::select('SELECT * FROM lead_user WHERE user_id = '.$newUserId.' AND lead_id = '.$this->lead->id)) {
            DB::insert('INSERT INTO lead_user SET user_id = '.$newUserId.', lead_id = '.$this->lead->id.', role_id = '
                .'1');
        }

        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = 1;
        $inputContact['firstname'] = Request::get('owner_firstname');
        $inputContact['lastname'] = Request::get('owner_surname');
        $inputContact['email'] = Request::get('owner_email');
        $inputContact['tel'] = Request::get('owner_cell');

        $userMails['email'] = 'rstenzhorn@in2assets.com';

        foreach ($userMails as $userMail) {
            $subject = 'User has submitted a details and are interested in networking';
            $name = 'Dear Administrator,<br>';
            $emailData = '<p>'.$inputContact['firstname'].' '.$inputContact['lastname'].'
            has submitted details of their company and are interested in networking with us.</p>
            <p><strong>Email: </strong>'.$inputContact['email'].'</p>
            <p><strong>Tel: </strong>'.$inputContact['tel'].'</p>
            <a href="https://www.in2assets.co.za/leads/'.$this->lead->id.'">Go to lead</a>';

            Mail::to($userMail)
                ->cc('tmunsamy@in2assets.com')
                ->send(new newLead($subject, $name, $emailData, $userMail));
        }
        Flash::message('Your details have been saved and we will contact you shortly');

        return redirect()->back();
    }

    public function saveUserEnquiry()
    {
        $input = Request::all();

        $refCheck = strtoupper(substr($input['address'], 0, 5));

        if ($refCheck == 'SALE-' || $refCheck == 'RENT-' || $refCheck == 'AUCT-') {
            $property = $this->property->
            where('property.reference', '=', $input['address'])->
            first();
            $property_id = $property->id;
        } else {
            $address_array = explode(' - ', $input['address']);
            $street_number = $address_array[0];
            $street_address = $address_array[1];
            $property = $this->property->
            where('property.street_number', '=', $street_number)->
            where('property.street_address', '=', $street_address)->
            get();

            if (count($property) > 1) {
                Flash::message('There are more than one properties with this address, please use the reference
                number to save this property enquiry.');

                return redirect()->back();
            } else {
                $property_id = $property[0]->id;
            }
        }

        if (! DB::select('SELECT * FROM lead_user lu INNER JOIN leads l ON l.id
                            = lu.lead_id  WHERE user_id = '.$input['user_id'].' AND l.property_id = '.$property_id)
        ) {
            $details = DB::select('SELECT u.email, u.firstname, u.lastname, p.reference, u.id as agent_id, p.complex_number, p.complex_name,
                                p.street_number, p.street_address, p.listing_type FROM property p INNER JOIN vetting v ON
                                p.id = v.property_id INNER JOIN users u ON v.rep_id_number = u.id WHERE p.id = '.$property_id);

            $reference = $this->getReference();

            $this->lead->fill(['lead_type' => 2, 'lead_ref_type' => 9, 'lead_ref' => 'Property Enquiry',
                'lead_prop_trans_type' => $details[0]->listing_type, 'bdm_allocated' => $details[0]->agent_id,
                'reference' => $reference, 'property_id' => $property_id, ])->save();
            $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
            DB::table('lead_user')->insert(['user_id' => $input['user_id'], 'lead_id' => $this->lead->id, 'role_id' => $tag->id]);

            if (Request::get('archive_lead')) {
                $this->lead->delete();
            }

            $user = $this->user->whereId($input['user_id'])->first();

            if ($input['message'] == '') {
                $input['message'] = 'No Message';
            }

            $this->prop_notes->note = $user->firstname.' '.$user->lastname.' has enquired about a property
            reference number: '.$details[0]->reference.'<br>'.$input['message'];
            $this->prop_notes->lead_id = $this->lead->id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->property_id = $property_id;
            $this->prop_notes->save();

            $inputContact['lead_id'] = $this->lead->id;
            $inputContact['role_id'] = 11;
            $inputContact['firstname'] = $user->firstname;
            $inputContact['lastname'] = $user->lastname;
            $inputContact['email'] = $user->email;
            $inputContact['tel'] = $user->cellnumber;

            $url = 'https://www.in2assets.co.za/leads/'.$this->lead->id.'/edit';
            $bdm_name = $details[0]->firstname.' '.$details[0]->lastname;
            $bdm_email = $details[0]->email;

            $subject = 'You have received a property enquiry';
            $name = 'Dear '.$bdm_name.',<br>';

            $emailData = '<h4>You have received a property enquiry <br>
                        Name: '.$inputContact['firstname'].'<br>
                        Email: '.$inputContact['email'].'<br>
                        Tel: '.$inputContact['tel'].'<br>
                        Property Ref #: '.$details[0]->reference.'<br>
                        Address: '.trim($details[0]->complex_number.' '.$details[0]->complex_name.' '.
                    $details[0]->street_number.' '.$details[0]->street_address)."<br>
                        or <a href='".$url."'>Click here</a> to view the lead.</h4>";

            Mail::to($bdm_email)
                ->cc(['tmunsamy@in2assets.com', 'rmoodley@in2assets.com'])
                ->send(new newLead($subject, $name, $emailData, $bdm_email));

            Flash::message('Property Enquiry has been saved for this user');
        } else {
            Flash::message('This user has already enquired about this property');
        }

        return redirect()->back();
    }

    public function savePropertyEnquiry()
    {
        $input = Request::all();

        if ($input['user'] == '') {
            if ($input['property_enq_email'] != '') {
                $user_id = App::make(\App\Http\Controllers\RegistrationController::class)->storeLeadUser($input['property_enq_email'], $input['property_enq_firstname'],
                    $input['property_enq_lastname'], $input['property_enq_number'], 11);
                $user = User::find($user_id);
                $this->mailer->sendWelcomeMessageToAddedUser($user);
            } else {
                Flash::message('Please enter email address.');

                return redirect()->back();
            }
        } else {
            $user = $this->getUserDetails($input['user']);
        }

        $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
        $tagName = DB::table('tags')->whereId($tag->id)->first();
        if ($user->hasTag($tagName->name) == false) {
            $user->tags()->attach($tag->id);
        }

        if ($user == 'More') {
            Flash::message('There are more than one users with this name, please search by email or contact number');

            return redirect()->back();
        }

        if (! DB::select('SELECT * FROM lead_user lu INNER JOIN leads l ON l.id
                            = lu.lead_id WHERE user_id = '.$user->id.' AND l.property_id = '.$input['property_id'])
        ) {
            $query = 'SELECT u.email, u.firstname, u.lastname, p.reference, u.id as agent_id, p.complex_number,
                                p.complex_name, p.street_number, p.street_address, p.listing_type FROM property p
                                INNER JOIN vetting v ON p.id = v.property_id INNER JOIN users u ON v.rep_id_number = u.id
                                WHERE p.id = '.$input['property_id'];

            $details = DB::select($query);

            $reference = $this->getReference();
            switch ($details[0]->listing_type) {
                case 'in2assets':
                    $lead_prop_trans_type = 1;

                    break;
                case 'in2rentals':
                    $lead_prop_trans_type = 2;

                    break;
                case 'weauction':
                    $lead_prop_trans_type = 3;

                    break;
                default:
                    $lead_prop_trans_type = 1;

                    break;
            }

            if (Request::get('archive_lead')) {
                $this->lead->fill(['lead_type' => 2, 'lead_ref_type' => 9, 'lead_ref' => 'Property Enquiry',
                    'lead_prop_trans_type' => $lead_prop_trans_type, 'bdm_allocated' => $details[0]->agent_id,
                    'reference' => $reference, 'property_id' => $input['property_id'], 'deleted_at' => date('Y-m-d H:i:s'), ])->save();
            } else {
                $this->lead->fill(['lead_type' => 2, 'lead_ref_type' => 9, 'lead_ref' => 'Property Enquiry',
                    'lead_prop_trans_type' => $lead_prop_trans_type, 'bdm_allocated' => $details[0]->agent_id,
                    'reference' => $reference, 'property_id' => $input['property_id'], ])->save();
            }
            $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
            DB::table('lead_user')->insert(['user_id' => $user->id, 'lead_id' => $this->lead->id, 'role_id' => $tag->id]);

            if ($input['message'] == '') {
                $input['message'] = 'No Message';
            }

            $this->prop_notes->note = $user->firstname.' '.$user->lastname.' has enquired about a property
            reference number: '.$details[0]->reference.'<br>'.$input['message'];
            $this->prop_notes->lead_id = $this->lead->id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->property_id = $input['property_id'];
            $this->prop_notes->save();

            $inputContact['lead_id'] = $this->lead->id;
            $inputContact['role_id'] = 11;
            $inputContact['firstname'] = $user->firstname;
            $inputContact['lastname'] = $user->lastname;
            $inputContact['email'] = $user->email;
            $inputContact['tel'] = $user->cellnumber;

            if (Request::get('archive_lead')) {
            } else {
                $url = 'https://www.in2assets.co.za/leads/'.$this->lead->id.'/edit';
                $bdm_name = $details[0]->firstname.' '.$details[0]->lastname;
                $bdm_email = $details[0]->email;

                $subject = 'You have received a property enquiry';
                $name = 'Dear '.$bdm_name.',<br>';

                $emailData = '<h4>You have received a property enquiry <br>
                        Name: '.$inputContact['firstname'].'<br>
                        Email: '.$inputContact['email'].'<br>
                        Tel: '.$inputContact['tel'].'<br>
                        Property Ref #: '.$details[0]->reference.'<br>
                        Address: '.trim($details[0]->complex_number.' '.$details[0]->complex_name.' '.
                        $details[0]->street_number.' '.$details[0]->street_address)."<br>
                        or <a href='".$url."'>Click here</a> to view the lead.</h4>";

                // Queue email
                Mail::to($bdm_email)
                    ->cc(['tmunsamy@in2assets.com', 'rmoodley@in2assets.com'])
                    ->send(new newLead($subject, $name, $emailData, $bdm_email));
            }
            Flash::message('Property Enquiry has been saved for this user');
        } else {
            Flash::message('This user has already enquired about this property');
        }

        return redirect()->back();
    }

    public function getReference()
    {
        $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
        $lead_ref = $lead_ref[0]->lead_ref;
        $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
        $lead_ref++;
        DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

        return $reference;
    }

    public function propertyEnquiry()
    {
        $input = Request::all();
        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            foreach ($validate->errors()->all() as $error) {
                Flash::message('Please check the "I am not a robot" field.');
            }

            return redirect()->back()->withInput(Request::all());
        }

        if (Request::get('sign_up')) {
            $user_id = App::make(\App\Http\Controllers\RegistrationController::class)->storePropEnqUser(Request::get('name'), Request::get('email'),
                Request::get('contact'), Request::get('_token'));

            if ($this->user->find($user_id)->hasTag('BUYER') == false) {
                $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
                $this->user->find($user_id)->assignTag($tag->id);
            }
        }

        $details = DB::select('SELECT u.email, u.firstname, u.lastname, p.reference, u.id as agent_id, p.complex_number, p.complex_name,
                                p.street_number, p.street_address, p.listing_type, p.property_type, p.listing_type, p.x_coord, p.y_coord
                                 FROM property p INNER JOIN vetting v ON p.id = v.property_id INNER JOIN users u ON v.rep_id_number = u.id
                                 WHERE p.id = '.$input['property_id']);

        $reference = $this->getReference();

        $this->lead->fill(['lead_type' => 2, 'lead_ref_type' => 9, 'lead_ref' => 'Property Enquiry',
            'lead_prop_trans_type' => 0, 'bdm_allocated' => $details[0]->agent_id,
            'reference' => $reference, 'country' => 'South Africa', 'street_number' => $details[0]->street_number,
            'street_name' => $details[0]->street_address, 'complex_number' => $details[0]->complex_number,
            'complex_name' => $details[0]->complex_number, 'x_coord' => $details[0]->x_coord, 'y_coord' => $details[0]->y_coord,
            'property_id' => $input['property_id'], ])
            ->save();

        //find all users attatched to property
        $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            if ($this->user->find($user_id)->hasTag('BUYER') == false) {
                $this->user->find($user_id)->assignTag($tag->id);
            }
            DB::table('lead_user')->insert(['user_id' => Auth::user()->id, 'lead_id' => $this->lead->id, 'role_id' => $tag->id]);
            DB::table('property_user')->insert(['user_id' => Auth::user()->id, 'property_id' => $input['property_id'], 'role_id' => $tag->id]);
        } else {
            DB::table('lead_user')->insert(['user_id' => $user_id, 'lead_id' => $this->lead->id, 'role_id' => $tag->id]);
            DB::table('property_user')->insert(['user_id' => $user_id, 'property_id' => $input['property_id'], 'role_id' => $tag->id]);
        }

        $property_users = DB::select('SELECT * FROM property_user WHERE property_id = '.$input['property_id'].
            ' AND user_id <> '.$user_id);

        //attatch all property users to lead users
        foreach ($property_users as $property_user) {
            DB::insert('INSERT INTO lead_user SET lead_id = '.$this->lead->id.', user_id = '.
                $property_user->user_id.', role_id = '.$property_user->role_id);
        }

        $this->prop_notes->note = $input['name'].' has enquired about a property reference number: '.$details[0]->reference
            .'<br>'.$input['message'];
        $this->prop_notes->lead_id = $this->lead->id;
        $this->prop_notes->property_id = $input['property_id'];
        $this->prop_notes->users_id = 1;
        $this->prop_notes->save();

        $name_array = explode(' ', Request::get('name'));
        $firstname = $name_array[0];
        $lastname = end($name_array);

        $inputContact['lead_id'] = $this->lead->id;
        $inputContact['role_id'] = 11;
        $inputContact['firstname'] = $firstname;
        $inputContact['lastname'] = $lastname;
        $inputContact['email'] = Request::get('email');
        $inputContact['tel'] = Request::get('contact');

        $url = 'https://www.in2assets.co.za/leads/'.$this->lead->id.'/edit';
        $bdm_name = $details[0]->firstname.' '.$details[0]->lastname;
        $bdm_email = $details[0]->email;

        $subject = 'You have received a property enquiry';
        $name = 'Dear '.$bdm_name.',<br>';

        $emailData = '<h4>You have received a property enquiry <br>
                        Name: '.$inputContact['firstname'].' '.$inputContact['lastname'].'<br>
                        Email: '.$inputContact['email'].'<br>
                        Tel: '.$inputContact['tel'].'<br>
                        Property Ref #: '.$details[0]->reference.'<br>
                        Address: '.trim($details[0]->complex_number.' '.$details[0]->complex_name.' '.$details[0]->street_number.' '.$details[0]->street_address).'<br>
                        Message: '.$input['message']."<br>
                        or <a href='".$url."'>Click here</a> to view the lead.</h4>";

        Mail::to($bdm_email)
            ->cc(['tmunsamy@in2assets.com', 'rmoodley@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, $bdm_email));

        $property = $this->property->whereId($input['property_id'])->first();

        Flash::message('Thank you for your enquiry, we will contact you shortly');

        return view::make('pages.thank-you', ['id' => $property->id, 'slug' => $property->slug]);
    }

    public function printLeadNotes($lead_id)
    {
        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.lead_id = ".$lead_id.'
                  ORDER BY p.created_at DESC';
        $lead = $this->lead->whereId($lead_id)->first();
        $previous_notes = DB::select($query);
        $html = '<html><body>
                <h4>Lead notes for '.($lead->lead_title == null ? 'No Title' : $lead->lead_title).'</h4>';
        foreach ($previous_notes as $note) {
            $html .= '<p><q>'.$note->note.' : </q>
                            <i>'.$note->name.' '.$note->date.'</i></p><hr>';
        }
        $html .= '</body></html>';

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();
    }

    public function excelLeadNotes($lead_id)
    {
        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.lead_id = ".$lead_id.'
                  ORDER BY p.created_at DESC';
        $lead = $this->lead->whereId($lead_id)->first();
        $previous_notes = DB::select($query);

//        function convert(&$item, $key)
//        {
//            $item = (array)$item;
//        }

//        array_walk($previous_notes, 'convert');
//        (new Collection($lead, $previous_notes))->storeExcel(
//            'public/excel',
//            $disk = null,
//            $writerType = null,
//            $headings = false
//        );

//        Excel::create('LeadNotes', function ($excel) use ($lead, $previous_notes) {
//            // Set the title
//            $excel->setTitle('Lead notes for ' . ($lead->lead_title == null ? $lead->reference : $lead->lead_title));
//
//            // Chain the setters
//            $excel->setCreator('In2Assets')
//                ->setCompany('In2Assets');
//
//            // Call them separately
//            $excel->setDescription('Lead notes for ' . ($lead->lead_title == null ? $lead->reference : $lead->lead_title));
//
//            $excel->sheet('Sheetname', function ($sheet) use ($previous_notes) {
//
//                // Sheet manipulation
//                $sheet->setOrientation('landscape');
//
//                $sheet->fromArray($previous_notes);
//
//            });
//
//        })->export('xls');
    }

    public function searchLeadNotes()
    {
        $search_note = Request::get('search_note');
        $agent = Request::get('search_by_bdm');
        $property_type = Request::get('search_by_property');
        $lead_ref_type = Request::get('search_by_lead_type');
        $contact = Request::get('Contacts');

        if (Session::has(Auth::user()->id.'Lead_Note')) {
            Session::forget(Auth::user()->id.'Lead_Note');
            Session::put(Auth::user()->id.'Lead_Note', $search_note);
        } else {
            Session::put(Auth::user()->id.'Lead_Note', $search_note);
        }

        if ($search_note == '' && $agent == '' && $property_type == '' && $lead_ref_type == '' && $contact == '' &&
            Request::get('current_tags') == ''
        ) {
            return redirect()->route('user.leads', ['user_id' => Auth::user()->id]);
        }

        if ($search_note == '') {
            $searchable_note = '%';
        } else {
            $searchable_note = '%'.$search_note.'%';
        }

        $note_sql = "SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE '".$searchable_note."'
                                    AND pn.lead_id IS NOT NULL GROUP BY pn.lead_id";

        $db_results = DB::select($note_sql);

        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }

        if (empty($lead_array)) {
            Flash::message('There are no leads with this word/phrase in notes.');

            return redirect()->route('user.leads', ['user_id' => Auth::user()->id]);
        }

        if ($agent == '') {
            $agent = '%';
        }

        if ($property_type == '') {
            $property_type = '%';
        }

        if ($lead_ref_type == '') {
            $lead_ref_type = '%';
        }

        if ($contact == '') {
            $contact_first = '%';
            $contact_last = '%';
        } else {
            $contact_array = explode(' ', $contact);

            if (count($contact_array) > 1) {
                $contact_first = '%'.$contact_array[0].'%';
                $contact_last = '%'.end($contact_array).'%';
            } else {
                $contact_first = '%'.$contact_array[0].'%';
                $contact_last = '%'.$contact_array[0].'%';
            }
        }

        $leads = $this->getSearchLeads($agent, $property_type, $lead_ref_type, $contact_first, $contact_last,
            $searchable_note, $lead_array, Request::get('current_tags'), 0);

        $leads = $this->getLeadsbyDescription($leads);

        $agents = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->pluck('fullname', 'id');

        $agents = [Auth::user()->id => Auth::user()->firstname.' '.Auth::user()->lastname] + $agents;
        $lead_ref_types = DB::table('lead_ref_types')->where('active', '1')->pluck('type', 'id');

        if ($searchable_note != '%') {
            $searchable_note = str_replace('%', '', $searchable_note);
        } else {
            $searchable_note = '%25';
        }

        $contacts = DB::select('SELECT CONCAT(u.firstname, " ", u.lastname) AS name
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                GROUP BY name
                                UNION ALL
                                SELECT CONCAT(lc.firstname, " ", lc.lastname) AS name
                                FROM lead_contacts lc
                                INNER JOIN leads l ON lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                GROUP BY NAME');

        $contact_array = [];
        foreach ($contacts as $contact) {
            $contact_array[] = trim($contact->name);
        }

        $tagDB = DB::table('tags')->get();
        foreach ($tagDB as $tag) {
            $tags[] = $tag->name;
        }
        $tags = implode('", "', $tags);

        $current_tags = explode(',', Request::get('current_tags'));

        if ($current_tags[0] == '') {
            $current_tags = [];
        }

        return view('leads.index_notes', ['search_note' => $searchable_note, 'leads' => $leads,
            'agent' => urlencode($agent), 'property_type' => urlencode($property_type), 'lead_ref_type' => urlencode($lead_ref_type),
            'agents' => $agents, 'lead_ref_types' => $lead_ref_types, 'contact_first' => urlencode($contact_first),
            'contact_last' => urlencode($contact_last), 'contacts' => json_encode($contact_array), 'tags' => $tags,
            'current_tags' => $current_tags, ]);
    }

    public function searchLeadNotesArchived()
    {
        $search_note = Request::get('search_note');
        $agent = Request::get('search_by_bdm');
        $property_type = Request::get('search_by_property');
        $lead_ref_type = Request::get('search_by_lead_type');
        $contact = Request::get('Contacts');

        if (Session::has(Auth::user()->id.'Lead_Note')) {
            Session::forget(Auth::user()->id.'Lead_Note');
            Session::put(Auth::user()->id.'Lead_Note', $search_note);
        } else {
            Session::put(Auth::user()->id.'Lead_Note', $search_note);
        }

        if ($search_note == '' && $agent == '' && $property_type == '' && $lead_ref_type == '' && $contact == '' &&
            Request::get('current_tags') == ''
        ) {
            return redirect()->route('leads.archived');
        }

        if ($search_note == '') {
            $searchable_note = '%';
        } else {
            $searchable_note = '%'.$search_note.'%';
        }

        $db_results = DB::select('SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE "'.$searchable_note.'"
                                    AND pn.lead_id IS NOT NULL
                                    GROUP BY pn.lead_id');

        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }

        if (empty($lead_array)) {
            Flash::message('There are no archived leads with this word/phrase in notes.');

            return redirect()->route('user.leads', ['user_id' => Auth::user()->id]);
        }

        if ($agent == '') {
            $agent = '%';
        }

        if ($property_type == '') {
            $property_type = '%';
        }

        if ($lead_ref_type == '') {
            $lead_ref_type = '%';
        }

        if ($contact == '') {
            $contact_first = '%';
            $contact_last = '%';
        } else {
            $contact_array = explode(' ', $contact);
            if (count($contact_array) > 1) {
                $contact_first = '%'.$contact_array[0].'%';
                $contact_last = '%'.$contact_array[1].'%';
            } else {
                $contact_first = '%'.$contact_array[0].'%';
                $contact_last = '%'.$contact_array[0].'%';
            }
        }

        $leads = $this->getSearchLeads($agent, $property_type, $lead_ref_type, $contact_first, $contact_last,
            $searchable_note, $lead_array, Request::get('current_tags'), 1);

        $leads = (object) $leads;

        $leads = $this->getLeadsbyDescription($leads);

        $agents = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->pluck('fullname', 'id');

        $agents = [Auth::user()->id => Auth::user()->firstname.' '.Auth::user()->lastname] + $agents;
        $lead_ref_types = DB::table('lead_ref_types')->where('active', '1')->pluck('type', 'id');

        if ($searchable_note != '%') {
            $searchable_note = str_replace('%', '', $searchable_note);
        } else {
            $searchable_note = '%25';
        }

        $current_tags = explode(',', Request::get('current_tags'));

        $contacts = DB::select('SELECT CONCAT(u.firstname, " ", u.lastname) AS name
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                GROUP BY name
                                UNION ALL
                                SELECT CONCAT(lc.firstname, " ", lc.lastname) AS name
                                FROM lead_contacts lc
                                INNER JOIN leads l ON lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                GROUP BY NAME');

        $contact_array = [];
        foreach ($contacts as $contact) {
            $contact_array[] = trim($contact->name);
        }

        $tagDB = DB::table('tags')->get();
        foreach ($tagDB as $tag) {
            $tags[] = $tag->name;
        }
        $tags = implode('", "', $tags);

        if ($current_tags[0] == '') {
            $current_tags = [];
        }

        return view('leads.index_notes', ['search_note' => $searchable_note, 'leads' => $leads,
            'agent' => urlencode($agent), 'property_type' => urlencode($property_type), 'lead_ref_type' => urlencode($lead_ref_type),
            'agents' => $agents, 'lead_ref_types' => $lead_ref_types, 'contact_first' => urlencode($contact_first),
            'contact_last' => urlencode($contact_last), 'contacts' => json_encode($contact_array), 'tags' => $tags,
            'current_tags' => $current_tags, ]);
    }

    public function searchLeadNotesPage($search_note)
    {
        if ($search_note == '') {
            return redirect()->route('user.leads', ['user_id' => Auth::user()->id]);
        }

        $searchable_note = '%'.$search_note.'%';

        $sql = 'SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE "'.$searchable_note.'"
                                    AND pn.lead_id IS NOT NULL
                                    GROUP BY pn.lead_id';

        $db_results = DB::select($sql);

        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }

        $leads = $this->lead->whereIn('id', $lead_array)->orderBy('created_at')->get();

        $leads = $this->getLeadsbyDescription($leads);

        return view('leads.index_notes', ['search_note' => $search_note, 'leads' => $leads]);
    }

    public function searchLeadNotesPageArchived($search_note)
    {
        if ($search_note == '') {
            return redirect()->route('user.leads', ['user_id' => Auth::user()->id]);
        }

        $searchable_note = '%'.$search_note.'%';
        $db_results = DB::select('SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE "'.$searchable_note.'"
                                    AND pn.lead_id IS NOT NULL
                                    GROUP BY pn.lead_id');

        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }

        $leads = $this->lead->whereIn('id', $lead_array)->orderBy('created_at')->onlyTrashed()->get();

        $leads = $this->getLeadsbyDescription($leads);

        return view('leads.index_notes', ['search_note' => $search_note, 'leads' => $leads]);
    }

    public function getSearchLeads($agent, $property_type, $lead_ref_type, $contact_first, $contact_last, $searchable_note,
                                   $lead_array, $current_tags, $archived)
    {
        if ($agent == '%') {
            $agent_sql = ' AND (bdm_allocated LIKE "%" OR bdm_allocated IS NULL)';
        } else {
            $agent_sql = ' AND bdm_allocated = "'.$agent.'"';
        }

        if ($property_type == '%') {
            $property_type_sql = ' AND (property_type LIKE "%" OR property_type IS NULL)';
        } else {
            $property_type_sql = ' AND property_type LIKE "'.$property_type.'"';
        }

        if ($lead_ref_type == '%') {
            $lead_ref_type_sql = ' AND (lead_ref_type LIKE "%" OR lead_ref_type IS NULL)';
        } else {
            $lead_ref_type_sql = ' AND lead_ref_type LIKE "'.$lead_ref_type.'"';
        }
//        de($current_tags);
        $current_tags = explode(',', $current_tags);

        if ($current_tags[0] != '') {
            foreach ($current_tags as $tag) {
                $tag_dets = DB::table('tags')->whereName($tag)->first();
                if (is_object($tag_dets)) {
                    $tag_ids[] = $tag_dets->id;
                }
            }

            $contact_first_sql = '';
            $contact_last_sql = '';
            $tag_user_join = ' INNER JOIN tag_user tu ON tu.user_id = u.id';

            if ($contact_first == '%') {
                $user_first_sql = ' AND ((tu.tag_id IN ('.implode(',', $tag_ids).')) OR (u.firstname LIKE "%" OR u.firstname IS NULL)';
            } else {
                $user_first_sql = ' AND ((tu.tag_id IN ('.implode(',', $tag_ids).')) OR (u.firstname LIKE "'.$contact_first.'"';
            }

            if ($contact_last == '%') {
                $user_last_sql = ' OR (u.lastname LIKE "%" OR u.lastname IS NULL))';
            } elseif ($contact_first == $contact_last) {
                $user_last_sql = ' OR u.lastname LIKE "'.$contact_last.'"))';
            } else {
                $user_last_sql = ' AND u.lastname LIKE "'.$contact_last.'"))';
            }
        } else {
            $tag_user_join = '';
            if ($contact_first == '%') {
                $contact_first_sql = ' AND ((lc.firstname LIKE "%" OR lc.firstname IS NULL)';
            } else {
                $contact_first_sql = ' AND ((lc.firstname LIKE "'.$contact_first.'"';
            }

            if ($contact_last == '%') {
                $contact_last_sql = ' AND (lc.lastname LIKE "%" OR lc.lastname IS NULL)';
            } elseif ($contact_first == $contact_last) {
                $contact_last_sql = ' OR lc.lastname LIKE "'.$contact_last.'")';
            } else {
                $contact_last_sql = ' AND lc.lastname LIKE "'.$contact_last.'")';
            }

            if ($contact_first == '%') {
                $user_first_sql = ' OR (u.firstname LIKE "%" OR u.firstname IS NULL)';
            } else {
                $user_first_sql = ' OR (u.firstname LIKE "'.$contact_first.'"';
            }

            if ($contact_last == '%') {
                $user_last_sql = ' OR (u.lastname LIKE "%" OR u.lastname IS NULL))';
            } elseif ($contact_first == $contact_last) {
                $user_last_sql = ' OR u.lastname LIKE "'.$contact_last.'"))';
            } else {
                $user_last_sql = ' AND u.lastname LIKE "'.$contact_last.'"))';
            }
        }
        if ($archived == 0) {
            $query = 'SELECT l.*, lt.type as lead_type_description, lptt.type as listing_type, lc.firstname, lc.lastname
                          FROM leads l LEFT JOIN lead_contacts lc ON l.id = lc.lead_id LEFT JOIN lead_types lt
                          ON lt.id = l.lead_type LEFT JOIN lead_prop_trans_types lptt ON lptt.id = l.lead_prop_trans_type
                          LEFT JOIN lead_user lu ON l.id = lu.lead_id LEFT JOIN users u ON lu.user_id = u.id
                          '.$tag_user_join.' WHERE ( l.deleted_at IS NULL';
        } else {
            $query = 'SELECT l.*, lt.type as lead_type_description, lptt.type as listing_type, lc.firstname, lc.lastname
                          FROM leads l LEFT JOIN lead_contacts lc ON l.id = lc.lead_id LEFT JOIN lead_types lt
                          ON lt.id = l.lead_type LEFT JOIN lead_prop_trans_types lptt ON lptt.id = l.lead_prop_trans_type
                          LEFT JOIN lead_user lu ON l.id = lu.lead_id LEFT JOIN users u ON lu.user_id = u.id
                          '.$tag_user_join.' WHERE ( l.deleted_at IS NOT NULL';
        }
        $end_of_query = ' GROUP BY l.id';
        if ($searchable_note == '%') {
            $in_leads_sql = '';
        } else {
            $in_leads_sql = ' AND (l.id IN ('.implode(',', $lead_array).'))';
        }

        $final_query = $query.$agent_sql.$property_type_sql.$lead_ref_type_sql.$contact_first_sql.
            $contact_last_sql.$user_first_sql.$user_last_sql.')'.$in_leads_sql.$end_of_query;

        $leads = DB::select($final_query);

        return $leads;
    }

    public function getLeadsByNote($search_note, $agent, $property_type, $lead_ref_type, $contact_first, $contact_last,
                                   $current_tags)
    {
        if ($search_note != '%') {
            $searchable_note = '%'.$search_note.'%';
        } else {
            $searchable_note = '%';
        }

        $query = 'SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE "'.$searchable_note.'"
                                    AND pn.lead_id IS NOT NULL
                                    GROUP BY pn.lead_id';

        $db_results = DB::select($query);
        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }

        $agent = str_replace('%25', '%', $agent);
        $property_type = str_replace('%25', '%', $property_type);
        $lead_ref_type = str_replace('%25', '%', $lead_ref_type);
        $contact_first = str_replace('%25', '%', $contact_first);
        $contact_last = str_replace('%25', '%', $contact_last);

        $current_tags = json_decode($current_tags);
        $current_tag_string = '';
        foreach ($current_tags as $tag) {
            $current_tag_string .= $tag.',';
        }
        rtrim($current_tag_string, ',');

        $leads = $this->getSearchLeads($agent, $property_type, $lead_ref_type, $contact_first, $contact_last,
            $searchable_note, $lead_array, $current_tag_string, 0);

        $leads = json_decode(json_encode($leads), true);

        $lead_editors = User::whereHas('roles', function ($q) {
            $q->where('name', 'Admin')->orWhere('name', 'Agent')->orWhere('name', 'LeadReviewer');
        })->get();

        $lead_editor_ids = [];

        foreach ($lead_editors as $lead_editor) {
            $lead_editor_ids[] = $lead_editor->id;
        }

        $lead_id_array = [];

        foreach ($lead_editor_ids as $lead_editor_id) {
            $sessions = DB::select('SELECT * FROM lead_session WHERE user_id ='.$lead_editor_id);
            if (! empty($sessions)) {
                $lead_id_array[$lead_editor_id] = $sessions[0]->lead_id;
            }
        }

        $user_id = Auth::user()->id;
        //dd($leads);
        return Datatable::collection(new Collection($leads))
            ->addColumn('created_at', function ($Lead) {
                return date('Y-m-d H:i', strtotime($Lead['created_at']));
            })
            ->showColumns('reference')
            ->addColumn('bdm', function ($Lead) {
                if ($Lead['bdm_allocated'] == 0) {
                    return 'None allocated';
                } else {
                    $bdm = User::find($Lead['bdm_allocated']);
                    $firstname = $bdm['firstname'];

                    return '<a href="/leads/'.$Lead['bdm_allocated'].'/user" >'.$firstname.'</a>';
                }
            })
            ->showColumns('lead_title')
            ->addColumn('address', function ($Lead) {
                if ($Lead['street_number'] != null) {
                    $address = $Lead['street_number'];
                } else {
                    $address = '';
                }
                if ($Lead['street_name'] != null) {
                    $address .= ' '.$Lead['street_name'];
                } else {
                    $address .= '';
                }
                if ($Lead['suburb_town_city'] != null) {
                    $address .= ' '.$Lead['suburb_town_city'];
                } else {
                    $address .= '';
                }
                if ($address == '') {
                    return 'No Address';
                } else {
                    return $address;
                }
            })
            ->addColumn('transaction_type', function ($Lead) {
                $type = DB::table('lead_prop_trans_types')->select('type')->whereId($Lead['lead_prop_trans_type'])
                    ->first();
                if (! empty($type)) {
                    if ($type->type == 'Other') {
                        if ($Lead['lead_prop_trans_type_other'] == '') {
                            return 'Other';
                        }

                        return $Lead['lead_prop_trans_type_other'];
                    } else {
                        return $type->type;
                    }
                } else {
                    return 'Not Selected';
                }
            })
            ->addColumn('contact_firstname', function ($Lead) {
                $query = 'SELECT u.firstname, u.lastname
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                AND lu.lead_id = '.$Lead['id'].'
                                UNION ALL
                                SELECT lc.firstname, lc.lastname
                                FROM lead_contacts lc
                                INNER JOIN leads l ON l.id = lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                AND l.id = '.$Lead['id'].'
                                LIMIT 1';

                $contact = DB::select($query);

                if (empty($contact)) {
                    return 'No Contacts';
                }

                return $contact[0]->firstname.' '.$contact[0]->lastname;
            })
            ->addColumn('contact_email', function ($Lead) {
                $query = 'SELECT u.email, u.cellnumber AS tel
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                AND lu.lead_id = '.$Lead['id'].'
                                UNION ALL
                                SELECT lc.email, lc.tel
                                FROM lead_contacts lc
                                INNER JOIN leads l ON l.id = lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                AND l.id = '.$Lead['id'].'
                                LIMIT 1';

                $contact = DB::select($query);

                if (empty($contact)) {
                    return 'No Contacts';
                }
                $email = '';
                if ($contact[0]->email != '') {
                    $email = '<strong>Email:</strong> '.$contact[0]->email.'<br>';
                }
                $tel = '';
                if ($contact[0]->tel != '') {
                    $tel = '<strong>Tel: </strong>'.$contact[0]->tel;
                }

                return $email.$tel;
            })
            ->addColumn('note', function ($Lead) {
                $query = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                          INNER JOIN users u ON u.id = p.users_id
                          WHERE p.lead_id = '.$Lead['id'].'
                          ORDER BY p.created_at DESC
                          LIMIT 1';
                $last_note = DB::select($query);
                if (empty($last_note)) {
                    return 'No Notes';
                }

                return $last_note[0]->note.' <i>...'.$last_note[0]->date.'</i>';
            })
            ->addColumn('actions', function ($Lead) use ($user_id, $lead_id_array) {
                if (in_array($Lead['id'], $lead_id_array)) {
                    $editing_user_id = array_search($Lead['id'], $lead_id_array);
                    $session = DB::select('SELECT * FROM lead_session WHERE user_id ='.$editing_user_id);
                    if (($editing_user_id == Auth::user()->id) || (strtotime($session[0]->created_at) <= strtotime('-1 hours'))) {
                        return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                    }
                    $editing_user = DB::table('users')->whereId($editing_user_id)->first();

                    return $editing_user->firstname.' is busy with lead';
                } else {
                    if ($user_id == 'Archived') {
                        return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/restore" title="Restore" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
                    } else {
                        $query = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                                  INNER JOIN users u ON u.id = p.users_id
                                  WHERE p.lead_id = '.$Lead['id'];
                        $last_note = DB::select($query);
                        if (count($last_note) > 1) {
                            $Lead['check_lead'] = 1;
                        }
                        if ((Auth::user()->id == $Lead['bdm_allocated'] && $Lead['check_lead'] == null) || (Auth::user()->hasRole('Admin') && $Lead['check_lead'] == null)) {
                            return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                            <a href="/leads/'.$Lead['id'].'/check_lead" title="Mark as Viewed" ><i class="i-circled i-light i-alt i-small icon-check" style="color:#FF3100"></i></a>';
                        } elseif (Auth::user()->hasRole('Admin') && $Lead['check_lead'] == 1) {
                            return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                            <a href="/leads/'.$Lead['id'].'/check_lead" title="Mark as Viewed" ><i class="i-circled i-light i-alt i-small icon-check" style="color:#00FF48"></i></a>';
                        } else {
                            return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                        }
                    }
                }
            })
            ->searchColumns('created_at', 'reference', 'bdm', 'lead_title', 'address', 'transaction_type', 'contact_firstname', 'contact_email', 'note')
            ->orderColumns('created_at', 'reference', 'bdm', 'lead_title', 'address', 'transaction_type', 'contact_firstname', 'contact_email', 'note')
            ->make();
    }

    public function getLeadsByNoteArchived($search_note, $agent, $property_type, $lead_ref_type, $contact_first,
                                           $contact_last, $current_tags)
    {
        if ($search_note != '%') {
            $searchable_note = '%'.$search_note.'%';
        } else {
            $searchable_note = '%';
        }

        $query = 'SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE "'.$searchable_note.'"
                                    AND pn.lead_id IS NOT NULL
                                    GROUP BY pn.lead_id';

        $db_results = DB::select($query);
        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }
        $agent = urldecode($agent);
        $property_type = urldecode($property_type);
        $lead_ref_type = urldecode($lead_ref_type);
        $contact_first = urldecode($contact_first);
        $contact_last = urldecode($contact_last);

        $current_tags = json_decode($current_tags);
        $current_tag_string = '';
        foreach ($current_tags as $tag) {
            $current_tag_string .= $tag.',';
        }
        rtrim($current_tag_string, ',');

        $leads = $this->getSearchLeads($agent, $property_type, $lead_ref_type, $contact_first, $contact_last,
            $searchable_note, $lead_array, $current_tag_string, 1);

        $leads = json_decode(json_encode($leads), true);

        $lead_editors = User::whereHas('roles', function ($q) {
            $q->where('name', 'Admin')->orWhere('name', 'Agent')->orWhere('name', 'LeadReviewer');
        })->get();

        $lead_editor_ids = [];

        foreach ($lead_editors as $lead_editor) {
            $lead_editor_ids[] = $lead_editor->id;
        }

        $lead_id_array = [];

        foreach ($lead_editor_ids as $lead_editor_id) {
            $sessions = DB::select('SELECT * FROM lead_session WHERE user_id ='.$lead_editor_id);
            if (! empty($sessions)) {
                $lead_id_array[$lead_editor_id] = $sessions[0]->lead_id;
            }
        }

        $user_id = Auth::user()->id;

        return Datatable::collection(new Collection($leads))
            ->addColumn('created_at', function ($Lead) {
                return date('Y-m-d H:i', strtotime($Lead['created_at']));
            })
            ->showColumns('reference')
            ->addColumn('bdm', function ($Lead) {
                if ($Lead['bdm_allocated'] == 0) {
                    return 'None allocated';
                } else {
                    $bdm = User::find($Lead['bdm_allocated']);
                    $firstname = $bdm['firstname'];

                    return '<a href="/leads/'.$Lead['bdm_allocated'].'/user" >'.$firstname.'</a>';
                }
            })
            ->showColumns('lead_title')
            ->addColumn('address', function ($Lead) {
                if ($Lead['street_number'] != null) {
                    $address = $Lead['street_number'];
                } else {
                    $address = '';
                }
                if ($Lead['street_name'] != null) {
                    $address .= ' '.$Lead['street_name'];
                } else {
                    $address .= '';
                }
                if ($Lead['suburb_town_city'] != null) {
                    $address .= ' '.$Lead['suburb_town_city'];
                } else {
                    $address .= '';
                }
                if ($address == '') {
                    return 'No Address';
                } else {
                    return $address;
                }
            })
            ->addColumn('transaction_type', function ($Lead) {
                $type = DB::table('lead_prop_trans_types')->select('type')->whereId($Lead['lead_prop_trans_type'])
                    ->first();
                if (! empty($type)) {
                    if ($type->type == 'Other') {
                        if ($Lead['lead_prop_trans_type_other'] == '') {
                            return 'Other';
                        }

                        return $Lead['lead_prop_trans_type_other'];
                    } else {
                        return $type->type;
                    }
                } else {
                    return 'Not Selected';
                }
            })
            ->addColumn('contact_firstname', function ($Lead) {
                $query = 'SELECT u.firstname, u.lastname
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                AND lu.lead_id = '.$Lead['id'].'
                                UNION ALL
                                SELECT lc.firstname, lc.lastname
                                FROM lead_contacts lc
                                INNER JOIN leads l ON l.id = lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                AND l.id = '.$Lead['id'].'
                                LIMIT 1';

                $contact = DB::select($query);

                if (empty($contact)) {
                    return 'No Contacts';
                }

                return $contact[0]->firstname.' '.$contact[0]->lastname;
            })
            ->addColumn('contact_email', function ($Lead) {
                $query = 'SELECT u.email, u.cellnumber AS tel
                                FROM users u
                                INNER JOIN lead_user lu ON u.id = lu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "
                                AND lu.lead_id = '.$Lead['id'].'
                                UNION ALL
                                SELECT lc.email, lc.tel
                                FROM lead_contacts lc
                                INNER JOIN leads l ON l.id = lc.lead_id
                                WHERE CONCAT(lc.firstname, " ",lc.lastname) IS NOT NULL
                                AND CONCAT(lc.firstname, " ",lc.lastname) <> " "
                                AND l.id = '.$Lead['id'].'
                                LIMIT 1';

                $contact = DB::select($query);

                if (empty($contact)) {
                    return 'No Contacts';
                }
                $email = '';
                if ($contact[0]->email != '') {
                    $email = '<strong>Email:</strong> '.$contact[0]->email.'<br>';
                }
                $tel = '';
                if ($contact[0]->tel != '') {
                    $tel = '<strong>Tel: </strong>'.$contact[0]->tel;
                }

                return $email.$tel;
            })
            ->addColumn('note', function ($Lead) {
                $query = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                          INNER JOIN users u ON u.id = p.users_id
                          WHERE p.lead_id = '.$Lead['id'].'
                          ORDER BY p.created_at DESC
                          LIMIT 1';
                $last_note = DB::select($query);
                if (empty($last_note)) {
                    return 'No Notes';
                }

                return $last_note[0]->note.' <i>...'.$last_note[0]->date.'</i>';
            })
            ->addColumn('actions', function ($Lead) use ($user_id, $lead_id_array) {
                if (in_array($Lead['id'], $lead_id_array)) {
                    $editing_user_id = array_search($Lead['id'], $lead_id_array);
                    $session = DB::select('SELECT * FROM lead_session WHERE user_id ='.$editing_user_id);
                    if (($editing_user_id == Auth::user()->id) || (strtotime($session[0]->created_at) <= strtotime('-1 hours'))) {
                        return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                    }
                    $editing_user = DB::table('users')->whereId($editing_user_id)->first();

                    return $editing_user->firstname.' is busy with lead';
                } else {
                    if ($user_id == 'Archived') {
                        return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/restore" title="Restore" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
                    } else {
                        $query = 'SELECT p.note, p.created_at AS date FROM prop_notes p
                                  INNER JOIN users u ON u.id = p.users_id
                                  WHERE p.lead_id = '.$Lead['id'];
                        $last_note = DB::select($query);
                        if (count($last_note) > 1) {
                            $Lead['check_lead'] = 1;
                        }
                        if ((Auth::user()->id == $Lead['bdm_allocated'] && $Lead['check_lead'] == null) || (Auth::user()->hasRole('Admin') && $Lead['check_lead'] == null)) {
                            return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                            <a href="/leads/'.$Lead['id'].'/check_lead" title="Mark as Viewed" ><i class="i-circled i-light i-alt i-small icon-check" style="color:#FF3100"></i></a>';
                        } elseif (Auth::user()->hasRole('Admin') && $Lead['check_lead'] == 1) {
                            return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>
                            <a href="/leads/'.$Lead['id'].'/check_lead" title="Mark as Viewed" ><i class="i-circled i-light i-alt i-small icon-check" style="color:#00FF48"></i></a>';
                        } else {
                            return '<a href="/leads/'.$Lead['id'].'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/leads/'.$Lead['id'].'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                        }
                    }
                }
            })
            ->searchColumns('created_at', 'reference', 'bdm', 'lead_title', 'address', 'transaction_type', 'contact_firstname', 'contact_email', 'note')
            ->orderColumns('created_at', 'reference', 'bdm', 'lead_title', 'address', 'transaction_type', 'contact_firstname', 'contact_email', 'note')
            ->make();
    }

    public function getLeadsbyDescription($leads)
    {
        foreach ($leads as $lead) {
            switch ($lead->lead_type_description) {
                case 'Property':
                    $lead->content = $lead->street_number.' '.$lead->street_name;
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }

                    break;
                case 'Person':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('role_id', '<>', '', 'and')
                        ->join('tags', 'role_id', '=', 'tags.id')
                        ->first();
                    if ($contact != null) {
                        $lead->content = $contact->firstname.' '.$contact->lastname;
                    } else {
                        $lead->content = ' ';
                    }
                    if ($lead->content == ' ') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->name;

                    break;
                case 'Company/Trust':
                    $contact = $this->contact->whereLead_id($lead->id)
                        ->where('company_type', '<>', '', 'and')
                        ->join('company_types', 'company_type', '=', 'company_types.id')
                        ->first();
                    if ($contact == null) {
                        goto defaultLabel;
                    } else {
                        $lead->content = $contact->firstname;
                    }
                    if ($lead->content == '') {
                        goto defaultLabel;
                    }
                    $lead->listing_type = $contact->type;

                    break;
                default:
                    defaultLabel:
                    $note = $this->prop_notes
                        ->whereLead_id($lead->id)
                        ->orderBy('prop_notes.id', 'Desc')
                        ->first();
                    $lead->content = $note['note'];
                    if ($lead->content == null) {
                        $lead->content = 'Empty';
                    }

                    break;
            }
        }

        return $leads;
    }

    public function disconnectProperty()
    {
        $input = Request::all();
        $property_id = $this->lead->whereId($input['lead_id'])->first();

        $property = $this->property->whereId($property_id->property_id)->first();

        DB::update('Update leads set property_id = NULL WHERE id = '.$property_id->id);

        $prop_notes = new PropNotes();
        $prop_notes->property_id = $property->id;
        $prop_notes->lead_id = $input['lead_id'];
        $prop_notes->users_id = Auth::user()->id;
        $message = 'This lead was disconnected from property '.$property->reference.'<br>';
        if ($input['message'] == '') {
            $message .= 'User: ';
        } else {
            $message .= $input['message'];
        }
        $prop_notes->note = $message;

        $prop_notes->save();
    }

    public function connectProperty()
    {
        $input = Request::all();

        $refCheck = strtoupper(substr($input['address'], 0, 5));

        if ($refCheck == 'SALE-' || $refCheck == 'RENT-' || $refCheck == 'AUCT-') {
            $property = $this->property->
            where('property.reference', '=', $input['address'])->
            get();
            $property_id = $property[0]->id;
        } else {
            $address_array = explode(' - ', $input['address']);
            $street_number = $address_array[0];
            $street_address = $address_array[1];
            $property = $this->property->
            where('property.street_number', '=', $street_number)->
            where('property.street_address', '=', $street_address)->
            get();

            if (count($property) > 1) {
                return 'multiple';
            } else {
                $property_id = $property[0]->id;
            }
        }
        $lead = $this->lead->whereId($input['lead_id'])->first();

        $lead->property_id = $property_id;

        $lead->update();
        $prop_notes = new PropNotes();
        $prop_notes->property_id = $property_id;
        $prop_notes->lead_id = $input['lead_id'];
        $prop_notes->users_id = Auth::user()->id;
        $message = 'This lead was connected to property '.$property[0]->reference.'<br>';
        if ($input['message'] == '') {
            $message .= 'User: ';
        } else {
            $message .= $input['message'];
        }
        $prop_notes->note = $message;

        $prop_notes->save();

        return $property[0];
    }

    public function getUserDetails($user)
    {
        if (is_numeric($user)) {
            $user = $this->user->whereCellnumber($user)->first();
        } elseif (! filter_var($user, FILTER_VALIDATE_EMAIL)) {
            $user_array = explode(' - ', $user);
            $user = $this->user->whereFirstname($user_array[0])->whereLastname($user_array[1])->get();
            if (count($user) > 1) {
                $user = 'More';
            } else {
                $user = $this->user->whereFirstname($user_array[0])->whereLastname($user_array[1])->first();
            }
        } else {
            $user = $this->user->whereEmail($user)->first();
        }

        return $user;
    }

    public function deleteLeadUser($lead_id, $user_id, $role_id)
    {
        DB::delete('DELETE FROM lead_user WHERE lead_id = '.$lead_id.' AND user_id = '.$user_id.' AND role_id = '.$role_id);

        $tag = DB::table('tagss')->whereId($role_id)->first();
        $user = DB::table('users')->select(['firstname', 'lastname'])->whereId($user_id)->first();
        $prop_notes = new PropNotes();
        $prop_notes->lead_id = $lead_id;
        $prop_notes->users_id = Auth::user()->id;
        $message = $user->firstname.' '.$user->lastname.' who had the '.$tag->description.' tag was removed from this lead';
        $prop_notes->note = $message;

        $prop_notes->save();

        return redirect()->back();
    }

    public function addPropertyEnquiry()
    {
        $users_array = json_decode(Request::get('file_json'));
        $property_id = Request::get('property_id');
        $returnLine = '';

        foreach ($users_array as $user) {
            if (isset($user->email) && trim($user->email) != '' && trim($user->Telephone) != '') {
                try {
                    $fullname = trim($user->Name);
                    Log::debug('Name: '.$fullname." \n");
                    $email = trim($user->email);

                    $name_array = explode(' ', $fullname);

                    $firstname = trim($name_array[0]);
                    $lastname = '';
                    if (isset($name_array[1])) {
                        $lastname = trim($name_array[1]);
                    }
                    $cellnumber = trim($user->Telephone);

                    $bidder = trim($user->Bidder);

                    if (! $user = $this->user->whereEmail($email)->first()) {
                        $user_id = App::make(\App\Http\Controllers\RegistrationController::class)->storeLeadUser($email, $firstname, $lastname, $cellnumber, 11);

                        $user = User::find($user_id);

                        if ($bidder == 1) {
                            $tag = DB::table('tags')->where('name', '=', 'AUCTION BIDDER')->first();
                            $tagName = DB::table('tags')->whereId($tag->id)->first();
                            if ($user->hasTag($tagName->name) == false) {
                                $user->tags()->attach($tag->id);
                            }
                            $this->mailer->sendWelcomeMessageToAuctionBidder($user);
                        } else {
                            $this->mailer->sendWelcomeMessageToAddedUser($user);
                        }
                    }

                    $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
                    $tagName = DB::table('tags')->whereId($tag->id)->first();
                    if ($user->hasTag($tagName->name) == false) {
                        $user->tags()->attach($tag->id);
                    }
                    if (! DB::select('SELECT * FROM lead_user lu INNER JOIN leads l ON l.id
                            = lu.lead_id WHERE user_id = '.$user->id.' AND l.property_id = '.$property_id)
                    ) {
                        $query = 'SELECT u.email, u.firstname, u.lastname, p.reference, u.id as agent_id, p.complex_number,
                                p.complex_name, p.street_number, p.street_address, p.listing_type FROM property p
                                INNER JOIN vetting v ON p.id = v.property_id INNER JOIN users u ON v.rep_id_number = u.id
                                WHERE p.id = '.$property_id;

                        $details = DB::select($query);

                        $reference = $this->getReference();

                        $lead = new Lead();

                        $lead->fill(['lead_type' => 2, 'lead_ref_type' => 9, 'lead_ref' => 'Property Enquiry',
                            'lead_prop_trans_type' => $details[0]->listing_type, 'bdm_allocated' => $details[0]->agent_id,
                            'reference' => $reference, 'property_id' => $property_id, 'deleted_at' => date('Y-m-d H:i:s'), ])->save();
                        $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
                        DB::table('lead_user')->insert(['user_id' => $user->id, 'lead_id' => $lead->id, 'role_id' => $tag->id]);

                        $input['message'] = 'User added with CSV file';

                        $prop_notes = new PropNotes();

                        $prop_notes->note = $user->firstname.' '.$user->lastname.' has enquired about a property
            reference number: '.$details[0]->reference.'<br>'.$input['message'];
                        $prop_notes->lead_id = $lead->id;
                        $prop_notes->users_id = Auth::user()->id;
                        $prop_notes->property_id = $property_id;
                        $prop_notes->save();

                        $inputContact['lead_id'] = $lead->id;
                        $inputContact['role_id'] = 11;
                        $inputContact['firstname'] = $user->firstname;
                        $inputContact['lastname'] = $user->lastname;
                        $inputContact['email'] = $user->email;
                        $inputContact['tel'] = $user->cellnumber;
                    }
                } catch (exception $e) {
                    Log::error($e->getMessage().'\n'.json_encode($name_array).'\n');
                    $returnLine = 'Error';
                }
            }
        }

        if ($returnLine == 'Error') {
            return 'Error';
        } else {
            return 'Done';
        }
    }
}
