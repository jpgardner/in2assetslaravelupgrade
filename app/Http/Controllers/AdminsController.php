<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     * GET /admins.
     *
     * @return Response
     */
    public function index()
    {
//        App::make('App\Http\Controllers\FunctionController')->rolesToTags();

        $propertyCount = DB::table('property')
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('vetting_status', '=', '2')
            ->count();
        $usersCount = DB::table('users')->count();
        $serviceProviderCount = DB::table('service_provider')->count();
        $classifiedsCount = DB::table('classifieds')->whereStatus('live')->count();

        return view('admin.index', ['propertyCount' => $propertyCount, 'usersCount' => $usersCount,
            'serviceProviderCount' => $serviceProviderCount, 'classifiedsCount' => $classifiedsCount, ]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /admins/create.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /admins.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /admins/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /admins/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /admins/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /admins/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
