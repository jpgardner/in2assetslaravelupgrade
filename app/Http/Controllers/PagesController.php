<?php

namespace App\Http\Controllers;

use App\In2Assets\Blogs\BlogRepository;
use App\In2Assets\Forms\PublishBlogForm;
use App\Mail\newLead;
use App\Models\Auction;
use App\Models\AuctionRegister;
use App\Models\BrokerLogos;
use App\Models\Lead;
use App\Models\PhysicalAuctionRegister;
use App\Models\Property;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class PagesController extends Controller
{
//    use CommandBus;
    protected $blogRepository;

    protected $publishBlogForm;

    public function __construct(BlogRepository $blogRepository, PublishBlogForm $publishBlogForm, Property $property,
                         Auction $auction, BrokerLogos $broker_logos, Lead $lead, AuctionRegister $auction_register,
                         PhysicalAuctionRegister $physical_auction_register)
    {
        $this->blogRepository = $blogRepository;
        $this->publishBlogForm = $publishBlogForm;
        $this->property = $property;
        $this->auction = $auction;
        $this->broker_logos = $broker_logos;
        $this->lead = $lead;
        $this->auction_register = $auction_register;
        $this->physical_auction_register = $physical_auction_register;
    }

    public function createLeadReferences()
    {
        $leads = $this->lead->withTrashed()->get();
        foreach ($leads as $lead) {
            if ($lead->reference == null) {
                $lead_ref = DB::select('SELECT `lead_ref` FROM `reference` WHERE `id` = ?', [1]);
                $lead_ref = $lead_ref[0]->lead_ref;
                $reference = 'LEAD-'.str_pad($lead_ref, 6, '0', STR_PAD_LEFT);
                $lead_ref++;
                DB::update('UPDATE `reference` SET `lead_ref` = ?, `updated_at` = NOW()', [$lead_ref]);

                $lead->reference = $reference;

                $lead->update();
            }
        }
    }

    //WeSell
    public function wesell()
    {
        return view('wesell.wesell');
    }

    public function WeSellLogin()
    {
        return view('wesell.registration');
    }

    public function WeSellPropertyInfo()
    {
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->groupBy('suburb_town_city')->groupBy('province')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        return view('wesell.property_info', ['cities' => $cities]);
    }

    public function WeSellConGrats()
    {
        return view('wesell.congratulations');
    }

    public function WesellHowItWorks()
    {
        return view('wesell.how_it_works');
    }

    //--Homepage
    public function home()
    {
        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas = implode('", "', $areas);

        $featured = App::make(\App\Http\Controllers\PropertyController::class)->getFeaturedProperties('property');

        $blogs = $this->blogRepository->getPaginated();
        $references = [];
        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        $propertiesProvince = $this->property
            ->selectRaw('trim(province) as province_trim, count(province) as count, count(*)')
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
            ->orderBy('count', 'desc')
            ->groupBy('province_trim')
            ->get();

        $propertiesCity = $this->property
            ->selectRaw('trim(area) as area_trim, trim(province) as province_trim, count(area) as count, count(*)')
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
            ->orderBy('count', 'desc')
            ->groupBy('area_trim', 'province_trim')
            ->get();

        $slug = new Slugify();

        return view('pages.home', ['references' => $references, 'provinces' => $provinces, 'areas' => $areas,
            'cities' => $cities, 'blogs' => $blogs, 'featured' => $featured, 'propertiesProvince' => $propertiesProvince,
            'slug' => $slug, 'propertiesCity' => $propertiesCity, ]);
    }

    public function searchByArea($area, $type)
    {
        $propertiesCity = null;
        $propertiesSuburb = null;
        $area = str_replace('-', ' ', $area);
        if ($type == 'province') {
            $propertiesCity = $this->property
                ->selectRaw('trim(area) as area_trim, province,  count(area) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('province', 'LIKE', '%'.$area)
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('area_trim')
                ->groupBy('area_trim')
                ->get();

            $propertiesSuburb = $this->property
                ->selectRaw('trim(suburb_town_city) as suburb_town_city_trim, province, area, count(suburb_town_city) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('province', 'LIKE', '%'.$area)
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('suburb_town_city_trim')
                ->groupBy('suburb_town_city_trim')
                ->get();
        } elseif ($type == 'city') {
            $propertiesSuburb = $this->property
                ->selectRaw('trim(suburb_town_city) as suburb_town_city_trim, province, area, count(suburb_town_city) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('area', 'LIKE', '%'.$area)
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('suburb_town_city_trim')
                ->groupBy('suburb_town_city_trim')
                ->get();
        }

        $slug = new Slugify();

        return view('property.search', ['propertiesCity' => $propertiesCity, 'propertiesSuburb' => $propertiesSuburb,
            'area' => $area, 'slug' => $slug, ]);
    }

    public function makeContact()
    {
        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            return Response::json([
                'errors' => $validate->errors(),
            ]);
        }

        $name = Request::get('template-contactform-name');
        $email = Request::get('template-contactform-email');
        $phone = Request::get('template-contactform-phone');
        $subject = Request::get('template-contactform-subject');
        $service = Request::get('template-contactform-service');
        $message = Request::get('template-contactform-message');

        $userName = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets Contact Form - '.$subject;

        $adminMail = 'auctions@in2assets.com';

        $emailData = "<p>A contact request was received from the website.</p>
    <table border='0'>
        <tr><td>Name </td><td>".$name."</td></tr>
        <tr><td>Email </td><td><a href='mailto:".$email."'>".$email.'</a></td></tr>
        <tr><td>Phone </td><td>'.$phone.'</td></tr>
        <tr><td>Subject </td><td>'.$subject.'</td></tr>
        <tr><td>Service </td><td>'.$service.'</td></tr>
        <tr><td>Message </td><td>'.$message.'</td></tr>
    </table>
    ';

        // Queue email
        Mail::to('auctions@in2assets.com')
            ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, $adminMail));

        return Response::json([
            'success' => true,
            'data' => 'Form Submitted',
        ]);
    }

    public function mobileAdvSearch($property_type, $listing_type, $province, $city, $suburb, $price_range = '0-15000000+',
                                    $rent_price_range = '0-30000+', $floor_range = '0-2000+', $land_range = '0-20000+',
                                    $sub_type = 'None', $bedrooms = 'None', $bathrooms = 'None')
    {
        $province_slug = $province;
        $city_slug = $city;
        $suburb_slug = $suburb;

        if ($property_type == 'residential') {
            $featured = App::make(\App\Http\Controllers\PropertyController::class)->getFeaturedProperties('Residential');
        } else {
            $featuredCom = App::make(\App\Http\Controllers\PropertyController::class)->getFeaturedProperties('Commercial');
            $featuredFarm = App::make(\App\Http\Controllers\PropertyController::class)->getFeaturedProperties('Farm');
            $featured = $featuredCom->merge($featuredFarm);
        }
        if ($province == 'all') {
            $province = '';
            $city = '';
            $suburb = '';
            $suburb2 = '';
        } elseif ($province != 'all' && $city != 'all' && $suburb != 'all') {
            $province = App::make(\App\Http\Controllers\PropertyController::class)->getProvince($province);
            $city = App::make(\App\Http\Controllers\PropertyController::class)->getCity($city);
            $suburb = App::make(\App\Http\Controllers\PropertyController::class)->getSuburb($suburb);
            $suburb2 = $province.', '.$city.', '.$suburb;
        } elseif ($city == 'all' && $suburb == 'all') {
            $province = App::make(\App\Http\Controllers\PropertyController::class)->getProvince($province);
            $city = '';
            $suburb = '';
            $suburb2 = $province;
        } elseif ($city != 'all' && $suburb == 'all') {
            $province = App::make(\App\Http\Controllers\PropertyController::class)->getProvince($province);
            $city = App::make(\App\Http\Controllers\PropertyController::class)->getCity($city);
            $suburb = '';
            $suburb2 = $province.', '.$city;
        } elseif ($suburb != 'all') {
            $province = App::make(\App\Http\Controllers\PropertyController::class)->getProvince($province);
            $city = '';
            $suburb = App::make(\App\Http\Controllers\PropertyController::class)->getSuburb($suburb);
            $suburb2 = $province.', '.$suburb;
        }

        switch ($listing_type) {
            case 'for-sale':
                $set_action_type = 'wesell';

                break;
            case 'to-rent':
            case 'to-let':
                $set_action_type = 'welet';

                break;
            case 'on-auction':
                $set_action_type = 'weauction';

                break;
            default:
                $set_action_type = 'all-listings';

                break;
        }

        $areas = App::make(\App\Http\Controllers\PropertyController::class)->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $set_all_type = $property_type;

        $adv_search_set['floor_space_range'] = $floor_range;
        $adv_search_set['land_size_range'] = $land_range;
        $adv_search_set['price_range'] = $price_range;
        $adv_search_set['price_range_rent'] = $rent_price_range;
        $adv_search_set['sub_type'] = $sub_type;
        $adv_search_set['bathrooms'] = $bathrooms;
        $adv_search_set['bedrooms'] = $bedrooms;

        if (Request::get('agent') > 0) {
            $agent = Request::get('agent');
        } else {
            $agent = '';
        }

        if ($property_type == 'residential') {
            $urlExt = '/'.$price_range.'/'.$rent_price_range.'/'.$floor_range.'/'.$land_range.'/'.$sub_type
                .'/'.$bathrooms.'/'.$bedrooms;
        } else {
            $urlExt = '/'.$price_range.'/'.$rent_price_range.'/'.$floor_range.'/'.$land_range.'/'.$sub_type;
        }

        return view('property.mobile_adv_search', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'listing' => 'Search', 'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, 'province' => $province,
            'city' => $city, 'suburb' => $suburb, 'province_slug' => $province_slug, 'city_slug' => $city_slug,
            'suburb_slug' => $suburb_slug, 'agent' => $agent, 'urlExt' => $urlExt, 'listing_type' => $listing_type,
            'suburb2' => $suburb2, ]);
    }

    // public function wesell()
    // {
//     $set_action_type = 'All Actions';
//     $set_all_type = 'Residential';

//     $provincesDB = DB::table('areas')->groupBy('province')->get();
//     $areasDB = DB::table('areas')->groupBy('area')->get();
//     $citiesDB = DB::table('areas')->groupBy('suburb_town_city')->get();

//     foreach($citiesDB as $city ){$cities_adv[] = $city->suburb_town_city;}
//     $cities_adv = implode('", "',$cities_adv);

//     foreach($provincesDB as $province ){$provinces_adv[] = $province->province;}
//     $provinces_adv = implode('", "',$provinces_adv);

//     foreach($areasDB as $area ){$areas_adv[] = $area->area;}
//     $areas_adv = implode('", "',$areas_adv);

//     $adv_search_set['listing_type'] = '';
//     $adv_search_set['property_type'] = 'Residential';
//     $adv_search_set['province'] = '';
//     $adv_search_set['area'] = '';
//     $adv_search_set['suburb_town_city'] = '';
//     $adv_search_set['sale_type'] = 'all';
//     $adv_search_set['floor_space_range'] = '0 - 2000+';
//     $adv_search_set['land_size_range'] = '0 - 20000+';
//     $adv_search_set['price_range'] = '0 - 15000000+';
//     $adv_search_set['price_range_rent'] = '0 - 30000+';

//     return view::make('pages.wesell.landing_page', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
//        'provinces_adv' => $provinces_adv, 'cities_adv' => $cities_adv, 'areas_adv' => $areas_adv, 'adv_search_set' => $adv_search_set]);
    // }

    //--About
    public function about()
    {
        return view('pages.about.about');
    }

    public function in2assetsOverview()
    {
        return view('pages.about.in2assetsoverview');
    }

    public function wesellOverview()
    {
        return view('pages.about.weselloverview');
    }

    public function why()
    {
        return view('pages.about.why');
    }

    public function careers()
    {
        return view('pages.about.careers');
    }

    public function affiliates()
    {
        return view('pages.about.affiliates');
    }

    public function board_of_directors()
    {
        return view('pages.about.BoardofDirectors');
    }

    public function termsConditions()
    {
        return view('pages.terms_conditions');
    }

    public function Showcase()
    {
        $properties = $this->property
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('final_price', '>', '1')
            ->where(function ($q) {
                $q->where('property.deleted_at', '>', '2015-12-31')
                    ->orWhere('property.deleted_at', '=', null);
            })
            ->where('lead_image', '!=', 'null')
            ->where(function ($q) {
                $q->where('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 6)
                    ->orWhere('vetting_status', '=', 10);
            })
            ->orderBy('final_price', 'desc')
            ->withTrashed()
            ->get();

        $title_types = DB::table('title_deed_types')->pluck('type', 'id');

        $provinces = DB::table('areas')->groupBy('province')->pluck('province', 'province');

        $title_types->put('', 'Select a title type');
        $provinces->put('', 'Province');

        return view('pages.showcase', ['properties' => $properties, 'title_types' => $title_types, 'provinces' => $provinces]);
    }

    public function termsConditionsWesell()
    {
        return view('pages.mandates.wesell');
    }

    public function termsConditionsIn2assets()
    {
        return view('pages.mandates.in2assets');
    }

    public function termsConditionsLetting()
    {
        return view('pages.mandates.letting');
    }

    public function termsConditionsLettingIn2Assets()
    {
        return view('pages.mandates.lettingin2assets');
    }

    //--How it Works
    public function howitworks()
    {
        return view('pages.howitworks.howitworks');
    }

    public function howitworksResidential()
    {
        return view('pages.howitworks.residential');
    }

    public function howitworksCommercial()
    {
        return view('pages.howitworks.commercial');
    }

    public function howitworksAuction()
    {
        return view('pages.howitworks.auction');
    }

    //--Classifieds
    public function classifieds()
    {
        return view('pages.classifieds');
    }

    //--Blog
    public function blog()
    {
        return view('pages.blog');
    }

    //--Auction Calendar
    public function calendar()
    {
        $date = date('m/d/Y');
        $yesterday = date('Y-m-d', strtotime($date.'-1 days'));

        //gets all bid to buy properties
        $btobproperties = $this->property->
        select('auction.property_id', 'start_date', 'end_date', 'guideline', 'reference', 'dwelling_type', 'commercial_type',
            'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->whereHas('auction', function ($q) {
                $date = date('m/d/Y');
                $yesterday = date('Y-m-d', strtotime($date.'-1 days'));
                $q->where('auction_type', '=', 2)
                    ->where('end_date', '>', $yesterday.' 23:59');
            })
            ->orWhereHas('auction', function ($q) {
                $date = date('m/d/Y');
                $yesterday = date('Y-m-d', strtotime($date.'-1 days'));
                $q->where('auction_type', '=', 3)
                    ->where('end_date', '>', $yesterday.' 23:59');
            })
            ->orderBy('start_date')
            ->get();

        //gets all online properties
        $onlineproperties = $this->property->select('auction.property_id', 'start_date', 'end_date', 'guideline', 'reference',
            'dwelling_type', 'commercial_type', 'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->where('start_date', '>', $yesterday.' 23:59')
            ->where('auction.auction_type', '=', 4)
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->orderBy('start_date')
            ->get();

        //gets all onsite properties
        $onSiteAuctions = $this->property->select('auction.id as auction_id', 'auction_image', 'auction_name', 'auction_description', 'auction.property_id', 'start_date', 'end_date', 'guideline', 'reference',
            'dwelling_type', 'commercial_type', 'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->where('start_date', '>', $yesterday.' 23:59')
            ->where('auction.auction_type', '=', 5)
            ->where('auction.deleted_at', '=', null)
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->orderBy('start_date')
            ->get();

        foreach ($onSiteAuctions as $auction) {
            $auction->images = $this->property->
            select('file')->
            join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')
                ->where('property.id', $auction->property_id)->orderBy('uploads.position')->get();
        }

        //gets all physical auctions
        $physicalAuctions = DB::table('physical_auctions')->where('start_date', '>', $yesterday.' 23:59')->whereDeleted_at(null)
            ->orderBy('start_date')->get();
//        dd(AppHelper::getLastQuery());
        foreach ($physicalAuctions as $auction) {
            $auctionCount = $this->auction->where('physical_auction', '=', $auction->id)->count();
            $auctionPics = $this->property->join('auction', 'auction.property_id', '=', 'property.id')->select('lead_image', 'property_id')
                ->where('auction.physical_auction', '=', $auction->id)->get();

            $auction->physical_auction_count = $auctionCount;
            $auction->lead_images = $auctionPics;
        }

        $auctionCalendarEvents = DB::table('auction')->select(['start_date', DB::raw('COUNT(*) AS start_date_count')])
            ->where('start_date', '>', $yesterday.' 23:59')->groupBy('start_date')
            ->orderBy('start_date', 'ASC')
            ->get();

        $register_array = [];
        if (Auth::check()) {
            $registered = $this->auction_register->whereUser_id(Auth::user()->id)->get();
            foreach ($registered as $register) {
                $register_array[] = $register->auction_id;
            }
        }

        $physical_register_array = [];
        if (Auth::check()) {
            $physical_registered = $this->physical_auction_register->whereUser_id(Auth::user()->id)->get();

            foreach ($physical_registered as $physical_register) {
                $physical_register_array[] = $physical_register->auction_id;
            }
        }

        $auction_count = count($physicalAuctions) + count($onSiteAuctions);

        $modal_title = 'Please login or register with our site to pre-register for auction';

        $calendarDate = date('Y-m');

        return view('pages.calendar', ['btobproperties' => $btobproperties, 'onlineproperties' => $onlineproperties,
            'auctionCalendarEvents' => $auctionCalendarEvents, 'physicalauctions' => $physicalAuctions, 'calendar_date' => $calendarDate,
            'onSiteAuctions' => $onSiteAuctions, 'find' => 0, 'register_array' => $register_array, 'auction_count' => $auction_count,
            'physical_register_array' => $physical_register_array, 'modal_title' => $modal_title, ]);
    }

    public function calendarDetail($id, $name = '')
    {
        $physicalproperties = $this->auction->wherePhysical_auction($id)
            ->select('auction.property_id', 'start_date', 'end_date', 'guideline', 'reference', 'dwelling_type', 'commercial_type',
                'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug', 'vetting.rep_firstname', 'vetting.rep_surname', 'vetting.rep_cell_num')
            ->join('property', 'auction.property_id', '=', 'property.id')
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->leftjoin('vetting', 'vetting.property_id', '=', 'property.id')
            ->orderBy('start_date')->get();

        $physicalAuction = DB::table('physical_auctions')->where('id', '=', $id)->whereDeleted_at(null)
            ->orderBy('start_date')->get();

//        dd($physicalAuction);
        foreach ($physicalproperties as $physicalproperty) {
            $physicalproperty->images = $this->property->
            join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')
                ->where('property.id', $physicalproperty->property_id)->orderBy('uploads.position')
                ->get();
        }

        return view('pages.calendar-detail', ['physicalproperties' => $physicalproperties,
            'physicalauction' => $physicalAuction, ]);
    }

    public function findauctions($auctionDate)
    {
        $auctionDateplus1 = date('Y-m-d', strtotime($auctionDate.'+1 days'));

        $btobproperties = $this->property->
        select('auction.property_id', 'start_date', 'end_date', 'guideline', 'reference', 'dwelling_type', 'commercial_type',
            'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->where(function ($q) {
                $q->where('auction_type', '=', 2)
                    ->orWhere('auction_type', '=', 3);
            })
            ->where('auction.start_date', '>=', $auctionDate)
            ->where('auction.end_date', '<=', $auctionDateplus1)
            ->get();

        $onlineproperties = $this->property
            ->select('auction.property_id', 'start_date', 'end_date', 'guideline', 'reference', 'dwelling_type', 'commercial_type',
                'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->where('auction.auction_type', '=', 4)
            ->where('auction.start_date', '>=', $auctionDate)
            ->where('auction.start_date', '<=', $auctionDateplus1)
            ->get();

        $onSiteAuctions = $this->property->select('auction_image', 'auction_name', 'auction_description', 'auction.property_id', 'start_date', 'end_date', 'guideline', 'reference',
            'dwelling_type', 'commercial_type', 'farm_type', 'suburb_town_city', 'province', 'listing_title', 'property.slug')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->where('auction.start_date', '>=', $auctionDate)
            ->where('auction.start_date', '<=', $auctionDateplus1)
            ->where('auction.auction_type', '=', 5)
            ->where('auction.deleted_at', '=', null)
            ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
            ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
            ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
            ->orderBy('start_date')
            ->get();

        foreach ($onSiteAuctions as $auction) {
            $auction->images = $this->property->
            join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')
                ->where('property.id', $auction->property_id)->orderBy('uploads.position')->get();
        }

        $date = date('m/d/Y');
        $yesterday = date('Y-m-d', strtotime($date.'-1 days'));
        $auctionCalendarEvents = DB::table('auction')->select(['start_date', DB::raw('COUNT(*) AS start_date_count')])
            ->where('start_date', '>', $yesterday.' 23:59')->groupBy('start_date')->orderBy('start_date', 'ASC')->get();

        $physicalAuctions = DB::table('physical_auctions')->where('start_date', '>=', $auctionDate)
            ->where('start_date', '<=', $auctionDateplus1)->get();

        foreach ($physicalAuctions as $auction) {
            $auctionCount = $this->auction->where('physical_auction', '=', $auction->id)->count();
            $auctionPics = $this->property->join('auction', 'auction.property_id', '=', 'property.id')->select('lead_image', 'property_id')
                ->where('auction.physical_auction', '=', $auction->id)->get();

            $auction->physical_auction_count = $auctionCount;
            $auction->lead_images = $auctionPics;
        }

        $register_array = [];
        if (Auth::check()) {
            $registered = $this->auction_register->whereUser_id(Auth::user()->id)->get();
            foreach ($registered as $register) {
                $register_array[] = $register->auction_id;
            }
        }

        $physical_register_array = [];
        if (Auth::check()) {
            $physical_registered = $this->physical_auction_register->whereUser_id(Auth::user()->id)->get();

            foreach ($physical_registered as $physical_register) {
                $physical_register_array[] = $physical_register->auction_id;
            }
        }

        $auction_count = count($physicalAuctions) + count($onSiteAuctions);

        $caledarDate = date('Y-m', strtotime($auctionDate));
        //dd($physicalproperties->toArray());
        //dd($auctionCalendarEvents);
        return view('pages.calendar', ['btobproperties' => $btobproperties, 'onlineproperties' => $onlineproperties,
            'onSiteAuctions' => $onSiteAuctions, 'auctionCalendarEvents' => $auctionCalendarEvents, 'physicalauctions' => $physicalAuctions,
            'calendar_date' => $caledarDate, 'find' => 1, 'register_array' => $register_array, 'auction_count' => $auction_count,
            'physical_register_array' => $physical_register_array, ]);
    }

    //--Help & Advice
    public function helpadvice()
    {
        return view('pages.help-advice.helpadvice');
    }

    public function helpadvicePropertyLibrary()
    {
        return view('pages.help-advice.property-library.propertylibrary');
    }

    public function Marketing()
    {
        return view('pages.help-advice.marketing');
    }

    public function helpadvicePropertyGlossary()
    {
        return view('pages.help-advice.propertyGlossary');
    }

    public function helpadvicePropertyLibraryCPARegulations()
    {
        return view('pages.help-advice.property-library.CPARegulations');
    }

    public function helpadvicePropertyLibraryFICARegulations()
    {
        return view('pages.help-advice.property-library.FICARegulations');
    }

    public function helpadvicePropertyLibraryMunicipalityRegulations()
    {
        return view('pages.help-advice.property-library.municipalityRegulations');
    }

    public function helpadvicePropertyLibraryPropertyValuation()
    {
        return view('pages.help-advice.property-library.propertyValuation');
    }

    public function helpadvicePropertyLibraryRealEstateAgents()
    {
        return view('pages.help-advice.property-library.realEstateAgents');
    }

    public function helpadvicePropertyLibraryRealEstateLaw()
    {
        return view('pages.help-advice.property-library.realEstateLaw');
    }

    //---Contact
    public function contact()
    {
        return view('pages.contact');
    }

    public function BuyerCalculator($price)
    {
        return view('pages.buyer_calculator', ['price' => $price]);
    }

    public function getBuyerFigures()
    {
    }

    public function complianceCerts()
    {
        return view('pages.compliance_certs');
    }

    public function CommercialBuy()
    {
        $sold_commercials = $this->property->
        select('property.id', 'lead_image')->
        join('vetting', 'vetting.property_id', '=', 'property.id')->
        whereProperty_type('Commercial')->
        whereVetting_status('4')->
        orderByRaw('RAND() LIMIT 8')->
        get();

        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas = implode('", "', $areas);

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        $references = [];
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        $approvedPropertys = $this->property
            ->join('commercial', 'property.id', '=', 'commercial.property_id')
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('vetting_status', 2)
            ->orderByRaw('RAND() LIMIT 10')
            ->get();

        return view('pages.commercial_buy', ['sold_commercials' => $sold_commercials, 'references' => $references,
            'provinces' => $provinces, 'areas' => $areas, 'cities' => $cities, 'approvedPropertys' => $approvedPropertys, ]);
    }

    public function ResidentialBuy()
    {
        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas = implode('", "', $areas);

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        $approvedPropertys = $this->property
            ->join('residential', 'property.id', '=', 'residential.property_id')
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('vetting_status', 2)
            ->orderByRaw('RAND() LIMIT 10')
            ->get();

        return view('pages.residential_buy', ['references' => $references, 'provinces' => $provinces, 'areas' => $areas,
            'cities' => $cities, 'approvedPropertys' => $approvedPropertys, ]);
    }

    public function IndustrialBuy()
    {
        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas = implode('", "', $areas);

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        $approvedPropertys = $this->property
            ->join('commercial', 'property.id', '=', 'commercial.property_id')
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('vetting_status', 2)
            ->where('commercial_type', 'LIKE', '%industrial%')
            ->orderByRaw('RAND() LIMIT 10')
            ->get();

        return view('pages.industrial_buy', ['references' => $references, 'provinces' => $provinces, 'areas' => $areas,
            'cities' => $cities, 'approvedPropertys' => $approvedPropertys, ]);
    }

    public function AgriculturalBuy()
    {
        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas = implode('", "', $areas);

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        $approvedPropertys = $this->property
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('vetting_status', 2)
            ->orderByRaw('RAND() LIMIT 10')
            ->whereProperty_type('farm')->get();

        return view('pages.agricultural_buy', ['references' => $references, 'provinces' => $provinces, 'areas' => $areas,
            'cities' => $cities, 'approvedPropertys' => $approvedPropertys, ]);
    }

    public function CommercialSell()
    {
        $properties = $this->property
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('final_price', '>', '1')
            ->where(function ($q) {
                $q->where('property.deleted_at', '>', '2015-12-31')
                    ->orWhere('property.deleted_at', '=', null);
            })
            ->where('lead_image', '!=', 'null')
            ->where(function ($q) {
                $q->where('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 6)
                    ->orWhere('vetting_status', '=', 10);
            })
            ->orderBy('final_price', 'desc')
            ->withTrashed()
            ->get();

        $title_types = DB::table('title_deed_types')->pluck('type', 'id');
        $title_types->put('', 'Select a title type');

        $provinces = DB::table('areas')->groupBy('province')->pluck('province', 'province');
        $provinces->put('', 'Province');

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        $references = [];
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        return view('pages.commercial_sell', ['references' => $references, 'title_types' => $title_types,
            'category' => 'Property', 'properties' => $properties, 'provinces' => $provinces, ]);
    }

    public function PropertyBuy()
    {
        $properties = $this->property
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('final_price', '>', '1')
            ->where(function ($q) {
                $q->where('property.deleted_at', '>', '2015-12-31')
                    ->orWhere('property.deleted_at', '=', null);
            })
            ->where('lead_image', '!=', 'null')
            ->where(function ($q) {
                $q->where('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 6)
                    ->orWhere('vetting_status', '=', 10);
            })
            ->orderBy('final_price', 'desc')
            ->withTrashed()
            ->get();

        $title_types = DB::table('title_deed_types')->pluck('type', 'id');
        $title_types->put('', 'Select a title type');
        $provinces = DB::table('areas')->groupBy('province')->pluck('province', 'province');
        $provinces->put('', 'Province');

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        return view('pages.property_buy', ['references' => $references, 'title_types' => $title_types,
            'category' => 'Property', 'properties' => $properties, 'provinces' => $provinces, ]);
    }

    public function CommercialSellCat($category)
    {
        if ($category == 'agricultural') {
            $category = 'farm';
            $search_category = 'Farm';
        } elseif ($category == 'residential') {
            $category = $category.' property';
            $search_category = 'Residential';
        } else {
            $category = $category.' property';
            $search_category = 'Commercial';
        }

        $sold_commercials = $this->property->
        select('property.id', 'lead_image')->
        join('vetting', 'vetting.property_id', '=', 'property.id')->
        whereProperty_type($search_category)->
        whereVetting_status('4')->
        orderByRaw('RAND() LIMIT 8')->
        get();

        $title_types = DB::table('title_deed_types')->pluck('type', 'id');
        $title_types->put('', 'Select a title type');

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        return view('pages.commercial_sell_cat', ['sold_commercials' => $sold_commercials, 'references' => $references,
            'title_types' => $title_types, 'category' => $category, ]);
    }

    public function CommercialLet()
    {
        $sold_commercials = $this->property->
        select('property.id', 'lead_image')->
        join('vetting', 'vetting.property_id', '=', 'property.id')->
        whereProperty_type('Commercial')->
        whereVetting_status('4')->
        orderByRaw('RAND() LIMIT 8')->
        get();

        $title_types = DB::table('title_deed_types')->pluck('type', 'id');
        $title_types->put('', 'Select a title type');

        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        return view('pages.commercial_let', ['sold_commercials' => $sold_commercials, 'references' => $references,
            'title_types' => $title_types, ]);
    }

    public function Network()
    {
        $sold_commercials = $this->property->
        select('property.id', 'lead_image')->
        join('vetting', 'vetting.property_id', '=', 'property.id')->
        whereProperty_type('Commercial')->
        whereVetting_status('4')->
        orderByRaw('RAND() LIMIT 8')->
        get();

        $logos = $this->broker_logos->get();

        return view('pages.network', ['sold_commercials' => $sold_commercials, 'logos' => $logos]);
    }

    public function RichardsBayMall()
    {
        return view('pages.richards_bay_mall');
    }

    public function Opus1()
    {
        return view('pages.opus1');
    }

    public function MidlandsHotel()
    {
        return view('pages.midlands_hotel');
    }

    public function OpusOffer()
    {
        $input = Request::all();
        DB::insert('INSERT INTO opus1 SET name = "'.$input['name'].'", email = "'.$input['email'].'", contact_number = "'
            .$input['contact_number'].'"');

        $name = $input['name'];
        $email = $input['email'];
        $phone = $input['contact_number'];

        $userName = 'Dear Rainer,';
        $subject = 'You have recieved a lead for the midlands hotel';
        $adminMail = 'rstenzhorn@in2assets.com';

        $emailData = "<p>You have recieved a lead for the midlands hotel</p>
                        <table border='0'>
                        <tr><td>Name </td><td>".$name."</td></tr>
                        <tr><td>Email </td><td><a href='mailto:".$email."'>".$email.'</a></td></tr>
                        <tr><td>Phone </td><td>'.$phone.'</td></tr>
                        </table>';

        // Queue email
        Mail::to('rstenzhorn@in2assets.com')
            ->cc('rmoodley@in2assets.com')
            ->send(new newLead($subject, $userName, $emailData, $adminMail));
        Flash::message('Thanks for contacting us we will get back to you shortly.');

        return redirect()->back();
    }

    public function CommercialSellExplained()
    {
        $title_types = DB::table('title_deed_types')->pluck('type', 'id');
        $title_types->put('', 'Select a title type');

        return view('pages.commercial_sell_explained', ['title_types' => $title_types]);
    }

    public function partialContactRequest()
    {
        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            return Response::json([
                'errors' => $validate->errors(),
            ]);
        }

        $name = Request::get('fullname');
        $email = Request::get('email');
        $phone = Request::get('contact_num');
        $message = Request::get('message');

        $userName = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets Contact Form - Enquiry';

        $adminMail = 'info@in2assets.com';

        $emailData = "<p>A contact request was received from the website.</p>
                        <table border='0'>
                            <tr><td>Name </td><td>".$name."</td></tr>
                            <tr><td>Email </td><td><a href='mailto:".$email."'>".$email.'</a></td></tr>
                            <tr><td>Phone </td><td>'.$phone.'</td></tr>
                            <tr><td>Message </td><td>'.$message.'</td></tr>
                        </table>';

        // Queue email
        Mail::to($adminMail)
            ->cc('rmoodley@in2assets.com')
            ->send(new newLead($subject, $userName, $emailData, $adminMail));

        return redirect()->back();
    }

    public function brochure($pdf)
    {
        return view('pages.brochure', ['pdf' => $pdf]);
    }
}
