<?php

namespace App\Http\Controllers;

class ErrorsController extends Controller
{
    public function __construct(Errors $Errors)
    {
        $this->errors = $Errors;
    }

    public function log_error($exception)
    {
        dd($exception->errorInfo);
    }
}
