<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use In2Assets\Blogs\Blog;
use In2Assets\Mailers\UserMailer;
use In2Assets\Users\User;

class WeSellPagesController extends Controller
{
    public function __construct(Property $property, Auction $auction, User $user)
    {
        $this->property = $property;
        $this->auction = $auction;
        $this->user = $user;
    }

    //WeSell
    public function wesell()
    {
        return view('wesell.wesell');
    }

    public function WeSellLogin()
    {
        return view('wesell.registration', ['signup' => false]);
    }

    public function WeSellSignup()
    {
        return view('wesell.registration', ['signup' => true]);
    }

    public function WeSellPropertyInfo()
    {
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->groupBy('suburb_town_city')->groupBy('province')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        return view('wesell.property_info', ['cities' => $cities]);
    }

    public function WeSellConGrats()
    {
        $id = Request::get('id');

        return view('wesell.congratulations', ['property_id' => $id]);
    }

    public function WesellHowItWorks()
    {
        return view('wesell.how_it_works');
    }

    public function PropertyManagement($id, $tab)
    {
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->groupBy('suburb_town_city')->groupBy('province')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $property = App::make('PropertyController')->getPropertyDetsbyId($id);

        $user = $this->user->whereId($property->user_id)->first();

        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->get();

        $images = $this->property->
        join('uploads', 'uploads.link', '=', 'property.id')->
        whereClass('property')->
        whereType('image')->
        where('property.id', $id)->
        get();

        $percent = App::make('PropertyController')->form_complete_percent($id);

        return view('wesell.property_management', ['property' => $property, 'listing_type' => 'wesell', 'cities' => $cities,
        'files' => $files, 'images' => $images, 'tab' => $tab, 'percent' => $percent, ]);
    }

    public function howToGuides()
    {
        return view('wesell.how_to_guide');
    }

    public function howToGuide($page)
    {
        return view('wesell.how_to_guides.'.$page);
    }

    public function Terms()
    {
        return view('wesell.terms_conditions');
    }

    public function About()
    {
        return view('wesell.about');
    }
}
