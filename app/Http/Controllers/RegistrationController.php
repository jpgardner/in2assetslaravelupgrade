<?php

namespace App\Http\Controllers;

use App\In2Assets\Forms\RegistrationForm;
use App\In2Assets\Mailers\UserMailer;
use App\In2Assets\Users\User;
use App\Mail\newLead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Laracasts\Flash\Flash;

class RegistrationController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    private $registrationForm;

    public function __construct(RegistrationForm $registrationForm, User $user, UserMailer $mailer)
    {
        $this->mailer = $mailer;
        $this->registrationForm = $registrationForm;
        $this->user = $user;
        //Filter and limit to guests only
        //$this->beforeFilter('guest');
    }

    public function loginWithFacebook()
    {
        // get data from input
        $code = Request::get('code');

        // get fb service
        $fb = OAuth::consumer('Facebook');

        // check if code is valid

        // if code is provided get user data and sign in
        if (! empty($code)) {
            if (! isset($_SESSION['token'])) {
                // This was a callback request from facebook, get the token
                $token = $fb->requestAccessToken($code);
                $_SESSION['token'] = $token->getAccessToken();
            }

            // Send a request with it
            $result = json_decode($fb->request('/me'), true);

            $message = 'Your unique facebook user id is: '.$result['id'].' and your name is '.$result['name'];
            echo $message.'<br/>';

        //Var_dump
            //display whole array().
        } // if not ask for permission first
        else {
            // get fb authorization
            $url = $fb->getAuthorizationUri();

            // return to facebook login url
            return redirect()->to((string) $url);
        }
    }

    public function create()
    {
        if (Auth::check()) {
            Flash::message('You cannot register an account when logged in.');
            //Redirect Home
            return redirect()->home();
        }
        $citiesDB = DB::table('areas')->groupBy('province', 'area', 'suburb_town_city')->get();
        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);

        return view('registration.create', ['cities' => $cities]);
    }

    public function loginGoogle()
    {
        $email = Request::get('email');

        // if code is provided get user data and sign in
        if (! empty($email)) {
            if ($user = User::whereEmail($email)->first()) {
                Auth::login($user);
                DB::table('google_session')->insert(['user_id' => Auth::user()->id, 'google_login' => 1]);
                if (Auth::user()->hasRole('Admin')) {
                    return URL::route('admin.index', [], false);
                } else {
                    return URL::route('home', [], false);
                }
            } else {
                $input = Request::all();
                $fullNameArray = explode(' ', $input['fullname']);

                $input['firstname'] = $fullNameArray[0];
                $input['lastname'] = '.';
                if (trim(end($fullNameArray)) != trim($fullNameArray[0])) {
                    $input['lastname'] = end($fullNameArray);
                }
                $input['password'] = 'Password';

                $this->registrationForm->validate($input);
                extract(Request::only('title', 'companyname', 'streetaddress', 'suburb_town_city', 'area', 'province', 'country',
                    'postcode', 'email', 'password', 'phonenumber', 'cellnumber', 'communicationpref', 'newsletter', 'avatar'));

                $firstname = $input['firstname'];
                $lastname = '.';
                if (trim(end($fullNameArray)) != trim($fullNameArray[0])) {
                    $lastname = end($fullNameArray);
                }

                $newsletter = 'no';

                $allow_contact = 1;

                $user = $this->execute(
                    new RegisterUserCommand('', $firstname, $lastname, '', '', '', '',
                        '', '', '', $email, '', '', '', '', $newsletter,
                        '', $allow_contact)
                );
                DB::table('google_session')->insert(['user_id' => Auth::user()->id, 'google_login' => 1]);
                //Authenticate User
                Auth::login($user);

                Auth::user()->roles()->attach(11);

                return URL::route('preferences.create', [], false);
            }
        }

        return URL::route('home', [], false);
    }

    /**
     * Generate a random string, using a cryptographically secure
     * pseudorandom number generator (random_int).
     *
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     *
     * @param int $length How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    public function randomPassword($length)
    {
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        do {
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;

            for ($i = 0; $i < $length; $i++) {
                $str .= $keyspace[random_int(0, $max)];
            }
        } while (! preg_match('#[0-9]+#', $str));

        return $str;
    }

    public function storeLeadUser($email, $firstname, $lastname, $cellnumber, $contact_type)
    {
        $password = $this->randomPassword(8);
        $user = DB::table('users')->select('id')->whereEmail($email)->first();

        if ($user != null) {
            // return "Done";
            return $user->id;
        }

        $input['email'] = $email;
        $input['firstname'] = $firstname;
        $input['lastname'] = $lastname;
        $input['cellnumber'] = $cellnumber;
        $input['password'] = $password;

        $newsletter = 'no';

        $allow_contact = 1;

        $user = $this->execute(
            new RegisterUserCommand('', $firstname, $lastname, '', '', '', '', '', '', '', $email, $password,
                '', $cellnumber, '', $newsletter, '', $allow_contact)
        );

        $user->roles()->attach($contact_type);

        //check if admin is creating user
        $encrypt = $this->encryptPassword($password);

        DB::insert('INSERT INTO user_pass (user_id, encrypted_pass) VALUES ('.$user['id'].', "'.$encrypt.'");');
        // return "Done";
        return $user->id;
    }

    public function store()
    {
        $url = route('users.add');

        if (Request::get('modal') == null && URL::previous() != $url) {
            //Validate All inputs, fetch required inputs, pass to command bus, execute
            $validate = Validator::make(Request::all(), [
                'g-recaptcha-response' => 'required|captcha',
            ]);

            if ($validate->fails()) {
                foreach ($validate->errors()->all() as $error) {
                    Flash::message('Please check the "I am not a robot" field.');
                }

                return redirect()->back()->withInput(Request::all());
            }
        }

        $input = Request::all();

        if (empty($input['firstname'])) {
            $fullNameArray = explode(' ', $input['fullname']);

            $input['firstname'] = $fullNameArray[0];
            $input['lastname'] = '.';
            if (trim(end($fullNameArray)) != trim($fullNameArray[0])) {
                $input['lastname'] = end($fullNameArray);
            }
        }

        extract(Request::only('title', 'companyname', 'streetaddress', 'suburb_town_city', 'area', 'province', 'country',
            'postcode', 'email', 'password', 'phonenumber', 'cellnumber', 'communicationpref', 'newsletter', 'avatar'));

        if (URL::previous() == $url) {
            $password = $this->randomPassword(8);
            $input['password'] = $password;
            $user = DB::table('users')->select('id')->whereEmail(Request::get('email'))->first();

            if ($user != null) {
                Flash::message('This user already exists.');

                return redirect()->route('users.adminlist', ['id' => $user->id]);
            }
        }
        $this->registrationForm->validate($input);

        $firstname = $input['firstname'];
        $lastname = $input['lastname'];

        $newsletter = 'yes';
        if (Request::get('newsletter') == null) {
            $newsletter = 'no';
        }

        $allow_contact = 1;

        $user = $this->execute(
            new RegisterUserCommand('', $firstname, $lastname, '', '', '', '', '', '', '', $input['email'], $input['password'],
                '', $input['cellnumber'], '', $newsletter, '', $allow_contact)
        );

        //check if admin is creating user
        if (URL::previous() == $url) {
            $encrypt = $this->encryptPassword($input['password']);
            DB::insert('INSERT INTO user_pass (user_id, encrypted_pass) VALUES ('.$user['id'].', "'.$encrypt.'");');

            //send welcome email if send email is checked
            if (Request::get('auction_bidder')) {
                $tag = DB::table('tags')->where('name', '=', 'AUCTION BIDDER')->first();
                User::find($user->id)->assignTag($tag->id);
            }
            if (Request::get('send_email')) {
                if (Request::get('auction_bidder')) {
                    $this->mailer->sendWelcomeMessageToAuctionBidder($user);
                } else {
                    $this->mailer->sendWelcomeMessageToAddedUser($user);
                }
            } else {
            }

            return redirect()->route('users.adminlist', ['id' => $user['id']]);
        }

        //Authenticate User
        Auth::login($user);

        Auth::user()->roles()->attach(11);

        if (Request::get('interest_res') == null) {
            $interest_res = 0;
        } else {
            $interest_res = 1;
        }

        if (Request::get('interest_com') == null) {
            $interest_com = 0;
        } else {
            $interest_com = 1;
        }

        if (Request::get('interest_farm') == null) {
            $interest_farm = 0;
        } else {
            $interest_farm = 1;
        }

        if (Request::get('interest_auc') == null) {
            $interest_auc = 0;
        } else {
            $interest_auc = 1;
        }

        DB::table('user_interest')->insert(['users_id' => Auth::user()->id, 'interest_res' => $interest_res, 'interest_com' => $interest_com, 'interest_farm' => $interest_farm, 'interest_auc' => $interest_auc]);

        $actual_link = "$_SERVER[HTTP_REFERER]";

        if ($actual_link == URL::to('marketing/sell-commercial-package')) {
            return redirect()->route('marketing.packages');
        }

        Flash::message('Thank you for registering with In2assets, please tell us about your property preferences.');
        if (Request::get('modal')) {
            return redirect()->to(Request::get('modal'));
        } else {
            return redirect()->route('preferences.create');
        }
//         return redirect()->route('portal_path');
    }

    public function storePropEnqUser($name, $email, $contact, $token)
    {
        $fullNameArray = explode(' ', $name);

        $input['firstname'] = $fullNameArray[0];
        $input['lastname'] = '.';
        if (trim(end($fullNameArray)) != trim($fullNameArray[0])) {
            $input['lastname'] = end($fullNameArray);
        }

        extract(Request::only('title', 'companyname', 'streetaddress', 'suburb_town_city', 'area', 'province', 'country',
            'postcode', 'email', 'password', 'phonenumber', 'cellnumber', 'communicationpref', 'newsletter', 'avatar'));

        $input['email'] = $email;

        $password = $this->randomPassword(8);
        $input['password'] = $password;
        $user = DB::table('users')->select('id')->whereEmail($email)->first();

        if ($user != null) {
            return $user->id;
        }

        $input['cellnumber'] = $contact;

        $input['_token'] = $token;

        $this->registrationForm->validate($input);

        $firstname = $input['firstname'];
        $lastname = $input['lastname'];
        $newsletter = 'no';

        $allow_contact = 1;

        $user = $this->execute(
            new RegisterUserCommand('', $firstname, $lastname, '', '', '', '', '', '', '', $input['email'], $input['password'],
                '', $input['cellnumber'], '', $newsletter, '', $allow_contact)
        );

        $userMail = $user['email'];
        $subject = 'Property Enquiry User created';
        $name = $user['firstname'].' '.$user['lastname'].'<br>';
        $emailData = 'This content needs to be added and this is the password generated for the client '.$password;

        // Queue email
//        Mail::to($user['email'])
//            ->send(new newLead($subject, $name, $emailData, $userMail));

        return $user['id'];
    }

    public function encryptPassword($password)
    {
        $key = 'djPlsjiDCN6soeif2JSGb';
        $salt = sha1(mt_rand());

        // Initialization Vector, randomly generated and saved each time
        // Note: This does not need to be kept secret
        $iv = substr(sha1(mt_rand()), 0, 16);

//        de( "\n Key: $key \n Password: $password \n Salt: $salt \n IV: $iv\n");

        $encrypted = openssl_encrypt(
            "$password", 'aes-256-cbc', "$salt:$key", null, $iv
        );

        $encrypted_pass = "$salt:$iv:$encrypted";

        return $encrypted_pass;

        // Save it... (make sure to use bind variables/prepared statements!)
        /* db_write( "insert into sensitive_table encrypted_msg values (:msg_bundle)",
            $msg_bundle ); */
    }

//    public function storeWeSell()
//
//    {
//
//        //Validate All inputs, fetch required inputs, pass to command bus, execute
//        $this->registrationForm->validate(Request::all());
//        extract(Request::only('firstname', 'lastname', 'email', 'password', 'phonenumber', 'cellnumber'));
//
//        $user = $this->execute(
//            new RegisterUserCommand('', $firstname, $lastname, '', '', '', '', '', '', '', $email, $password, $phonenumber, $cellnumber, '', '', '', '')
//        );
//
//        //Authenticate User
//        Auth::login($user);
//
//        Auth::user()->roles()->attach(11);
//
//        return redirect()->route('wesell-property-info');
//        // return redirect()->route('portal_path');
//    }
}
