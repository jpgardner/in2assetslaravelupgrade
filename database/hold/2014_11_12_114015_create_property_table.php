<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('reference')->unique();
            $table->string('lead_image')->nullable();
            $table->string('image_gallery')->nullable();
            $table->string('street_address');
            $table->string('suburb_town_city');
            $table->string('area');
            $table->string('province');
            $table->string('country');
            $table->string('property_type');
            $table->string('short_descrip');
            $table->text('long_descrip');
            $table->string('listing_type');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property');
    }
}
