<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company');
            $table->string('contact_firstname');
            $table->string('contact_lastname');
            $table->string('division');
            $table->string('acc_number');
            $table->string('cell_number');
            $table->string('office_number');
            $table->string('email');
            $table->tinyInteger('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_provider');
    }
}
